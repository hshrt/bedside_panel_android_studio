package com.hsh.esdbsp.global;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.network.ApiRequest;
import com.hsh.esdbsp.network.XMLCaller;
import com.hsh.esdbsp.widget.Log;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;

public class ImageCacheManager extends Object {

    private String TAG = "ImageCacheManager";
    private ArrayList<String> fileNameList;
    private ArrayList<String> extList;

    private int loadIndex = 0;
    private int totalNum = 0;
    private int retryTime = 0;

    private int inBetweenImageTime = 250;

    private static int RETRY_LIMIT = 5;

    private SharedPreferences settings;

    private ImageView testIV = new ImageView(MainApplication.getContext());

    public ImageCacheManager() {
        fileNameList = new ArrayList<String>();
        extList = new ArrayList<String>();
    }

    private static ImageCacheManager imageCacheManager = new ImageCacheManager();

    public static synchronized ImageCacheManager getInstance() {
        return imageCacheManager;
    }

    public void getImageFileList() {

        DataCacheManager.getInstance().cacheLogging("getImageFileList_start");

        try {
            loadIndex = 0;

            settings = MainApplication.getContext().getSharedPreferences(MainApplication.PREFS_NAME, 0);

            URL url = new URL(MainApplication.getCmsApiBase() + MainApplication.getCurrentActivity().getString(
                    R.string.get_all_photo) + "?getAllName=true");
            Log.e(TAG, "url = " + url);

            Bundle bundle = new Bundle();
            ApiRequest.request(new XMLCaller.InternetCallbacks() {
                @Override
                public boolean hasInternet() {
                    return Helper.hasInternet(false);
                }

                @Override
                public void onError(int failMode, boolean isPostExecute) {
                    DataCacheManager.getInstance().cacheLogging("getImageFileList_api_error");
                    onGetPhotoError();
                }

                @Override
                public void postExecute(String json) {

                    DataCacheManager.getInstance().cacheLogging("getImageFileList_api_success");

                    Log.i(TAG, json);
                    JSONObject restObject;
                    try {
                        restObject = new JSONObject(json);

                        if (restObject.has("data")) {
                            JSONArray jArray = restObject.getJSONArray("data");
                            totalNum = jArray.length();

                            for (int x = 0; x < totalNum; x++) {
                                JSONObject fileObj = jArray.getJSONObject(x);
                                fileNameList.add(fileObj.getString("fn"));
                                extList.add(fileObj.getString("fileExt"));
                            }

                            Log.i(TAG, fileNameList.size() + "");

                            String imageLink = "";

                            String ext = extList.get(0).equalsIgnoreCase("j") ? "jpg" : "png";

                            if (fileNameList.size() > 0) {
                                retryTime = 0;
                                imageLink = MainApplication.getUploadApiBase() + fileNameList.get(0) + "_m." + ext;
                            } else {
                                hideLoading();
                                return;
                            }
                            try {
                                loadImage(imageLink, testIV);
                            } catch (Exception e) {
                                DataCacheManager.getInstance().cacheLogging("getImageFileList_loadImage_error");
                                hideLoading();
                            }
                        }

                    } catch (JSONException e1) {
                        DataCacheManager.getInstance().cacheLogging("getImageFileList_json_paser_error");
                        e1.printStackTrace();
                        hideLoading();
                    }
                }

                @Override
                public void postExecuteWithCellId(String xml, String cellId) {

                }
            }, url, "get", bundle);

        } catch (MalformedURLException e) {
            DataCacheManager.getInstance().cacheLogging("getImageFileList_api_error");
            onGetPhotoError();
        } catch (Exception e) {
            DataCacheManager.getInstance().cacheLogging("getImageFileList_api_error");
            onGetPhotoError();
        }
    }

    public void onGetPhotoError() {
        // no need to handle there, as the "universalimageloader" library has
        // its own error handling Method
        retryTime++;
        DataCacheManager.getInstance().cacheLogging("onGetPhotoError_retry_"+retryTime);

        if (retryTime < RETRY_LIMIT) {
            getImageFileList();
        } else {
            hideLoading();
        }
    }

    private void loadImage(String imageLink, ImageView imageView) {
        // Load image, decode it to Bitmap and display Bitmap in ImageView (or
        // any other view
        // which implements ImageAware interface)

        Log.i(TAG, "loadImage 's imageLink = " + imageLink);
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.bbg)
                .showImageForEmptyUri(R.drawable.bbg)
                .showImageOnFail(R.drawable.bbg).cacheInMemory(true)
                .cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        String tempString = imageLink;
        try {
            ImageLoader.getInstance().displayImage(tempString, imageView, options, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view,
                                            FailReason failReason) {
                    Log.i(TAG, "failed");
                    Log.i(TAG, imageUri);
                    Log.i(TAG, failReason.toString());

                    retryTime++;
                    if (retryTime >= RETRY_LIMIT) {
                        Log.i(TAG, "Skip to next Image");
                        // skip current image, go to download next image
                        loadIndex++;

                        if (loadIndex >= totalNum) {
                            hideLoading();
                            return;
                        }
                        retryTime = 0;
                    }

                    try {
                                /*
                                 * String imageLink =
								 * MainApplication.getContext(
								 * ).getString(R.string
								 * .api_base)+"cms/upload/"+fileNameList
								 * .get(loadIndex)+"_m.jpg";
								 * loadImage(imageLink, testIV);
								 */

                        final String ext = extList.get(loadIndex).equalsIgnoreCase("j") ? "jpg" : "png";
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                String imageLink = MainApplication.getUploadApiBase() + fileNameList.get(loadIndex) + "_m." + ext;
                                loadImage(imageLink, testIV);
                            }

                        }, inBetweenImageTime);
                    } catch (Exception e) {

                        retryTime++;
                        if (retryTime >= RETRY_LIMIT) {
                            // skip current image, go to download next
                            // image
                            loadIndex++;
                            retryTime = 0;

                            if (loadIndex >= totalNum) {
                                hideLoading();
                                return;
                            }
                        }

                        final String ext = extList.get(loadIndex).equalsIgnoreCase("j") ? "jpg" : "png";
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                String imageLink = "wrongLink";
                                if (fileNameList.size() > loadIndex) {
                                    imageLink = MainApplication.getUploadApiBase() + fileNameList.get(loadIndex) + "_m." + ext;
                                } else {
                                    Log.i(TAG,
                                            "use wrong Link in onLoading Failed!!");
                                }
                                loadImage(imageLink, testIV);
                            }

                        }, inBetweenImageTime);

                    }

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    Log.i(TAG, "Loading complete");
                    Log.i(TAG, "Loading index = " + loadIndex);

                    // set retryTime to 0, to give next image has enough
                    // retry time
                    retryTime = 0;

                    loadIndex++;

                    Log.i(TAG, "loadIndex after ++ = " + loadIndex);
                    Log.i(TAG, "totalNum = " + totalNum);

                    if (loadIndex < totalNum) {
                        try {

                            Handler handler = new Handler();
                            final String ext = extList.get(loadIndex).equalsIgnoreCase("j") ? "jpg" : "png";
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    String imageLink = MainApplication.getUploadApiBase() + fileNameList.get(loadIndex) + "_m." + ext;
                                    loadImage(imageLink, testIV);
                                }
                            }, inBetweenImageTime);

                        } catch (Exception e) {

                            retryTime++;
                            if (retryTime >= RETRY_LIMIT) {
                                // skip current image, go to download
                                // next image
                                loadIndex++;

                                retryTime = 0;
                                // handle the last image case
                                if (loadIndex >= totalNum) {
                                    hideLoading();
                                    return;
                                }
                            }

                            Log.i(TAG, "No Return works");

                            Handler handler = new Handler();
                            final String ext = extList.get(loadIndex).equalsIgnoreCase("j") ? "jpg" : "png";
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    String imageLink = "wrongLink";
                                    if (fileNameList.size() > loadIndex) {
                                        imageLink = MainApplication.getUploadApiBase() + fileNameList.get(loadIndex) + "_m" + ext;

                                    } else {
                                        Log.i(TAG, "use wrong Link in onLoadingComplete!");
                                    }
                                    loadImage(imageLink, testIV);
                                }

                            }, inBetweenImageTime);

                        }
                    } else {
                        long finishTime = System.currentTimeMillis();
                        long downloadTime = finishTime - DataCacheManager.getInstance().startTime;
                        Log.i(TAG, "download time = " + downloadTime);

                        // for Flurry log
                        final Map<String, String> map = new HashMap<String, String>();
                        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                        map.put("time", downloadTime + "");
                        map.put("MAC address", Helper.getMacAddress(MainApplication.getContext()));
                        FlurryAgent.logEvent("ImageAndDataDownloadTime", map);

                        settings.edit().putLong("lastMediaUpdateTime", getMASTime()).commit();

                        hideLoading();
                    }
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }

            }, new ImageLoadingProgressListener() {
                @Override
                public void onProgressUpdate(String imageUri, View view, int current, int total) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            hideLoading();
        }
    }

    public void stop() {
        ImageLoader.getInstance().stop();
    }

    private void hideLoading() {
        stop();
        if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
            ((RoomControlActivity) MainApplication.getCurrentActivity()).hideLoading();
        }
        DataCacheManager.getInstance().cacheLogging("cache_cycle_end");
    }

    private Long getMASTime() {
        String year = MainApplication.getMAS().getData("year");
        String month = MainApplication.getMAS().getData("month");
        String day = MainApplication.getMAS().getData("day");
        String hour = MainApplication.getMAS().getData("hour");
        String minute = MainApplication.getMAS().getData("minute");

        Long lastUpdateTime = System.currentTimeMillis();
        if (year.length() > 0) {
            String time = month + "/" + day + "/" + year + " " + hour + ":" + minute;

            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            try {
                Date date = formatter.parse(time);
                android.util.Log.i(TAG, "date to String =" + date.toString());
                android.util.Log.i(TAG, "date to timestamp = " + date.getTime());
                lastUpdateTime = date.getTime();
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return lastUpdateTime;
    }

}
