package com.hsh.esdbsp.global;

import java.util.ArrayList;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.model.Dictionary;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.model.Movie;

public class GlobalValue {

	private String notificationCountLastUpdateTime;
	private String password;

	private ArrayList<GeneralItem> allItemArray;
	private ArrayList<Dictionary> allDictArray;
	private ArrayList<Movie> allMovieArray;
	
	private Boolean allowMoveTop = true;
	private String hotel;
	
	private String dict;
	
	private GlobalValue() {
		allItemArray = new ArrayList<GeneralItem>();
		allDictArray = new ArrayList<Dictionary>();
	}

	private static GlobalValue mGlobalValueObj = new GlobalValue();

	public static synchronized GlobalValue getInstance() {
		return mGlobalValueObj;
	}

	public String getNotificationCountLastUpdateTime() {
		return notificationCountLastUpdateTime;
	}

	public void setNotificationCountLastUpdateTime(
			String notificationCountLastUpdateTime) {
		this.notificationCountLastUpdateTime = notificationCountLastUpdateTime;
	}

	public String getPw() {
		return password;
	}

	public ArrayList<GeneralItem> getAllItemArray() {
		return allItemArray;
	}

	public void setAllItemArray(ArrayList<GeneralItem> allItemArray) {
		this.allItemArray = allItemArray;
	}

	public String getDict() {
		return dict;
	}

	public void setDict(String dict) {
		this.dict = dict;
	}

	public ArrayList<Dictionary> getAllDictArray() {
		return allDictArray;
	}

	public void setAllDictArray(ArrayList<Dictionary> allDictArray) {
		this.allDictArray = allDictArray;
	}

	public ArrayList<Movie> getAllMovieArray() {
		return allMovieArray;
	}

	public void setAllMovieArray(ArrayList<Movie> allMovieArray) {
		this.allMovieArray = allMovieArray;
	}
	public boolean checkIfThereIsOneCanOrder(){
		boolean hasOneCanOrder = false;
		for(GeneralItem g: allItemArray){
			if (g.getCanOrder()==1){
				hasOneCanOrder = true;
				break;
			}
		}
		
		return hasOneCanOrder;
	}

	public Boolean getAllowMoveTop() {
		return allowMoveTop;
	}

	public void setAllowMoveTop(Boolean allowMoveTop) {
		this.allowMoveTop = allowMoveTop;
	}

	public String getHotel() {
		if (hotel == null) {
			hotel = BuildConfig.FLAVOR;
			if (hotel.length() > 3) {
				hotel = hotel.substring(0, 3);
			}
		}
		return hotel;
	}
}
