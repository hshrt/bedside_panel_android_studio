package com.hsh.esdbsp.global;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.BaseActivity;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.model.Dictionary;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.network.ApiRequest;
import com.hsh.esdbsp.network.XMLCaller;
import com.hsh.esdbsp.parser.GetAllDictParser;
import com.hsh.esdbsp.parser.GetAllDictParser.GetAllDictParserInterface;
import com.hsh.esdbsp.parser.GetGeneralItemParser;
import com.hsh.esdbsp.parser.GetGeneralItemParser.GetGeneralItemParserInterface;

public class DataCacheManager extends Object {

    private String TAG = "DataCacheManager";

    private SharedPreferences settings;

    private boolean isCacheInited = false;

    private Handler dataHandler = new Handler();

    private AlertDialog alertDialog;
    private boolean showingDialog = false; // whether the no internet box is
    private boolean itemNeedUpdate = false;
    private boolean dictNeedUpdate = false;
    private boolean mediaNeedUpdate = false;
    public long startTime;
    private boolean isFirstTimeCall = true;

    private String lang = "E";

    private int retryTime = 0;

    private static DataCacheManager dataCacheManager = new DataCacheManager();

    public static synchronized DataCacheManager getInstance() {
        return dataCacheManager;
    }

    private Runnable loadDataRunnable = new Runnable() {

        public void run() {

            cacheLogging("cache_cycle_start");

            isCacheInited = true;
            dataHandler.removeCallbacks(loadDataRunnable);

            if (showingDialog) {
                if (alertDialog != null) {
                    alertDialog.dismiss();
                    alertDialog = null;
                }
                showingDialog = false;
                cacheLogging("cache_cycle_dismiss_dialog");
            }

            if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {

                //only sleeping will do update
                if(MainApplication.getScreenSta() == 0) {
                    dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
                    retryTime = 0;
                    cacheLogging("cache_cycle_in_room_control");
                    getAllLastUpdateTime();
                } else {

                    // must need data as its null even using
                    if(settings.getLong("lastItemUpdateTime", 0) == 0 || isFirstTimeCall) {
                        dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
                        retryTime = 0;
                        cacheLogging("cache_cycle_in_room_control");
                        getAllLastUpdateTime();
                    } else {
                        cacheLogging("cache_cycle_not_sleeping");
                        cacheLogging("cache_cycle_end");
                        dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
                    }
                }
            } else {
                cacheLogging("cache_cycle_not_in_room_control");
                cacheLogging("cache_cycle_end");
                dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
            }
        }
    };

    public void startLoadData() {

        if (!isCacheInited) {
            settings = MainApplication.getContext().getSharedPreferences(MainApplication.PREFS_NAME, 0);
            dataHandler.removeCallbacks(loadDataRunnable);
            dataHandler.postDelayed(loadDataRunnable, 500);

            //double check if really started
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    if (!isCacheInited) {
                        startLoadData();
                    }
                }
            }, 2000);
        }
    }

    private void getAllLastUpdateTime() {

        cacheLogging("getAllLastUpdateTime_start");

        if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
            ((BaseActivity) MainApplication.getCurrentActivity()).showLoading();
        }

        startTime = System.currentTimeMillis();

        try {
            URL url = new URL(MainApplication.getCmsApiBase() + MainApplication.getContext().getString(R.string.get_all_last_update_time));
            Bundle bundle = new Bundle();
            bundle.putString("appVersion", MainApplication.getAppVersion());

            ApiRequest.request(new XMLCaller.InternetCallbacks() {
                @Override
                public boolean hasInternet() {
                    return Helper.hasInternet(false);
                }

                @Override
                public void onError(int failMode, boolean isPostExecute) {
                    onGetAllLastUpdateTimeError();
                }

                @Override
                public void postExecute(String json) {

                    JSONObject timeObject = null;
                    JSONObject jo;

                    try {
                        jo = new JSONObject(json);
                        JSONArray jArray = jo.getJSONArray("data");
                        timeObject = (JSONObject) jArray.get(0);
                    } catch (JSONException e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                    Date dictLastupdateDate = null;
                    Date itemLastupdateDate = null;
                    Date mediaLastupdateDate = null;

                    if (timeObject == null) {
                        cacheLogging("getAllLastUpdateTime_timeObject_null");
                        onGetAllLastUpdateTimeError();
                        return;
                    }

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    try {
                        dictLastupdateDate = (Date) formatter.parse(timeObject.getString("dictLastUpdate"));
                        itemLastupdateDate = (Date) formatter.parse(timeObject.getString("itemLastUpdate"));
                        mediaLastupdateDate = (Date) formatter.parse(timeObject.getString("mediaLastUpdate"));
                    } catch (Exception e) {
                        cacheLogging("getAllLastUpdateTime_dateformat_fail");
                        onGetAllLastUpdateTimeError();
                        return;
                    }

                    if (dictLastupdateDate == null || itemLastupdateDate == null || mediaLastupdateDate == null) {
                        cacheLogging("getAllLastUpdateTime_getdate_fail");
                        onGetAllLastUpdateTimeError();
                        return;
                    }

                    itemNeedUpdate = false;
                    if (itemLastupdateDate != null && itemLastupdateDate.getTime() > settings.getLong("lastItemUpdateTime", 0)) {
                        itemNeedUpdate = true;
                    }

                    dictNeedUpdate = false;
                    if (dictLastupdateDate != null && dictLastupdateDate.getTime() > settings.getLong("lastDictUpdateTime", 0)) {
                        dictNeedUpdate = true;
                    }

                    mediaNeedUpdate = false;
                    if (mediaLastupdateDate != null && mediaLastupdateDate.getTime() > settings.getLong("lastMediaUpdateTime", 0)) {
                        mediaNeedUpdate = true;
                    }

                    if (itemNeedUpdate || dictNeedUpdate || mediaNeedUpdate) {
                        if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
                            if (itemNeedUpdate) {
                                cacheLogging("getAllLastUpdateTime_item_need_update");
                                getItemFromNetwork();
                            } else {
                                cacheLogging("getAllLastUpdateTime_item_no_need_update");
                                getItemFromLocal();
                            }
                        } else {
                            cacheLogging("getAllLastUpdateTime_need_update_but_not_in_roomcontrol");
                            cacheLogging("cache_cycle_end");
                            dataHandler.removeCallbacks(loadDataRunnable);
                            dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
                        }
                    }
                    // if there is no update from Server
                    else {

                        if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
                            // if there is no data in memory
                            //can change logic to check one by one
                            if (GlobalValue.getInstance().getAllItemArray().size() == 0 || GlobalValue.getInstance().getAllDictArray().size() == 0) {
                                cacheLogging("getAllLastUpdateTime_no_need_update_but_value_null");
                                getItemFromLocal();
                            } else {
                                cacheLogging("getAllLastUpdateTime_no_need_update");
                                cacheLogging("cache_cycle_end");
                                if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
                                    ((RoomControlActivity) MainApplication.getCurrentActivity()).hideLoading();
                                }
                                dataHandler.removeCallbacks(loadDataRunnable);
                                dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
                            }
                        } else {
                            cacheLogging("getAllLastUpdateTime_no_need_update_but_not_in_roomcontrol");
                            cacheLogging("cache_cycle_end");
                            dataHandler.removeCallbacks(loadDataRunnable);
                            dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
                        }
                    }
                    isFirstTimeCall = false;
                }

                @Override
                public void postExecuteWithCellId(String xml, String cellId) {

                }
            }, url, "get", bundle);

        } catch (MalformedURLException e) {
            onGetAllLastUpdateTimeError();
        } catch (Exception e) {
            onGetAllLastUpdateTimeError();
        }
    }


    private void getItemFromNetwork() {

        cacheLogging("getItemFromNetwork_start");

        try {
            URL url = new URL(MainApplication.getCmsApiBase() + MainApplication.getContext().getString(R.string.get_item_list));

            Bundle bundle = new Bundle();
            bundle.putInt("getAll", 1);
            bundle.putString("appVersion", MainApplication.getAppVersion());
            ApiRequest.request(new XMLCaller.InternetCallbacks() {
                @Override
                public boolean hasInternet() {
                    return Helper.hasInternet(false);
                }

                @Override
                public void onError(int failMode, boolean isPostExecute) {
                    cacheLogging("getItemFromNetwork_api_error");
                    onGetItemError();
                }

                @Override
                public void postExecute(String json) {
                    cacheLogging("getItemFromNetwork_api_success");
                    parseItem(json, true);
                }

                @Override
                public void postExecuteWithCellId(String xml, String cellId) {

                }
            }, url, "post", bundle);

        } catch (MalformedURLException e) {
            cacheLogging("getItemFromNetwork_api_error");
            onGetItemError();
        } catch (Exception e) {
            cacheLogging("getItemFromNetwork_api_error");
            onGetItemError();
        }

    }

    private void getItemFromLocal() {

        cacheLogging("getItemFromLocal_start");

        String filename = "item.json";
        String read_data = null;
        InputStream inputStream = null;
        try {
            inputStream = MainApplication.getContext().openFileInput(filename);
            InputStreamReader inputStreamReader = null;
            BufferedReader bufferedReader = null;

            try {
                if (inputStream != null) {
                    inputStreamReader = new InputStreamReader(inputStream);
                    bufferedReader = new BufferedReader(inputStreamReader);
                    String receiveString = "";
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((receiveString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(receiveString);
                    }
                    inputStream.close();
                    read_data = stringBuilder.toString();
                }
            } catch (FileNotFoundException e) {
                cacheLogging("getItemFromLocal_io_error");
                getItemFromNetwork();
            } catch (IOException e) {
                cacheLogging("getItemFromLocal_io_error");
                getItemFromNetwork();
            } finally {
                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            }

        } catch (FileNotFoundException e) {
            cacheLogging("getItemFromLocal_io_error");
            getItemFromNetwork();
        } catch (IOException e) {
            cacheLogging("getItemFromLocal_io_error");
            getItemFromNetwork();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {

            }
            cacheLogging("getItemFromLocal_io_success");
            parseItem(read_data, false);
        }

    }

    private void getDictFromNetwork() {

        cacheLogging("getDictFromNetwork_start");

        try {
            URL url = new URL(MainApplication.getCmsApiBase() + MainApplication.getContext().getString(R.string.get_lang_key));

            Bundle bundle = new Bundle();
            bundle.putInt("getAll", 1);
            bundle.putString("appVersion", MainApplication.getAppVersion());
            ApiRequest.request(new XMLCaller.InternetCallbacks() {
                @Override
                public boolean hasInternet() {
                    return Helper.hasInternet(false);
                }

                @Override
                public void onError(int failMode, boolean isPostExecute) {
                    cacheLogging("getDictFromNetwork_api_error");
                    onGetDictError();
                }

                @Override
                public void postExecute(final String json) {
                    cacheLogging("getDictFromNetwork_api_success");
                    parseDict(json, true);
                }

                @Override
                public void postExecuteWithCellId(String xml, String cellId) {

                }
            }, url, "post", bundle);

        } catch (MalformedURLException e) {
            cacheLogging("getDictFromNetwork_api_error");
            onGetDictError();
        } catch (Exception e) {
            cacheLogging("getDictFromNetwork_api_error");
            onGetDictError();
        }
    }

    private void getDictFromLocal() {
        String filename = "dict.json";
        String read_data = null;
        InputStream inputStream = null;
        try {
            inputStream = MainApplication.getContext().openFileInput(filename);
            InputStreamReader inputStreamReader = null;
            BufferedReader bufferedReader = null;

            try {
                if (inputStream != null) {
                    inputStreamReader = new InputStreamReader(inputStream);
                    bufferedReader = new BufferedReader(inputStreamReader);
                    String receiveString = "";
                    StringBuilder stringBuilder = new StringBuilder();

                    while ((receiveString = bufferedReader.readLine()) != null) {
                        stringBuilder.append(receiveString);
                    }
                    inputStream.close();
                    read_data = stringBuilder.toString();
                }
            } catch (FileNotFoundException e) {
                getDictFromNetwork();
            } catch (IOException e) {
                getDictFromNetwork();
            } finally {
                if (inputStreamReader != null) {
                    inputStreamReader.close();
                }
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            }

        } catch (FileNotFoundException e) {
            getDictFromNetwork();
        } catch (IOException e) {
            getDictFromNetwork();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {

            }
            parseDict(read_data, false);
        }
    }

    public void forceReload() {

        settings = MainApplication.getContext().getSharedPreferences(MainApplication.PREFS_NAME, 0);
        settings.edit().putLong("lastItemUpdateTime", 0).commit();
        settings.edit().putLong("lastDictUpdateTime", 0).commit();
        isCacheInited = false;
        Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
        MainApplication.getCurrentActivity().startActivity(intent);
    }

    private void onGetAllLastUpdateTimeError() {
        retryTime++;
        cacheLogging("onGetAllLastUpdateTimeError_retry_" + retryTime);
        if (retryTime < 3) {
            getAllLastUpdateTime();
        } else {
            if(settings.getLong("lastItemUpdateTime", 0) == 0){
                showErrorDialog();
            } else {

                if(isFirstTimeCall) {
                    if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
                        // if there is no data in memory
                        //can change logic to check one by one
                        if (GlobalValue.getInstance().getAllItemArray().size() == 0 || GlobalValue.getInstance().getAllDictArray().size() == 0) {
                            cacheLogging("getAllLastUpdateTime_no_need_update_but_value_null");
                            getItemFromLocal();
                        } else {
                            cacheLogging("getAllLastUpdateTime_no_need_update");
                            cacheLogging("cache_cycle_end");
                            if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
                                ((RoomControlActivity) MainApplication.getCurrentActivity()).hideLoading();
                            }
                            dataHandler.removeCallbacks(loadDataRunnable);
                            dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
                        }
                    } else {
                        cacheLogging("getAllLastUpdateTime_no_need_update_but_not_in_roomcontrol");
                        cacheLogging("cache_cycle_end");
                        dataHandler.removeCallbacks(loadDataRunnable);
                        dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
                    }
                    isFirstTimeCall = false;
                } else {
                    cacheLogging("onGetAllLastUpdateTimeError_skip_update");
                    cacheLogging("cache_cycle_end");
                    dataHandler.removeCallbacks(loadDataRunnable);
                    dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
                    if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
                        ((RoomControlActivity) MainApplication.getCurrentActivity()).hideLoading();
                    }
                }
            }
        }
    }

    private void onGetItemError() {
        retryTime++;
        cacheLogging("onGetItemError_retry_" + retryTime);
        if (retryTime < 3) {
            getItemFromNetwork();
        } else if (retryTime < 5) {
            getItemFromLocal();
        } else {
            showErrorDialog();
        }
    }

    private void onGetDictError() {
        retryTime++;
        cacheLogging("onGetDictError_retry_" + retryTime);
        if (retryTime < 3) {
            getDictFromNetwork();
        } else if (retryTime < 5) {
            getDictFromLocal();
        } else {
            showErrorDialog();
        }
    }

    private void parseItem(final String json, final boolean writeToStorage) {

        cacheLogging("parseItem_start");

        if (json != null && json.length() > 0) {
            GetGeneralItemParser parser = new GetGeneralItemParser(json, new GetGeneralItemParserInterface() {
                @Override
                public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute) {
                    cacheLogging("parseItem_error");
                    onGetItemError();
                }

                @Override
                public void onGetGeneralItemFinishParsing(ArrayList<GeneralItem> generalItemArray) {
                    GlobalValue.getInstance().getAllItemArray().clear();
                    GlobalValue.getInstance().getAllItemArray().addAll(generalItemArray);

                    if (writeToStorage) {

                        // write items inside internal storage
                        String filename = "item.json";
                        FileOutputStream outputStream = null;

                        try {
                            outputStream = MainApplication.getContext().openFileOutput(filename, Context.MODE_PRIVATE);
                            outputStream.write(json.getBytes());
                            outputStream.flush();
                        } catch (FileNotFoundException ex) {
                            onGetItemError();
                            Log.i(TAG, "error of writing item.file = " + ex.toString());
                        } catch (IOException ex) {
                            onGetItemError();
                            Log.i(TAG, "error of writing item.file = " + ex.toString());
                        } finally {
                            if (outputStream != null) {
                                try {
                                    outputStream.close();
                                } catch (IOException ex) {

                                }
                            }
                            retryTime = 0;
                            settings.edit().putLong("lastItemUpdateTime", getMASTime()).commit();

                            cacheLogging("parseItem_success");

                            if (dictNeedUpdate) {
                                getDictFromNetwork();
                            } else {
                                getDictFromLocal();
                            }
                        }
                    } else {

                        retryTime = 0;
                        settings.edit().putLong("lastItemUpdateTime", getMASTime()).commit();

                        cacheLogging("parseItem_success");

                        if (dictNeedUpdate) {
                            getDictFromNetwork();
                        } else {
                            getDictFromLocal();
                        }
                    }
                }

                @Override
                public void onGetGeneralItemError() {
                    cacheLogging("parseItem_error");
                    onGetItemError();
                }
            });
            parser.startParsing();
        } else {
            cacheLogging("parseItem_json_null");
            getItemFromNetwork();
        }
    }


    private void parseDict(final String json, final boolean writeToStorage) {

        cacheLogging("parseDict_start");

        if (json != null && json.length() > 0) {

            GetAllDictParser parser = new GetAllDictParser(json, new GetAllDictParserInterface() {
                @Override
                public void onGetDictParsingError(int failMode, boolean isPostExecute) {
                    cacheLogging("parseDict_error");
                    onGetDictError();
                }

                @Override
                public void onGetDictFinishParsing(ArrayList<Dictionary> dictArray) {
                    GlobalValue.getInstance().getAllDictArray().clear();
                    GlobalValue.getInstance().getAllDictArray().addAll(dictArray);

                    if(writeToStorage) {
                        // write items inside internal storage
                        String filename = "dict.json";
                        FileOutputStream outputStream = null;

                        try {
                            outputStream = MainApplication.getContext().openFileOutput(filename, Context.MODE_PRIVATE);
                            outputStream.write(json.getBytes());
                            outputStream.flush();
                        } catch (FileNotFoundException ex) {
                            onGetDictError();
                            Log.i(TAG, "error of writing item.file = " + ex.toString());
                        } catch (IOException ex) {
                            onGetDictError();
                            Log.i(TAG, "error of writing item.file = " + ex.toString());
                        } finally {
                            if (outputStream != null) {
                                try {
                                    outputStream.close();
                                } catch (IOException ex) {

                                }
                            }

                            retryTime = 0;
                            settings.edit().putLong("lastDictUpdateTime", getMASTime()).commit();

                            cacheLogging("parseDict_success");

                            if (mediaNeedUpdate) {
                                cacheLogging("parseItem_go_to_ImageCacheManager");
                                ImageCacheManager.getInstance().getImageFileList();
                            } else {
                                if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
                                    ((RoomControlActivity) MainApplication.getCurrentActivity()).hideLoading();
                                }
                                cacheLogging("parseDict_end_no_need_media");
                                cacheLogging("cache_cycle_end");
                            }
                            MainApplication.getConfig().init();
                            dataHandler.removeCallbacks(loadDataRunnable);
                            dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());


                        }
                    } else {
                        retryTime = 0;
                        settings.edit().putLong("lastDictUpdateTime", getMASTime()).commit();

                        cacheLogging("parseDict_success");

                        if (mediaNeedUpdate) {
                            cacheLogging("parseItem_go_to_ImageCacheManager");
                            ImageCacheManager.getInstance().getImageFileList();
                        } else {
                            if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
                                cacheLogging("parseDict_end_no_need_media");
                                cacheLogging("cache_cycle_end");
                                ((RoomControlActivity) MainApplication.getCurrentActivity()).hideLoading();
                            }
                        }
                        dataHandler.removeCallbacks(loadDataRunnable);
                        dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getDataCacheInterval());
                    }


                }

                @Override
                public void onGetDictError() {
                    cacheLogging("parseDict_error");
                    DataCacheManager.this.onGetDictError();
                }
            });
            parser.startParsing();


        } else {
            cacheLogging("parseDict_json_error");
            getDictFromNetwork();
        }
    }

    private Long getMASTime() {
        String year = MainApplication.getMAS().getData("year");
        String month = MainApplication.getMAS().getData("month");
        String day = MainApplication.getMAS().getData("day");
        String hour = MainApplication.getMAS().getData("hour");
        String minute = MainApplication.getMAS().getData("minute");

        Long lastUpdateTime = System.currentTimeMillis();
        if (year.length() > 0) {
            String time = month + "/" + day + "/" + year + " " + hour + ":" + minute;

            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
            try {
                Date date = formatter.parse(time);
                Log.i(TAG, "date to String =" + date.toString());
                Log.i(TAG, "date to timestamp = " + date.getTime());
                lastUpdateTime = date.getTime();
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return lastUpdateTime;
    }


    private void showErrorDialog() {

        if(showingDialog){
            return;
        }
        if(MainApplication.getCurrentActivity() != null) {
            showingDialog = true;
            Helper.showAlertBox(MainApplication.getCurrentActivity(),
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    retryTime = 0;
                                    showingDialog = false;
                                    isCacheInited = false;
                                    startLoadData();
                                    break;
                            }
                        }
                    },
                    MainApplication.getContext().getString(R.string.error_no_internet), "Try Again");
        }
    }


    public boolean getIsCacheInited() {
        return isCacheInited;
    }

    public void setIsCacheInited(boolean isCacheInited) {
        this.isCacheInited = isCacheInited;
    }

    public AlertDialog getAlertDialog() {
        return alertDialog;
    }

    public void setAlertDialog(AlertDialog alertDialog) {
        this.alertDialog = alertDialog;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void cacheLogging(String logStep) {

        Log.d(TAG, logStep);

        // for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String date = df.format(today);
        map.put("Time", date);
        map.put("Step", logStep);

        String LogTypeEvent = "DataCache";
        FlurryAgent.logEvent(LogTypeEvent, map);


        if (MainApplication.getConfig().isCMSDebug()) {
            try {
                Date dateNow = new Date();
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd%20HH:mm:ss");
                String dateString = dateFormat.format(dateNow);
                String urlString = new StringBuilder()
                        .append(MainApplication.getCmsApiBase())
                        .append(MainApplication.getContext().getString(R.string.post_event_log)).append("?")
                        .append("roomId=").append(MainApplication.getMAS().getCurRoom()).append("&")
                        .append("date=").append(dateString).append("&")
                        .append("event=").append(LogTypeEvent).append("&")
                        .append("string=").append(logStep)
                        .toString().trim();

                URL url = new URL(urlString);
                ApiRequest.request(new XMLCaller.InternetCallbacks() {
                    @Override
                    public boolean hasInternet() {
                        return Helper.hasInternet(false);
                    }

                    @Override
                    public void onError(int failMode, boolean isPostExecute) {
                    }

                    @Override
                    public void postExecute(String json) {
                    }

                    @Override
                    public void postExecuteWithCellId(String xml, String cellId) {

                    }
                }, url, "get", null);

            } catch (MalformedURLException e) {
            } catch (Exception e) {
            }

        }

    }


}
