package com.hsh.esdbsp.global;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.graphics.Shader.TileMode;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.widget.TypeFaceProvider;


@SuppressWarnings("unused")
public class Helper {
	
	private static Boolean showingWarning = false;
	
	public static  SharedPreferences settings;

	public static  void setTimeDigiFonts(TextView textView){
		Typeface face = TypeFaceProvider.getTypeFace(MainApplication.getContext(), "digi.otf");
		textView.setTypeface(face);
	}

	public static  void setAppFonts(TextView textView){
		Typeface face = TypeFaceProvider.getTypeFace(MainApplication.getContext(), "gill_san_light.otf");
		textView.setTypeface(face);
	}

	public static void setAppFonts(Button button, String fontPath) { // Config all fonts
		Typeface face = TypeFaceProvider.getTypeFace(MainApplication.getContext(), "gill_san_light.otf");
		button.setTypeface(face);
	}

	public static void setMinionFonts(TextView textView) { // Config all fonts
		Typeface face = TypeFaceProvider.getTypeFace(MainApplication.getContext(), "minion.otf");
		textView.setTypeface(face);
	}



//	public static void setFonts(TextView textView, String fontPath, int style) { // Config all fonts
//		Typeface face = TypeFaceProvider.getTypeFace(MainApplication.getContext(), fontPath);
//
//	    textView.setTypeface(face, style);
//	}
//	public static void setFonts(Button button, String fontPath, int style) { // Config all fonts
//		Typeface face = TypeFaceProvider.getTypeFace(MainApplication.getContext(), fontPath);
//		button.setTypeface(face, style);
//	}

	 public static int getPixelFromDp(int dp){
		 float scale = MainApplication.getContext().getResources().getDisplayMetrics().density;
		 //Log.i("scale = ", scale+"");
		 
		 if(scale ==2){
			 scale = 1.4f;
		 }
		 
		 int w = (int) (dp * scale + 0.5f);
		 return w;
	 }
	 public static int getPixelFromDpNormalScale(int dp){
		 float scale = MainApplication.getContext().getResources().getDisplayMetrics().density;
		 //Log.i("scale = ", scale+"");
		 
		 if(scale ==2){
			 
		 }
		 
		 int w = (int) (dp * scale + 0.5f);
		 return w;
	 }
	 public static float getDimenFromResource(int resource){
		 
		 Display display = MainApplication.getCurrentActivity().getWindowManager().getDefaultDisplay(); 
		 int width = display.getWidth();
		 int height = display.getHeight();
		 //Log.i("Helper = ", width+"");
		 
		 if(width>2000){
			 return MainApplication.getCurrentActivity().getResources().getDimension(resource)/2;
		 }
		 else{
			 return MainApplication.getCurrentActivity().getResources().getDimension(resource);
		 }
		
	 }
	 public static String addZeroToDollarText(String text){
			
			for(int i =0;i < text.length();i++)
			{
				if(text.charAt(i) == '.' && i == text.length()-2) //check if the '.' is at the the position before the last char  e.g: 42.5
				{
					text = text + '0';
				}
			}
			return text;
	 }
	 public static Bitmap drawShadow(Bitmap bitmap, int leftRightThk, int bottomThk, int padTop) {
		    int w = bitmap.getWidth();
		    int h = bitmap.getHeight();

		    int newW = w - (leftRightThk * 2);
		    int newH = h - (bottomThk + padTop);

		    Bitmap.Config conf = Bitmap.Config.ARGB_8888;
		    Bitmap bmp = Bitmap.createBitmap(w, h, conf);
		    Bitmap sbmp = Bitmap.createScaledBitmap(bitmap, newW, newH, false);

		    Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		    Canvas c = new Canvas(bmp);

		    // Left
		    int leftMargin = (leftRightThk + 7)/2;
		    Shader lshader = new LinearGradient(0, 0, leftMargin, 0, Color.TRANSPARENT, Color.BLACK, TileMode.CLAMP);
		    paint.setShader(lshader);
		    c.drawRect(0, padTop, leftMargin, newH, paint); 

		    // Right
		    Shader rshader = new LinearGradient(w - leftMargin, 0, w, 0, Color.BLACK, Color.TRANSPARENT, TileMode.CLAMP);
		    paint.setShader(rshader);
		    c.drawRect(newW, padTop, w, newH, paint);

		    // Bottom
		    Shader bshader = new LinearGradient(0, newH, 0, bitmap.getHeight(), Color.BLACK, Color.TRANSPARENT, TileMode.CLAMP);
		    paint.setShader(bshader);
		    c.drawRect(leftMargin -3, newH, newW + leftMargin + 3, bitmap.getHeight(), paint);
		    c.drawBitmap(sbmp, leftRightThk, 0, null);

		    return bmp;
	}
	 
	 public static String enlargeFirstLetter(String t){
		 String s;
		 Boolean includeFirstTwo = false;
		 
		 if(t.equalsIgnoreCase("TV") || t.equalsIgnoreCase("TV / Audio") || t.equalsIgnoreCase("TV Apps")){
			 includeFirstTwo = true;
		 }
		 
		 //only for english, if not english dont do thing
		 if(!MainApplication.getMAS().getData("data_language").toString().equalsIgnoreCase("E"))
		 {
			 return t;
		 }
		 
		 s = "<big>";
				 
		for(int i = 0;i<t.length();i++){
			char c = t.charAt(i);
			
			s += c;
			
			if(i==0){
				if(includeFirstTwo){
					s += t.charAt(i+1);
					i++;
				}
				s+="</big>";
			}
			
			if(c==' '){
				s+="<big>"+t.charAt(i+1)+"</big>";
				i++;
			}
			
		}
		//Log.i("Helper", "enlargeFirstLetter return: " + s);
		 return s;
	 }
		
	public static String processDate(String t){
	    String [] a= t.split("-");
	    String monthString = "";
	    String dayString = "";
	    
	    for(int i =0; i < a.length ; i++){
	        Log.d("videoListApaer a ", a[i]);
	    }
	   //Log.d("videoListApaer a ", Integer.parseInt(a[2],10)+"");
	    switch (Integer.parseInt(a[1]))
	    {
	    case 1:  monthString = "January";
	    		break;
		case 2:  monthString = "February";
		        break;
		case 3:  monthString = "March";
		        break;
		case 4:  monthString = "April";
		        break;
		case 5:  monthString = "May";
		        break;
		case 6:  monthString = "June";
		        break;
		case 7:  monthString = "July";
		        break;
		case 8:  monthString = "August";
		        break;
		case 9:  monthString = "September";
		        break;
		case 10: monthString = "October";
		        break;
		case 11: monthString = "November";
		        break;
		case 12: monthString = "December";
		        break;
	    }
	    
	    if(a[2].subSequence(0, 0) == "0")
	    {
	    	a[2] = a[2].subSequence(1, 1).toString();
	    }
		return monthString +" "+ a[2] +", "+a[0];  // the require date format is "December 31, 2012"
	}
/*	//check if the device is a tablet
	public static Boolean isTablet(){
		 Boolean isTablet = MainApplication.getContent().getResources().getBoolean(R.bool.isTablet);
		 return isTablet;
	}*/
	public static boolean hasInternet(boolean allowRoaming) {
    	MainApplication.getContext();
		NetworkInfo info = ((ConnectivityManager) MainApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
		if (info == null || !info.isConnected() || (!allowRoaming && info.isRoaming())) {
			return false;
		}
		
		return true;
    }
	
	public static void setViewMargin(View v, int left, int top, int right, int bottom){
		FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
    	lp.setMargins(left, top, right, bottom);
    	v.setLayoutParams(lp);
	}
	
	
    
    public static void showInternetWarningBox(Context a, DialogInterface.OnClickListener listener, String message){
    	
    	if(!showingWarning){
        AlertDialog.Builder builder = new AlertDialog.Builder(a);
        builder.setMessage(message).setPositiveButton("OK", listener)
        .show();
        showingWarning = true;
    	}
    }
    
    public static void showAlertBox(Context a, DialogInterface.OnClickListener listener,  String message, String positiveMessage, String negativeMessage){
    
        AlertDialog.Builder builder = new AlertDialog.Builder(a);
        builder.setMessage(message).setPositiveButton(positiveMessage, listener).setNegativeButton(negativeMessage, listener)
        .show();
    	
    }
    
    public static void showAlertBox(Context a, DialogInterface.OnClickListener listener,  String message, String positiveMessage){
        AlertDialog.Builder builder = new AlertDialog.Builder(a);
		builder.setCancelable(false);
        builder.setMessage(message).setPositiveButton(positiveMessage, listener).show();
        DataCacheManager.getInstance().setAlertDialog(builder.create());
    }
    
    public static void enlargeClickableArea(final View delegate, final Rect rect) {
    	final View parent = (View)delegate.getParent();
    	parent.post( new Runnable() {
    	    // Post in the parent's message queue to make sure the parent
    	    // lays out its children before we call getHitRect()
    	    public void run() {
    	        final Rect r = new Rect();
    	        delegate.getHitRect(r);
    	        r.top += rect.top;
    	        r.left += rect.left;
    	        r.bottom += rect.bottom;
    	        r.right += rect.right;
    	        parent.setTouchDelegate( new TouchDelegate(r, delegate));
    	    }
    	});
    }
    
    public static String getMacAddress(Context context) {

		final String marshmallowMacAddress = "02:00:00:00:00:00";

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

			try {
				List<NetworkInterface> all = Collections.list(NetworkInterface.getNetworkInterfaces());
				for (NetworkInterface nif : all) {
					if (!nif.getName().equalsIgnoreCase("wlan0")) continue;

					byte[] macBytes = nif.getHardwareAddress();
					if (macBytes == null) {
						return "";
					}

					StringBuilder res1 = new StringBuilder();
					for (byte b : macBytes) {
						res1.append(String.format("%02X:",b));
					}

					if (res1.length() > 0) {
						res1.deleteCharAt(res1.length() - 1);
					}
					return res1.toString();
				}
			} catch (Exception ex) {
			}
			return marshmallowMacAddress;

		} else {
			WifiManager wimanager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			String macAddress = wimanager.getConnectionInfo().getMacAddress();
			if (macAddress == null) {
				macAddress = "Device don't have mac address or wi-fi is disabled";
			}
			return macAddress;
		}


	}
    
    public static Bitmap getBitmapOfView(View v) {
		v.setDrawingCacheEnabled(false);
		v.setDrawingCacheEnabled(true);
		Bitmap bmp = v.getDrawingCache();
		return bmp;
	}
    
    public static String getFileContent(String targetFilePath){
    	
    	FileInputStream fileInputStream = null;
        File file = new File(targetFilePath);
        try {
                 fileInputStream = new FileInputStream(file);
        
        } catch (FileNotFoundException e) {
               // TODO Auto-generated catch block
               Log.e("","fileinput error = " + e.toString());
        }
        StringBuilder sb = null;
        try {
			while(fileInputStream.available() > 0) {

			      if(null== sb)  sb = new StringBuilder();

			 sb.append((char)fileInputStream.read());
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    String fileContent = null;
    if(null!=sb){
         fileContent= sb.toString();
         // This is your fileContent in String.


    }
    try {
       fileInputStream.close();
    }
    catch(Exception e){
        // TODO Auto-generated catch block
        Log.e("",""+e.toString());
    }
        return fileContent;
}	
    
    public static void showInternetWarningToast(){
    	
    	if(!Helper.hasInternet(false)){
	    	Context context = MainApplication.getContext();
	    	CharSequence text = MainApplication.getLabel("flighttracks.networkissue.errormessage");
	    	int duration = Toast.LENGTH_LONG;
	
	    	Toast toast = Toast.makeText(context, text, duration);
	    	toast.show();
    	}
    }
    
    public static String processPrice(String _price){
		Log.i("Helper", "Index of . = " + _price.indexOf("."));
		
		if(_price.indexOf(".") != _price.length()-3){
			Log.i("Helper", "process Price do!");
			_price= _price + "0";
		}
		return _price;
	}
    
    public static String changeLangCode(String lang){
    	//no Korean, Russian and traditional Chinese.
    	//Korean and Russian will return Eng, T.Chinese will return S.Chinese
    	Log.i("Helper", "klang " + lang);
    	
    	if(lang.equalsIgnoreCase("E"))
			return "en";
    	else if(lang.equalsIgnoreCase("S"))
			return "es";
    	else if(lang.equalsIgnoreCase("F"))
			return "fr";
    	else if(lang.equalsIgnoreCase("J"))
			return "ja";
    	else if(lang.equalsIgnoreCase("A"))
			return "ar";
    	else if(lang.equalsIgnoreCase("K"))
			return "en";
    	else if(lang.equalsIgnoreCase("CS"))
			return "zh_cn";
    	else if(lang.equalsIgnoreCase("CT"))
			return "zh_tw";
    	else if(lang.equalsIgnoreCase("R"))
			return "en";
    	else if(lang.equalsIgnoreCase("P"))
			return "pt";
    	else if(lang.equalsIgnoreCase("G"))
			return "de";
    	else 
    		return "en";
    }
    
    public static String changeLangCodeEarthTV(String lang){
    	if(lang.equalsIgnoreCase("E"))
			return "en_US";
    	else if(lang.equalsIgnoreCase("S"))
			return "es_ES";
    	else if(lang.equalsIgnoreCase("F"))
			return "fr_FR";
    	else if(lang.equalsIgnoreCase("J"))
			return "ja_JP";
    	else if(lang.equalsIgnoreCase("A"))
			return "ar_SA";
    	else if(lang.equalsIgnoreCase("K"))
			return "ko_KR";
    	else if(lang.equalsIgnoreCase("CS"))
			return "zh-cn_CN";
    	else if(lang.equalsIgnoreCase("CT"))
			return "zh-hk_HK";
    	else if(lang.equalsIgnoreCase("R"))
			return "ru_RU";
    	else if(lang.equalsIgnoreCase("P"))
			return "pt_PT";
    	else if(lang.equalsIgnoreCase("G"))
			return "de_DE";
    	else 
    		return "en_US";
    }

	public static String changeLangCodePrint(String lang){
		//no Korean, Russian and traditional Chinese.
		//Korean and Russian will return Eng, T.Chinese will return S.Chinese
		Log.i("Helper", "klang " + lang);

		if(lang.equalsIgnoreCase("E"))
			return "en";
		else if(lang.equalsIgnoreCase("S"))
			return "es";
		else if(lang.equalsIgnoreCase("F"))
			return "fr";
		else if(lang.equalsIgnoreCase("J"))
			return "jp";
		else if(lang.equalsIgnoreCase("A"))
			return "ar";
		else if(lang.equalsIgnoreCase("K"))
			return "ko";
		else if(lang.equalsIgnoreCase("CS"))
			return "zh_cn";
		else if(lang.equalsIgnoreCase("CT"))
			return "zh_hk";
		else if(lang.equalsIgnoreCase("R"))
			return "ru";
		else if(lang.equalsIgnoreCase("P"))
			return "pt";
		else if(lang.equalsIgnoreCase("G"))
			return "de";
		else
			return "en";
	}

    public static Boolean checkIsSameString(String a, String b){
		Log.i("Helper", "string1 = " + a);
		Log.i("Helper", "string2 = " + b);
		
		return a.equalsIgnoreCase(b);
	}

    /*
    public static Dialog displayAlertView(Context c, View.OnClickListener listener, String body, String posText, String negText) {
    	Dialog mDialog;
    	mDialog = new Dialog(c, R.style.Dialog_Confirmation);
		mDialog.setContentView(R.layout.dialog_confirmation);
		
		TextView msg = (TextView) mDialog.findViewById(R.id.dialog_text);
		Helper.setFonts(msg, c.getResources().getString(R.string.helvetica_bold));
		msg.setText(body);

		Button btnPositive = (Button) mDialog.findViewById(R.id.btn_positive);
		if(posText != null) {
			Helper.setFonts(btnPositive, c.getResources().getString(R.string.helvetica_bold));
			btnPositive.setText(posText);
			btnPositive.setOnClickListener(listener);
		} else {
			btnPositive.setVisibility(View.GONE);
		}
		
		Button btnNegative = (Button) mDialog.findViewById(R.id.btn_negative);
		if(negText != null) {
			Helper.setFonts(btnNegative, c.getResources().getString(R.string.helvetica_bold));
			btnNegative.setText(negText);
			btnNegative.setOnClickListener(listener);
		} else {
			btnNegative.setVisibility(View.GONE);	
		}

		mDialog.setCancelable(false);
		
		return mDialog;
    }
    
    public static void displayAlertView(Context context, String title, String content, String btn1Title, String btn2Title, DialogInterface.OnClickListener listener) {
    	AlertDialog.Builder builder = new AlertDialog.Builder(context);
    	if(title != null) {
    		builder.setTitle(title);
    	}
    	if(content != null) {
    		builder.setMessage(content);
    	}
    	if(btn1Title != null) {
    		builder.setPositiveButton(btn1Title, listener);
    	}
    	if(btn2Title != null) {
    		builder.setNegativeButton(btn2Title, listener);
    	}
    	builder.show();	
    }
    
    */


	 
}
