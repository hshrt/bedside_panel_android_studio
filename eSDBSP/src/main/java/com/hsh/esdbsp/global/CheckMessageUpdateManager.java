package com.hsh.esdbsp.global;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.Time;
import android.util.Log;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.activity.MessageActivity;
import com.hsh.esdbsp.comm.MASComm;
import com.hsh.esdbsp.network.ApiRequest;
import com.hsh.esdbsp.network.XMLCaller;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class CheckMessageUpdateManager extends Object implements XMLCaller.InternetCallbacks {

    public interface MessageListener {

        void onMessageIconChanged(boolean isOn);

        void onPopupMessage(JSONArray messageObjects);
    }

    private String TAG = "CheckMessageUpdateManager";

    private SharedPreferences settings;

    private Handler dataHandler = new Handler();

    private long checkMessageDelay = MainApplication.getConfig().getCheckMessageInterval();

    private Boolean startLoop = false;

    private String lastMsgSource = null;
    private Boolean isMsgOn = false;

    private AsyncHttpClient client;

    JSONArray popupMsgObjects = null;
    JSONObject messageObject = null;

    private ArrayList<MessageListener> messageListenerArrayList = new ArrayList<MessageListener>();

    public enum ApiCallType {
        CHECK_MSG_UPDATE
    }

    ApiCallType apiCallType;

    private Runnable loadDataRunnable = new Runnable() {
        public void run() {
            dataHandler.removeCallbacks(loadDataRunnable);
            checkMessageLastUpdate();
        }
    };

    private static CheckMessageUpdateManager checkRoomStatusManager = new CheckMessageUpdateManager();

    public static synchronized CheckMessageUpdateManager getInstance() {
        return checkRoomStatusManager;
    }

    public void startLoadData() {

        try {
            settings = MainApplication.getContext().getSharedPreferences(MainApplication.PREFS_NAME, 0);

            // start the data loading looping, the reason to delay half second
            // is because need wait for setup Wifi first
            if (!startLoop) {
                dataHandler.postDelayed(loadDataRunnable, 500);
            }
        } catch (Exception e) {
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    startLoadData();
                }
            }, 1000);
        }
    }

    @Override
    public boolean hasInternet() {
        return Helper.hasInternet(false);
    }

    @Override
    public void onError(int failMode, boolean isPostExecute) {

        String errorMsg = "";

        if (failMode == 1) {
            errorMsg = "no internet";
        }

        Log.i(TAG, "failMode = " + errorMsg);
        Log.i(TAG, "onerror");
        // networkError = true;

        // for safe reason
        dataHandler.removeCallbacks(loadDataRunnable);
        dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getCheckMessageInterval());

    }

    public void handlePopupMsg() {
        for (MessageListener messageListener : messageListenerArrayList) {
            messageListener.onPopupMessage(popupMsgObjects);
        }
    }

    private void checkMessageLastUpdate() {
        Log.i(TAG, "checkMessageLastUpdate start!!!");
        String msg = MainApplication.getMAS().getData("data_msg");
        String fax = MainApplication.getMAS().getData("data_fax");

        if (msg.equalsIgnoreCase("ON") || fax.equalsIgnoreCase("ON")) {
            Log.i(TAG, "msg from MAS is ON");
            isMsgOn = true;

            for(MessageListener messageListener : messageListenerArrayList){
                messageListener.onMessageIconChanged(true);
            }

            if (fax.equalsIgnoreCase("ON") && MainApplication.getConfig().isBigFaxOn()) {

                if (!(MainApplication.getCurrentActivity() instanceof MessageActivity)) {
                    settings.edit().putBoolean("pending_fax", true).commit();
                }

                if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
                    ((RoomControlActivity) MainApplication.getCurrentActivity()).showFaxBigEnvelopePopup();
                }
                checkMessageDelay = MainApplication.getConfig().getCheckMessageShortInterval();
            }

        } else {

            Log.i(TAG, "msg from MAS is OFF");
            isMsgOn = false;

            for(MessageListener messageListener : messageListenerArrayList){
                messageListener.onMessageIconChanged(false);
            }

            if (MainApplication.getConfig().isBigFaxOn()) {
                settings.edit().putBoolean("pending_fax", false).commit();
                checkMessageDelay = MainApplication.getConfig().getCheckMessageInterval();
                if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
                    ((RoomControlActivity) MainApplication.getCurrentActivity()).hideFaxBigEnvelopePopup();
                }
            }
        }

        // for safe reason
        dataHandler.removeCallbacks(loadDataRunnable);
        dataHandler.postDelayed(loadDataRunnable, checkMessageDelay);

        /* new version we try to assign the check message to MAS to do */
        //now it is calling check regular update instead by check if there is message
        apiCallType = ApiCallType.CHECK_MSG_UPDATE;

        try {
            URL url = null;

            String currentTime = "";

            String year = MainApplication.getMAS().getData("year");
            String month = MainApplication.getMAS().getData("month");
            String day = MainApplication.getMAS().getData("day");
            String hour = MainApplication.getMAS().getData("hour");
            String minute = MainApplication.getMAS().getData("minute");

            if (year.equalsIgnoreCase("")) {
                Time today = new Time(Time.getCurrentTimezone());
                today.setToNow();
                year = today.year + "";
                month = (today.month + 1) + "";
                day = today.monthDay + "";

            }

            currentTime = year + "-" + month + "-" + day + "%20" + hour + ":" + minute + ":00";

            url = new URL(MainApplication.getCmsApiBase()
                    + MainApplication.getContext().getString(R.string.get_regular_update)
                    + "?roomId="
                    + MainApplication.getMAS().getData("data_myroom")
                    + "&currentTime=" + currentTime);

            Log.e(TAG, "url = " + url);

            Bundle bundle = new Bundle();

            ApiRequest.request(this, url, "get", bundle);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        } catch (Exception e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        }
    }

    public void setMsgIsRead(String messageId) {
        client = new AsyncHttpClient();

        String url = MainApplication.getCmsApiBase() + MainApplication.getContext().getString(
                R.string.update_message_status);
        RequestParams params = new RequestParams();

        params.put("room", MainApplication.getMAS().getData("data_myroom"));
        params.put("msgId", messageId);

        Log.i(TAG, "URL = " + url);

        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int arg0, Header[] headers, byte[] response) {
                String s = new String(response);
                Log.i(TAG, "success response = " + s);

            }

            @Override
            public void onFailure(int arg0, Header[] headers, byte[] response, Throwable e) {
                if (response != null) {
                    String s = new String(response);
                }
                Log.i(TAG, "Error = " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                Log.i(TAG, "retrying number = " + retryNo);
            }

        });
    }

    public void returnMsgSignalToMAS(int index) {

        client = new AsyncHttpClient();

        String url = "";

        if (index == 1) {
            url = "http://" + MainApplication.getMAS().getMASPath() + "/mas.php?cmd=SET%20HAVEMSG%20ON";
            MainApplication.getMAS().setData("msg", "ON");
        } else {
            url = "http://" + MainApplication.getMAS().getMASPath() + "/mas.php?cmd=SET%20HAVEMSG%20OFF";
            MainApplication.getMAS().setData("fax", "OFF");
            MainApplication.getMAS().setData("msg", "OFF");

        }
        Log.i(TAG, "URL = " + url);

        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int arg0, Header[] headers, byte[] response) {
                String s = new String(response);
                Log.i(TAG, "success response = " + s);

            }

            @Override
            public void onFailure(int arg0, Header[] headers, byte[] response, Throwable e) {
                if (response != null) {
                    String s = new String(response);
                    Log.i(TAG, s);
                }
                Log.i(TAG, "Error = " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                Log.i(TAG, "retrying number = " + retryNo);
            }

        });
    }

    @Override
    public void postExecute(String json) {

        // for safe reason
        dataHandler.removeCallbacks(loadDataRunnable);
        dataHandler.postDelayed(loadDataRunnable, MainApplication.getConfig().getCheckMessageInterval());

        settings = MainApplication.getContext().getSharedPreferences(MainApplication.PREFS_NAME, 0);

        int status = 0;

        Log.i(TAG, "postExecute");
        Log.i("XMLContent", "api xml =" + json);

        JSONObject jo;
        JSONObject dataObj = null;
        try {
            jo = new JSONObject(json);

            dataObj = jo.getJSONObject("data");

            if (dataObj.has("boardcastMessage")) {
                popupMsgObjects = dataObj.getJSONArray("boardcastMessage");
            }

            status = jo.getInt("status");
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        if (apiCallType == ApiCallType.CHECK_MSG_UPDATE && status == 1 && popupMsgObjects != null && popupMsgObjects.length() > 0) {

            handlePopupMsg();

        } else if (dataObj != null) {
            // this case we have no message return
            Log.i(TAG, "no message at all");
        }

    }

    @Override
    public void postExecuteWithCellId(String xml, String cellId) {
        // TODO Auto-generated method stub

    }

    public String getLastMsgSource() {
        return lastMsgSource;
    }

    public void setLastMsgSource(String lastMsgSource) {
        this.lastMsgSource = lastMsgSource;
    }

    public Boolean getIsMsgOn() {
        return isMsgOn;
    }

    public void setIsMsgOn(Boolean isGold) {
        this.isMsgOn = isGold;
    }

    public CharSequence createDate(long timestamp) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timestamp);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return sdf.format(d);
    }

    public int addMessageListener(MessageListener listener) {

        if (messageListenerArrayList == null) {
            messageListenerArrayList = new ArrayList<MessageListener>();
        }
        messageListenerArrayList.add(listener);

        return messageListenerArrayList.size() - 1; //return position of the added listener
    }

    public void removeMessageListener(int position) {
        if(position < messageListenerArrayList.size()) {
            messageListenerArrayList.remove(position);
        }
    }


}
