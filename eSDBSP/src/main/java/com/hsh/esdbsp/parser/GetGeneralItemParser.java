package com.hsh.esdbsp.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.network.XMLCaller;

public class GetGeneralItemParser {

	public interface GetGeneralItemParserInterface {
		public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute);
		public void onGetGeneralItemFinishParsing(ArrayList<GeneralItem> generalItemArray);
		public void onGetGeneralItemError();
	}
	
	Map<String,String> stampBundle = new HashMap<String,String>();
	
	private String TAG = "GetGeneralItemParser";
	private String json;
	private GetGeneralItemParserInterface getGeneralItemParserInterface;
	
	private ArrayList<GeneralItem> generalItemArray;

	public GetGeneralItemParser(String json, GetGeneralItemParserInterface theInterface) {

		this.getGeneralItemParserInterface = theInterface;
		this.json = json;
		generalItemArray = new ArrayList<GeneralItem>();
	}

	@SuppressWarnings("unused")
	public void startParsing() {
		
		Log.i("GetGeneralItemParser", json);
		
		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray("data");
			
			for(int x = 0; x < jArray.length(); x++){
				JSONObject itemObj = jArray.getJSONObject(x);
				GeneralItem generalItem = new GeneralItem("");
				generalItem.setItemId(itemObj.getString("id"));
				generalItem.setTitleId(itemObj.getString("titleId"));
				generalItem.setDescriptionId(itemObj.getString("descriptionId"));
				generalItem.setParentId(itemObj.getString("parentId"));
				generalItem.setType(itemObj.getString("type"));
				generalItem.setOrder(Integer.parseInt(itemObj.getString("order")));
				generalItem.setLayout(Integer.parseInt(itemObj.getString("layout")));
				if(itemObj.has("command") == true)
				{generalItem.setCommand(itemObj.getString("command"));}
				if(itemObj.has("availTime") == true)
				{generalItem.setAvailTime(itemObj.getString("availTime"));}
				//Log.i(TAG, "image name =" + itemObj.getString("image"));	
				//Log.i(TAG, "iconName  =" + itemObj.getString("iconName"));	
				generalItem.setImageName(itemObj.getString("image"));
				generalItem.setIconName(itemObj.getString("iconName"));
				generalItem.setExt(itemObj.getString("ext"));
				generalItem.setPrice(Double.parseDouble(itemObj.getString("price")));
				
				if(itemObj.has("minChoice")){
					generalItem.setMinChoice((Integer.parseInt(itemObj.getString("minChoice"))));
				}
				if(itemObj.has("maxChoice")){
					generalItem.setMaxChoice((Integer.parseInt(itemObj.getString("maxChoice"))));
				}
				if(itemObj.has("maxQuantity")){
					generalItem.setMaxQuantity((Integer.parseInt(itemObj.getString("maxQuantity"))));
				}
				
				if(itemObj.has("print") == true){
				generalItem.setPrint(itemObj.getString("print"));
				}
				if(itemObj.has("startTime")) {
					generalItem.setStartTime(itemObj.getString("startTime"));
				}
				if(itemObj.has("endTime")) {
					generalItem.setEndTime(itemObj.getString("endTime"));
				}
				if(itemObj.has("startTime2")) {
					generalItem.setStartTime2(itemObj.getString("startTime2"));
				}
				if(itemObj.has("endTime2")) {
					generalItem.setEndTime2(itemObj.getString("endTime2"));
				}
				if(itemObj.has("optionSetIds")) {
					generalItem.setOptionSetIds(itemObj.getString("optionSetIds"));
				}
				if(itemObj.has("canOrder")) {
					generalItem.setCanOrder(itemObj.getInt("canOrder"));
				}
				if(itemObj.has("complexOption")) {
					generalItem.setComplexOption(itemObj.getInt("complexOption"));
				}
				if(itemObj.has("videoId")) {
					generalItem.setVideoId(itemObj.getString("videoId"));
				}
				if(itemObj.has("videoThumbnail")) {
					generalItem.setVideoThumbnail(itemObj.getString("videoThumbnail"));
					if (!"null".equals(generalItem.getVideoThumbnail())) {
						Log.d(TAG, "!!!!! videoThumbnail :" + generalItem.getVideoThumbnail());
					}
				}
				generalItemArray.add(generalItem);
				
				//Log.i(TAG, "the layout = " + Integer.parseInt(itemObj.getString("layout")));
				
			}
			
			
			/*if(restObject.has("movieData")){
				JSONArray movieArray = restObject.getJSONArray("movieData");
				
				ArrayList<Movie> tempMovieArray = new ArrayList<Movie>();
				
				for(int x = 0; x < movieArray.length(); x++){
					JSONObject itemObj = movieArray.getJSONObject(x);
					Movie m = new Movie("");
					m.setThumbFileName(itemObj.getString("thumbId"));
					m.setImageFileName(itemObj.getString("imageId"));
					
					tempMovieArray.add(m);
				}
				
				GlobalValue.getInstance().getAllMovieArray().clear();
				GlobalValue.getInstance().getAllMovieArray().addAll(tempMovieArray);				
			}*/
			

			if (generalItemArray.size() > 0) {
				Log.i(TAG, "dius");
				getGeneralItemParserInterface.onGetGeneralItemFinishParsing(generalItemArray);
			} 
			else {
				getGeneralItemParserInterface.onGetGeneralItemError();
			}
			
		} catch (JSONException e1) {
			
			Log.i(TAG, "ERROR!");
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getGeneralItemParserInterface.onGetGeneralItemParsingError(XMLCaller.FAIL_MODE_DISPLAY_RETRY, true);
		}

	}
}
