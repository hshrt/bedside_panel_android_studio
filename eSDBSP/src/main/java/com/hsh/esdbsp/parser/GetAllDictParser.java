package com.hsh.esdbsp.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.Dictionary;
import com.hsh.esdbsp.network.XMLCaller;

public class GetAllDictParser {

	public interface GetAllDictParserInterface {
		public void onGetDictParsingError(int failMode, boolean isPostExecute);

		public void onGetDictFinishParsing(
				ArrayList<Dictionary> generalItemArray);

		public void onGetDictError();
	}

	Map<String, String> stampBundle = new HashMap<String, String>();

	private String TAG = "GetAllDictParser";
	private String json;
	private GetAllDictParserInterface getAllDictParserInterface;

	private ArrayList<Dictionary> dictArray;

	public GetAllDictParser(String json, GetAllDictParserInterface theInterface) {
		this.getAllDictParserInterface = theInterface;
		this.json = json;
		dictArray = new ArrayList<Dictionary>();
	}

	@SuppressWarnings("unused")
	public void startParsing() {

		Log.i("GetAllDictParser", json);

		int errorcode = -1;
		JSONObject restObject;
		try {
			restObject = new JSONObject(json);
			JSONArray jArray = restObject.getJSONArray("data");
			int length = jArray.length();

			for (int x = 0; x < length; x++) {
				JSONObject dictObj = jArray.getJSONObject(x);
				Dictionary dictionary = new Dictionary("");
				if(dictObj.has("id"))
				dictionary.setDictId(dictObj.getString("id"));
				if(dictObj.has("key"))
				dictionary.setKey(dictObj.getString("key"));
				//dictionary.setKey(dictObj.getString("fatafat"));
				if(dictObj.has("E"))
				dictionary.setEn(dictObj.getString("E"));
				if(dictObj.has("S"))
				dictionary.setEs(dictObj.getString("S"));
				if(dictObj.has("F"))
				dictionary.setFr(dictObj.getString("F"));
				if(dictObj.has("J"))
				dictionary.setJp(dictObj.getString("J"));
				if(dictObj.has("A"))
				dictionary.setAr(dictObj.getString("A"));
				if(dictObj.has("K"))
				dictionary.setKo(dictObj.getString("K"));
				if(dictObj.has("CS"))
				dictionary.setZh_cn(dictObj.getString("CS"));
				if(dictObj.has("CT"))
				dictionary.setZh_hk(dictObj.getString("CT"));
				if(dictObj.has("R"))
				dictionary.setRu(dictObj.getString("R"));
				if(dictObj.has("P"))
				dictionary.setPt(dictObj.getString("P"));
				if(dictObj.has("G"))
				dictionary.setDe(dictObj.getString("G"));
				if(dictObj.has("I"))
					dictionary.setTr(dictObj.getString("I"));

				dictArray.add(dictionary);
			}
			if (dictArray.size() > 0) {
				Log.i(TAG, "good");
				getAllDictParserInterface.onGetDictFinishParsing(dictArray);
			} else {
				getAllDictParserInterface.onGetDictError();
			}

		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			getAllDictParserInterface.onGetDictParsingError(
					XMLCaller.FAIL_MODE_PARSE_ERROR, true);
		}

	}
}
