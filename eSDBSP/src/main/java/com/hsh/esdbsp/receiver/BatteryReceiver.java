package com.hsh.esdbsp.receiver;

import com.hsh.esdbsp.widget.BatteryChecker;
import com.hsh.esdbsp.widget.Log;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

public class BatteryReceiver extends BroadcastReceiver {

    BatteryChecker batteryChecker;

    public BatteryReceiver(BatteryChecker batteryChecker) {
        this.batteryChecker = batteryChecker;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_POWER_CONNECTED)) {

            batteryChecker.setBattery(true, getBatteryLevel(intent));

        } else if (intent.getAction().equals(Intent.ACTION_POWER_DISCONNECTED)) {

            batteryChecker.setBattery(false, getBatteryLevel(intent));

        } else if (intent.getAction().equals(Intent.ACTION_BATTERY_CHANGED)) {

            int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
            boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
            boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

            if (usbCharge || acCharge) {
                batteryChecker.setBattery(true, getBatteryLevel(intent));
            } else {
                batteryChecker.setBattery(false, getBatteryLevel(intent));
            }
        }
    }

    private int getBatteryLevel(Intent intent) {
        int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        return Math.round((level / (float) scale) * 100);
    }


}
