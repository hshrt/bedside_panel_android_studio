package com.hsh.esdbsp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.widget.BatteryChecker;
import com.hsh.esdbsp.widget.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WifiReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        ConnectivityManager conMan = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = conMan.getActiveNetworkInfo();

        String roomNumber = MainApplication.getMAS().getData("data_myroom");
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String date = df.format(today);

        if (networkInfo != null && networkInfo.getType() == ConnectivityManager.TYPE_WIFI){
            Map<String, String> map = new HashMap<String, String>();
            map.put("Room", roomNumber);
            map.put("Time", date);
            map.put("Status", "ON");
            FlurryAgent.logEvent("WifiStatus", map);

        } else{
            Map<String, String> map = new HashMap<String, String>();
            map.put("Room", roomNumber);
            map.put("Time", date);
            map.put("Status", "OFF");
            FlurryAgent.logEvent("WifiStatus", map);
        }

    }
};