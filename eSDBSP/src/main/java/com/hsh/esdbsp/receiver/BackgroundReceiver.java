package com.hsh.esdbsp.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.hsh.esdbsp.MainApplication;

/**
 * Created by benjamintang on 17/4/2018.
 */

public class BackgroundReceiver extends BroadcastReceiver
{
    @Override
    public void onReceive(Context context, Intent intent)
    {
        Bundle bData = intent.getExtras();
        if (bData.get("msg").equals("mMASRunnable"))
        {
            MainApplication.getMAS().setupMASPath();
            MainApplication.getMAS().getMASFeed();
        }
    }
}
