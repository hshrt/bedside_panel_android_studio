package com.hsh.esdbsp.comm;


import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import com.hsh.esdbsp.MainApplication;

import android.os.AsyncTask;
import android.provider.ContactsContract.Data;
import android.util.Log;
import android.widget.TextView;

 
public class MASAsyncDataSend extends AsyncTask<String, String, Void> {
	
	
	static String myFeed = "";
	
	@Override
    protected void onPreExecute() {
		try {

			myFeed = "";

		} catch (Exception e) {}
		
        super.onPreExecute();
        
    }	
	
	

	@Override
	protected Void doInBackground(String... params) {
		// TODO Auto-generated method stub

        try {     

        	final String URL = "http://" + MainApplication.getMAS().getMASPath() + "/mas.php?cmd=" + params[0] + "&sta="; 
        	Log.v("MASSEND","send-here start:"+URL);
        	
        	
        	URL ww = new URL(URL);             
        	URLConnection tc = ww.openConnection();
        	tc.setConnectTimeout(500);
        	
        	BufferedReader in = new BufferedReader(new InputStreamReader(                     
        			tc.getInputStream()));    
        	
        	String line = "";
        	String fullSTA = "";
        	while ((line = in.readLine()) != null) {  
        		fullSTA = fullSTA + line;    
        	}         	
        	
        	myFeed = fullSTA;
        	
        	in.close();

        	
/*        	
			InputStream inputStream = null;

			// create HttpClient
			HttpClient httpclient = new DefaultHttpClient();
	
			// make GET request to the given URL
			HttpResponse httpResponse = httpclient.execute(new HttpGet(URL));
	
			// receive response as inputStream
			inputStream = httpResponse.getEntity().getContent();
	

			myFeed = inputStream.toString();
			
			inputStream.close();
*/        	
        	
        	Log.v("MASSEND","send-here complete:"+URL);

        } catch (Exception e) {
        	Log.v("MASSEND", "die=" + e.toString());
        	
        	
        }		
		
		return null;
	}
	
	
	
	@Override
	protected void onPostExecute(Void data) {
		try {
			
			Log.v("MASSEND","sent:" + myFeed);
			
		} catch (Exception e) {}
	}
	

	
 }

