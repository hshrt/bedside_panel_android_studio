package com.hsh.esdbsp.comm;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.model.Hotel;

import android.os.AsyncTask;


public class RadioAsyncGenLink extends AsyncTask<String, String, Void> {

    @Override
    protected Void doInBackground(String... params) {
        // TODO Auto-generated method stub

        String radioPath = (BuildConfig.BUILD_TYPE.equals("debug")) ? MainApplication.getContext().getString(R.string.radio_path_dev) : MainApplication.getContext().getString(R.string.radio_path);

        try {
            URL ww = null;
            String myLink = "";

            if (params[0].equalsIgnoreCase("START")) {
                myLink = params[1];

                if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                    ww = new URL("http://" + radioPath + "/gen.php?myPort=" + MainApplication.getRadio().getPort() + "&myLink=\"" + myLink + "\"");
                } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                    ww = new URL("http://" + MainApplication.getMAS().getMASPath() + "/gen.php?myPort=" + MainApplication.getRadio().getPort() + "&myLink=\"" + myLink + "\"");
                }
            } else {

                if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                    ww = new URL("http://" + radioPath + "/stop.php?myPort=" + MainApplication.getRadio().getPort());
                } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                    ww = new URL("http://" + MainApplication.getMAS().getMASPath() + "/stop.php?myPort=" + MainApplication.getRadio().getPort());
                }

            }

            final URLConnection tc = ww.openConnection();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    tc.getInputStream()));

            in.close();

        } catch (Exception e) {
        }

        return null;
    }


    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);


    }

}

