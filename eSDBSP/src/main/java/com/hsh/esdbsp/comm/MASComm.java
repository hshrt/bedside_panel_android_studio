package com.hsh.esdbsp.comm;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.RoomControlActivity;

import android.app.Activity;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;

public class MASComm {


    public interface MASListener {

        void onAlarmChanged(boolean isAlarmOn, String hour, String minute, String snooze, String time, String settime, String curtime);

        void onRoomChanged(String roomNumber, String roomType);

        void onDateTimeChanged(String currentTime, String currentDate);

        void onLanguageChanged(String language);

        void onMURChanged(boolean isMur);

        void onDNDChanged(boolean isDnd);

        void onValetChanged(boolean isValet);

        void onRoomControlChanged(String temperature, String temperatureCF, String fan, boolean isNightLightOn, String roomLight, boolean isMasterLightOn);

        void onFaxChanged(boolean haveFax);

        void onMessageChanged(boolean haveMessage);

        void onMASFailed();

    }

    private boolean alarmChangedIndicator = false;
    private boolean roomChangedIndicator = false;
    private boolean dateTimeChangedIndicator = false;
    private boolean languageChangedIndicator = false;
    private boolean murChangedIndicator = false;
    private boolean dndChangedIndicator = false;
    private boolean valetChangedIndicator = false;
    private boolean roomcontrolChangedIndicator = false;
    private boolean faxChangedIndicator = false;
    private boolean messageChangedIndicator = false;

    private MASListener masListener = null;

    private static String myLOG = "MASComm";
    private static boolean isInited = false;

    private static String mMASBase = "";
    private static String mMASPath = "";

    private static String mBSPBase = "";

    private static String data_myroom = "";
    private static String data_roomtype = "";

    private static String data_curdate = "";
    private static String data_curtime = "";
    private static String data_language = "";
    private static String data_mur = "";
    private static String data_dnd = "";
    private static String data_valet = "";
    private static String data_roommasterlight = "";
    private static String data_roomlight = "";
    private static String data_nightlight = "";
    private static String data_temperature = "";
    private static String data_temperature_cf = "";
    private static String data_fan = "";
    private static String data_alarm = "";
    private static String data_alarm_hr = "";
    private static String data_alarm_min = "";
    private static String data_alarm_snooze = "";
    private static String data_alarm_time = "";
    private static String data_alarm_settime = "";

    private static String data_fax = "";
    private static String data_msg = "";

    public static Date data_last_update = new Date();

    private static MASAsyncDataFeed mMASFeed;
    private static MASAsyncDataSend mMASSend;

    static Handler mMASSendHandler = new Handler();
    static int mMASSendDelay = 300;

    static String mCMD = "";

    static int myCurRoom = 100; // 100 - 104
    static int myCurSpinnerPos = 0;

    static String curMASIPPref = "192.168.255.";

    ///////////////////////////////////////////////////////////////////////////////

    public MASComm() {
        getMASFeed();
    }

    public static void enableStrictMode() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
    }


//////////////////////////////////////////////////////////////////////////////////////


    public static void specialSend(String cmd) {

        String result = "";
        String url = "http://" + mMASPath + "/mas.php?cmd=" + cmd;

        if (MainApplication.getContext().getString(R.string.noMAS).equalsIgnoreCase("1")) {
            return;
        }

/*		
        new Thread() {
			   @Override  
			   public void run() {  
				   

			    
			   }  
		}.start();  

*/


        try {

            enableStrictMode();

            Log.v(myLOG, "cmd in progress=" + url);

            InputStream inputStream = null;

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();


        } catch (Exception e) {
            Log.v(myLOG, e.toString());
        }

    }


//////////////////////////////////////////////////////////////////////////////////////


    private static Runnable mMASSendRunnable = new Runnable() {

        public void run() {
            try {

                if (MainApplication.getContext().getString(R.string.noMAS).equalsIgnoreCase("1")) {
                    return;
                }


                Log.v(myLOG, "waiting to send:" + mCMD);

                mMASSendHandler.removeCallbacks(mMASSendRunnable);

                try {
                    if (mMASSend != null) {
                        mMASSend.cancel(true);
                    }
                } catch (Exception e) {
                }


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
                    mMASSend = (MASAsyncDataSend) new MASAsyncDataSend()
                            .executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,
                                    mCMD);
                else
                    mMASSend = (MASAsyncDataSend) new MASAsyncDataSend()
                            .execute(mCMD);


//				specialSend(mCMD);

                //mMASSend = (MASAsyncDataSend) new MASAsyncDataSend().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,	mCMD);

            } catch (Exception e) {
            }
        }
    };


    public static void sendMASCmd(String cmd, int nowFire) {

        Log.v(myLOG, "nowFire=" + nowFire + ",cmd=" + cmd);

        if (MainApplication.getContext().getString(R.string.noMAS).equalsIgnoreCase("1")) {
            Log.v(myLOG, "block send command");
            return;
        }

        try {
            mCMD = cmd;

            //if (MainApplication.getScreenSta() == 1) {

            if (nowFire == 1) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    mMASSend = (MASAsyncDataSend) new MASAsyncDataSend().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mCMD);
                } else {
                    mMASSend = (MASAsyncDataSend) new MASAsyncDataSend().execute(mCMD);
                }

            } else {
                Log.v(myLOG, "it is a in-q cmd=" + cmd);

                //specialSend(mCMD);

                // org is the below

                mMASSendHandler.removeCallbacks(mMASSendRunnable);
                mMASSendHandler.postDelayed(mMASSendRunnable, mMASSendDelay);
            }

            //}

            Log.v(myLOG, cmd);

        } catch (Exception e) {

            Log.v(myLOG, e.toString());
        }

    }

    // ////////////////////////////////////////////////////////////////////////////////////////////////

    public void getMASFeed() {

        if (MainApplication.getContext().getString(R.string.noMAS).equalsIgnoreCase("1")) {
            if(masListener != null) {
                masListener.onMASFailed();
            }
            return;
        }
        // it will continue feed itself
        if (mMASFeed != null) {
            mMASFeed.cancel(true);
            mMASFeed.disconnect();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mMASFeed = (MASAsyncDataFeed) new MASAsyncDataFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            mMASFeed = (MASAsyncDataFeed) new MASAsyncDataFeed().execute();
        }

    }

    // ////////////////////////////////////////////////////////////////////

    // for the room control spinner
    public void setSelectedArea(int i) {
        myCurSpinnerPos = i;
    }

    public int getSelectedArea() {
        return myCurSpinnerPos;
    }


    ///////////////////////////////////////////////////////////////////////

    // for the mas IP and comm
    public void setCurRoom(int i) {
        myCurRoom = i;
        mMASPath = setupMASPath();

        getMASFeed();
    }

    public int getCurRoom() {

        return myCurRoom;
    }

    public String getMASPath() {

        if ((mMASPath == null) || (mMASPath == "")) {

            mMASPath = setupMASPath();
        }

        //Log.v(myLOG,"MAS:" + mMASPath);
        return (mMASPath);
    }

    public String setupMASPath() {
        curMASIPPref = getIpAddr();
        mMASPath = curMASIPPref + getCurRoom();

        if (BuildConfig.BUILD_TYPE.equals("debug")) {
            mMASPath = "192.168.199.10";
        }
        return (mMASPath);
    }

    public void setData(String name, String... value) {

        switch (name) {

            case "myroom":

                if (!value[0].equals(data_myroom)) {
                    roomChangedIndicator = true;
                    data_myroom = value[0];
                }
                break;

            case "roomtype":
                if (!value[0].equals(data_roomtype)) {
                    roomChangedIndicator = true;
                    data_roomtype = value[0];
                }
                break;

            case "mur":
                if (!value[0].equals(data_mur)) {
                    murChangedIndicator = true;
                    data_mur = value[0];
                }
                break;

            case "dnd":
                if (!value[0].equals(data_dnd)) {
                    dndChangedIndicator = true;
                    data_dnd = value[0];
                }
                break;

            case "valet":
                if (!value[0].equals(data_valet)) {
                    valetChangedIndicator = true;
                    data_valet = value[0];
                }
                break;

            case "temperature":
                if (!value[0].equals(data_temperature)) {
                    roomcontrolChangedIndicator = true;
                    data_temperature = value[0];
                }
                if (!value[1].equals(data_temperature_cf)) {
                    roomcontrolChangedIndicator = true;
                    data_temperature_cf = value[1];
                }
                break;

            case "fan":
                if (!value[0].equals(data_fan)) {
                    roomcontrolChangedIndicator = true;
                    data_fan = value[0];
                }
                break;

            case "alarm":

                if (!value[0].equals(data_alarm)) {
                    alarmChangedIndicator = true;
                    data_alarm = value[0];
                }
                if (!value[1].equals(data_alarm_hr)) {
                    alarmChangedIndicator = true;
                    data_alarm_hr = value[1];
                }
                if (!value[2].equals(data_alarm_min)) {
                    alarmChangedIndicator = true;
                    data_alarm_min = value[2];
                }
                if (!value[3].equals(data_alarm_snooze)) {
                    alarmChangedIndicator = true;
                    data_alarm_snooze = value[3];
                }
                if (!value[4].equals(data_alarm_time)) {
                    alarmChangedIndicator = true;
                    data_alarm_time = value[4];
                }
                if (!value[5].equals(data_alarm_settime)) {
                    alarmChangedIndicator = true;
                    data_alarm_settime = value[5];
                }
                break;

            case "language":

                if (!value[0].equals(data_language)) {
                    languageChangedIndicator = true;
                    data_language = value[0];
                }
                break;

            case "curtime":
                if (!value[0].equals(data_curtime)) {
                    dateTimeChangedIndicator = true;
                    data_curtime = value[0];
                }
                break;

            case "curdate":
                if (!value[0].equals(data_curdate)) {
                    dateTimeChangedIndicator = true;
                    data_curdate = value[0];
                }
                break;

            case "nightlight":
                if (!value[0].equals(data_nightlight)) {
                    roomcontrolChangedIndicator = true;
                    data_nightlight = value[0];
                }
                break;

            case "roomlight":
                if (!value[0].equals(data_roomlight)) {
                    roomcontrolChangedIndicator = true;
                    data_roomlight = value[0];
                }
                break;

            case "roommasterlight":
                if (!value[0].equals(data_roommasterlight)) {
                    roomcontrolChangedIndicator = true;
                    data_roommasterlight = value[0];
                }
                break;

            case "fax":
                if (!value[0].equals(data_fax)) {
                    faxChangedIndicator = true;
                    data_fax = value[0];
                }
                data_fax = value[0];
                break;

            case "msg":
                if (!value[0].equals(data_msg)) {
                    messageChangedIndicator = true;
                    data_msg = value[0];
                }
                break;

            default:
                break;
        }
        data_last_update = Calendar.getInstance().getTime();

    }

    public String getData(String name) {

        String dataReturn = "";

        switch (name) {

            case "data_myroom":
                dataReturn = data_myroom;
                break;

            case "data_roomtype":
                dataReturn = data_roomtype;
                break;

            case "data_mur":
                dataReturn = data_mur;
                break;

            case "data_dnd":
                dataReturn = data_dnd;
                break;

            case "data_valet":
                dataReturn = data_valet;
                break;

            case "data_temperature":
                dataReturn = data_temperature;
                break;

            case "data_temperature_cf":
                dataReturn = data_temperature_cf;
                break;

            case "data_fan":
                dataReturn = data_fan;
                break;

            case "data_alarm":
                dataReturn = data_alarm;
                break;

            case "data_alarm_hr":
                dataReturn = data_alarm_hr;
                break;

            case "data_alarm_min":
                dataReturn = data_alarm_min;
                break;

            case "data_alarm_snooze":
                dataReturn = data_alarm_snooze;
                break;

            case "data_alarm_time":
                dataReturn = data_alarm_time;
                break;

            case "data_alarm_settime":
                dataReturn = data_alarm_settime;
                break;

            case "data_language":
                dataReturn = data_language;
                break;

            case "data_curtime":
                dataReturn = data_curtime;
                break;

            case "data_curdate":
                dataReturn = data_curdate;
                break;

            case "year":
                try {
                    dataReturn = data_curdate.substring(0, 4);
                } catch (Exception ex){
                    dataReturn = "";
                }
                break;

            case "month":
                try {
                    dataReturn = data_curdate.substring(5, 7);
                } catch (Exception ex){
                    dataReturn = "";
                }
                break;

            case "day":
                try {
                    dataReturn = data_curdate.substring(8, 10);
                } catch (Exception ex){
                    dataReturn = "";
                }
                break;

            case "hour":
                try {
                    dataReturn = data_curtime.substring(0, 2);
                } catch (Exception ex){
                    dataReturn = "";
                }
                break;

            case "minute":
                try {
                    dataReturn = data_curtime.substring(3, 5);
                } catch (Exception ex){
                    dataReturn = "";
                }
                break;

            case "data_nightlight":
                dataReturn = data_nightlight;
                break;

            case "data_roomlight":
                dataReturn = data_roomlight;
                break;

            case "data_roommasterlight":
                dataReturn = data_roommasterlight;
                break;

            case "data_fax":
                dataReturn = data_fax;
                break;

            case "data_msg":
                dataReturn = data_msg;
                break;
            default:
                break;
        }
        return dataReturn;
    }

    public void setDataInited(boolean isInited){
        this.isInited = isInited;
    }

    public boolean isInited(){
        return this.isInited;
    }

    // //////////////////////////////////////////////////////////////////////////////////////

    public String getBSPBase() {
        return mBSPBase;
    }

    private String getIpAddr() {
        WifiManager wifiManager = (WifiManager) MainApplication.getContext().getApplicationContext().getSystemService(MainApplication.getContext().WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();

        String ipString = String.format("%d.%d.%d.", (ip & 0xff), (ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));
        mBSPBase = String.format("%d", (ip >> 24 & 0xff));

        return ipString;
    }

    public void addMASListener(MASListener listener) {
        masListener = listener;
    }

    public void removeMASListener() {
        masListener = null;
    }


    public void triggerListener() {

        if(masListener == null){
            if (data_alarm_settime.equals(data_curtime)) {
                Activity a = MainApplication.getCurrentActivity();
                if (a != null) {
                    if (RoomControlActivity.class.isInstance(a)) {
                        ((RoomControlActivity)a).wakeBSP();
                    }
                }
            }
            return;
        }

        if (alarmChangedIndicator) {
            masListener.onAlarmChanged((data_alarm.equals("ON") ? true : false), data_alarm_hr, data_alarm_min, data_alarm_snooze, data_alarm_time, data_alarm_settime, data_curtime);
        }

        if (roomChangedIndicator) {
            masListener.onRoomChanged(data_myroom, data_roomtype);
        }

        if (dateTimeChangedIndicator) {
            masListener.onDateTimeChanged(data_curtime, data_curdate);
        }

        if (languageChangedIndicator) {
            masListener.onLanguageChanged(data_language);
        }

        if (murChangedIndicator) {
            masListener.onMURChanged((data_mur.equals("ON") ? true : false));
        }

        if (dndChangedIndicator) {
            masListener.onDNDChanged((data_dnd.equals("ON") ? true : false));
        }

        if (valetChangedIndicator) {
            masListener.onValetChanged((data_valet.equals("ON") ? true : false));
        }

        if (roomcontrolChangedIndicator) {
            masListener.onRoomControlChanged(data_temperature, data_temperature_cf, data_fan, (data_nightlight.equals("ON") ? true : false), data_roomlight, (data_roommasterlight.equals("ON") ? true : false));
        }

        if (faxChangedIndicator) {
            masListener.onFaxChanged((data_fax.equals("ON") ? true : false));
        }

        if (messageChangedIndicator) {
            masListener.onMessageChanged((data_msg.equals("ON") ? true : false));
        }

        alarmChangedIndicator = false;
        roomChangedIndicator = false;
        dateTimeChangedIndicator = false;
        languageChangedIndicator = false;
        murChangedIndicator = false;
        dndChangedIndicator = false;
        valetChangedIndicator = false;
        roomcontrolChangedIndicator = false;
        faxChangedIndicator = false;
        messageChangedIndicator = false;
    }

    public void indicateMASFailed(){
        if(masListener != null) {
            masListener.onMASFailed();
        }
    }
}
