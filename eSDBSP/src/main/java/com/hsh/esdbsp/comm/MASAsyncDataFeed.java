package com.hsh.esdbsp.comm;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.parser.XMLParser;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;


public class MASAsyncDataFeed extends AsyncTask<Void, String, Void> {

    static String myLOG = "MASAsyncDataFeed";

//    static Handler mMASHandler = new Handler();
    static int mMASDelay = 500; // will read from setting file

    static String myFeed = "";
    static String lastFeed = "";
    HttpURLConnection tc = null;

    public void disconnect() {
        if (tc != null) {
            tc.disconnect();
            tc = null;
        }
    }

    @Override
    protected void onPreExecute() {
        myFeed = "";
        super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {

            myFeed = "";

            final String URL = "http://" + MainApplication.getMAS().getMASPath() + "/mas.php?cmd=GET%20STATUS&sta=" + lastFeed;
//        	Log.v(myLOG,URL);
            Log.v(myLOG, "MAS Get status - " + lastFeed);

            URL ww = new URL(URL);
            tc = (HttpURLConnection) ww.openConnection();
            tc.setConnectTimeout(500);

            try {

                BufferedReader in = new BufferedReader(new InputStreamReader(tc.getInputStream()));

                String line = "";
                String fullSTA = "";
                while ((line = in.readLine()) != null) {
                    fullSTA = fullSTA + line;
                }
                myFeed = fullSTA;
                in.close();

            } catch (Exception e) {
                //e.printStackTrace();
            }
            tc = null;

        } catch (Exception e) {
//            mMASHandler.removeCallbacks(mMASRunnable);
//            mMASHandler.postDelayed(mMASRunnable, mMASDelay);
            MainApplication.delayCall(mMASDelay);
        }

        return null;
    }


    @Override
    protected void onCancelled() {
        super.onCancelled();
        MainApplication.getMAS().indicateMASFailed();
        Log.d(myLOG, "ASyncTask cancelled...");
    }


//    private Runnable mMASRunnable = new Runnable() {
//
//        public void run() {
//
//            try {
//                mMASHandler.removeCallbacks(mMASRunnable);
//                MainApplication.getMAS().setupMASPath();
//                MainApplication.getMAS().getMASFeed();
//
//            } catch (Exception e) {
//                mMASHandler.removeCallbacks(mMASRunnable);
//                mMASHandler.postDelayed(mMASRunnable, mMASDelay);
//            }
//        }
//    };


    @Override
    protected void onPostExecute(Void data) {
        try {

            if (myFeed.length() > 10) {

                final String KEY_ITEM = "data"; // parent node

                XMLParser parser = new XMLParser();
                Document doc = parser.getDomElement(myFeed); // getting DOM element

                Log.d("MASFEED", myFeed);

                NodeList nl = doc.getElementsByTagName(KEY_ITEM);

                // looping through all item nodes <item>
                for (int i = 0; i < nl.getLength(); i++) {

                    Node nNode = nl.item(i);
                    Element eElement = (Element) nNode;

                    String name = eElement.getAttribute("name");

                    switch (name) {

                        case "org":
                            lastFeed = eElement.getAttribute("value");
                            break;

                        case "myroom":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "roomtype":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "mur":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "dnd":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "valet":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "temperature":
                            if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                                MainApplication.getMAS().setData(name, eElement.getAttribute("value"), eElement.getAttribute("cf"));
                            } else {
                                MainApplication.getMAS().setData(name, eElement.getAttribute("orgvalue"), eElement.getAttribute("cf"));
                            }
                            break;

                        case "fan":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "alarm":
                            MainApplication.getMAS().setData(name,
                                    eElement.getAttribute("value"),
                                    eElement.getAttribute("hour"),
                                    eElement.getAttribute("minute"),
                                    eElement.getAttribute("snooze"),
                                    eElement.getAttribute("time"),
                                    eElement.getAttribute("settime"));
                            break;

                        case "language":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "curtime":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "curdate":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "nightlight":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "roomlight":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "roommasterlight":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "fax":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        case "msg":
                            MainApplication.getMAS().setData(name, eElement.getAttribute("value"));
                            break;

                        default:
                            break;
                    }
                }
                MainApplication.getMAS().setDataInited(true);
                MainApplication.getMAS().triggerListener();

            }
//            mMASHandler.removeCallbacks(mMASRunnable);
//            mMASHandler.postDelayed(mMASRunnable, 50);

            MainApplication.delayCall(50);
        } catch (Exception e) {
            Log.v(myLOG, "Error:" + e.toString());
//            mMASHandler.removeCallbacks(mMASRunnable);
//            mMASHandler.postDelayed(mMASRunnable, 50);

            MainApplication.delayCall(50);
        }

    }

}

