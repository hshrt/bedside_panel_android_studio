package com.hsh.esdbsp.comm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.model.IBahnMovie;
import com.hsh.esdbsp.model.IBahnMovie.Language;
import com.hsh.esdbsp.model.IBahnTvChannel;

import android.os.AsyncTask;
import android.util.Log;

public class IBahnComm {

	private static final String TAG = "IBahnComm";
	private enum RequestType {
		
		GET_MOVIES("/smartphone/src/Version_2/php/remote_control_get_movies.php?"), 
		GET_TV_CHANNELS("/smartphone/src/Version_2/php/remote_control_get_tv_channels.php?"),
		SELECT_MOVIE("/smartphone/src/Version_2/php/remote_control_select_movie.php?"),
		SELECT_TV_CHANNEL("/smartphone/src/Version_2/php/remote_control_select_tv_channel.php?"),
		REMOTE_BUTTON("/smartphone/src/Version_2/php/remote_control_remote_button.php?");
		
		private final String apiUrl;
		private RequestType(String apiUrl) {
			this.apiUrl = apiUrl;
		}
		public String getApiUrl() {
			return apiUrl;
		}
	};
	public enum ButtonCode {
		KEY_MUTE, KEY_VOLUMEDOWN, KEY_VOLUMEUP, KEY_CHUP, KEY_CHDOWN, KEY_POWER, 
		KEY_TP_PLAY, KEY_TP_PAUSE, KEY_TP_STOP, KEY_TP_FF, KEY_TP_RW, KEY_TP_SKIPF, KEY_TP_SKIPB,
		KEY_LEFT("105"), KEY_RIGHT("106"), KEY_UP("103"), KEY_DOWN("108"), KEY_ENTER("28"), 
		KEY_RED("229"), KEY_GREEN("230"), KEY_YELLOW("231");
		
		private String value = null;
		ButtonCode() {
		}
		ButtonCode(String value) {
			this.value = value;
		}
		public String getValue() {
			if (value != null) {
				return value;
			} else {
				return toString();
			}
		}
	}
	public static interface Callback {
		public void onGetMovieResult(List<IBahnMovie> movieResult);
		public void onGetTvChannelResult(List<IBahnTvChannel> tvChannelResult);
	}

	public void getMovies(String room, Callback callback) {
		HashMap<String, String> param = new HashMap<>();
		param.put("Room", room);
		IBahnRequest request = new IBahnRequest(param, callback);
		request.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new RequestType[]{RequestType.GET_MOVIES});
	}

	public void getTVChannels(String room, String box, String token, Callback callback) {
		HashMap<String, String> param = new HashMap<>();
		param.put("Room", room);
		param.put("Box", box);
		param.put("Token", token);
		IBahnRequest request = new IBahnRequest(param, callback);
		request.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new RequestType[]{RequestType.GET_TV_CHANNELS});
	}
	
	public void selectMovie(String room, String token, String gvid, Callback callback) {
		HashMap<String, String> param = new HashMap<>();
		param.put("Room", room);
		param.put("Token", token);
		param.put("GVID", gvid);
		IBahnRequest request = new IBahnRequest(param, callback);
		request.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new RequestType[]{RequestType.SELECT_MOVIE});
	}
	
	public void selectTVChannel(String room, String box, String token, String tvChannel, Callback callback) {
		HashMap<String, String> param = new HashMap<>();
		param.put("Room", room);
		param.put("Box", box);
		param.put("Token", token);
		param.put("TvChannel", tvChannel);
		IBahnRequest request = new IBahnRequest(param, callback);
		request.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new RequestType[]{RequestType.SELECT_TV_CHANNEL});
	}
	
	public void remoteButton(String room, String box, String token, ButtonCode buttonCode, Callback callback) {
		HashMap<String, String> param = new HashMap<>();
		param.put("Room", room);
		//param.put("Box", box);
		param.put("Token", token);
		param.put("ButtonCode", buttonCode.getValue());
		IBahnRequest request = new IBahnRequest(param, callback);
		request.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new RequestType[]{RequestType.REMOTE_BUTTON});
	}

	private class IBahnRequest extends AsyncTask<RequestType, Void, String> {
		
		private Map<String, String> param;
		private Callback callback;
		private RequestType requestType;
		private String apiBase;

		public IBahnRequest(Map<String, String> param, Callback callback) {
			super();
			this.param = param;
			this.callback = callback;
			this.apiBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? MainApplication.getContext().getString(R.string.ibahn_api_base_dev) : MainApplication.getContext().getString(R.string.ibahn_api_base);
		}
		
		private String replaceURL(String in) {
			if (in == null) {
				return null;
			} else {
				return in.replaceAll("https?://smartphoneapi.ibahn.com", apiBase);
			}
		}
		
		@Override
		protected String doInBackground(RequestType... params) {
			requestType = params[0];
			URL ww;
			try {

				String urlString = apiBase + requestType.getApiUrl();
				if (param.containsKey("Room") && param.get("Room") != null) {
					urlString += "Room=" + param.get("Room") + "&";
				}
				if (param.containsKey("Box") && param.get("Box") != null) {
					urlString += "Box=" + param.get("Box") + "&";
				}
				if (param.containsKey("Token") && param.get("Token") != null) {
					urlString += "Token=" + param.get("Token") + "&";
				}
				if (param.containsKey("GVID") && param.get("GVID") != null) {
					urlString += "GVID=" + param.get("GVID") + "&";
				}
				if (param.containsKey("TvChannel") && param.get("TvChannel") != null) {
					urlString += "TvChannel=" + param.get("TvChannel") + "&";
				}
				if (param.containsKey("ButtonCode") && param.get("ButtonCode") != null) {
					urlString += "ButtonCode=" + param.get("ButtonCode") + "&";
				}
				if (requestType == RequestType.GET_TV_CHANNELS) {
					urlString += "LogoFormat=png&LogoWidth=500&LogoHeight=500&";
				}
				Log.d(TAG, "Sending request: " + urlString);
				ww = new URL(urlString);
	        	URLConnection tc = ww.openConnection();
	        	tc.setConnectTimeout(500);
	        	StringBuilder sb = new StringBuilder();
	        	BufferedReader br = new BufferedReader(new InputStreamReader(tc.getInputStream()));
	        	String line;
	        	while ((line = br.readLine()) != null) {
	        		sb.append(line);
	        	}
	        	br.close();
	        	String response = sb.toString(); 
	        	//java.util.Scanner s = new java.util.Scanner(tc.getInputStream()).useDelimiter("\\A");
	            //String response = s.hasNext() ? s.next() : "";
	            return response;
	            
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        	
			return null;
		}
		
		
		@Override
		protected void onPostExecute(String result) {
			if (result != null && !"".equals(result)) {
				switch (requestType) {
				case GET_MOVIES:
					List<IBahnMovie> movies = parseMovies(result);
					if (callback != null) {
						callback.onGetMovieResult(movies);
					}
					break;
				case GET_TV_CHANNELS:
					List<IBahnTvChannel> channels = parseChannel(result);
					if (callback != null) {
						callback.onGetTvChannelResult(channels);
					}
				case SELECT_MOVIE:
					break;
				default:
					break;
				}
			}
		}



		private List<IBahnMovie>parseMovies(String jsonString) {
			ArrayList<IBahnMovie> movies = new ArrayList<>();
			try {
				JSONObject rootJson = new JSONObject(jsonString);
				JSONArray moviesJson = rootJson.getJSONArray("Movies");
				for (int i = 0; i < moviesJson.length(); i++) {
					JSONObject movieJson = moviesJson.getJSONObject(i);
					IBahnMovie movie = new IBahnMovie();
					movie.setContentId(movieJson.getString("content_id"));
					movie.setTitle(movieJson.getString("title"));
					movie.setTagline(movieJson.getString("tagline"));
					movie.setDescription(movieJson.getString("description"));
					movie.setPrice(movieJson.getString("price"));
					JSONArray tagsJson = movieJson.getJSONArray("tags");
					ArrayList<String> tags = new ArrayList<>();
					for (int j = 0; j < tagsJson.length(); j++) {
						tags.add(tagsJson.getString(j));
					}
					movie.setTags(tags);
					movie.setCertificate(movieJson.getString("certificate"));
					movie.setWebTrailerUrl(replaceURL(movieJson.getString("web_trailer_url")));
					movie.setWebTrailerAndroidUrl(replaceURL(movieJson.getString("web_trailer_android_url")));
					movie.setWebPosterUrl(replaceURL(movieJson.getString("web_poster_url")));
					movie.setTicketId(movieJson.getString("ticket_id"));
					JSONArray languagesJson = movieJson.getJSONArray("languages");
					ArrayList<Language> languages = new ArrayList<>();
					for (int j = 0; j < languagesJson.length(); j++) {
						JSONObject languageJson = languagesJson.getJSONObject(j);
						Language language = new IBahnMovie.Language();
						language.setLandCode(languageJson.getString("lang_code"));
						language.setVodUrl(languageJson.getString("vod_url"));
						language.setVodType(languageJson.getString("vod_type"));
						language.setGvid(languageJson.getString("gvid"));
						language.setEncrypted(languageJson.getString("encrypted"));
						language.setFormat(languageJson.getString("format"));
						language.setName(languageJson.getString("name"));
						languages.add(language);
					}
					movie.setLanguages(languages);
					movies.add(movie);
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return movies;
		}
		
		private List<IBahnTvChannel> parseChannel(String jsonString) {
			ArrayList<IBahnTvChannel> channels = new ArrayList<>();
			try {
				JSONObject rootJson = new JSONObject(jsonString);
				JSONArray channelsJson = rootJson.getJSONArray("TvChannels");
				for (int i = 0; i < channelsJson.length(); i++) {
					IBahnTvChannel channel = new IBahnTvChannel();
					JSONObject channelJson = ((JSONObject)channelsJson.get(i)).getJSONObject("TvChannel");
					channel.setNumber(channelJson.getString("Number"));
					channel.setName(channelJson.getString("Name"));
					channel.setLogoUrl(replaceURL(channelJson.getString("LogoUrl")));
					channel.setContentId(channelJson.getString("ContentId"));
					channel.setLiveFeedUrl(channelJson.getString("LiveFeedURL"));
					channels.add(channel);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return channels;
		}
	}
}
