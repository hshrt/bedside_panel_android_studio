package com.hsh.esdbsp.comm;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

import org.json.JSONObject;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.model.Hotel;

import android.os.AsyncTask;
import android.util.Log;


public class RadioAsyncFeed extends AsyncTask<String, String, Void> {

    String myLOG = "RadioAsyncFeed";


    @Override
    protected Void doInBackground(String... params) {
        // TODO Auto-generated method stub

        String radioPath = (BuildConfig.BUILD_TYPE.equals("debug")) ? MainApplication.getContext().getString(R.string.radio_path_dev) : MainApplication.getContext().getString(R.string.radio_path);

        try {
            URL ww = null;
            String myFilter = params[0].replace(" ", "%20");
            String myType = params[1].toString();

            String line = "";
            String inJSON = "";
            String fullJSON = "";
            JSONObject jr = null;
            JSONObject jrmain = null;

            if (myFilter.equalsIgnoreCase("BASE")) {
                // country genre

                if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                    ww = new URL("http://" + radioPath + "/rj.php?mytype=" + myType);
                } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)
                        || GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)
                        || GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                    ww = new URL("http://" + MainApplication.getMAS().getMASPath() + "/rj.php?mytype=" + myType);
                }

                URLConnection tc = ww.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        tc.getInputStream()));

                while ((line = in.readLine()) != null) {
                    fullJSON = fullJSON + line;
                }

                in.close();

                if (myType.equalsIgnoreCase("Country")) {
                    MainApplication.getRadio().setCountry(fullJSON);
                } else {
                    MainApplication.getRadio().setGenre(fullJSON);
                }

            } else if (myFilter.equalsIgnoreCase("PRESET")) {

                if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                    ww = new URL("http://" + radioPath + "/pre.php");
                } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)
                        || GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)
                        || GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                    ww = new URL("http://" + MainApplication.getMAS().getMASPath() + "/pre.php");
                }

                URLConnection tc = ww.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        tc.getInputStream()));

                while ((line = in.readLine()) != null) {
                    fullJSON = fullJSON + line;
                }

                in.close();
                MainApplication.getRadio().setPreset(fullJSON);

            } else {

                Log.v("radio", "http://" + radioPath + "/rj.php?mytype=" + myType + "&myFilter=" + myFilter + "");

                if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                    ww = new URL("http://" + radioPath + "/rj.php?mytype=" + myType + "&myFilter=" + myFilter + "");
                } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                    ww = new URL("http://" + MainApplication.getMAS().getMASPath() + "/rj.php?mytype=" + myType + "&myFilter=" + myFilter + "");
                }

                URLConnection tc = ww.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        tc.getInputStream(), "ISO-8859-1"));

                while ((line = in.readLine()) != null) {
                    Log.v("radio", line);
                    fullJSON = fullJSON + line;
                }

                in.close();
                MainApplication.getRadio().setRadio(fullJSON);
            }


        } catch (Exception e) {
        }

        return null;
    }


//////////////////////////////////////////////////////////////


}



