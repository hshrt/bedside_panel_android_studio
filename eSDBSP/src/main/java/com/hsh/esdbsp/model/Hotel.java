package com.hsh.esdbsp.model;

/**
 * Created by lawrencetsang on 2017-11-27.
 */

public class Hotel {

    public static String HONGKONG = "phk";
    public static String BEVERLYHILLS = "pbh";
    public static String BEIJING = "pbj";
    public static String CHICAGO = "pch";
    public static String LONDON = "pln";
    public static String NEWYORK = "pny";
    public static String ISTANBUL = "pit";
    public static String YANGON = "pyn";
    public static String SHANGHAI = "psh";

}
