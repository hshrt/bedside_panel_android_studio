package com.hsh.esdbsp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class GeneralItem implements Parcelable {
	private String itemId;
	private String titleId;
	private String descriptionId;
	private String parentId;
	private String type;
	private double price;
	private int order;
	private int layout; //type of layout appear in "In Room Dining", only for item in In Room Dining
	private String command; //the command that needed to be send to HOTSOS
	private String availTime; //temporary variable for PCH in Room Dinning, will delete when develop a full In Room Dinning
	private int maxChoice; //for in Room Dining
	private int minChoice; //for in Room Dining
	private int maxQuantity; //for in Room Dining
	private String imageName;
	private String iconName;
	private String ext;
	private String print;
	private String startTime;
	private String endTime;
	private String startTime2;
	private String endTime2;
	private String optionSetIds;
	private int canOrder;
	private int complexOption;
	private String videoId;
	private String videoThumbnail;

	public GeneralItem(String name) {
		itemId = name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(itemId);
		out.writeString(titleId);
		out.writeString(descriptionId);
		out.writeString(parentId);
		out.writeString(type);
		out.writeInt(order);
		out.writeInt(layout);
		out.writeDouble(price);
		out.writeString(imageName);
		out.writeString(ext);
		out.writeString(iconName);
		out.writeString(command);
		out.writeString(availTime);
		out.writeInt(maxChoice);
		out.writeInt(minChoice);
		out.writeInt(maxQuantity);
		out.writeString(print);
		out.writeString(startTime);
		out.writeString(endTime);
		out.writeString(startTime2);
		out.writeString(endTime2);
		out.writeString(optionSetIds);
		out.writeInt(canOrder);
		out.writeInt(complexOption);
		out.writeString(videoId);
	}

	public static final Parcelable.Creator<GeneralItem> CREATOR = new Parcelable.Creator<GeneralItem>() {
		public GeneralItem createFromParcel(Parcel in) {
			return new GeneralItem(in);
		}

		public GeneralItem[] newArray(int size) {
			return new GeneralItem[size];
		}
	};

	private GeneralItem(Parcel in) {
		itemId = in.readString();
		titleId = in.readString();
		descriptionId = in.readString();
		parentId = in.readString();
		type = in.readString();
		order = in.readInt();
		layout = in.readInt();
		price = in.readDouble();
		imageName = in.readString();
		ext = in.readString();
		iconName = in.readString();
		command = in.readString();
		availTime = in.readString();
		maxChoice = in.readInt(); 
		minChoice = in.readInt(); 
		maxQuantity = in.readInt();
		print = in.readString();
		startTime = in.readString();
		endTime = in.readString();
		startTime2 = in.readString();
		endTime2 = in.readString();
		optionSetIds = in.readString();
		canOrder = in.readInt();
		complexOption = in.readInt();
		videoId = in.readString();
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getDescriptionId() {
		return descriptionId;
	}

	public void setDescriptionId(String descriptionId) {
		this.descriptionId = descriptionId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getTitleId() {
		return titleId;
	}

	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getLayout() {
		return layout;
	}

	public void setLayout(int layout) {
		this.layout = layout;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public String getAvailTime() {
		return availTime;
	}

	public void setAvailTime(String availTime) {
		this.availTime = availTime;
	}

	public String getIconName() {
		return iconName;
	}

	public void setIconName(String iconName) {
		this.iconName = iconName;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getPrint() {
		return print;
	}

	public void setPrint(String print) {
		this.print = print;
	}

	public int getMaxChoice() {
		return maxChoice;
	}

	public void setMaxChoice(int maxChoice) {
		this.maxChoice = maxChoice;
	}

	public int getMinChoice() {
		return minChoice;
	}

	public void setMinChoice(int minChoice) {
		this.minChoice = minChoice;
	}

	public int getMaxQuantity() {
		return maxQuantity;
	}

	public void setMaxQuantity(int maxQuantity) {
		this.maxQuantity = maxQuantity;
	}

	public String getStartTime() {
		return startTime;
	}
	
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}
	
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getOptionSetIds() {
		return optionSetIds;
	}

	public void setOptionSetIds(String optionSetIds) {
		this.optionSetIds = optionSetIds;
	}

	public int getCanOrder() {
		return canOrder;
	}

	public void setCanOrder(int canOrder) {
		this.canOrder = canOrder;
	}

	public int getComplexOption() {
		return complexOption;
	}

	public void setComplexOption(int complexOption) {
		this.complexOption = complexOption;
	}

	public String getStartTime2() {
		return startTime2;
	}

	public void setStartTime2(String startTime2) {
		this.startTime2 = startTime2;
	}

	public String getEndTime2() {
		return endTime2;
	}

	public void setEndTime2(String endTime2) {
		this.endTime2 = endTime2;
	}

	public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}

	public String getVideoThumbnail() {
		return videoThumbnail;
	}

	public void setVideoThumbnail(String videoThumbnail) {
		this.videoThumbnail = videoThumbnail;
	}

}
