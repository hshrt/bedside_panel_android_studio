package com.hsh.esdbsp.model;

import java.util.ArrayList;

public class FoodOrderData {

	private ArrayList<FoodOrder> foodOrder; 
	
	String startTime = null;
	String endTime = null;
	String startTime2 = null;
	String endTime2 = null;;
	

	private FoodOrderData() {
		foodOrder = new ArrayList<>();
	}
	
	private static final FoodOrderData INSTANCE = new FoodOrderData();
	
	public static synchronized FoodOrderData getInstance() {
		return INSTANCE;
	}
	
	public ArrayList<FoodOrder> getFoodOrder() {
		return foodOrder;
	}
	
	
	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartTime2() {
		return startTime2;
	}

	public void setStartTime2(String startTime2) {
		this.startTime2 = startTime2;
	}

	public String getEndTime2() {
		return endTime2;
	}

	public void setEndTime2(String endTime2) {
		this.endTime2 = endTime2;
	}
	
	
}
