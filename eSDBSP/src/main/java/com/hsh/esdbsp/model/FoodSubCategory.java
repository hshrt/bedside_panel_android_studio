package com.hsh.esdbsp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FoodSubCategory implements Parcelable {
	private String id;
	private String catId;
	private String name_en;

	public FoodSubCategory(String _name) {
		name_en = _name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(id);
		out.writeString(catId);
		out.writeString(name_en);
	}

	public static final Parcelable.Creator<FoodSubCategory> CREATOR = new Parcelable.Creator<FoodSubCategory>() {
		public FoodSubCategory createFromParcel(Parcel in) {
			return new FoodSubCategory(in);
		}

		public FoodSubCategory[] newArray(int size) {
			return new FoodSubCategory[size];
		}
	};

	private FoodSubCategory(Parcel in) {
		id = in.readString();
		catId = in.readString();
		name_en = in.readString();
	}

	public String getName() {
		return name_en;
	}

	public void setName(String _name) {
		this.name_en = _name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCatId() {
		return catId;
	}

	public void setCatId(String catId) {
		this.catId = catId;
	}

	

}
