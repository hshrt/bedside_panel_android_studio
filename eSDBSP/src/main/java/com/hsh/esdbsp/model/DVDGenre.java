package com.hsh.esdbsp.model;

import java.util.HashMap;
import java.util.Map;

public class DVDGenre {
	
	private Map languageMap = new HashMap<String, String>();
	private int genreId; 
	
	public DVDGenre() {
		
	}


	public Map getLanguageMap() {
		return languageMap;
	}

	public void setLanguageMap(Map languageMap) {
		this.languageMap = languageMap;
	}


	public int getGenreId() {
		return genreId;
	}


	public void setGenreId(int genreId) {
		this.genreId = genreId;
	}
	
	
}