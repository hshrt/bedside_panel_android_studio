package com.hsh.esdbsp.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import com.hsh.esdbsp.adapter.dining.GeneralItemWrapper;

public class FoodOrder {
	GeneralItemWrapper mainFood;
	int deliveryDay, deliveryHour, deliveryMin;
	int orderQty;

	public static final ArrayList<GeneralItemWrapper> EMPTY_OPTION = new ArrayList<>(0);
	
	public FoodOrder(GeneralItemWrapper food, int orderQty) {
		this.mainFood = food;
		this.orderQty = orderQty;
	}
	
	public GeneralItemWrapper getFood() {
		return mainFood;
	}
	
	public int getDeliveryDay() {
		return deliveryDay;
	}

	public int getDeliveryHour() {
		return deliveryHour;
	}

	public int getDeliveryMin() {
		return deliveryMin;
	}

	public int getOrderQty() {
		return orderQty;
	}

	public String getDeliveryTimeStr() {
		Calendar deliveryCal = Calendar.getInstance();
		if (deliveryDay == 1) {
			deliveryCal.add(Calendar.DAY_OF_MONTH, 1);
		}
		return new SimpleDateFormat("yyyy-MM-dd").format(deliveryCal.getTime())
				+ String.format(" %02d:%02d", deliveryHour, deliveryMin);
	}
	
	public void setDelivery(int day, int hour, int min) {
		this.deliveryDay = day;
		this.deliveryHour = hour;
		this.deliveryMin = min;
	}
	
}