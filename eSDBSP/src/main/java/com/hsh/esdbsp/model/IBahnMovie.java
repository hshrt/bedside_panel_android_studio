package com.hsh.esdbsp.model;

import java.util.List;

public class IBahnMovie {

	public static class Language {
		private String landCode;
		private String vodUrl;
		private String vodType;
		private String gvid;
		private String encrypted;
		private String format;
		private String name;
		
		public String getLandCode() {
			return landCode;
		}
		public void setLandCode(String landCode) {
			this.landCode = landCode;
		}
		public String getVodUrl() {
			return vodUrl;
		}
		public void setVodUrl(String vodUrl) {
			this.vodUrl = vodUrl;
		}
		public String getVodType() {
			return vodType;
		}
		public void setVodType(String vodType) {
			this.vodType = vodType;
		}
		public String getGvid() {
			return gvid;
		}
		public void setGvid(String gvid) {
			this.gvid = gvid;
		}
		public String getEncrypted() {
			return encrypted;
		}
		public void setEncrypted(String encrypted) {
			this.encrypted = encrypted;
		}
		public String getFormat() {
			return format;
		}
		public void setFormat(String format) {
			this.format = format;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
	}
	
	private String contentId;
	private String title;
	private String tagline;
	private String description;
	private String price;
	private List<String> tags;
	private String certificate;
	private String webTrailerUrl;
	private String webTrailerAndroidUrl;
	private String webPosterUrl;
	private String ticketId;
	private List<Language> languages;
	
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getTagline() {
		return tagline;
	}
	public void setTagline(String tagline) {
		this.tagline = tagline;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public String getCertificate() {
		return certificate;
	}
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	public String getWebTrailerUrl() {
		return webTrailerUrl;
	}
	public void setWebTrailerUrl(String webTrailerUrl) {
		this.webTrailerUrl = webTrailerUrl;
	}
	public String getWebTrailerAndroidUrl() {
		return webTrailerAndroidUrl;
	}
	public void setWebTrailerAndroidUrl(String webTrailerAndroidUrl) {
		this.webTrailerAndroidUrl = webTrailerAndroidUrl;
	}
	public String getWebPosterUrl() {
		return webPosterUrl;
	}
	public void setWebPosterUrl(String webPosterUrl) {
		this.webPosterUrl = webPosterUrl;
	}
	public String getTicketId() {
		return ticketId;
	}
	public void setTicketId(String ticketId) {
		this.ticketId = ticketId;
	}
	public List<Language> getLanguages() {
		return languages;
	}
	public void setLanguages(List<Language> languages) {
		this.languages = languages;
	}
	
}
