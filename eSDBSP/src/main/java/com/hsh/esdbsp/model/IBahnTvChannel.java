package com.hsh.esdbsp.model;

public class IBahnTvChannel {

	private String number;
	private String name;
	private String logoUrl;
	private String contentId;
	private String liveFeedUrl;
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogoUrl() {
		return logoUrl;
	}
	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public String getLiveFeedUrl() {
		return liveFeedUrl;
	}
	public void setLiveFeedUrl(String liveFeedUrl) {
		this.liveFeedUrl = liveFeedUrl;
	}
	
	
	
}
