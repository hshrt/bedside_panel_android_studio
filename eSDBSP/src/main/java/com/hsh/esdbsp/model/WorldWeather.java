package com.hsh.esdbsp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class WorldWeather implements Parcelable {
	private String city;
	private int temperature;
	private int icon;
	private String timezone;
	private String time;



	public WorldWeather(String name) {
		city = name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(city);
		out.writeInt(temperature);
		out.writeInt(icon);
		out.writeString(timezone);
		out.writeString(time);
	}

	public static final Parcelable.Creator<WorldWeather> CREATOR = new Parcelable.Creator<WorldWeather>() {
		public WorldWeather createFromParcel(Parcel in) {
			return new WorldWeather(in);
		}

		public WorldWeather[] newArray(int size) {
			return new WorldWeather[size];
		}
	};

	private WorldWeather(Parcel in) {
		city = in.readString();
		temperature = in.readInt();
		icon = in.readInt();
		timezone = in.readString();
		time = in.readString();
	}


	public int getTemperature() {
		return temperature;
	}

	public void setTemperature(int temperature) {
		this.temperature = temperature;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	

	

}
