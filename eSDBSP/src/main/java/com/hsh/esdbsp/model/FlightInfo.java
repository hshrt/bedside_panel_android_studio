package com.hsh.esdbsp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FlightInfo implements Parcelable {
	private String flightId;
	private String carrierCode;
	private String airline;
	private String flightNumber;
	private String place;
	private String departureAirportCode;
	private String arrivalAirportCode;
	private String departureDate;
	private String arrivalDate;
	private String status;
	private String lons;
	private String lats;
	private String planed_lons;
	private String planed_lats;


	public FlightInfo(String name) {
		flightId = name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		
		out.writeString(flightId);
		out.writeString(carrierCode);
		out.writeString(flightNumber);
		out.writeString(place);
		out.writeString(departureAirportCode);
		out.writeString(arrivalAirportCode);
		out.writeString(departureDate);
		out.writeString(arrivalDate);
		out.writeString(status);
		out.writeString(lons);
		out.writeString(lats);
	}

	public static final Parcelable.Creator<FlightInfo> CREATOR = new Parcelable.Creator<FlightInfo>() {
		public FlightInfo createFromParcel(Parcel in) {
			return new FlightInfo(in);
		}

		public FlightInfo[] newArray(int size) {
			return new FlightInfo[size];
		}
	};

	private FlightInfo(Parcel in) {
		flightId = in.readString();
		carrierCode = in.readString();
		flightNumber = in.readString();
		place = in.readString();
		departureAirportCode = in.readString();
		arrivalAirportCode = in.readString();
		departureDate = in.readString();
		arrivalDate = in.readString(); 
		status = in.readString();
		lats = in.readString();
		lons = in.readString();
	}

	public String getFlightId() {
		return flightId;
	}

	public void setFlightId(String flightId) {
		this.flightId = flightId;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getDepartureAirportCode() {
		return departureAirportCode;
	}

	public void setDepartureAirportCode(String departureAirportCode) {
		this.departureAirportCode = departureAirportCode;
	}

	public String getArrivalAirportCode() {
		return arrivalAirportCode;
	}

	public void setArrivalAirportCode(String arrivalAirportCode) {
		this.arrivalAirportCode = arrivalAirportCode;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getAirline() {
		return airline;
	}

	public void setAirline(String airline) {
		this.airline = airline;
	}

	public String getLons() {
		return lons;
	}

	public void setLons(String lons) {
		this.lons = lons;
	}

	public String getLats() {
		return lats;
	}

	public void setLats(String lats) {
		this.lats = lats;
	}

	public String getPlaned_lons() {
		return planed_lons;
	}

	public void setPlaned_lons(String planed_lons) {
		this.planed_lons = planed_lons;
	}

	public String getPlaned_lats() {
		return planed_lats;
	}

	public void setPlaned_lats(String planed_lats) {
		this.planed_lats = planed_lats;
	}

}
