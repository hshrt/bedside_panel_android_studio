package com.hsh.esdbsp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Movie implements Parcelable {
	private String itemId;
	private String titleId;
	private String descriptionId;
	private String thumbFileName; //small images
	private String imageFileName; // big images


	public Movie(String name) {
		itemId = name;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
		out.writeString(itemId);
		out.writeString(titleId);
		out.writeString(descriptionId);
		out.writeString(thumbFileName);
		out.writeString(imageFileName);
	}

	public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
		public Movie createFromParcel(Parcel in) {
			return new Movie(in);
		}

		public Movie[] newArray(int size) {
			return new Movie[size];
		}
	};

	private Movie(Parcel in) {
		itemId = in.readString();
		titleId = in.readString();
		descriptionId = in.readString();
		thumbFileName = in.readString();
		imageFileName = in.readString();
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getDescriptionId() {
		return descriptionId;
	}

	public void setDescriptionId(String descriptionId) {
		this.descriptionId = descriptionId;
	}

	public String getThumbFileName() {
		return thumbFileName;
	}

	public void setThumbFileName(String thumbFileName) {
		this.thumbFileName = thumbFileName;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	

}
