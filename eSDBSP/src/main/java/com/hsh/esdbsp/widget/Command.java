package com.hsh.esdbsp.widget;

import android.content.Context;
import android.os.AsyncTask;
import android.util.*;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.BaseActivity;
import com.hsh.esdbsp.activity.TVAppActivity;
import com.hsh.esdbsp.comm.IBahnComm;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.model.Hotel;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import static com.hsh.esdbsp.widget.Command.RemoteButton.BACK;
import static com.hsh.esdbsp.widget.Command.RemoteButton.CC;
import static com.hsh.esdbsp.widget.Command.RemoteButton.CHANNELDOWN;
import static com.hsh.esdbsp.widget.Command.RemoteButton.CHANNELUP;
import static com.hsh.esdbsp.widget.Command.RemoteButton.DOWN;
import static com.hsh.esdbsp.widget.Command.RemoteButton.FASTFORWARD10;
import static com.hsh.esdbsp.widget.Command.RemoteButton.GREEN;
import static com.hsh.esdbsp.widget.Command.RemoteButton.LAST;
import static com.hsh.esdbsp.widget.Command.RemoteButton.LEFT;
import static com.hsh.esdbsp.widget.Command.RemoteButton.MENU;
import static com.hsh.esdbsp.widget.Command.RemoteButton.MOVIE;
import static com.hsh.esdbsp.widget.Command.RemoteButton.MUTE;
import static com.hsh.esdbsp.widget.Command.RemoteButton.OFF;
import static com.hsh.esdbsp.widget.Command.RemoteButton.OK;
import static com.hsh.esdbsp.widget.Command.RemoteButton.ON;
import static com.hsh.esdbsp.widget.Command.RemoteButton.PAUSE;
import static com.hsh.esdbsp.widget.Command.RemoteButton.PLAY;
import static com.hsh.esdbsp.widget.Command.RemoteButton.PLAYPAUSE;
import static com.hsh.esdbsp.widget.Command.RemoteButton.PURPLE;
import static com.hsh.esdbsp.widget.Command.RemoteButton.RED;
import static com.hsh.esdbsp.widget.Command.RemoteButton.REWIND10;
import static com.hsh.esdbsp.widget.Command.RemoteButton.RIGHT;
import static com.hsh.esdbsp.widget.Command.RemoteButton.SKIP_BACK;
import static com.hsh.esdbsp.widget.Command.RemoteButton.SKIP_FORWARD;
import static com.hsh.esdbsp.widget.Command.RemoteButton.SQUARE;
import static com.hsh.esdbsp.widget.Command.RemoteButton.STOP;
import static com.hsh.esdbsp.widget.Command.RemoteButton.UNMUTE;
import static com.hsh.esdbsp.widget.Command.RemoteButton.UP;
import static com.hsh.esdbsp.widget.Command.RemoteButton.VOLUMNDOWN;
import static com.hsh.esdbsp.widget.Command.RemoteButton.VOLUMNUP;
import static com.hsh.esdbsp.widget.Command.RemoteButton.YELLOW;

/**
 * Created by lawrencetsang on 2018-01-26.
 */

public class Command {

    //change false if  MAS2
    private static boolean isOldMAS = true;

    public enum RemoteButton {
        OK,
        BACK,
        ON,
        OFF,
        UP,
        LEFT,
        SQUARE,
        RIGHT,
        DOWN,
        RED,
        GREEN,
        YELLOW,
        PURPLE,
        VOLUMNUP,
        VOLUMNDOWN,
        MUTE,
        UNMUTE,
        CHANNELUP,
        CHANNELDOWN,
        PLAY,
        PAUSE,
        PLAYPAUSE,
        STOP,
        REWIND10,
        FASTFORWARD10,
        REWIND60,
        FASTFORWARD60,
        SKIP_BACK,
        SKIP_FORWARD,
        MOVIE,
        CC,
        MENU,
        LAST,
        NEXT
    }

    public enum STBItem {
        AIRPLAY,
        CHORMECAST,
        IPTV,
        NETFLIX,
        SPOTIFY,
        YOUTUBE,
        GOOGLEPLAY,
        TED,
        WEATHER
    }

    public static class RoomControl {

        public static String WAKEUP_PANEL = "SET%20WAKEPANEL";
        public static String WAKEUP_PANEL_V2 = "SET%20WAKEPANEL";

        public static void WakeupPanel() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(WAKEUP_PANEL, 0);
            } else {
                MainApplication.getMAS().sendMASCmd(WAKEUP_PANEL_V2, 0);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String MASTERLIGHT_ON = "SET%20ROOMMASTERLIGHT%20ON";
        public static String MASTERLIGHT_OFF = "SET%20ROOMMASTERLIGHT%20OFF";
        public static String MASTERLIGHT_ON_V2 = "SET%20ROOMMASTERLIGHT%20ON";
        public static String MASTERLIGHT_OFF_V2 = "SET%20ROOMMASTERLIGHT%20OFF";

        public static void MasterLightOn() {
            MasterLight(true);
        }

        public static void MasterLightOff() {
            MasterLight(false);
        }

        private static void MasterLight(boolean turnOn) {
            if (isOldMAS) {
                if (turnOn) {
                    MainApplication.getMAS().sendMASCmd(MASTERLIGHT_ON, 1);
                } else {
                    MainApplication.getMAS().sendMASCmd(MASTERLIGHT_OFF, 1);
                }
            } else {
                if (turnOn) {
                    MainApplication.getMAS().sendMASCmd(MASTERLIGHT_ON_V2, 1);
                } else {
                    MainApplication.getMAS().sendMASCmd(MASTERLIGHT_OFF_V2, 1);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String NIGHTLIGHT_ON = "SET%20NIGHTLIGHT%20ON";
        public static String NIGHTLIGHT_OFF = "SET%20NIGHTLIGHT%20OFF";
        public static String NIGHTLIGHT_ON_V2 = "SET%20NIGHTLIGHT%20ON";
        public static String NIGHTLIGHT_OFF_V2 = "SET%20NIGHTLIGHT%20OFF";

        public static void NightLightOn() {
            NightLight(true);
        }

        public static void NightLightOff() {
            NightLight(false);
        }

        private static void NightLight(boolean turnOn) {
            if (isOldMAS) {
                if (turnOn) {
                    MainApplication.getMAS().sendMASCmd(NIGHTLIGHT_ON, 0);
                } else {
                    MainApplication.getMAS().sendMASCmd(NIGHTLIGHT_OFF, 0);
                }
            } else {
                if (turnOn) {
                    MainApplication.getMAS().sendMASCmd(NIGHTLIGHT_ON_V2, 0);
                } else {
                    MainApplication.getMAS().sendMASCmd(NIGHTLIGHT_OFF_V2, 0);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String TEMP_C = "SET%20TEMPCF%20C";
        public static String TEMP_F = "SET%20TEMPCF%20F";
        public static String TEMP_C_V2 = "SET%20TEMPCF%20C";
        public static String TEMP_F_V2 = "SET%20TEMPCF%20F";

        public static void TempCF(boolean isCelsius) {
            if (isOldMAS) {
                if (isCelsius) {
                    MainApplication.getMAS().sendMASCmd(TEMP_C, 0);
                } else {
                    MainApplication.getMAS().sendMASCmd(TEMP_F, 0);
                }
            } else {
                if (isCelsius) {
                    MainApplication.getMAS().sendMASCmd(TEMP_C_V2, 0);
                } else {
                    MainApplication.getMAS().sendMASCmd(TEMP_F_V2, 0);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String TEMP_CHANGE_PBH = "SET%20TEMPNEW%20";
        public static String TEMP_CHANGE = "SET%20TEMP%20";
        public static String TEMP_CHANGE_PBH_V2 = "SET%20TEMPNEW%20";
        public static String TEMP_CHANGE_V2 = "SET%20TEMP%20";

        public static void TempChange(float temperature) {
            if (isOldMAS) {
                if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                    MainApplication.getMAS().sendMASCmd(TEMP_CHANGE_PBH + (temperature * 10), 0);
                } else {
                    MainApplication.getMAS().sendMASCmd(TEMP_CHANGE + Math.round(temperature), 0);
                }
            } else {
                if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                    MainApplication.getMAS().sendMASCmd(TEMP_CHANGE_PBH_V2 + (temperature * 10), 0);
                } else {
                    MainApplication.getMAS().sendMASCmd(TEMP_CHANGE_V2 + Math.round(temperature), 0);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String FAN_TOGGLE = "SET%20FAN%20TOGGLE";
        public static String FAN_TOGGLE_V2 = "SET%20FAN%20TOGGLE";

        public static void FanToggle() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(FAN_TOGGLE, 0);
            } else {
                MainApplication.getMAS().sendMASCmd(FAN_TOGGLE_V2, 0);
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String FAN_SPEED_3_OFF = "SET%20FAN%20OFF";
        public static String FAN_SPEED_3_LOW = "SET%20FAN%20LOW";
        public static String FAN_SPEED_3_MID = "SET%20FAN%20MID";
        public static String FAN_SPEED_3_HIGH = "SET%20FAN%20HIGH";
        public static String FAN_SPEED_5 = "SET%20FAN%20";
        public static String FAN_SPEED_3_OFF_V2 = "SET%20FAN%20OFF";
        public static String FAN_SPEED_3_LOW_V2 = "SET%20FAN%20LOW";
        public static String FAN_SPEED_3_MID_V2 = "SET%20FAN%20MID";
        public static String FAN_SPEED_3_HIGH_V2 = "SET%20FAN%20HIGH";
        public static String FAN_SPEED_5_V2 = "SET%20FAN%20";

        public static void FanSpeed(int fanSpeed, String dot) {
            if (isOldMAS) {

                if (dot.equalsIgnoreCase("5")) {
                    MainApplication.getMAS().sendMASCmd((FAN_SPEED_5 + fanSpeed), 0);

                } else {
                    if (fanSpeed >= 3) {
                        MainApplication.getMAS().sendMASCmd(FAN_SPEED_3_HIGH, 0);
                    } else if (fanSpeed == 2) {
                        MainApplication.getMAS().sendMASCmd(FAN_SPEED_3_MID, 0);
                    } else if (fanSpeed == 1) {
                        MainApplication.getMAS().sendMASCmd(FAN_SPEED_3_LOW, 0);
                    } else {
                        MainApplication.getMAS().sendMASCmd(FAN_SPEED_3_OFF, 0);
                    }
                }
            } else {
                if (dot.equalsIgnoreCase("5")) {
                    MainApplication.getMAS().sendMASCmd((FAN_SPEED_5 + fanSpeed), 0);

                } else {
                    if (fanSpeed >= 3) {
                        MainApplication.getMAS().sendMASCmd(FAN_SPEED_3_HIGH, 0);
                    } else if (fanSpeed == 2) {
                        MainApplication.getMAS().sendMASCmd(FAN_SPEED_3_MID, 0);
                    } else if (fanSpeed == 1) {
                        MainApplication.getMAS().sendMASCmd(FAN_SPEED_3_LOW, 0);
                    } else {
                        MainApplication.getMAS().sendMASCmd(FAN_SPEED_3_OFF, 0);
                    }
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String ROOMLIGHT_TOGGLE = "SET%20ROOMLIGHT%20TOGGLE";
        public static String ROOMLIGHT_TOGGLE_V2 = "SET%20ROOMLIGHT%20TOGGLE";

        public static void RoomLightToggle() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(ROOMLIGHT_TOGGLE, 0);
            } else {
                MainApplication.getMAS().sendMASCmd(ROOMLIGHT_TOGGLE_V2, 0);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String ROOMLIGHT_CHANGE = "SET%20ROOMLIGHT%20";
        public static String ROOMLIGHT_CHANGE_V2 = "SET%20ROOMLIGHT%20";

        public static void RoomLightChange(int level) {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(ROOMLIGHT_CHANGE + level, 0);
            } else {
                MainApplication.getMAS().sendMASCmd(ROOMLIGHT_CHANGE_V2 + level, 0);
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String CURTAIN_TOGGLE = "SET%20CURTAIN%20TOGGLE";
        public static String CURTAIN_TOGGLE_V2 = "SET%20CURTAIN%20TOGGLE";

        public static void CurtainToggle() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(CURTAIN_TOGGLE, 0);
            } else {
                MainApplication.getMAS().sendMASCmd(CURTAIN_TOGGLE_V2, 0);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String CURTAIN_OPEN = "SET%20CURTAIN%20OPEN";
        public static String CURTAIN_CLOSE = "SET%20CURTAIN%20CLOSE";
        public static String CURTAIN_OPEN_V2 = "SET%20CURTAIN%20OPEN";
        public static String CURTAIN_CLOSE_V2 = "SET%20CURTAIN%20CLOSE";

        public static void CurtainChange(boolean open) {
            if (isOldMAS) {
                if (open) {
                    MainApplication.getMAS().sendMASCmd(CURTAIN_OPEN, 0);
                } else {
                    MainApplication.getMAS().sendMASCmd(CURTAIN_CLOSE, 0);
                }
            } else {
                if (open) {
                    MainApplication.getMAS().sendMASCmd(CURTAIN_OPEN_V2, 0);
                } else {
                    MainApplication.getMAS().sendMASCmd(CURTAIN_CLOSE_V2, 0);
                }
            }
        }
    }


    public static class Alarm {

        public static String ALARM_ON = "SET%20ALARM%20ON%20";
        public static String ALARM_OFF = "SET%20ALARM%20OFF";
        public static String ALARM_ON_V2 = "SET%20ALARM%20ON%20";
        public static String ALARM_OFF_V2 = "SET%20ALARM%20OFF";
        public static String ALARM_SNOOZE = "SET%20ALARMMARK%20";
        public static String ALARM_SNOOZE_V2 = "SET%20ALARMMARK%20";

        public static void AlarmOn(String time) {
            AlarmChange(true, time);
        }

        public static void AlarmOff() {
            AlarmChange(false, null);
        }

        private static void AlarmChange(boolean on, String time) {
            if (isOldMAS) {
                if (on) {
                    MainApplication.getMAS().sendMASCmd(ALARM_ON + time, 0);
                } else {
                    MainApplication.getMAS().sendMASCmd(ALARM_OFF, 0);
                }
            } else {
                if (on) {
                    MainApplication.getMAS().sendMASCmd(ALARM_ON_V2 + time, 0);
                } else {
                    MainApplication.getMAS().sendMASCmd(ALARM_OFF_V2, 0);
                }
            }
        }

        public static void AlarmSnooze(String time) {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(ALARM_SNOOZE + time, 0);
            } else {
                MainApplication.getMAS().sendMASCmd(ALARM_SNOOZE_V2 + time, 0);
            }
        }
    }


    public static class Radio {

        public static String AV_OFF = "SET%20AVOFF";
        public static String AV_OFF_V2 = "SET%20AVOFF";
        public static String VOLUMN_UP = "SET%20RADIOVOL%20UP";
        public static String VOLUMN_DOWN = "SET%20RADIOVOL%20DOWN";
        public static String VOLUMN_UP_V2 = "SET%20RADIOVOL%20UP";
        public static String VOLUMN_DOWN_V2 = "SET%20RADIOVOL%20DOWN";

        public static void RemoteButton(RemoteButton button) {

            if (isOldMAS) {
                if (button.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMN_UP, 1);
                } else if (button.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMN_DOWN, 1);
                } else if (button.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(AV_OFF, 1);
                }
            } else {
                if (button.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMN_UP_V2, 1);
                } else if (button.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMN_DOWN_V2, 1);
                } else if (button.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(AV_OFF_V2, 1);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String RADIOMODE = "SET%20RADIOMODE";
        public static String RADIOMODE_V2 = "SET%20RADIOMODE";
        public static String SWITCH_CHANNEL = "SET%20RADIO%20";
        public static String SWITCH_CHANNEL_V2 = "SET%20RADIO%20";

        public static void RadioMode() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(RADIOMODE, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(RADIOMODE_V2, 1);
            }
        }

        public static void SwitchChannel(String channel) {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(SWITCH_CHANNEL + channel, 0);
            } else {
                MainApplication.getMAS().sendMASCmd(SWITCH_CHANNEL_V2 + channel, 0);
            }
        }
    }


    public static class DVD {

        static String BASEDVD = "SET%20DVD%20";
        static String BASECARD = "SET%20DVD%20";
        static String BASEDVD_V2 = "SET%20DVD%20";
        static String BASECARD_V2 = "SET%20DVD%20";

        public static String BLURAY = BASEDVD + "TS05";
        public static String BLURAY_V2 = BASEDVD_V2 + "TS05";

        public static void Bluray() {

            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(BLURAY, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(BLURAY_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String SMARTDOCK = BASEDVD + "TS03";
        public static String SMARTDOCK_V2 = BASEDVD_V2 + "TS03";

        public static void SmartDock() {

            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(SMARTDOCK, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(SMARTDOCK_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String EXTERNAL_INPUT = BASEDVD + "TS07";
        public static String EXTERNAL_INPUT_V2 = BASEDVD_V2 + "TS07";

        public static void ExternalInput() {

            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(EXTERNAL_INPUT, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(EXTERNAL_INPUT_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String CARDREADER = BASEDVD + "TS06";
        public static String CARDREADER_V2 = BASEDVD_V2 + "TS06";

        public static void CardReader() {

            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(CARDREADER, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(CARDREADER_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String DVD_OPEN = BASEDVD + "DV0588";
        public static String DVD_OPEN_V2 = BASEDVD_V2 + "DV0588";

        public static void DVDOpen() {

            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(DVD_OPEN, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(DVD_OPEN_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String DVD_OFF = BASEDVD + "TS00";
        public static String BUTTON_RED = BASEDVD + "DV0148";
        public static String BUTTON_GREEN = BASEDVD_V2 + "DV0948";
        public static String BUTTON_YELLOW = BASEDVD_V2 + "DV0548";
        public static String BUTTON_PURPLE = BASEDVD_V2 + "DV0D48";
        public static String BUTTON_ARROW_UP = BASEDVD + "DV0908";
        public static String BUTTON_ARROW_LEFT = BASEDVD + "DV0D08";
        public static String BUTTON_SQUARE = BASEDVD + "DV0B08";
        public static String BUTTON_ARROW_RIGHT = BASEDVD + "DV0308";
        public static String BUTTON_ARROW_DOWN = BASEDVD + "DV0508";
        public static String RETURN = BASEDVD + "DV0D88";
        public static String SUBTITLE = BASEDVD + "DV0F08";
        public static String BACKWARD = BASEDVD + "DV0388";
        public static String FORWARD = BASEDVD + "DV0048";
        public static String PLAY = BASEDVD + "DV0B48";
        public static String PREVIOUS = BASEDVD + "DV0448";
        public static String NEXT = BASEDVD + "DV0848";
        public static String PAUSE = BASEDVD + "DV0788";
        public static String STOP = BASEDVD + "DV0F88";
        public static String PLUS = BASEDVD + "TS08";
        public static String MINUS = BASEDVD + "TS09";
        public static String SPEAKER_MUTE = BASEDVD + "TS10";
        public static String SPEAKER_UNMUTE = BASEDVD + "TS11";

        public static String DVD_OFF_V2 = BASEDVD_V2 + "TS00";
        public static String BUTTON_RED_V2 = BASEDVD + "DV0148";
        public static String BUTTON_GREEN_V2 = BASEDVD_V2 + "DV0948";
        public static String BUTTON_YELLOW_V2 = BASEDVD_V2 + "DV0548";
        public static String BUTTON_PURPLE_V2 = BASEDVD_V2 + "DV0D48";
        public static String BUTTON_ARROW_UP_V2 = BASEDVD_V2 + "DV0908";
        public static String BUTTON_ARROW_LEFT_V2 = BASEDVD_V2 + "DV0D08";
        public static String BUTTON_SQUARE_V2 = BASEDVD_V2 + "DV0B08";
        public static String BUTTON_ARROW_RIGHT_V2 = BASEDVD_V2 + "DV0308";
        public static String BUTTON_ARROW_DOWN_V2 = BASEDVD_V2 + "DV0508";
        public static String RETURN_V2 = BASEDVD_V2 + "DV0D88";
        public static String SUBTITLE_V2 = BASEDVD_V2 + "DV0F08";
        public static String BACKWARD_V2 = BASEDVD_V2 + "DV0388";
        public static String FORWARD_V2 = BASEDVD_V2 + "DV0048";
        public static String PLAY_V2 = BASEDVD_V2 + "DV0B48";
        public static String PREVIOUS_V2 = BASEDVD_V2 + "DV0448";
        public static String NEXT_V2 = BASEDVD_V2 + "DV0848";
        public static String PAUSE_V2 = BASEDVD_V2 + "DV0788";
        public static String STOP_V2 = BASEDVD_V2 + "DV0F88";
        public static String PLUS_V2 = BASEDVD_V2 + "TS08";
        public static String MINUS_V2 = BASEDVD_V2 + "TS09";
        public static String SPEAKER_MUTE_V2 = BASEDVD_V2 + "TS10";
        public static String SPEAKER_UNMUTE_V2 = BASEDVD_V2 + "TS11";


        public static void DVDRemoteButton(RemoteButton button) {

            if (isOldMAS) {
                if (button.equals(RemoteButton.UP)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_UP, 1);
                } else if (button.equals(RemoteButton.LEFT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_LEFT, 1);
                } else if (button.equals(RemoteButton.SQUARE)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_SQUARE, 1);
                } else if (button.equals(RemoteButton.RIGHT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_RIGHT, 1);
                } else if (button.equals(RemoteButton.DOWN)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_DOWN, 1);
                } else if (button.equals(RemoteButton.RED)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_RED, 1);
                } else if (button.equals(RemoteButton.GREEN)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_GREEN, 1);
                } else if (button.equals(RemoteButton.YELLOW)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_YELLOW, 1);
                } else if (button.equals(RemoteButton.PURPLE)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_PURPLE, 1);
                } else if (button.equals(RemoteButton.BACK)) {
                    MainApplication.getMAS().sendMASCmd(RETURN, 1);
                } else if (button.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(DVD_OFF, 1);
                } else if (button.equals(RemoteButton.CC)) {
                    MainApplication.getMAS().sendMASCmd(SUBTITLE, 1);
                } else if (button.equals(RemoteButton.REWIND10)) {
                    MainApplication.getMAS().sendMASCmd(BACKWARD, 1);
                } else if (button.equals(RemoteButton.FASTFORWARD10)) {
                    MainApplication.getMAS().sendMASCmd(FORWARD, 1);
                } else if (button.equals(RemoteButton.PLAY)) {
                    MainApplication.getMAS().sendMASCmd(PLAY, 1);
                } else if (button.equals(RemoteButton.LAST)) {
                    MainApplication.getMAS().sendMASCmd(PREVIOUS, 1);
                } else if (button.equals(RemoteButton.NEXT)) {
                    MainApplication.getMAS().sendMASCmd(NEXT, 1);
                } else if (button.equals(RemoteButton.PAUSE)) {
                    MainApplication.getMAS().sendMASCmd(PAUSE, 1);
                } else if (button.equals(RemoteButton.STOP)) {
                    MainApplication.getMAS().sendMASCmd(STOP, 1);
                } else if (button.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(PLUS, 1);
                } else if (button.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(MINUS, 1);
                } else if (button.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(SPEAKER_MUTE, 1);
                } else if (button.equals(RemoteButton.UNMUTE)) {
                    MainApplication.getMAS().sendMASCmd(SPEAKER_UNMUTE, 1);
                }

            } else {
                if (button.equals(RemoteButton.UP)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_UP_V2, 1);
                } else if (button.equals(RemoteButton.LEFT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_LEFT_V2, 1);
                } else if (button.equals(RemoteButton.SQUARE)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_SQUARE_V2, 1);
                } else if (button.equals(RemoteButton.RIGHT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_RIGHT_V2, 1);
                } else if (button.equals(RemoteButton.DOWN)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_DOWN_V2, 1);
                } else if (button.equals(RemoteButton.RED)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_RED_V2, 1);
                } else if (button.equals(RemoteButton.GREEN)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_GREEN_V2, 1);
                } else if (button.equals(RemoteButton.YELLOW)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_YELLOW_V2, 1);
                } else if (button.equals(RemoteButton.PURPLE)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_PURPLE_V2, 1);
                } else if (button.equals(RemoteButton.BACK)) {
                    MainApplication.getMAS().sendMASCmd(RETURN_V2, 1);
                } else if (button.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(DVD_OFF_V2, 1);
                } else if (button.equals(RemoteButton.CC)) {
                    MainApplication.getMAS().sendMASCmd(SUBTITLE_V2, 1);
                } else if (button.equals(RemoteButton.REWIND10)) {
                    MainApplication.getMAS().sendMASCmd(BACKWARD_V2, 1);
                } else if (button.equals(RemoteButton.FASTFORWARD10)) {
                    MainApplication.getMAS().sendMASCmd(FORWARD_V2, 1);
                } else if (button.equals(RemoteButton.PLAY)) {
                    MainApplication.getMAS().sendMASCmd(PLAY_V2, 1);
                } else if (button.equals(RemoteButton.LAST)) {
                    MainApplication.getMAS().sendMASCmd(PREVIOUS_V2, 1);
                } else if (button.equals(RemoteButton.NEXT)) {
                    MainApplication.getMAS().sendMASCmd(NEXT_V2, 1);
                } else if (button.equals(RemoteButton.PAUSE)) {
                    MainApplication.getMAS().sendMASCmd(PAUSE_V2, 1);
                } else if (button.equals(RemoteButton.STOP)) {
                    MainApplication.getMAS().sendMASCmd(STOP_V2, 1);
                } else if (button.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(PLUS_V2, 1);
                } else if (button.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(MINUS_V2, 1);
                } else if (button.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(SPEAKER_MUTE_V2, 1);
                } else if (button.equals(RemoteButton.UNMUTE)) {
                    MainApplication.getMAS().sendMASCmd(SPEAKER_UNMUTE_V2, 1);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String TOPMENU = BASEDVD + "DV0E28";
        public static String TOPMENU_V2 = BASEDVD_V2 + "DV0E28";

        public static void TopMenu() {

            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(TOPMENU, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(TOPMENU_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String POPUP_MENU = BASEDVD + "DV0E98";
        public static String POPUP_MENU_V2 = BASEDVD_V2 + "DV0E98";

        public static void PopupMenu() {

            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(POPUP_MENU, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(POPUP_MENU_V2, 1);
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String OPTION = BASEDVD + "DV0E38";
        public static String OPTION_V2 = BASEDVD_V2 + "DV0E38";

        public static void Option() {

            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(OPTION, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(OPTION_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String AUDIO = BASEDVD + "DV0648";
        public static String AUDIO_V2 = BASEDVD_V2 + "DV0648";

        public static void Audio() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(AUDIO, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(AUDIO_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String DISPLAY = BASEDVD + "DV0E18";
        public static String DISPLAY_V2 = BASEDVD_V2 + "DV0E18";

        public static void Display() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(DISPLAY, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(DISPLAY_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String HOME = BASEDVD + "DV0E68";
        public static String HOME_V2 = BASEDVD_V2 + "DV0E68";

        public static void Home() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(HOME, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(HOME_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String CARD_MOVIE = BASECARD + "Vid";
        public static String CARD_MOVIE_V2 = BASECARD_V2 + "Vid";

        public static void CardMovie() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(CARD_MOVIE, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(CARD_MOVIE_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String CARD_PHOTO = BASECARD + "Pic";
        public static String CARD_PHOTO_V2 = BASECARD_V2 + "Pic";

        public static void CardPhoto() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(CARD_PHOTO, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(CARD_PHOTO_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String CARD_MUSIC = BASECARD + "Mus";
        public static String CARD_MUSIC_V2 = BASECARD_V2 + "Mus";

        public static void CardMusic() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(CARD_MUSIC, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(CARD_MUSIC_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String CARD_ROTATE = BASECARD + "Okx";
        public static String CARD_ROTATE_V2 = BASECARD_V2 + "Okx";

        public static void CardRotate() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(CARD_ROTATE, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(CARD_ROTATE_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String BUTTON_CARD_ARROW_UP = BASECARD + "Upx";
        public static String BUTTON_CARD_ARROW_LEFT = BASECARD + "Lef";
        public static String BUTTON_CARD_SQUARE = BASECARD + "Okx";
        public static String BUTTON_CARD_ARROW_RIGHT = BASECARD + "Rig";
        public static String BUTTON_CARD_ARROW_DOWN = BASECARD + "Dow";
        public static String CARD_PLAY_PAUSE = BASECARD + "PnP";
        public static String CARD_STOP = BASECARD + "Stp";
        public static String CARD_PLUS = BASECARD + "TS08";
        public static String CARD_MINUS = BASECARD + "TS09";
        public static String CARD_BACKWARD = BASECARD + "Rew";
        public static String CARD_FORWARD = BASECARD + "FFw";
        public static String CARD_PREVIOUS = BASECARD + "Pre";
        public static String CARD_NEXT = BASECARD + "Nex";
        public static String CARD_MENU = BASECARD + "Hom";
        public static String CARD_BACK = BASECARD + "Nex";
        public static String CARD_MUTE = BASEDVD + "TS10";
        public static String CARD_UNMUTE = BASEDVD + "TS11";


        public static String BUTTON_CARD_ARROW_UP_V2 = BASECARD_V2 + "Upx";
        public static String BUTTON_CARD_ARROW_LEFT_V2 = BASECARD_V2 + "Lef";
        public static String BUTTON_CARD_SQUARE_V2 = BASECARD_V2 + "Okx";
        public static String BUTTON_CARD_ARROW_RIGHT_V2 = BASECARD_V2 + "Rig";
        public static String BUTTON_CARD_ARROW_DOWN_V2 = BASECARD_V2 + "Dow";
        public static String CARD_PLAY_PAUSE_V2 = BASECARD_V2 + "PnP";
        public static String CARD_STOP_V2 = BASECARD_V2 + "Stp";
        public static String CARD_PLUS_V2 = BASECARD_V2 + "TS08";
        public static String CARD_MINUS_V2 = BASECARD_V2 + "TS09";
        public static String CARD_BACKWARD_V2 = BASECARD_V2 + "Rew";
        public static String CARD_FORWARD_V2 = BASECARD_V2 + "FFw";
        public static String CARD_PREVIOUS_V2 = BASECARD_V2 + "Pre";
        public static String CARD_NEXT_V2 = BASECARD_V2 + "Nex";
        public static String CARD_MENU_V2 = BASECARD_V2 + "Hom";
        public static String CARD_BACK_V2 = BASECARD_V2 + "Nex";
        public static String CARD_MUTE_V2 = BASEDVD + "TS10";
        public static String CARD_UNMUTE_V2 = BASEDVD + "TS11";

        public static void CardRemoteButton(RemoteButton button) {

            if (isOldMAS) {
                if (button.equals(RemoteButton.UP)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_CARD_ARROW_UP, 1);
                } else if (button.equals(RemoteButton.LEFT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_CARD_ARROW_LEFT, 1);
                } else if (button.equals(RemoteButton.SQUARE)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_CARD_SQUARE, 1);
                } else if (button.equals(RemoteButton.RIGHT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_CARD_ARROW_RIGHT, 1);
                } else if (button.equals(RemoteButton.DOWN)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_CARD_ARROW_DOWN, 1);
                } else if (button.equals(RemoteButton.PLAYPAUSE)) {
                    MainApplication.getMAS().sendMASCmd(CARD_PLAY_PAUSE, 1);
                } else if (button.equals(RemoteButton.STOP)) {
                    MainApplication.getMAS().sendMASCmd(CARD_STOP, 1);
                } else if (button.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(CARD_PLUS, 1);
                } else if (button.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(CARD_MINUS, 1);
                } else if (button.equals(RemoteButton.REWIND10)) {
                    MainApplication.getMAS().sendMASCmd(CARD_BACKWARD, 1);
                } else if (button.equals(RemoteButton.FASTFORWARD10)) {
                    MainApplication.getMAS().sendMASCmd(CARD_FORWARD, 1);
                } else if (button.equals(RemoteButton.LAST)) {
                    MainApplication.getMAS().sendMASCmd(CARD_PREVIOUS, 1);
                } else if (button.equals(RemoteButton.NEXT)) {
                    MainApplication.getMAS().sendMASCmd(CARD_NEXT, 1);
                } else if (button.equals(RemoteButton.MENU)) {
                    MainApplication.getMAS().sendMASCmd(CARD_MENU, 1);
                } else if (button.equals(RemoteButton.BACK)) {
                    MainApplication.getMAS().sendMASCmd(CARD_BACK, 1);
                } else if (button.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(CARD_MUTE, 1);
                } else if (button.equals(RemoteButton.UNMUTE)) {
                    MainApplication.getMAS().sendMASCmd(CARD_UNMUTE, 1);
                }
            } else {
                if (button.equals(RemoteButton.UP)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_CARD_ARROW_UP_V2, 1);
                } else if (button.equals(RemoteButton.LEFT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_CARD_ARROW_LEFT_V2, 1);
                } else if (button.equals(RemoteButton.SQUARE)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_CARD_SQUARE_V2, 1);
                } else if (button.equals(RemoteButton.RIGHT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_CARD_ARROW_RIGHT_V2, 1);
                } else if (button.equals(RemoteButton.DOWN)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_CARD_ARROW_DOWN_V2, 1);
                } else if (button.equals(RemoteButton.PLAYPAUSE)) {
                    MainApplication.getMAS().sendMASCmd(CARD_PLAY_PAUSE_V2, 1);
                } else if (button.equals(RemoteButton.STOP)) {
                    MainApplication.getMAS().sendMASCmd(CARD_STOP_V2, 1);
                } else if (button.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(CARD_PLUS_V2, 1);
                } else if (button.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(CARD_MINUS_V2, 1);
                } else if (button.equals(RemoteButton.REWIND10)) {
                    MainApplication.getMAS().sendMASCmd(CARD_BACKWARD_V2, 1);
                } else if (button.equals(RemoteButton.FASTFORWARD10)) {
                    MainApplication.getMAS().sendMASCmd(CARD_FORWARD_V2, 1);
                } else if (button.equals(RemoteButton.LAST)) {
                    MainApplication.getMAS().sendMASCmd(CARD_PREVIOUS_V2, 1);
                } else if (button.equals(RemoteButton.NEXT)) {
                    MainApplication.getMAS().sendMASCmd(CARD_NEXT_V2, 1);
                } else if (button.equals(RemoteButton.MENU)) {
                    MainApplication.getMAS().sendMASCmd(CARD_MENU_V2, 1);
                } else if (button.equals(RemoteButton.BACK)) {
                    MainApplication.getMAS().sendMASCmd(CARD_BACK_V2, 1);
                } else if (button.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(CARD_MUTE_V2, 1);
                } else if (button.equals(RemoteButton.UNMUTE)) {
                    MainApplication.getMAS().sendMASCmd(CARD_UNMUTE_V2, 1);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////


        public static String EXTERNAL_PLUS = BASEDVD + "TS08";
        public static String EXTERNAL_MINUS = BASEDVD + "TS09";
        public static String EXTERNAL_MUTE = BASEDVD + "TS10";
        public static String EXTERNAL_UNMUTE = BASEDVD + "TS11";

        public static String EXTERNAL_PLUS_V2 = BASEDVD + "TS08";
        public static String EXTERNAL_MINUS_V2 = BASEDVD + "TS09";
        public static String EXTERNAL_MUTE_V2 = BASEDVD + "TS10";
        public static String EXTERNAL_UNMUTE_V2 = BASEDVD + "TS11";

        public static void ExteralRemoteButton(RemoteButton button) {

            if (isOldMAS) {

                if (button.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(EXTERNAL_PLUS, 1);
                } else if (button.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(EXTERNAL_MINUS, 1);
                } else if (button.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(EXTERNAL_MUTE, 1);
                } else if (button.equals(RemoteButton.UNMUTE)) {
                    MainApplication.getMAS().sendMASCmd(EXTERNAL_UNMUTE, 1);
                }
            } else {
                if (button.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(EXTERNAL_PLUS_V2, 1);
                } else if (button.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(EXTERNAL_MINUS_V2, 1);
                } else if (button.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(EXTERNAL_MUTE_V2, 1);
                } else if (button.equals(RemoteButton.UNMUTE)) {
                    MainApplication.getMAS().sendMASCmd(EXTERNAL_UNMUTE_V2, 1);
                }
            }
        }


        public static String SMART_PLUS = BASEDVD + "TS08";
        public static String SMART_MINUS = BASEDVD + "TS09";
        public static String SMART_MUTE = BASEDVD + "TS10";
        public static String SMART_UNMUTE = BASEDVD + "TS11";

        public static String SMART_PLUS_V2 = BASEDVD + "TS08";
        public static String SMART_MINUS_V2 = BASEDVD + "TS09";
        public static String SMART_MUTE_V2 = BASEDVD + "TS10";
        public static String SMART_UNMUTE_V2 = BASEDVD + "TS11";

        public static void SmartRemoteButton(RemoteButton button) {

            if (isOldMAS) {

                if (button.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(SMART_PLUS, 1);
                } else if (button.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(SMART_MINUS, 1);
                } else if (button.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(SMART_MUTE, 1);
                } else if (button.equals(RemoteButton.UNMUTE)) {
                    MainApplication.getMAS().sendMASCmd(SMART_UNMUTE, 1);
                }
            } else {
                if (button.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(SMART_PLUS_V2, 1);
                } else if (button.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(SMART_MINUS_V2, 1);
                } else if (button.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(SMART_MUTE_V2, 1);
                } else if (button.equals(RemoteButton.UNMUTE)) {
                    MainApplication.getMAS().sendMASCmd(SMART_UNMUTE_V2, 1);
                }
            }
        }
    }


    public static class Movie {

        public static String AVOFF = "SET%20AVOFF";
        public static String AVOFF_V2 = "SET%20AVOFF";

        public static void AVOff() {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(AVOFF, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(AVOFF_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String BACK = "SET%20IRC%20TM1B";
        public static String OK = "SET%20IRC%20TM1A";
        public static String BUTTON_ARROW_UP = "SET%20IRC%20TM17";
        public static String BUTTON_ARROW_LEFT = "SET%20IRC%20TM16";
        public static String BUTTON_ARROW_RIGHT = "SET%20IRC%20TM18";
        public static String BUTTON_ARROW_DOWN = "SET%20IRC%20TM19";
        public static String CHANNEL_PLUS = "SET%20IRC%20TM0F";
        public static String CHANNEL_MINUS = "SET%20IRC%20TM11";
        public static String SOUND_PLUS = "SET%20RADIOVOL%20UP";
        public static String SOUND_MINUS = "SET%20RADIOVOL%20DOWN";
        public static String SOUND_MUTE = "SET%20RADIOVOL%20MUTE";
        public static String PLAYPAUSE = "SET%20IRC%20TM1A";
        public static String STOP = "SET%20IRC%20TM1B";
        public static String REWIND10 = "SET%20IRC%20TM16";
        public static String FASTFORWARD10 = "SET%20IRC%20TM18";
        public static String REWIND60 = "SET%20IRC%20TM19";
        public static String FASTFORWARD60 = "SET%20IRC%20TM17";

        public static String BACK_V2 = "SET%20IRC%20TM1B";
        public static String OK_V2 = "SET%20IRC%20TM1A";
        public static String BUTTON_ARROW_UP_V2 = "SET%20IRC%20TM17";
        public static String BUTTON_ARROW_LEFT_V2 = "SET%20IRC%20TM16";
        public static String BUTTON_ARROW_RIGHT_V2 = "SET%20IRC%20TM18";
        public static String BUTTON_ARROW_DOWN_V2 = "SET%20IRC%20TM19";
        public static String CHANNEL_PLUS_V2 = "SET%20IRC%20TM0F";
        public static String CHANNEL_MINUS_V2 = "SET%20IRC%20TM11";
        public static String SOUND_PLUS_V2 = "SET%20RADIOVOL%20UP";
        public static String SOUND_MINUS_V2 = "SET%20RADIOVOL%20DOWN";
        public static String SOUND_MUTE_V2 = "SET%20RADIOVOL%20MUTE";
        public static String PLAYPAUSE_V2 = "SET%20IRC%20TM1A";
        public static String STOP_V2 = "SET%20IRC%20TM1B";
        public static String REWIND10_V2 = "SET%20IRC%20TM16";
        public static String FASTFORWARD10_V2 = "SET%20IRC%20TM18";
        public static String REWIND60_V2 = "SET%20IRC%20TM19";
        public static String FASTFORWARD60_V2 = "SET%20IRC%20TM17";

        public static void RemoteButton(RemoteButton remoteButton) {

            if (isOldMAS) {
                if (remoteButton.equals(RemoteButton.UP)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_UP, 1);
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_LEFT, 1);
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_RIGHT, 1);
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_DOWN, 1);
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    MainApplication.getMAS().sendMASCmd(OK, 1);
                } else if (remoteButton.equals(RemoteButton.BACK)) {
                    MainApplication.getMAS().sendMASCmd(BACK, 1);
                } else if (remoteButton.equals(RemoteButton.CHANNELUP)) {
                    MainApplication.getMAS().sendMASCmd(CHANNEL_PLUS, 1);
                } else if (remoteButton.equals(RemoteButton.CHANNELDOWN)) {
                    MainApplication.getMAS().sendMASCmd(CHANNEL_MINUS, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(SOUND_PLUS, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(SOUND_MINUS, 1);
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(SOUND_MUTE, 1);
                } else if (remoteButton.equals(RemoteButton.PLAYPAUSE)) {
                    MainApplication.getMAS().sendMASCmd(PLAYPAUSE, 1);
                } else if (remoteButton.equals(RemoteButton.STOP)) {
                    MainApplication.getMAS().sendMASCmd(STOP, 1);
                } else if (remoteButton.equals(RemoteButton.REWIND10)) {
                    MainApplication.getMAS().sendMASCmd(REWIND10, 1);
                } else if (remoteButton.equals(RemoteButton.FASTFORWARD10)) {
                    MainApplication.getMAS().sendMASCmd(FASTFORWARD10, 1);
                } else if (remoteButton.equals(RemoteButton.REWIND60)) {
                    MainApplication.getMAS().sendMASCmd(REWIND60, 1);
                } else if (remoteButton.equals(RemoteButton.FASTFORWARD60)) {
                    MainApplication.getMAS().sendMASCmd(FASTFORWARD60, 1);
                }
            } else {
                if (remoteButton.equals(RemoteButton.UP)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_UP_V2, 1);
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_LEFT_V2, 1);
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_RIGHT_V2, 1);
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    MainApplication.getMAS().sendMASCmd(BUTTON_ARROW_DOWN_V2, 1);
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    MainApplication.getMAS().sendMASCmd(OK_V2, 1);
                } else if (remoteButton.equals(RemoteButton.BACK)) {
                    MainApplication.getMAS().sendMASCmd(BACK_V2, 1);
                } else if (remoteButton.equals(RemoteButton.CHANNELUP)) {
                    MainApplication.getMAS().sendMASCmd(CHANNEL_PLUS_V2, 1);
                } else if (remoteButton.equals(RemoteButton.CHANNELDOWN)) {
                    MainApplication.getMAS().sendMASCmd(CHANNEL_MINUS_V2, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(SOUND_PLUS_V2, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(SOUND_MINUS_V2, 1);
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(SOUND_MUTE_V2, 1);
                } else if (remoteButton.equals(RemoteButton.PLAYPAUSE)) {
                    MainApplication.getMAS().sendMASCmd(PLAYPAUSE_V2, 1);
                } else if (remoteButton.equals(RemoteButton.STOP)) {
                    MainApplication.getMAS().sendMASCmd(STOP_V2, 1);
                } else if (remoteButton.equals(RemoteButton.REWIND10)) {
                    MainApplication.getMAS().sendMASCmd(REWIND10_V2, 1);
                } else if (remoteButton.equals(RemoteButton.FASTFORWARD10)) {
                    MainApplication.getMAS().sendMASCmd(FASTFORWARD10_V2, 1);
                } else if (remoteButton.equals(RemoteButton.REWIND60)) {
                    MainApplication.getMAS().sendMASCmd(REWIND60_V2, 1);
                } else if (remoteButton.equals(RemoteButton.FASTFORWARD60)) {
                    MainApplication.getMAS().sendMASCmd(FASTFORWARD60_V2, 1);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String WATCH = "SET%20IRC%20TC4D";
        public static String WATCH_V2 = "SET%20IRC%20TC4D";

        public static void Watch(String currentChoice) {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(WATCH + currentChoice, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(WATCH_V2 + currentChoice, 1);
            }
        }
    }


    public static class Music {

        public static String AVOFF = "SET%20AVOFF";
        public static String VOLUMN_UP = "SET%20RADIOVOL%20UP";
        public static String VOLUMN_DOWN = "SET%20RADIOVOL%20DOWN";


        public static String AVOFF_V2 = "SET%20AVOFF";
        public static String VOLUMN_UP_V2 = "SET%20RADIOVOL%20UP";
        public static String VOLUMN_DOWN_V2 = "SET%20RADIOVOL%20DOWN";

        public static void RemoteButton(RemoteButton remoteButton) {

            if (isOldMAS) {
                if (remoteButton.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(AVOFF, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMN_UP, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMN_DOWN, 1);
                }
            } else {
                if (remoteButton.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(AVOFF_V2, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMN_UP_V2, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMN_DOWN_V2, 1);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String MUSICMODE = "SET%20RADIOMODE";
        public static String MUSICMODE_V2 = "SET%20RADIOMODE";

        public static void MusicMode() {

            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(MUSICMODE, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(MUSICMODE_V2, 1);
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String MUSICCHANNEL = "SET%20RADIO%20";
        public static String MUSICCHANNEL_V2 = "SET%20RADIO%20";

        public static void MusicChannel(String channel) {

            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(MUSICCHANNEL + channel, 0);
            } else {
                MainApplication.getMAS().sendMASCmd(MUSICCHANNEL_V2 + channel, 0);
            }
        }
    }


    public static class IBahnMovie {

        public static String AVON = "SET%20TVMODE";
        public static String AVOFF = "SET%20AVOFF";
        public static String VOLUMNUP = "SET%20RADIOVOL%20UP";
        public static String VOLUMNDOWN = "SET%20RADIOVOL%20DOWN";
        public static String MUTE = "SET%20RADIOVOL%20MUTE";

        public static String AVON_V2 = "SET%20TVMODE";
        public static String AVOFF_V2 = "SET%20AVOFF";
        public static String VOLUMNUP_V2 = "SET%20RADIOVOL%20UP";
        public static String VOLUMNDOWN_V2 = "SET%20RADIOVOL%20DOWN";
        public static String MUTE_V2 = "SET%20RADIOVOL%20MUTE";


        public static void RemoteButton(RemoteButton remoteButton, String room, String box, String token) {

            if (isOldMAS) {
                if (remoteButton.equals(RemoteButton.ON)) {
                    MainApplication.getMAS().sendMASCmd(AVON, 1);
                } else if (remoteButton.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(AVOFF, 1);
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_ENTER, null);
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMNUP, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMNDOWN, 1);
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(MUTE, 1);
                } else if (remoteButton.equals(RemoteButton.PLAY)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_PLAY, null);
                } else if (remoteButton.equals(RemoteButton.PAUSE)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_PAUSE, null);
                } else if (remoteButton.equals(RemoteButton.STOP)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_STOP, null);
                } else if (remoteButton.equals(RemoteButton.REWIND10)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_RW, null);
                } else if (remoteButton.equals(RemoteButton.FASTFORWARD10)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_FF, null);
                } else if (remoteButton.equals(RemoteButton.SKIP_BACK)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_SKIPB, null);
                } else if (remoteButton.equals(RemoteButton.SKIP_FORWARD)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_SKIPF, null);
                } else if (remoteButton.equals(RemoteButton.UP)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_UP, null);
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_LEFT, null);
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_RIGHT, null);
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_DOWN, null);
                }
            } else {
                if (remoteButton.equals(RemoteButton.ON)) {
                    MainApplication.getMAS().sendMASCmd(AVON_V2, 1);
                } else if (remoteButton.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(AVOFF_V2, 1);
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_ENTER, null);
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMNUP_V2, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMNDOWN_V2, 1);
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(MUTE_V2, 1);
                } else if (remoteButton.equals(RemoteButton.PLAY)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_PLAY, null);
                } else if (remoteButton.equals(RemoteButton.PAUSE)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_PAUSE, null);
                } else if (remoteButton.equals(RemoteButton.STOP)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_STOP, null);
                } else if (remoteButton.equals(RemoteButton.REWIND10)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_RW, null);
                } else if (remoteButton.equals(RemoteButton.FASTFORWARD10)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_FF, null);
                } else if (remoteButton.equals(RemoteButton.SKIP_BACK)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_SKIPB, null);
                } else if (remoteButton.equals(RemoteButton.SKIP_FORWARD)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_SKIPF, null);
                } else if (remoteButton.equals(RemoteButton.UP)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_UP, null);
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_LEFT, null);
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_RIGHT, null);
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_DOWN, null);
                }
            }
        }
    }

    public static class IBahnTV {

        public static String AVON = "SET%20TVMODE";
        public static String AVOFF = "SET%20AVOFF";
        public static String VOLUMNUP = "SET%20TVVOL%20UP";
        public static String VOLUMNDOWN = "SET%20TVVOL%20DOWN";
        public static String MUTE = "SET%20TVVOL%20MUTE";

        public static String AVON_V2 = "SET%20TVMODE";
        public static String AVOFF_V2 = "SET%20AVOFF";
        public static String VOLUMNUP_V2 = "SET%20TVVOL%20UP";
        public static String VOLUMNDOWN_V2 = "SET%20TVVOL%20DOWN";
        public static String MUTE_V2 = "SET%20TVVOL%20MUTE";


        public static void RemoteButton(RemoteButton remoteButton, String room, String box, String token) {

            if (isOldMAS) {
                if (remoteButton.equals(RemoteButton.ON)) {
                    MainApplication.getMAS().sendMASCmd(AVON, 1);
                } else if (remoteButton.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(AVOFF, 1);
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_ENTER, null);
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMNUP, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMNDOWN, 1);
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(MUTE, 1);
                } else if (remoteButton.equals(RemoteButton.RED)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_RED, null);
                } else if (remoteButton.equals(RemoteButton.GREEN)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_GREEN, null);
                } else if (remoteButton.equals(RemoteButton.YELLOW)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_YELLOW, null);
                } else if (remoteButton.equals(RemoteButton.CHANNELUP)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_CHUP, null);
                } else if (remoteButton.equals(RemoteButton.CHANNELDOWN)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_CHDOWN, null);
                } else if (remoteButton.equals(RemoteButton.UP)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_UP, null);
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_LEFT, null);
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_RIGHT, null);
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_DOWN, null);
                }
            } else {
                if (remoteButton.equals(RemoteButton.ON)) {
                    MainApplication.getMAS().sendMASCmd(AVON_V2, 1);
                } else if (remoteButton.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(AVOFF_V2, 1);
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_ENTER, null);
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMNUP_V2, 1);
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    MainApplication.getMAS().sendMASCmd(VOLUMNDOWN_V2, 1);
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    MainApplication.getMAS().sendMASCmd(MUTE_V2, 1);
                } else if (remoteButton.equals(RemoteButton.PLAY)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_PLAY, null);
                } else if (remoteButton.equals(RemoteButton.PAUSE)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_PAUSE, null);
                } else if (remoteButton.equals(RemoteButton.STOP)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_STOP, null);
                } else if (remoteButton.equals(RemoteButton.REWIND10)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_RW, null);
                } else if (remoteButton.equals(RemoteButton.FASTFORWARD10)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_FF, null);
                } else if (remoteButton.equals(RemoteButton.SKIP_BACK)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_SKIPB, null);
                } else if (remoteButton.equals(RemoteButton.SKIP_FORWARD)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_TP_SKIPF, null);
                } else if (remoteButton.equals(RemoteButton.UP)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_UP, null);
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_LEFT, null);
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_RIGHT, null);
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    new IBahnComm().remoteButton(room, box, token, IBahnComm.ButtonCode.KEY_DOWN, null);
                }
            }
        }


    }


    public static class TV {

        public static String AVON = "SET%20TVMODE";
        public static String AVOFF = "SET%20AVOFF";
        public static String UP = "SET%20IRC%20TM17";
        public static String UP_PCH_PNY = "SET%20IRC%20TV15";
        public static String DOWN = "SET%20IRC%20TM19";
        public static String DOWN_PCH_PNY = "SET%20IRC%20TV19";
        public static String LEFT = "SET%20IRC%20TM16";
        public static String LEFT_PCH_PNY = "SET%20IRC%20TV16";
        public static String RIGHT = "SET%20IRC%20TM18";
        public static String RIGHT_PCH_PNY = "SET%20IRC%20TV18";
        public static String BACK = "SET%20IRC%20TM1B";
        public static String BACK_PCH_PNY = "SET%20IRC%20TV0B";
        public static String MOVIE = "SET%20DVD%20TS02";
        public static String MOVIE_PCH_PNY = "SET%20IRC%20TV29";
        public static String CC = "SET%20IRC%20TV28";
        public static String MENU = "SET%20IRC%20TV12";
        public static String OK = "SET%20IRC%20TM1A";
        public static String OK_PCH_PNY = "SET%20IRC%20TV17";
        public static String CHANNELUP = "SET%20IRC%20TM0F";
        public static String CHANNELUP_PCH_PNY = "SET%20IRC%20TV11";
        public static String CHANNELDOWN = "SET%20IRC%20TM11";
        public static String CHANNELDOWN_PCH_PNY = "SET%20IRC%20TV14";
        public static String VOLUMNUP = "SET%20RADIOVOL%20UP";
        public static String VOLUMNUP_PCH_PNY = "SET%20IRC%20TS08";
        public static String VOLUMNDOWN = "SET%20RADIOVOL%20DOWN";
        public static String VOLUMNDOWN_PCH_PNY = "SET%20IRC%20TS09";
        public static String MUTE = "SET%20RADIOVOL%20MUTE";
        public static String MUTE_PCH_PNY = "SET%20IRC%20TS10";
        public static String PLAYPAUSE = "SET%20IRC%20TV1F";
        public static String STOP = "SET%20IRC%20TV25";
        public static String REWIND = "SET%20IRC%20TV1E";
        public static String FORWARD = "SET%20IRC%20TV21";
        public static String LAST = "SET%20IRC%20TV23";
        public static String NEXT = "SET%20IRC%20TV24";

        public static String AVON_V2 = "SET%20TVMODE";
        public static String AVOFF_V2 = "SET%20AVOFF";
        public static String UP_V2 = "SET%20IRC%20TM17";
        public static String UP_PCH_PNY_V2 = "SET%20IRC%20TV15";
        public static String DOWN_V2 = "SET%20IRC%20TM19";
        public static String DOWN_PCH_PNY_V2 = "SET%20IRC%20TV19";
        public static String LEFT_V2 = "SET%20IRC%20TM16";
        public static String LEFT_PCH_PNY_V2 = "SET%20IRC%20TV16";
        public static String RIGHT_V2 = "SET%20IRC%20TM18";
        public static String RIGHT_PCH_PNY_V2 = "SET%20IRC%20TV18";
        public static String BACK_V2 = "SET%20IRC%20TM1B";
        public static String BACK_PCH_PNY_V2 = "SET%20IRC%20TV0B";
        public static String MOVIE_V2 = "SET%20DVD%20TS02";
        public static String MOVIE_PCH_PNY_V2 = "SET%20IRC%20TV29";
        public static String CC_V2 = "SET%20IRC%20TV28";
        public static String MENU_V2 = "SET%20IRC%20TV12";
        public static String OK_V2 = "SET%20IRC%20TM1A";
        public static String OK_PCH_PNY_V2 = "SET%20IRC%20TM1A";
        public static String CHANNELUP_V2 = "SET%20IRC%20TM0F";
        public static String CHANNELUP_PCH_PNY_V2 = "SET%20IRC%20TV11";
        public static String CHANNELDOWN_V2 = "SET%20IRC%20TM11";
        public static String CHANNELDOWN_PCH_PNY_V2 = "SET%20IRC%20TV14";
        public static String VOLUMNUP_V2 = "SET%20RADIOVOL%20UP";
        public static String VOLUMNUP_PCH_PNY_V2 = "SET%20IRC%20TS08";
        public static String VOLUMNDOWN_V2 = "SET%20RADIOVOL%20DOWN";
        public static String VOLUMNDOWN_PCH_PNY_V2 = "SET%20IRC%20TS09";
        public static String MUTE_V2 = "SET%20RADIOVOL%20MUTE";
        public static String MUTE_PCH_PNY_V2 = "SET%20IRC%20TS10";
        public static String PLAYPAUSE_V2 = "SET%20IRC%20TV1F";
        public static String STOP_V2 = "SET%20IRC%20TV25";
        public static String REWIND_V2 = "SET%20IRC%20TV1E";
        public static String FORWARD_V2 = "SET%20IRC%20TV21";
        public static String LAST_V2 = "SET%20IRC%20TV23";
        public static String NEXT_V2 = "SET%20IRC%20TV24";

        public static void RemoteButton(RemoteButton remoteButton) {

            if (isOldMAS) {
                if (remoteButton.equals(RemoteButton.ON)) {
                    MainApplication.getMAS().sendMASCmd(AVON, 1);
                } else if (remoteButton.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(AVOFF, 1);
                } else if (remoteButton.equals(RemoteButton.UP)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(UP_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(UP, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(DOWN_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(DOWN, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(LEFT_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(LEFT, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(RIGHT_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(RIGHT, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.BACK)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(BACK_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(BACK, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.MOVIE)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(MOVIE_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(MOVIE, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.CC)) {
                    MainApplication.getMAS().sendMASCmd(CC, 1);
                } else if (remoteButton.equals(RemoteButton.MENU)) {
                    MainApplication.getMAS().sendMASCmd(MENU, 1);
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(OK_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(OK, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.CHANNELUP)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(CHANNELUP_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(CHANNELUP, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.CHANNELDOWN)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(CHANNELDOWN_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(CHANNELDOWN, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(VOLUMNUP_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(VOLUMNUP, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(VOLUMNDOWN_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(VOLUMNDOWN, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(MUTE_PCH_PNY, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(MUTE, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.PLAYPAUSE)) {
                    MainApplication.getMAS().sendMASCmd(PLAYPAUSE, 1);
                } else if (remoteButton.equals(RemoteButton.STOP)) {
                    MainApplication.getMAS().sendMASCmd(STOP, 1);
                } else if (remoteButton.equals(RemoteButton.REWIND10)) {
                    MainApplication.getMAS().sendMASCmd(REWIND, 1);
                } else if (remoteButton.equals(RemoteButton.FASTFORWARD10)) {
                    MainApplication.getMAS().sendMASCmd(FORWARD, 1);
                } else if (remoteButton.equals(RemoteButton.LAST)) {
                    MainApplication.getMAS().sendMASCmd(LAST, 1);
                } else if (remoteButton.equals(RemoteButton.NEXT)) {
                    MainApplication.getMAS().sendMASCmd(NEXT, 1);
                }
            } else {
                if (remoteButton.equals(RemoteButton.ON)) {
                    MainApplication.getMAS().sendMASCmd(AVON_V2, 1);
                } else if (remoteButton.equals(RemoteButton.OFF)) {
                    MainApplication.getMAS().sendMASCmd(AVOFF_V2, 1);
                } else if (remoteButton.equals(RemoteButton.UP)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(UP_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(UP_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(DOWN_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(DOWN_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(LEFT_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(LEFT_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(RIGHT_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(RIGHT_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.BACK)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(BACK_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(BACK_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.MOVIE)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(MOVIE_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(MOVIE_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.CC)) {
                    MainApplication.getMAS().sendMASCmd(CC_V2, 1);
                } else if (remoteButton.equals(RemoteButton.MENU)) {
                    MainApplication.getMAS().sendMASCmd(MENU_V2, 1);
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(OK_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(OK_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.CHANNELUP)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(CHANNELUP_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(CHANNELUP_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.CHANNELDOWN)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(CHANNELDOWN_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(CHANNELDOWN_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(VOLUMNUP_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(VOLUMNUP_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(VOLUMNDOWN_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(VOLUMNDOWN_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                        MainApplication.getMAS().sendMASCmd(MUTE_PCH_PNY_V2, 1);
                    } else {
                        MainApplication.getMAS().sendMASCmd(MUTE_V2, 1);
                    }
                } else if (remoteButton.equals(RemoteButton.PLAYPAUSE)) {
                    MainApplication.getMAS().sendMASCmd(PLAYPAUSE_V2, 1);
                } else if (remoteButton.equals(RemoteButton.STOP)) {
                    MainApplication.getMAS().sendMASCmd(STOP_V2, 1);
                } else if (remoteButton.equals(RemoteButton.REWIND10)) {
                    MainApplication.getMAS().sendMASCmd(REWIND_V2, 1);
                } else if (remoteButton.equals(RemoteButton.FASTFORWARD10)) {
                    MainApplication.getMAS().sendMASCmd(FORWARD_V2, 1);
                } else if (remoteButton.equals(RemoteButton.LAST)) {
                    MainApplication.getMAS().sendMASCmd(LAST_V2, 1);
                } else if (remoteButton.equals(RemoteButton.NEXT)) {
                    MainApplication.getMAS().sendMASCmd(NEXT_V2, 1);
                }
            }
        }

    }


    public static class TVApp {

        public static String IPTV = "iptvpoc";
        public static String NETFLIX = "netflix";
        public static String SPOTIFY = "spotify";
        public static String YOUTUBE = "youtube";
        public static String GOOGLEPLAY = "googleplaymovies";
        public static String TED = "ted";
        public static String WEATHER = "peninsulaservice";

        public static String IPTV_V2 = "iptvpoc";
        public static String NETFLIX_V2 = "netflix";
        public static String SPOTIFY_V2 = "spotify";
        public static String YOUTUBE_V2 = "youtube";
        public static String GOOGLEPLAY_V2 = "googleplaymovies";
        public static String TED_V2 = "ted";
        public static String WEATHER_V2 = "peninsulaservice";

        public static void Item(STBItem item) {
            if (isOldMAS) {
                if (item.equals(STBItem.AIRPLAY)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/tv/KEY_HDMI2");
                } else if (item.equals(STBItem.CHORMECAST)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/tv/KEY_HDMI3");
                } else if (item.equals(STBItem.IPTV)) {
                    sendSTBAppCommand(IPTV);
                } else if (item.equals(STBItem.NETFLIX)) {
                    sendSTBAppCommand(NETFLIX);
                } else if (item.equals(STBItem.SPOTIFY)) {
                    sendSTBAppCommand(SPOTIFY);
                } else if (item.equals(STBItem.YOUTUBE)) {
                    sendSTBAppCommand(YOUTUBE);
                } else if (item.equals(STBItem.GOOGLEPLAY)) {
                    sendSTBAppCommand(GOOGLEPLAY);
                } else if (item.equals(STBItem.TED)) {
                    sendSTBAppCommand(TED);
                } else if (item.equals(STBItem.WEATHER)) {
                    sendSTBAppCommand(WEATHER);
                }
            } else {
                if (item.equals(STBItem.IPTV)) {
                    sendSTBAppCommand(IPTV_V2);
                } else if (item.equals(STBItem.NETFLIX)) {
                    sendSTBAppCommand(NETFLIX_V2);
                } else if (item.equals(STBItem.SPOTIFY)) {
                    sendSTBAppCommand(SPOTIFY_V2);
                } else if (item.equals(STBItem.YOUTUBE)) {
                    sendSTBAppCommand(YOUTUBE_V2);
                } else if (item.equals(STBItem.GOOGLEPLAY)) {
                    sendSTBAppCommand(GOOGLEPLAY_V2);
                } else if (item.equals(STBItem.TED)) {
                    sendSTBAppCommand(TED_V2);
                } else if (item.equals(STBItem.WEATHER)) {
                    sendSTBAppCommand(WEATHER_V2);
                }
            }
        }


        public static void RemoteButton(RemoteButton remoteButton) {

            if (isOldMAS) {
                if (remoteButton.equals(RemoteButton.ON)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/tv/KEY_POWER_ON");
                } else if (remoteButton.equals(RemoteButton.OFF)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/tv/KEY_POWER_OFF");
                } else if (remoteButton.equals(RemoteButton.UP)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_UP");
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_DOWN");
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_LEFT");
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_RIGHT");
                } else if (remoteButton.equals(RemoteButton.CC)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_HOME");
                } else if (remoteButton.equals(RemoteButton.MENU)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_BACK");
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_OK");
                } else if (remoteButton.equals(RemoteButton.CHANNELUP)) {
                    new TVCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "up");
                } else if (remoteButton.equals(RemoteButton.CHANNELDOWN)) {
                    new TVCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "down");
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_VOL_UP");
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_VOL_DOWN");
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_MUTE");

                }
            } else {
                if (remoteButton.equals(RemoteButton.ON)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/tv/KEY_POWER_ON");
                } else if (remoteButton.equals(RemoteButton.OFF)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/tv/KEY_POWER_OFF");
                } else if (remoteButton.equals(RemoteButton.UP)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_UP");
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_DOWN");
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_LEFT");
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_RIGHT");
                } else if (remoteButton.equals(RemoteButton.CC)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_HOME");
                } else if (remoteButton.equals(RemoteButton.MENU)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_BACK");
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_OK");
                } else if (remoteButton.equals(RemoteButton.CHANNELUP)) {
                    new TVCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "up");
                } else if (remoteButton.equals(RemoteButton.CHANNELDOWN)) {
                    new TVCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "down");
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_VOL_UP");
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_VOL_DOWN");
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_MUTE");

                }
            }
        }

    }

    public static class DemoTV {

        public static String PLAYPAUSE = "SET%20IRC%20TV1F";
        public static String STOP = "SET%20IRC%20TV25";
        public static String REWIND = "SET%20IRC%20TV1E";
        public static String FORWARD = "SET%20IRC%20TV21";
        public static String LAST = "SET%20IRC%20TV23";
        public static String NEXT = "SET%20IRC%20TV24";
        public static String BACK = "SET%20IRC%20TV0B";
        public static String MOVIE = "SET%20DVD%20TS02";;


        public static String PLAYPAUSE_V2 = "SET%20IRC%20TV1F";
        public static String STOP_V2 = "SET%20IRC%20TV25";
        public static String REWIND_V2 = "SET%20IRC%20TV1E";
        public static String FORWARD_V2 = "SET%20IRC%20TV21";
        public static String LAST_V2 = "SET%20IRC%20TV23";
        public static String NEXT_V2 = "SET%20IRC%20TV24";
        public static String BACK_V2 = "SET%20IRC%20TV0B";
        public static String MOVIE_V2 = "SET%20DVD%20TS02";;


        public static void RemoteButton(RemoteButton remoteButton) {

            if (isOldMAS) {
                if (remoteButton.equals(RemoteButton.ON)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/tv/KEY_POWER_ON");
                } else if (remoteButton.equals(RemoteButton.OFF)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/tv/KEY_POWER_OFF");
                } else if (remoteButton.equals(RemoteButton.UP)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_UP");
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_DOWN");
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_LEFT");
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_RIGHT");
                } else if (remoteButton.equals(RemoteButton.PLAYPAUSE)) {
                    MainApplication.getMAS().sendMASCmd(PLAYPAUSE, 1);
                }else if (remoteButton.equals(RemoteButton.STOP)) {
                    MainApplication.getMAS().sendMASCmd(STOP, 1);
                }else if (remoteButton.equals(RemoteButton.MOVIE)) {
                    MainApplication.getMAS().sendMASCmd(MOVIE, 1);
                }else if (remoteButton.equals(RemoteButton.REWIND10)) {
                    MainApplication.getMAS().sendMASCmd(REWIND, 1);
                }else if (remoteButton.equals(RemoteButton.FASTFORWARD10)) {
                    MainApplication.getMAS().sendMASCmd(FORWARD, 1);
                }else if (remoteButton.equals(RemoteButton.LAST)) {
                    MainApplication.getMAS().sendMASCmd(LAST, 1);
                }else if (remoteButton.equals(RemoteButton.NEXT)) {
                    MainApplication.getMAS().sendMASCmd(NEXT, 1);
                }else if (remoteButton.equals(RemoteButton.BACK)) {
                    MainApplication.getMAS().sendMASCmd(BACK, 1);
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_OK");
                } else if (remoteButton.equals(RemoteButton.CHANNELUP)) {
                    new TVCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "up");
                } else if (remoteButton.equals(RemoteButton.CHANNELDOWN)) {
                    new TVCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "down");
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_VOL_UP");
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_VOL_DOWN");
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_MUTE");
                }
            } else {
                if (remoteButton.equals(RemoteButton.ON)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/tv/KEY_POWER_ON");
                } else if (remoteButton.equals(RemoteButton.OFF)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/tv/KEY_POWER_OFF");
                } else if (remoteButton.equals(RemoteButton.UP)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_UP");
                } else if (remoteButton.equals(RemoteButton.DOWN)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_DOWN");
                } else if (remoteButton.equals(RemoteButton.LEFT)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_LEFT");
                } else if (remoteButton.equals(RemoteButton.RIGHT)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_RIGHT");
                } else if (remoteButton.equals(RemoteButton.PLAYPAUSE)) {
                    MainApplication.getMAS().sendMASCmd(PLAYPAUSE_V2, 1);
                }else if (remoteButton.equals(RemoteButton.STOP)) {
                    MainApplication.getMAS().sendMASCmd(STOP_V2, 1);
                }else if (remoteButton.equals(RemoteButton.MOVIE)) {
                    MainApplication.getMAS().sendMASCmd(MOVIE_V2, 1);
                }else if (remoteButton.equals(RemoteButton.REWIND10)) {
                    MainApplication.getMAS().sendMASCmd(REWIND_V2, 1);
                }else if (remoteButton.equals(RemoteButton.FASTFORWARD10)) {
                    MainApplication.getMAS().sendMASCmd(FORWARD_V2, 1);
                }else if (remoteButton.equals(RemoteButton.LAST)) {
                    MainApplication.getMAS().sendMASCmd(LAST_V2, 1);
                }else if (remoteButton.equals(RemoteButton.NEXT)) {
                    MainApplication.getMAS().sendMASCmd(NEXT_V2, 1);
                }else if (remoteButton.equals(RemoteButton.BACK)) {
                    MainApplication.getMAS().sendMASCmd(BACK_V2, 1);
                } else if (remoteButton.equals(RemoteButton.OK)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_OK");
                } else if (remoteButton.equals(RemoteButton.CHANNELUP)) {
                    new TVCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "up");
                } else if (remoteButton.equals(RemoteButton.CHANNELDOWN)) {
                    new TVCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "down");
                } else if (remoteButton.equals(RemoteButton.VOLUMNUP)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_VOL_UP");
                } else if (remoteButton.equals(RemoteButton.VOLUMNDOWN)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_VOL_DOWN");
                } else if (remoteButton.equals(RemoteButton.MUTE)) {
                    new URLCommandSender().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, MainApplication.getContext().getString(R.string.demoDayIRC) + "/stb/KEY_MUTE");
                }
            }
        }

    }


    public static class Language {

        public static String LANGUAGE = "SET%20LANG%20";
        public static String LANGUAGE_V2 = "SET%20LANG%20";

        public static void Language(String language) {
            if (isOldMAS) {
                MainApplication.getMAS().sendMASCmd(LANGUAGE + language, 1);
            } else {
                MainApplication.getMAS().sendMASCmd(LANGUAGE_V2 + language, 1);
            }
        }
    }


    public static class Other {

        public static String VALET_ON = "SET%20VALET%20ON";
        public static String VALET_OFF = "SET%20VALET%20OFF";
        public static String VALET_ON_V2 = "SET%20VALET%20ON";
        public static String VALET_OFF_V2 = "SET%20VALET%20OFF";

        public static void ValetOn() {
            Valet(true);
        }

        public static void ValetOff() {
            Valet(false);
        }

        private static void Valet(boolean on) {

            if (isOldMAS) {
                if (on) {
                    MainApplication.getMAS().sendMASCmd(VALET_ON, 1);
                } else
                    MainApplication.getMAS().sendMASCmd(VALET_OFF, 1);
            } else {
                if (on) {
                    MainApplication.getMAS().sendMASCmd(VALET_ON_V2, 1);
                } else {
                    MainApplication.getMAS().sendMASCmd(VALET_OFF_V2, 1);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String MUR_ON = "SET%20MUR%20ON";
        public static String MUR_OFF = "SET%20MUR%20OFF";
        public static String MUR_ON_V2 = "SET%20MUR%20ON";
        public static String MUR_OFF_V2 = "SET%20MUR%20OFF";

        public static void MUROn() {
            MUR(true);
        }

        public static void MUROff() {
            MUR(false);
        }

        private static void MUR(boolean on) {

            if (isOldMAS) {
                if (on) {
                    MainApplication.getMAS().sendMASCmd(MUR_ON, 1);
                } else
                    MainApplication.getMAS().sendMASCmd(MUR_OFF, 1);
            } else {
                if (on) {
                    MainApplication.getMAS().sendMASCmd(MUR_ON_V2, 1);
                } else {
                    MainApplication.getMAS().sendMASCmd(MUR_OFF_V2, 1);
                }
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////

        public static String DND_ON = "SET%20DND%20ON";
        public static String DND_OFF = "SET%20DND%20OFF";
        public static String DND_ON_V2 = "SET%20DND%20ON";
        public static String DND_OFF_V2 = "SET%20DND%20OFF";

        public static void DNDOn() {
            DND(true);
        }

        public static void DNDOff() {
            DND(false);
        }

        private static void DND(boolean on) {

            if (isOldMAS) {
                if (on) {
                    MainApplication.getMAS().sendMASCmd(DND_ON, 1);
                } else
                    MainApplication.getMAS().sendMASCmd(DND_OFF, 1);
            } else {
                if (on) {
                    MainApplication.getMAS().sendMASCmd(DND_ON_V2, 1);
                } else {
                    MainApplication.getMAS().sendMASCmd(DND_OFF_V2, 1);
                }
            }
        }

        public static void DirectCommand(String command, boolean nowFire){
            MainApplication.getMAS().sendMASCmd(command, (nowFire?1:0));
        }

        public static void DirectSTBCommand(String command){
            sendSTBAppCommand(command);
        }
    }

    public static void sendSTBAppCommand(String command) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(0, 10000);
        String url = MainApplication.getContext().getString(R.string.demoDaySTB) + "/" + command;
        android.util.Log.i(MainApplication.getContext().getClass().getSimpleName(), "URL = " + url);

        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                ((BaseActivity) MainApplication.getContext()).showProgress();
            }

            @Override
            public void onSuccess(int arg0, Header[] headers, byte[] response) {
                String s = new String(response);
                Log.i(MainApplication.getContext().getClass().getSimpleName(), "success response = " + s);
                ((BaseActivity) MainApplication.getContext()).hideProgress();
            }

            @Override
            public void onFailure(int arg0, Header[] headers, byte[] response, Throwable e) {
                if (response != null) {
                    String s = new String(response);
                    Log.i(MainApplication.getContext().getClass().getSimpleName(), s);
                }
                ((BaseActivity) MainApplication.getContext()).hideProgress();
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                Log.i(MainApplication.getContext().getClass().getSimpleName(), "retrying number = " + retryNo);
            }
        });
    }

    public static class TVCommandSender extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... arg0) {
            URL ww;
            StringBuilder sb = new StringBuilder();
            try {
                ww = new URL(MainApplication.getContext().getString(R.string.demoDaySTB) + "/channel/" + arg0[0]);
                android.util.Log.d("TVCommanderSender", ww.toString());
                URLConnection tc = ww.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(tc.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                String response = sb.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static class URLCommandSender extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... arg0) {
            StringBuilder sb = new StringBuilder();
            try {
                Log.d("URLCommanderSender", arg0[0]);
                URLConnection tc = new URL(arg0[0]).openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(tc.getInputStream()));
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();
                String response = sb.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}


