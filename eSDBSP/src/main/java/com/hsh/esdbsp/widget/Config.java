package com.hsh.esdbsp.widget;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.model.Hotel;

public class Config {

    //key
    final String KEY_MIN_TEMP_15 = "Config.minAC15";
    final String KEY_MIN_TEMP = "Config.minAC";
    final String KEY_MAX_TEMP = "Config.maxAC";
    final String KEY_CMS_DEBUG = "Config.isCMSDebug";
    final String KEY_RED_ENVELOPE = "alert.fax.big.red.envelope";
    final String KEY_DATA_CHACHE_INTERVAL = "Config.checkDataCacheInterval";
    final String KEY_CHECK_MSG_INTERVAL = "Config.checkMessageInterval";
    final String KEY_CHECK_MSG_SHORT_INTERVAL = "Config.checkMessageShortInterval";
    final String KEY_HOUSEKEEP_INTERVAL = "Config.housekeepInterval";
    final String KEY_BACK_TO_RC_INTERVAL = "Config.backToRoomControlInterval";
    final String KEY_DIM_SCREEN_INTERVAL = "Config.dimScreenInterval";
    final String KEY_CHECK_WEATHER_INTERVAL = "Config.checkWeatherInterval";

    //default value
    final boolean DEFAULT_IS_CMS_DEBUG = false;
    final int DEFAULT_MIN_TEMP_PBH = 64;
    final int DEFAULT_MIN_TEMP_PHK = 18;
    final int DEFAULT_MIN_TEMP = 18;
    final int DEFAULT_MAX_TEMP_PBH = 82;
    final int DEFAULT_MAX_TEMP = 28;
    final boolean DEFAULT_IS_PBH_MIN_TEMP_15 = false;
    final boolean DEFAULT_IS_BIG_FAX_ON = false;
    final long DEFAULT_DATACACHE_INTERVAL = 30 * 60 * 1000;
    final long DEFAULT_CHECK_MESSAGE_INTERVAL = 20 * 1000;
    final long DEFAULT_CHECK_MESSAGE_SHORT_INTERVAL = 5 * 1000;
    final long DEFAULT_HOUSEKEEP_INTERVAL = 30 * 1000;
    final long DEFAULT_BACK_TO_ROOMCONTROL_INTERVAL = 2 * 60 * 1000;
    final long DEFAULT_DIM_SCREEN_INTERVAL = 30 * 1000;
    final long DEFAULT_CHECK_WEATHER_INTERVAL = 15 * 60 * 1000;

    boolean isCMSDebug = DEFAULT_IS_CMS_DEBUG;
    boolean isPBHMinAC15 = DEFAULT_IS_PBH_MIN_TEMP_15;
    boolean isBigFaxOn = DEFAULT_IS_BIG_FAX_ON;
    int minAC = DEFAULT_MIN_TEMP; //PBH in F
    int maxAC = DEFAULT_MAX_TEMP; //PBH in F
    long dataCacheInterval = DEFAULT_DATACACHE_INTERVAL;
    long checkMessageInterval = DEFAULT_CHECK_MESSAGE_INTERVAL;
    long checkMessageShortInterval = DEFAULT_CHECK_MESSAGE_SHORT_INTERVAL;  //for if have red envolop
    long housekeepInterval = DEFAULT_HOUSEKEEP_INTERVAL;
    long backToRoomControlInterval = DEFAULT_BACK_TO_ROOMCONTROL_INTERVAL;
    long dimScreenInterval = DEFAULT_DIM_SCREEN_INTERVAL;
    long checkWeatherInterval = DEFAULT_CHECK_WEATHER_INTERVAL;

    public Config() {

    }

    public void init() {

        if (!MainApplication.getLabel(KEY_MIN_TEMP_15).equalsIgnoreCase(KEY_MIN_TEMP_15)) {
            String roomString = MainApplication.getLabel(KEY_MIN_TEMP_15);
            if (roomString.toLowerCase().contains("all")) {
                isPBHMinAC15 = true;
            } else {
                String[] roomArray = roomString.split(",");
                for (String room : roomArray) {
                    if ((room.trim()).equals(MainApplication.getMAS().getData("data_myroom"))) {
                        isPBHMinAC15 = true;
                    }
                }
            }
        } else {
            isPBHMinAC15 = false;
        }

        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            if (isPBHMinAC15) {
                minAC = 69; // 15C in 69F
            } else {
                minAC = DEFAULT_MIN_TEMP_PBH;
            }
            maxAC = DEFAULT_MAX_TEMP_PBH;
        } else if (GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            minAC = DEFAULT_MIN_TEMP_PHK;
            maxAC = DEFAULT_MAX_TEMP;
        } else {
            minAC = DEFAULT_MIN_TEMP;
            maxAC = DEFAULT_MAX_TEMP;
        }

        if (!MainApplication.getLabel(KEY_MIN_TEMP).equalsIgnoreCase(KEY_MIN_TEMP)) {
            try {
                Integer temp = Integer.valueOf(MainApplication.getLabel(KEY_MIN_TEMP));
                if (temp != null) {
                    minAC = temp;
                }
            } catch (NumberFormatException e) {

            }
        }

        if (!MainApplication.getLabel(KEY_MAX_TEMP).equalsIgnoreCase(KEY_MAX_TEMP)) {
            try {
                Integer temp = Integer.valueOf(MainApplication.getLabel(KEY_MAX_TEMP));
                if (temp != null) {
                    maxAC = temp;
                }
            } catch (NumberFormatException e) {

            }
        }

        if (!MainApplication.getLabel(KEY_CMS_DEBUG).equalsIgnoreCase(KEY_CMS_DEBUG)) {
            String roomString = MainApplication.getLabel(KEY_CMS_DEBUG);

            if (roomString.toLowerCase().contains("all")) {
                isCMSDebug = true;
            } else {
                String[] roomArray = roomString.split(",");
                for (String room : roomArray) {
                    if ((room.trim()).equals(MainApplication.getMAS().getData("data_myroom"))) {
                        isCMSDebug = true;
                    }
                }
            }
        } else {
            isCMSDebug = DEFAULT_IS_CMS_DEBUG;
        }


        if (!MainApplication.getLabel(KEY_RED_ENVELOPE).equalsIgnoreCase(KEY_RED_ENVELOPE)) {
            String roomString = MainApplication.getLabel(KEY_RED_ENVELOPE);

            if (roomString.toLowerCase().contains("all")) {
                isBigFaxOn = true;
            } else {
                String[] roomArray = roomString.split(",");
                for (String room : roomArray) {

                    if ((room.trim()).equals(MainApplication.getMAS().getData("data_myroom"))) {
                        isBigFaxOn = true;
                    }
                }
            }
        } else {
            isBigFaxOn = false;
        }

        if (!MainApplication.getLabel(KEY_DATA_CHACHE_INTERVAL).equalsIgnoreCase(KEY_DATA_CHACHE_INTERVAL)) {
            try {
                Integer second = Integer.valueOf(MainApplication.getLabel(KEY_DATA_CHACHE_INTERVAL));
                if (second != null) {
                    dataCacheInterval = second * 1000;
                }
            } catch (NumberFormatException e) {
                dataCacheInterval = DEFAULT_DATACACHE_INTERVAL;
            }
        } else {
            dataCacheInterval = DEFAULT_DATACACHE_INTERVAL;
        }

        if (!MainApplication.getLabel(KEY_CHECK_MSG_INTERVAL).equalsIgnoreCase(KEY_CHECK_MSG_INTERVAL)) {
            try {
                Integer second = Integer.valueOf(MainApplication.getLabel(KEY_CHECK_MSG_INTERVAL));
                if (second != null) {
                    checkMessageInterval = second * 1000;
                }
            } catch (NumberFormatException e) {
                checkMessageInterval = DEFAULT_CHECK_MESSAGE_INTERVAL;
            }
        } else {
            checkMessageInterval = DEFAULT_CHECK_MESSAGE_INTERVAL;
        }

        if (!MainApplication.getLabel(KEY_CHECK_MSG_SHORT_INTERVAL).equalsIgnoreCase(KEY_CHECK_MSG_SHORT_INTERVAL)) {
            try {
                Integer second = Integer.valueOf(MainApplication.getLabel(KEY_CHECK_MSG_SHORT_INTERVAL));
                if (second != null) {
                    checkMessageShortInterval = second * 1000;
                }
            } catch (NumberFormatException e) {
                checkMessageShortInterval = DEFAULT_CHECK_MESSAGE_SHORT_INTERVAL;
            }
        } else {
            checkMessageShortInterval = DEFAULT_CHECK_MESSAGE_SHORT_INTERVAL;
        }

        if (!MainApplication.getLabel(KEY_HOUSEKEEP_INTERVAL).equalsIgnoreCase(KEY_HOUSEKEEP_INTERVAL)) {
            try {
                Integer second = Integer.valueOf(MainApplication.getLabel(KEY_HOUSEKEEP_INTERVAL));
                if (second != null) {
                    housekeepInterval = second * 1000;
                }
            } catch (NumberFormatException e) {
                housekeepInterval = DEFAULT_HOUSEKEEP_INTERVAL;
            }
        } else {
            housekeepInterval = DEFAULT_HOUSEKEEP_INTERVAL;
        }

        if (!MainApplication.getLabel(KEY_BACK_TO_RC_INTERVAL).equalsIgnoreCase(KEY_BACK_TO_RC_INTERVAL)) {
            try {
                Integer second = Integer.valueOf(MainApplication.getLabel(KEY_BACK_TO_RC_INTERVAL));
                if (second != null) {
                    backToRoomControlInterval = second * 1000;
                }
            } catch (NumberFormatException e) {
                backToRoomControlInterval = DEFAULT_BACK_TO_ROOMCONTROL_INTERVAL;
            }
        } else {
            backToRoomControlInterval = DEFAULT_BACK_TO_ROOMCONTROL_INTERVAL;
        }

        if (!MainApplication.getLabel(KEY_DIM_SCREEN_INTERVAL).equalsIgnoreCase(KEY_DIM_SCREEN_INTERVAL)) {
            try {
                Integer second = Integer.valueOf(MainApplication.getLabel(KEY_DIM_SCREEN_INTERVAL));
                if (second != null) {
                    dimScreenInterval = second * 1000;
                }
            } catch (NumberFormatException e) {
                dimScreenInterval = DEFAULT_DIM_SCREEN_INTERVAL;
            }
        } else {
            dimScreenInterval = DEFAULT_DIM_SCREEN_INTERVAL;
        }

        if (!MainApplication.getLabel(KEY_CHECK_WEATHER_INTERVAL).equalsIgnoreCase(KEY_CHECK_WEATHER_INTERVAL)) {
            try {
                Integer second = Integer.valueOf(MainApplication.getLabel(KEY_CHECK_WEATHER_INTERVAL));
                if (second != null) {
                    checkWeatherInterval = second * 1000;
                }
            } catch (NumberFormatException e) {
                checkWeatherInterval = DEFAULT_CHECK_WEATHER_INTERVAL;
            }
        } else {
            checkWeatherInterval = DEFAULT_CHECK_WEATHER_INTERVAL;
        }
    }

    public boolean isCMSDebug() {
        return isCMSDebug;
    }

    public boolean isPBHMinAC15() {
        return isPBHMinAC15;
    }

    public void setPBHMinAC15(boolean PBHMinAC15) {
        isPBHMinAC15 = PBHMinAC15;
    }

    public int getMinAC() {
        if (isPBHMinAC15 && GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            return 69; //
        } else {
            return minAC;
        }
    }

    public void setMinAC(int minAC) {
        this.minAC = minAC;
    }

    public int getMaxAC() {
        return maxAC;
    }

    public void setMaxAC(int maxAC) {
        this.maxAC = maxAC;
    }

    public boolean isBigFaxOn() {
        return isBigFaxOn;
    }

    public void setCMSDebug(boolean CMSDebug) {
        isCMSDebug = CMSDebug;
    }

    public long getCheckMessageInterval() {
        return checkMessageInterval;
    }

    public void setCheckMessageInterval(long checkMessageInterval) {
        this.checkMessageInterval = checkMessageInterval;
    }

    public long getCheckMessageShortInterval() {
        return checkMessageShortInterval;
    }

    public void setCheckMessageShortInterval(long checkMessageShortInterval) {
        this.checkMessageShortInterval = checkMessageShortInterval;
    }

    public long getHousekeepInterval() {
        return housekeepInterval;
    }

    public void setHousekeepInterval(long housekeepInterval) {
        this.housekeepInterval = housekeepInterval;
    }

    public long getBackToRoomControlInterval() {
        return backToRoomControlInterval;
    }

    public void setBackToRoomControlInterval(long backToRoomControlInterval) {
        this.backToRoomControlInterval = backToRoomControlInterval;
    }

    public long getDimScreenInterval() {
        return dimScreenInterval;
    }

    public void setDimScreenInterval(long dimScreenInterval) {
        this.dimScreenInterval = dimScreenInterval;
    }

    public long getCheckWeatherInterval() {
        return checkWeatherInterval;
    }

    public void setCheckWeatherInterval(long checkWeatherInterval) {
        this.checkWeatherInterval = checkWeatherInterval;
    }

    public long getDataCacheInterval() {
        return dataCacheInterval;
    }

    public void setDataCacheInterval(long dataCacheInterval) {
        this.dataCacheInterval = dataCacheInterval;
    }
}