package com.hsh.esdbsp.widget;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.receiver.BatteryReceiver;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;

public class BatteryChecker {

    public interface OnBatteryChangedListener {
        void onBatteryChanged(int myBatteryDiv);

        void onPowerPlug(boolean plug);
    }

    private boolean isBatteryCharging = false;
    private int batteryLevel = -1;
    private BroadcastReceiver mReceiver;
    private OnBatteryChangedListener listener;


    public BatteryChecker(OnBatteryChangedListener listener) {
        this.listener = listener;
        loadReceiver();
    }

    public void setBattery(boolean isCharging, int level) {

        if(isBatteryCharging != isCharging){
            listener.onPowerPlug(isCharging);
        }
        isBatteryCharging = isCharging;
        batteryLevel = level;
        listener.onBatteryChanged(getBatteryLevel());
    }

    private int getBatteryLevel() {
        int myBatteryDiv = 0;
        if (isBatteryCharging) {
            myBatteryDiv = 100;
        } else {
            if (batteryLevel >= 90) {
                myBatteryDiv = 90;
            } else if (batteryLevel >= 75) {
                myBatteryDiv = 75;
            } else if (batteryLevel >= 50) {
                myBatteryDiv = 50;
            } else if (batteryLevel >= 25) {
                myBatteryDiv = 25;
            } else {
                myBatteryDiv = 25;
            }
        }
        return myBatteryDiv;
    }

///////////////////////////////////////////////

    private void loadReceiver() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        filter.addAction(Intent.ACTION_POWER_CONNECTED);
        filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        filter.addAction(Intent.ACTION_BATTERY_CHANGED);

        mReceiver = new BatteryReceiver(this);
        MainApplication.getContext().registerReceiver(mReceiver, filter);

    }

    public void unloadReceiver() {
        MainApplication.getContext().unregisterReceiver(mReceiver);
    }

}