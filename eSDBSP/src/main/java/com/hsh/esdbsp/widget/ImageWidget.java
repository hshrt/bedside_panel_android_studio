package com.hsh.esdbsp.widget;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;

import com.hsh.esdbsp.MainApplication;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.FileNotFoundException;

/**
 * Created by lawrencetsang on 2018-03-14.
 */

public class ImageWidget {

    public static final String SIZE_S = "_s";
    public static final String SIZE_M = "_m";

    public static void loadMediaPlayButtonImage(ImageView playBtn, String imageName, String ext, String size) {
        loadImage(playBtn, null, imageName, ext, size, true, true);
    }

    public static void loadImage(ImageView imageView, String imageName, String ext, String size) {
        loadImage(imageView, null, imageName, ext, size, true, false);
    }

    public static void loadImage(final ImageView imageView, final DisplayImageOptions options, final String imageName, final String ext, final String size) {
        loadImage(imageView, options, imageName, ext, size, true, false);
    }

    //firstTry is false = for trying to load png in next time
    public static void loadImage(final ImageView imageView, final DisplayImageOptions options, final String imageName, final String ext, final String size, final boolean firstTry, final boolean isMediaPlayButton) {

        //for no ext, will try jpg first then try png
        String imagePath = MainApplication.getUploadApiBase() + imageName + size + ".jpg";

        if (ext != null && (!ext.equalsIgnoreCase("null"))) {
            if (ext.equalsIgnoreCase("j")) {
                imagePath = MainApplication.getUploadApiBase() + imageName + size + ".jpg";
            } else {
                imagePath = MainApplication.getUploadApiBase() + imageName + size + ".png";
            }
        }

        //load media button
        if(isMediaPlayButton){
            ImageLoader.getInstance().loadImage(imagePath, new ImageLoadingListener() {

                @Override
                public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
                    imageView.setBackgroundDrawable(new BitmapDrawable(MainApplication.getContext().getResources(), arg2));
                }

                @Override
                public void onLoadingCancelled(String arg0, View arg1) {
                }

                @Override
                public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
                }

                @Override
                public void onLoadingStarted(String arg0, View arg1) {
                }
            });
        } else {
            //load other image

            try {
                ImageLoader.getInstance().displayImage(imagePath, imageView, options, new ImageLoadingListener() {


                    @Override
                    public void onLoadingStarted(String s, View view) {

                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {
                        //firstTry is false = for trying to load png in next time
                        if (firstTry && failReason.getCause() instanceof FileNotFoundException) {
                            loadImage(imageView, null, imageName, "p", size, false, false);
                        }
                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });

            } catch (Exception ex) {

            }
        }
    }
}

