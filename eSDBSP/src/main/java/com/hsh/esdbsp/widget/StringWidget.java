package com.hsh.esdbsp.widget;

/**
 * Created by lawrencetsang on 2018-03-12.
 */

public class StringWidget {

    public static String UrlDecoderReplaceSpecialChar(String original){
        String returnString = "";

        if(returnString.contains("%25") || returnString.contains("%2B")){
            returnString = original;
        } else {
            returnString = original.replaceAll("%(?![0-9a-fA-F]{2})", "%25");
            returnString = returnString.replaceAll("\\+", "%2B");
        }
        return returnString;
    }

}
