package com.hsh.esdbsp.adapter.dining;

import java.util.ArrayList;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.model.GeneralItem;

import android.util.Log;

public class GeneralItemWrapper implements Comparable<GeneralItemWrapper> {
	GeneralItem item;
	boolean isChecked;
	int quantity;
	GeneralItemWrapper parent;
	ArrayList<GeneralItemWrapper> options = new ArrayList<>();
	ArrayList<GeneralItemWrapper> optionSet = new ArrayList<>();
	
	public GeneralItemWrapper(GeneralItem item) {
		this(item, false, null);
	}
	
	public GeneralItemWrapper(GeneralItem item, boolean isChecked) {
		this(item, isChecked, null);
	}

	public GeneralItemWrapper(GeneralItem item, boolean isChecked, GeneralItemWrapper parent) {
		this.item = item;
		this.isChecked = isChecked;
		this.parent = parent;
	}

	public GeneralItem getItem() {
		return item;
	}
	
	public int getQuantity() {
		return quantity;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public boolean isChecked() {
		return isChecked;
	}
	
	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	public boolean isIdInList(ArrayList<GeneralItemWrapper> list) {
		for (GeneralItemWrapper item : list) {
			if (this.item.getItemId().equals(item.getItem().getItemId())) {
				return true;
			}
		}
		return false;
	}
	
	public ArrayList<GeneralItemWrapper> getOptions() {
		return options;
	}

	public ArrayList<GeneralItemWrapper> getOptionSet() {
		return optionSet;
	}

	public void fillChildren() {
		fillChildren("");
	}
	
	public void fillChildren(String indent) {
		//Log.d("OSiOS", indent + "In: " + item.getItemId() + "(" + MainApplication.getLabel(item.getTitleId()) + ")");
		options.clear();
		optionSet.clear();
		for (GeneralItem allItem : GlobalValue.getInstance().getAllItemArray()) {
			if (allItem.getParentId().equalsIgnoreCase(item.getItemId())) {
				//Log.d("OSiOS", indent + "find option: " + allItem.getItemId() + "(" + MainApplication.getLabel(allItem.getTitleId()) + ")");
				GeneralItemWrapper optionItem = new GeneralItemWrapper(allItem, false, this);
				options.add(optionItem);
			}
		}
		if (item.getOptionSetIds() != null && !"".equals(item.getOptionSetIds())) {
			for (String s : item.getOptionSetIds().split(",")) {
				for (GeneralItem allItem : GlobalValue.getInstance().getAllItemArray()) {
					if (allItem.getItemId().equalsIgnoreCase(s)) {
						//Log.d("OSiOS", indent + "find optionSet: " + s + "(" + MainApplication.getLabel(allItem.getTitleId()) + ")");
						GeneralItemWrapper optionSetItem = new GeneralItemWrapper(allItem, false, this);
						optionSet.add(optionSetItem);
						optionSetItem.fillChildren(indent + " ");
					}
				}
			}
		}
	}
	
	private GeneralItem getItem(String id) {
		for (GeneralItem item : GlobalValue.getInstance().getAllItemArray()) {
			if (item.getItemId().equalsIgnoreCase(id)) {
				return item;
			}
		}
		return null;
	}
	
	public GeneralItemWrapper findChild(String childId) {
		if (item.getItemId().equalsIgnoreCase(childId)) {
			return this;
		}
		for (GeneralItemWrapper i : options) {
			if (i.getItem().getItemId().equalsIgnoreCase(childId)) {
				return i;
			}
		}
		for (GeneralItemWrapper i : optionSet) {
			GeneralItemWrapper childInOptionSet = i.findChild(childId); 
			if (childInOptionSet != null) {
				return childInOptionSet;
			}
		}
		return null;
	}
	
	public void traverse(String indent) {
		Log.d("OSiOS", indent + "In: " + item.getItemId() + "(" + MainApplication.getLabel(item.getTitleId()) + ")");
		for (GeneralItemWrapper i : options) {
			Log.d("OSiOS", indent + "Option: " + i.getItem().getItemId() + "(" + MainApplication.getLabel(i.getItem().getTitleId()) + ")" + (i.isChecked ? " Checked " : ""));
		}
		for (GeneralItemWrapper i : optionSet) {
			Log.d("OSiOS", indent + "OptionSet: " + i.getItem().getItemId() + "(" + MainApplication.getLabel(i.getItem().getTitleId()) + ")" + (i.isChecked ? " Checked " : ""));
			i.traverse(indent + " ");
		}
	}
	
	public double getTotalPrice() {
		double price = 0.0;
		for (GeneralItemWrapper i : options) {
			if (i.isChecked) {
				price += i.getItem().getPrice() * i.getQuantity(); 
			}
		}
		for (GeneralItemWrapper i : optionSet) {
			price += i.getTotalPrice();
		}
		return price;
	}
	
	public String getTitleWithParents(GeneralItemWrapper rootItem) {
		//Log.d("OSiOS", "entry: " + this);
		if (parent != null && !parent.getItem().getItemId().equalsIgnoreCase(rootItem.getItem().getItemId())) {
			return parent.getTitleWithParents(rootItem) + " - " + MainApplication.getLabel(getItem().getTitleId());
		}
		Log.d("OSiOS", "parent is null");
		return MainApplication.getLabel(getItem().getTitleId());
			
	}
	
	public ArrayList<String> getCheckedItems(String prefix) {
		ArrayList<String> result = new ArrayList<>();
		for (GeneralItemWrapper i : options) {
			if (i.isChecked()) {
				String itemName = prefix + MainApplication.getLabel(i.getItem().getTitleId());
				if (i.getQuantity() > 1) {
					itemName += " x " + i.getQuantity();
				}
				result.add(itemName);
			}
		}
		for (GeneralItemWrapper i : optionSet) {
			result.addAll(i.getCheckedItems(prefix + MainApplication.getLabel(i.getItem().getTitleId()) + " - "));
		}
		return result;
	}
	
	public String toString() {
		return item.getItemId() + "(" + MainApplication.getLabel(item.getTitleId()) + ")";
	}

	public void uncheckSelfAndChildren() {
		this.isChecked = false;
		for (GeneralItemWrapper i : options) {
			i.isChecked = false;
		}
		for (GeneralItemWrapper i: optionSet) {
			i.isChecked = false;
			i.uncheckSelfAndChildren();
		}
		
	}
	
	public ArrayList<String> buildFoodIdString(String prefix) {
		ArrayList<String> result = new ArrayList<>();
		for (GeneralItemWrapper i : options) {
			if (i.isChecked) {
				if (!"".equals(prefix)) {
					result.add(prefix + "/" + i.getItem().getItemId());
				} else {
					result.add(i.getItem().getItemId());
				}
			}
		}
		for (GeneralItemWrapper i : optionSet) {
			if (!"".equals(prefix)) {
				result.addAll(i.buildFoodIdString(prefix + "/" + i.getItem().getItemId()));
			} else {
				result.addAll(i.buildFoodIdString(i.getItem().getItemId()));
			}
		}
		return result;
	}

	@Override
	public int compareTo(GeneralItemWrapper o) {
		return Integer.valueOf(getItem().getOrder()).compareTo(Integer.valueOf(o.getItem().getOrder()));
	}
}