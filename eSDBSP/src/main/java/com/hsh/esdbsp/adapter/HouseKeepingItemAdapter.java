package com.hsh.esdbsp.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.GeneralItem;

public class HouseKeepingItemAdapter extends ArrayAdapter<GeneralItem> implements ListAdapter {

	LayoutInflater mInflater;
	private ArrayList<GeneralItem> mainMenuItems;
	private ArrayList<ViewHolder> viewHolderArray;

	private static String TAG = "HouseKeepingItemAdapter";
	private View mParent;

	private Context mContext;

	private Boolean[] clickArr;

	public HouseKeepingItemAdapter(Context context, ArrayList<GeneralItem> items) {
		super(context, 0, items);
		this.mainMenuItems = items;
		mContext = context;
		mInflater = LayoutInflater.from(context);

		clickArr = new Boolean[items.size()];
		for (int x = 0; x < items.size(); x++) {
			clickArr[x] = false;
		}
		viewHolderArray = new ArrayList<ViewHolder>();
	}

	@Override
	public int getCount() {
		return mainMenuItems.size();
	}

	@Override
	public GeneralItem getItem(int position) {
		return mainMenuItems.get(position);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		View v = convertView;

		final ViewHolder holder;

		String itemName = MainApplication.getLabel(mainMenuItems.get(position).getTitleId());

		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.cms_housekeeping_item, parent, false);
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);

			holder.button.setTextColor(Color.parseColor("#ffffff"));

			holder.checker = (Button) v.findViewById(R.id.clickBtn);

			String currLang = "";
			currLang = MainApplication.getMAS().getData("data_language") == "" ? "E"
					: MainApplication.getMAS().getData("data_language");

			if (currLang.equalsIgnoreCase("")) {
				currLang = "E";
			}

			if (currLang.equalsIgnoreCase("R")
					|| DataCacheManager.getInstance().getLang()
							.equalsIgnoreCase("R")) {
				holder.button.setTextSize(16);

			}

			Helper.setAppFonts(holder.button);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		holder.button.setText(itemName);

		OnClickListener l = new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (clickArr[position]) {
					clickArr[position] = false;
					holder.checker.setBackgroundResource(R.drawable.checkmark_off);
				} else {
					clickArr[position] = true;
					holder.checker.setBackgroundResource(R.drawable.checkmark_on);
				}
			}
		};

		holder.button.setOnClickListener(l);
		holder.checker.setOnClickListener(l);

		if (clickArr[position] == false) {
			holder.checker.setBackgroundResource(R.drawable.checkmark_off);
		} else {
			holder.checker.setBackgroundResource(R.drawable.checkmark_on);
		}

		viewHolderArray.add(holder);
		return v;
	}

	public Boolean[] getClickArr() {
		return clickArr;
	}

	public void setClickArr(Boolean[] clickArr) {
		this.clickArr = clickArr;
	}

	public void resetClickArr(){
		for(Boolean tick : clickArr){
			tick = false;
		}
		for(ViewHolder viewHolder : viewHolderArray){
			viewHolder.checker.setBackgroundResource(R.drawable.checkmark_off);
		}
	}

	static class ViewHolder {
		Button button;
		Button checker;
	}

}
