package com.hsh.esdbsp.adapter.dining;

import java.util.List;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.FoodOrder;
import com.hsh.esdbsp.model.Hotel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

public class FoodOrderConfirmAdapter extends ArrayAdapter<FoodOrder> {

	public interface Callback {
		public void onEdit(int position, int day, int hour, int min);
		public void onDelete(int position);
	}

	private Context mContext;
	private LayoutInflater mInflater;
	private Callback mCallback;
	
	public FoodOrderConfirmAdapter(Context context, List<FoodOrder> objects) {
		super(context, 0, objects);
		mContext = context;
		mInflater = LayoutInflater.from(context);
	}

	public void setCallback(Callback callback) {
		mCallback = callback;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
		ViewHolder viewHolder;
		
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.cms_food_order_confirm_item, null, false);
			viewHolder = new ViewHolder();
			viewHolder.qtyBadge = (MyTextView) convertView.findViewById(R.id.qty_badge);
			viewHolder.itemName = (MyTextView) convertView.findViewById(R.id.item_name);
			viewHolder.optionsName = (MyTextView) convertView.findViewById(R.id.options_name);
			//viewHolder.conflictWarning = (ImageView) convertView.findViewById(R.id.conflictWarning);
			viewHolder.deliveryTime = (MyTextView) convertView.findViewById(R.id.deliveryTime);
			viewHolder.price = (MyTextView) convertView.findViewById(R.id.price);
			viewHolder.editBtn = (Button) convertView.findViewById(R.id.editBtn);
			viewHolder.deleteBtn = (Button) convertView.findViewById(R.id.deleteBtn);
			Helper.setAppFonts(viewHolder.editBtn);
			Helper.setAppFonts(viewHolder.deleteBtn);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		final FoodOrder item = getItem(position);
		String dollar = "US $";
		
		if(GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)){
			dollar = "RMB ";
		}
		if(GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)){
			dollar = "HK $";
		}
		
		double finalPrice = item.getFood().getItem().getPrice();
		finalPrice += item.getFood().getTotalPrice();
		
		viewHolder.price.setText(dollar + Helper.processPrice(finalPrice * item.getOrderQty()+""));
		if(finalPrice == 0) {
			viewHolder.price.setText("");
		}
		
		viewHolder.qtyBadge.setText(Integer.toString(item.getOrderQty()));
		viewHolder.itemName.setText(MainApplication.getLabel(item.getFood().getItem().getTitleId()));
		String optionNameString = "";

		for (String optionName : item.getFood().getCheckedItems("")) {
			if (!"".equals(optionNameString)) {
				optionNameString += ",\n";
			}
			optionNameString += optionName;
			/*
			if (option.getQuantity() > 1) {
				optionNameString += " x " + option.getQuantity();
			}
			*/
		}
		viewHolder.optionsName.setText(optionNameString);
		
//		viewHolder.deliveryTime.setText(MainApplication.getLabel("Delivery Time") + ":\n" + item.getDeliveryTimeStr());
		viewHolder.deliveryTime.setVisibility(View.GONE);
		viewHolder.editBtn.setText(MainApplication.getLabel("Edit"));
		viewHolder.editBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (mCallback != null) {
					FoodOrder item = getItem(position); 
					mCallback.onEdit(position, item.getDeliveryDay(), item.getDeliveryHour(), item.getDeliveryMin());
				}
			}
		});
		viewHolder.deleteBtn.setText(MainApplication.getLabel("Delete"));
		viewHolder.deleteBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (mCallback != null) {
					mCallback.onDelete(position);
				}
			}
		});
		return convertView;
	}
	
	static class ViewHolder {
		MyTextView qtyBadge;
		MyTextView itemName;
		MyTextView optionsName;
		ImageView conflictWarning;
		MyTextView deliveryTime;
		MyTextView price;
		Button editBtn;
		Button deleteBtn;
	}
}
