package com.hsh.esdbsp.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.Movie;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MovieItemAdapter extends ArrayAdapter<Movie> implements
		ListAdapter {

	LayoutInflater mInflater;
	private ArrayList<Movie> movieItems;

	private static String TAG = "MovieItemAdapter";
	private View mParent;

	private Context mContext;

	

	public MovieItemAdapter(Context context, ArrayList<Movie> items) {
		super(context, 0, items);
		this.movieItems = items;
		mContext = context;
		mInflater = LayoutInflater.from(context);

	}

	@Override
	public int getCount() {
		return movieItems.size();
	}

	@Override
	public Movie getItem(int position) {
		return movieItems.get(position);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Log.i(TAG, "Position = " + position);
		LayoutParams lp;
		View v = convertView;

		ViewHolder holder;

		/*String itemName = MainApplication.getLabel(mainMenuItems.get(position)
				.getTitleId());*/

		if (v == null) {
			
			holder = new ViewHolder();
			
			mParent = parent;
			v = mInflater.inflate(R.layout.movie_item, parent, false);


			holder.iconView = (ImageView) v.findViewById(R.id.icon);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}


		// String imageName = imgNameArr[position%10];
		try {
			/*
			 * String imageName =
			 * map.get(MainApplication.getEngLabel(mainMenuItems
			 * .get(position).getTitleId())); int resourceId =
			 * mContext.getResources().getIdentifier(imageName, "drawable",
			 * mContext.getPackageName());
			 * holder.button.setCompoundDrawablesWithIntrinsicBounds(
			 * resourceId, 0, 0, 0);
			 */


			DisplayImageOptions options = new DisplayImageOptions.Builder()
					.showImageOnLoading(R.drawable.thumb_transparent)
					.showImageForEmptyUri(R.drawable.thumb_transparent)
					.showImageOnFail(R.drawable.thumb_transparent)
					.cacheInMemory(true).cacheOnDisk(true)
					.considerExifParams(true)
					.bitmapConfig(Bitmap.Config.RGB_565).build();
			
			String lang = "e";
			String imageLink = MainApplication.getApiBase()
					+"cmspbj/"
					+ "movie/"
					+ "small" +
					+ (position+1) +"." + "png";
			Log.i(TAG, "imageLink = " + imageLink);

			ImageLoader.getInstance().displayImage(imageLink,
					holder.iconView, options);

		}
		catch(Exception e){
			
		}
		

		return v;
	}

	static class ViewHolder {
		Button button;
		ImageView iconView;
	}

}
