package com.hsh.esdbsp.adapter.dining;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.GeneralItem;

public class PreviewOrderAdapter extends ArrayAdapter<GeneralItem> implements ListAdapter {

    LayoutInflater mInflater;
    private ArrayList<GeneralItem> foodSubCatItems;

    private static String TAG = "PreviewOrderAdapter";
    private View mParent;

    private Context mContext;

    private Button firstButton;

    private int currentPosition = 0;

    private String parentName = "";

    public PreviewOrderAdapter(Context context, ArrayList<GeneralItem> items) {
        super(context, 0, items);
        this.foodSubCatItems = items;
        mInflater = LayoutInflater.from(context);
        mContext = context;
    }

    @Override
    public int getCount() {
        Log.i(TAG, "PreviewOrderAdapter getCount = " + foodSubCatItems.size());
        return foodSubCatItems.size();
    }

    @Override
    public GeneralItem getItem(int position) {
        return foodSubCatItems.get(position);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        Log.i(TAG, "Position = " + position);
        LayoutParams lp;
        View v = convertView;

        final ViewHolder holder;

        String itemName = foodSubCatItems.get(position).getTitleId();

        if (v == null) {
            mParent = parent;
            v = mInflater.inflate(R.layout.cms_in_room_preview_order_item_pch, parent,
                    false);
            holder = new ViewHolder();
            holder.orderItemText = (TextView) v.findViewById(R.id.orderItem);

            holder.priceItemText = (TextView) v.findViewById(R.id.priceItem);

            Helper.setAppFonts(holder.orderItemText);
            Helper.setAppFonts(holder.priceItemText);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

		/*if(position == 0){
			Log.i(TAG, "assign firstButton");
			firstButton = holder.button;
			holder.textView.setText("");
		}*/

        //holder.button.setText(itemName);
        holder.orderItemText.setText(itemName);

        String dollarSign = "US $";
        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
            dollarSign = "RMB ";
        }
        if (GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            dollarSign = "HK $";
        }

        holder.priceItemText.setText(dollarSign + foodSubCatItems.get(position).getPrice() + "");

        if (position == 0) {
            //holder.textView.setText("");
        }
        holder.orderItemText.setMaxLines(3);


        return v;
    }

    public Button getFirstButton() {
        return firstButton;
    }

    public void setFirstButton(Button firstButton) {
        this.firstButton = firstButton;
    }

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    static class ViewHolder {
        TextView orderItemText;
        TextView priceItemText;
    }

    public void setParentName(String s) {
        parentName = s;
    }

}
