package com.hsh.esdbsp.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.Helper;

public class TimeAdapter extends ArrayAdapter<String> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<String> timeItems;

	private static String TAG = "String";
	private View mParent;
	private Context mContext;
	
	private int type;
	
	public TimeAdapter(Context context, ArrayList<String> items) {
		super(context, 0, items);
		this.timeItems = items;
		mInflater = LayoutInflater.from(context);
		
		mContext = context;
	}
	@Override
	public int getCount() {
		return timeItems.size();
	}

	@Override
	public String getItem(int position){
		return timeItems.get(position);
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		//Log.i(TAG, "Position = " + position);
		LayoutParams lp;  
		View v = convertView;
		
		final ViewHolder holder;
		
		String timeInfo = timeItems.get(position);
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.flight_time_item, parent,
					false);
			holder = new ViewHolder();		
			
			holder.timeRange = (Button) v.findViewById(R.id.timeButton);
	
			
			Helper.setAppFonts(holder.timeRange);

			v.setTag(holder);
			
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		
		//holder.button.setText(itemName);
		holder.timeRange.setText(timeInfo);
		
		
		/*if(position == 0){
			firstButton = holder.button;
		}*/

		return v;
	}

	static class ViewHolder {
		TextView timeRange;

	}

}
