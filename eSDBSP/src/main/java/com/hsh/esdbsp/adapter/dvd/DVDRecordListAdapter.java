package com.hsh.esdbsp.adapter.dvd;

import java.util.ArrayList;
import java.util.List;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.model.DVDRecordListItem;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DVDRecordListAdapter extends RecyclerView.Adapter<DVDRecordListAdapter.ViewHolder> {

	Context context;
	
    private List<DVDRecordListItem> mData = new ArrayList<DVDRecordListItem>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public DVDRecordListAdapter(Context context, List<DVDRecordListItem> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.dvd_record_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
    	DVDRecordListItem item = mData.get(position);
    	 
        holder.title.setText(item.getTitle());
        String posterUrl = MainApplication.getApiBase() + context.getString(R.string.dvd_borrowing) + item.getPosterUrl();
        holder.genre.setText(item.getGenre());
        holder.year.setText(item.getYear());
        
        String[] borrowDateTimeString = item.getBorrowTime().split(" ");
        
        holder.borrowDate.setText(borrowDateTimeString[0]);
        holder.borrowTime.setText(borrowDateTimeString[1]);
        
        holder.actionButton.setVisibility(View.INVISIBLE);
        if(item.getStatusId() == 1) {
        	holder.status.setText(MainApplication.getLabel("dvd.pending"));
        } else if(item.getStatusId() == 2) {
        	holder.actionButton.setVisibility(View.VISIBLE);
        	holder.status.setText(MainApplication.getLabel("dvd.inuse"));
        } else if(item.getStatusId() == 3) {
        	holder.status.setText(MainApplication.getLabel("dvd.requestedreturn"));
        } else if(item.getStatusId() == 4) {
        	holder.status.setText(MainApplication.getLabel("dvd.returned"));
        } else if(item.getStatusId() == 5) {
        	holder.status.setText(MainApplication.getLabel("dvd.cancelled"));
        } 

  
        DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.thumb_transparent)
				.showImageForEmptyUri(R.drawable.thumb_transparent)
				.showImageOnFail(R.drawable.thumb_transparent)
				.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
        
        ImageLoader.getInstance().displayImage(posterUrl, holder.poster,options);

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    	public LinearLayout item;
        public ImageView poster;
        public TextView title;
        public TextView genre;
        public TextView year;
        public TextView borrowDate;
        public TextView borrowTime;
        public TextView status;
        public Button actionButton;

        public ViewHolder(View itemView) {
            super(itemView);
            item = (LinearLayout) itemView.findViewById(R.id.llRecordItem);
            poster = (ImageView) itemView.findViewById(R.id.ivPoster);
            title = (TextView) itemView.findViewById(R.id.tvTitle);
            genre = (TextView) itemView.findViewById(R.id.tvGenre);
            year = (TextView) itemView.findViewById(R.id.tvYear);
            borrowDate = (TextView) itemView.findViewById(R.id.tvBorrowDate);
            borrowTime = (TextView) itemView.findViewById(R.id.tvBorrowTime);
            status = (TextView) itemView.findViewById(R.id.tvStatus);
            actionButton = (Button) itemView.findViewById(R.id.btnAction);
            
        
            actionButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getPosition());
        }
    }

    // convenience method for getting data at click position
    public DVDRecordListItem getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}