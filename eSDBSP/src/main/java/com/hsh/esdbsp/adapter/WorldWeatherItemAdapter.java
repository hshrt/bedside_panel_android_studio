package com.hsh.esdbsp.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.WorldWeather;

public class WorldWeatherItemAdapter extends ArrayAdapter<WorldWeather> implements
		ListAdapter {

	LayoutInflater mInflater;
	private ArrayList<WorldWeather> wwItems;

	private static String TAG = "WorldWeatherItemAdapter";
	private View mParent;

	private Context mContext;

	public WorldWeatherItemAdapter(Context context, ArrayList<WorldWeather> items) {
		super(context, 0, items);
		this.wwItems = items;
		mContext = context;
		mInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return wwItems.size();
	}

	@Override
	public WorldWeather getItem(int position) {
		return wwItems.get(position);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Log.i(TAG, "Position = " + position);
		LayoutParams lp;
		View v = convertView;

		ViewHolder holder;

		
		
		WorldWeather wwItem = wwItems.get(position);
		
		String itemName = MainApplication.getLabel(wwItem
				.getCity());

		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.worldweather_item, parent, false);

			holder = new ViewHolder();
			holder.timeText = (TextView) v.findViewById(R.id.timeText);
			holder.icon = (ImageView) v.findViewById(R.id.icon);
			holder.tempText = (TextView) v.findViewById(R.id.tempText);
			holder.cityText = (TextView) v.findViewById(R.id.cityText);

			Helper.setAppFonts(holder.tempText);
			Helper.setAppFonts(holder.cityText);
			Helper.setAppFonts(holder.timeText);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}
		
		holder.timeText.setText(wwItem.getTime().substring(11, 16));
		
		Resources res = mContext.getResources();
		
		int code = wwItem.getIcon();
		if (code == 0){
			code = 11;
		}
		String mDrawableName = "w"+ code;
		Log.i(TAG, "drawable name = " + mDrawableName);
		int resID = res.getIdentifier(mDrawableName , "drawable", mContext.getPackageName());
		Log.i(TAG, "resID name = " + resID);
		Drawable drawable = res.getDrawable(resID);	
		
		holder.icon.setImageDrawable(drawable);
		
		holder.tempText.setText(Html.fromHtml(wwItem.getTemperature()+ "&deg;C"));
		holder.cityText.setText(MainApplication.getLabel("world_cities."+wwItem.getCity()));

		return v;
	}

	static class ViewHolder {
		TextView timeText;
		ImageView icon;
		TextView tempText;
		TextView cityText;
	}

}
