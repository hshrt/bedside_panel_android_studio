package com.hsh.esdbsp.adapter.ibahn;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.view.MyTextView;

public class IBahnMovieGenreAdapter extends ArrayAdapter<String> implements
		ListAdapter {

	LayoutInflater mInflater;
	private List<String> genres;

	private static String TAG = "MovieItemAdapter";
	private View mParent;

	private Context mContext;
	private int selectedItem;

	

	public IBahnMovieGenreAdapter(Context context, List<String> genres) {
		super(context, 0, genres);
		this.genres = genres;
		mContext = context;
		mInflater = LayoutInflater.from(context);

	}

	@Override
	public int getCount() {
		return genres.size();
	}

	@Override
	public String getItem(int position) {
		return genres.get(position);
	}

	public int getSelectedItem() {
		return selectedItem;
	}
	
	public void setSelectedItem(int selectedItem) {
		this.selectedItem = selectedItem;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		LayoutParams lp;
		View v = convertView;

		ViewHolder holder;

		if (v == null) {
			
			holder = new ViewHolder();
			
			mParent = parent;
			v = mInflater.inflate(R.layout.ibahn_movie_genre, parent, false);
			holder.genreNameView = (MyTextView) v.findViewById(R.id.genreName);
			holder.button = (ImageView) v.findViewById(R.id.button);

			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		holder.genreNameView.setText(MainApplication.getLabel("movie.genre." + getItem(position)));
		if (selectedItem == position) {
			holder.button.setBackgroundResource(R.drawable.listview_button_active);
		} else {
			holder.button.setBackgroundResource(R.drawable.listview_button);
		}

		return v;
	}

	static class ViewHolder {
		ImageView button;
		MyTextView genreNameView;
	}

}
