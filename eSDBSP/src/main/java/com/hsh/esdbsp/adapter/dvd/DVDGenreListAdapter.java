package com.hsh.esdbsp.adapter.dvd;

import java.util.ArrayList;
import java.util.List;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.fragment.DVDGenreListFragment;
import com.hsh.esdbsp.model.DVDGenre;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

public class DVDGenreListAdapter extends FragmentPagerAdapter {
	
	private final String TAG = this.getClass().getSimpleName();
	
	List<DVDGenre> genreList = new ArrayList<DVDGenre>();
	
    public DVDGenreListAdapter(FragmentManager fm) {
		super(fm);
	}
    
    public void initData(List<DVDGenre> genreList) {
    	this.genreList = genreList;
    }
	
    @Override
    public int getCount() {
        return this.genreList.size();
    }
    @Override
    public Fragment getItem(int position) {
    	int genreId = genreList.get(position).getGenreId();
        return DVDGenreListFragment.newInstance(genreId);
    }
    
    @Override
    public CharSequence getPageTitle(int position) {
 
        String currLang = MainApplication.getMAS().getData("data_language") == "" ? "E" : MainApplication.getMAS().getData("data_language");
        String genreTitle = (String)genreList.get(position).getLanguageMap().get(currLang);
        
        Log.v(TAG, "getPageTitle: POS:" + position + " Title:" + genreTitle);
        
        return genreTitle;
    }
    
}