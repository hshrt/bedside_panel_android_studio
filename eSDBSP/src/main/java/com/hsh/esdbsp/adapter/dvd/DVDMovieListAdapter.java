package com.hsh.esdbsp.adapter.dvd;

import java.util.ArrayList;
import java.util.List;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.model.DVDMovieListItem;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DVDMovieListAdapter extends RecyclerView.Adapter<DVDMovieListAdapter.ViewHolder> {

	Context context;
	
    private List<DVDMovieListItem> mData = new ArrayList<DVDMovieListItem>();
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public DVDMovieListAdapter(Context context, List<DVDMovieListItem> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.context = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.dvd_movie_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
    	DVDMovieListItem item = mData.get(position);
    	 
        holder.title.setText(item.getTitle());
        String posterUrl = MainApplication.getApiBase() + context.getString(R.string.dvd_borrowing) + item.getPosterUrl();
        
  
        DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.thumb_transparent)
				.showImageForEmptyUri(R.drawable.thumb_transparent)
				.showImageOnFail(R.drawable.thumb_transparent)
				.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
        
        ImageLoader.getInstance().displayImage(posterUrl, holder.poster,options);

    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    	public LinearLayout item;
        public ImageView poster;
        public TextView title;

        public ViewHolder(View itemView) {
            super(itemView);
            item = (LinearLayout) itemView.findViewById(R.id.llmovieItem);
            poster = (ImageView) itemView.findViewById(R.id.ivPoster);
            title = (TextView) itemView.findViewById(R.id.tvTitle);
            Typeface face = Typeface.createFromAsset(context.getAssets(), "minion.otf");
            title.setTypeface(face);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getPosition());
        }
    }

    // convenience method for getting data at click position
    public DVDMovieListItem getItem(int id) {
        return mData.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}