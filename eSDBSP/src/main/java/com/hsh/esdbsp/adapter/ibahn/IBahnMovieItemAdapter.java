package com.hsh.esdbsp.adapter.ibahn;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;

import com.hsh.esdbsp.R;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.IBahnMovie;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class IBahnMovieItemAdapter extends ArrayAdapter<IBahnMovie> implements
        ListAdapter {

    LayoutInflater mInflater;
    private List<IBahnMovie> movieItems;

    private static String TAG = "MovieItemAdapter";
    private View mParent;

    private Context mContext;

    public IBahnMovieItemAdapter(Context context, List<IBahnMovie> movieResult) {
        super(context, 0, movieResult);
        this.movieItems = movieResult;
        mContext = context;
        mInflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return movieItems.size();
    }

    @Override
    public IBahnMovie getItem(int position) {
        return movieItems.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Log.i(TAG, "Position = " + position);
        LayoutParams lp;
        View v = convertView;

        ViewHolder holder;

        if (v == null) {
            holder = new ViewHolder();

            mParent = parent;
            v = mInflater.inflate(R.layout.ibahn_movie_item, parent, false);

            holder.iconView = (ImageView) v.findViewById(R.id.icon);
            holder.movieNameView = (MyTextView) v.findViewById(R.id.movieName);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.movieNameView.setText(getItem(position).getTitle());

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.movie_poster_ibahn_empty)
                .showImageForEmptyUri(R.drawable.movie_poster_ibahn_empty)
                .showImageOnFail(R.drawable.movie_poster_ibahn_empty)
                .cacheInMemory(true).cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        ImageLoader.getInstance().displayImage(getItem(position).getWebPosterUrl(), holder.iconView, options);

        return v;
    }

    static class ViewHolder {
        Button button;
        ImageView iconView;
        MyTextView movieNameView;
    }

}
