package com.hsh.esdbsp.adapter.flight;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.ServiceAirportActivity;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.Airline;

public class AirlineAdapter extends ArrayAdapter<Airline> implements ListAdapter{

	LayoutInflater mInflater;
	private ArrayList<Airline> airlines;

	private static String TAG = "AirlineAdapter";
	private View mParent;
	private Context mContext;
	
	private Button firstButton;
	
	private int currentPosition = -1;
	
	public AirlineAdapter(Context context, ArrayList<Airline> airlines) {
		super(context, 0, airlines);
		
		this.airlines = airlines;
		mInflater = LayoutInflater.from(context);
		
		mContext = context;
	}

	@Override
	public int getCount() {
		return airlines.size();
	}

	@Override
	public Airline getItem(int position){
		return airlines.get(position);
	}
	
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		//Log.i(TAG, "Position = " + position);
		LayoutParams lp;  
		View v = convertView;
		
		final ViewHolder holder;
		
		    String itemName = airlines.get(position).getName();
		
		if (v == null) {
			mParent = parent;
			v = mInflater.inflate(R.layout.airport_airline_item, parent,
					false);
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);
			
			holder.button.setTextColor(Color.parseColor("#ffffff")); 
			
			holder.textview = (TextView) v.findViewById(R.id.text);
			
			Helper.setAppFonts(holder.textview);
			
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		//holder.button.setText(itemName);
		holder.textview.setText(itemName);
		
		if(position == 0){
			firstButton = holder.button;
		}
		
		holder.button.setBackgroundResource(R.drawable.dining_item_selector);
		
		if( position == currentPosition){
			holder.button.setBackgroundResource(R.drawable.dining_item_selected);
		}
		
		holder.button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.i(TAG, ((ViewGroup)parent).getChildCount()+"");
				
				for(int i=0; i<((ViewGroup)parent).getChildCount(); i++) {
				    View nextChild = ((ViewGroup)parent).getChildAt(i);
				    Log.i(TAG, nextChild.getClass().toString());
				    if (nextChild instanceof Button){
				    	Log.i(TAG, "done");
				    	nextChild.setBackgroundResource(R.drawable.dining_item_selector);
				    }
				    for(int y=0; y<((ViewGroup)nextChild).getChildCount(); y++) {
				    	View nextChild2 = ((ViewGroup)nextChild).getChildAt(y);
				    	if (nextChild2 instanceof Button){
					    	Log.i(TAG, "done");
					    	nextChild2.setBackgroundResource(R.drawable.dining_item_selector);
					    }	
				    }
				}
				
				holder.button.setBackgroundResource(R.drawable.dining_item_selected);
				
				if(mContext instanceof ServiceAirportActivity){
					((ServiceAirportActivity)mContext).onAirlinePressed(position,airlines.get(position));
				}
				
				currentPosition = position;
			}
		});
		
		return v;
	}
	public Button getFirstButton() {
		return firstButton;
	}

	public void setFirstButton(Button firstButton) {
		this.firstButton = firstButton;
	}
	public int getCurrentPosition() {
		return currentPosition;
	}

	public void setCurrentPosition(int currentPosition) {
		this.currentPosition = currentPosition;
	}
	static class ViewHolder {
		Button button;
		TextView textview;
	}

}
