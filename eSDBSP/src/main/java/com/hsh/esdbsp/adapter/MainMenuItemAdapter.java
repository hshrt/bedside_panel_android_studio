package com.hsh.esdbsp.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.ServiceGeneralMenuActivity;
import com.hsh.esdbsp.activity.ServiceMainActivity;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.model.Hotel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MainMenuItemAdapter extends ArrayAdapter<GeneralItem> implements
        ListAdapter {

    LayoutInflater mInflater;
    private ArrayList<GeneralItem> mainMenuItems;

    private static String TAG = "MainMenuItemAdapter";
    private View mParent;

    private Context mContext;

    private Boolean hasLeftIcon;

    public MainMenuItemAdapter(Context context, ArrayList<GeneralItem> items) {
        super(context, 0, items);
        this.mainMenuItems = items;
        mContext = context;
        mInflater = LayoutInflater.from(context);

        hasLeftIcon = true;

    }

    @Override
    public int getCount() {
        return mainMenuItems.size();
    }

    @Override
    public GeneralItem getItem(int position) {
        return mainMenuItems.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Log.i(TAG, "Position = " + position);
        LayoutParams lp;
        View v = convertView;

        ViewHolder holder;

        String itemName = MainApplication.getLabel(mainMenuItems.get(position).getTitleId());

        if (v == null) {
            mParent = parent;
            v = mInflater.inflate(R.layout.cms_main_menu_item, parent, false);

            holder = new ViewHolder();
            holder.button = (Button) v.findViewById(R.id.itemButton);

            int leftPadding = 100;
            int width = 0;

            if (MainApplication.getCurrentActivity() != null) {
                Display display = MainApplication.getCurrentActivity().getWindowManager().getDefaultDisplay();
                width = display.getWidth();
                int height = display.getHeight();
                Log.i("Helper = ", width + "");
            }

            if (width > 2000) {
                leftPadding = 200;

            }

            if (MainApplication.isArabic() && !checkIsFullEnglish(itemName, MainApplication.getEngLabel(mainMenuItems.get(position).getTitleId()))) {
                holder.button.setGravity(Gravity.RIGHT | Gravity.CENTER);
                holder.button.setPadding(0, 0, leftPadding/2, 0);
            } else {
                holder.button.setGravity(Gravity.LEFT | Gravity.CENTER);
                holder.button.setPadding(leftPadding, 0, leftPadding, 0);
            }
            holder.button.setTextColor(Color.parseColor("#ffffff"));

            holder.iconView = (ImageView) v.findViewById(R.id.icon);

            Helper.setAppFonts(holder.button);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.button.setText(itemName);
        holder.button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (mContext instanceof ServiceMainActivity) {
                    Log.i(TAG, "on click position = " + position);
                    ((ServiceMainActivity) mContext).onItemclick(mainMenuItems
                            .get(position));
                }

                if (mContext instanceof ServiceGeneralMenuActivity)
                    ((ServiceGeneralMenuActivity) mContext)
                            .onItemclick(position);
            }
        });

        if (!mainMenuItems.get(position).getIconName().equalsIgnoreCase("null")) {
            try {

                String imageName = mainMenuItems.get(position).getIconName();
                String extension = mainMenuItems.get(position).getExt();
                extension = extension.equalsIgnoreCase("j") ? "jpg" : "png";

                if (imageName.equalsIgnoreCase("null")) {
                    int leftPadding = 40;
                    Display display = MainApplication.getCurrentActivity().getWindowManager().getDefaultDisplay();
                    int width = display.getWidth();
                    int height = display.getHeight();
                    Log.i("Helper = ", width + "");

                    if (width > 2000) {
                        leftPadding = 80;
                    }

                    // holder.button.setTextColor(Color.parseColor("#ffffff"));
                    Log.i(TAG, "Itemname = " + itemName);
                    Log.i(TAG, "English Label = " + MainApplication.getEngLabel(mainMenuItems.get(position).getTitleId()));

                    if (MainApplication.isArabic() && !checkIsFullEnglish(itemName, MainApplication.getEngLabel(mainMenuItems.get(position).getTitleId()))) {
                        holder.button.setGravity(Gravity.RIGHT | Gravity.CENTER);
                        holder.button.setPadding(0, 0, leftPadding/2, 0);
                    } else {
                        holder.button.setGravity(Gravity.LEFT | Gravity.CENTER);
                        holder.button.setPadding(leftPadding, 0, leftPadding, 0);
                    }

                    holder.iconView.setVisibility(View.INVISIBLE);

                } else {

                    int leftPadding = 160;

                    DisplayImageOptions options = new DisplayImageOptions.Builder()
                            .showImageOnLoading(R.drawable.thumb_transparent)
                            .showImageForEmptyUri(R.drawable.thumb_transparent)
                            .showImageOnFail(R.drawable.thumb_transparent)
                            .cacheInMemory(true).cacheOnDisk(true)
                            .considerExifParams(true)
                            .bitmapConfig(Bitmap.Config.RGB_565).build();

                    String imageLink = MainApplication.getUploadApiBase()
                            + imageName + "_m." + extension;
                    Log.i(TAG, "imageLink = " + imageLink);

                    holder.iconView.setVisibility(View.VISIBLE);
                    ImageLoader.getInstance().displayImage(imageLink, holder.iconView, options);
                }

            } catch (Exception e) {

            }
        } else {
            Log.i(TAG, "use reserve icon");
            int leftPadding = 80;
            if (MainApplication.isArabic() && !checkIsFullEnglish(itemName, MainApplication.getEngLabel(mainMenuItems.get(position).getTitleId()))) {
                holder.button.setGravity(Gravity.RIGHT | Gravity.CENTER);
                holder.button.setPadding(0, 0, leftPadding/2, 0);
            } else {
                holder.button.setGravity(Gravity.LEFT | Gravity.CENTER);
                holder.button.setPadding(leftPadding, 0, leftPadding, 0);
            }
            // Dont do this at this moment, we don't want the sub-level button
            // have the P icon

            // int resourceId2 = mContext.getResources().getIdentifier("p_icon",
            // "drawable", mContext.getPackageName());
            // holder.button.setCompoundDrawablesWithIntrinsicBounds(
            // resourceId2, 0, 0, 0);
        }

        return v;
    }

    private boolean checkIsFullEnglish(String a, String b) {
        return a.equalsIgnoreCase(b);
    }

    public Boolean getHasLeftIcon() {
        return hasLeftIcon;
    }

    public void setHasLeftIcon(Boolean hasLeftIcon) {
        this.hasLeftIcon = hasLeftIcon;
    }

    static class ViewHolder {
        Button button;
        ImageView iconView;
    }

}
