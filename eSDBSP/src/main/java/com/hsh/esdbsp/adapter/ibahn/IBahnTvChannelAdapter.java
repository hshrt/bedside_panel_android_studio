package com.hsh.esdbsp.adapter.ibahn;

import java.util.HashMap;
import java.util.List;

import com.hsh.esdbsp.R;
import com.hsh.esdbsp.model.IBahnTvChannel;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;


public class IBahnTvChannelAdapter extends ArrayAdapter<IBahnTvChannel> implements ListAdapter {
    private Context context;
    private List<IBahnTvChannel> data;
    private DisplayImageOptions options;


    //////////////////////////////////////////////////////


    private HashMap<Integer, ImageView> views;

    ViewHolder holder = null;


    //////////////////////////////////////////////////////


    public IBahnTvChannelAdapter(Context context, int layoutResourceId, List<IBahnTvChannel> data) {
        super(context, layoutResourceId, data);
        this.context = context;
        this.data = data;
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.bbg)
                .showImageForEmptyUri(R.drawable.bbg)
                .showImageOnFail(R.drawable.bbg)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public IBahnTvChannel getItem(int position) {
        return data.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(R.layout.tv_channel_ibahn, parent, false);
            holder = new ViewHolder();
            holder.image = (ImageView) row.findViewById(R.id.channelicon);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        IBahnTvChannel item = data.get(position);
        ImageLoader.getInstance().displayImage(item.getLogoUrl(), holder.image, options);

        return row;
    }


    static class ViewHolder {
        TextView imageTitle;
        ImageView image;
    }
}







