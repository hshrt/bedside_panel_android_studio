package com.hsh.esdbsp.adapter.dining;

import java.util.ArrayList;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.Button;

public class FoodOrderItemAdapter extends ArrayAdapter<GeneralItemWrapper> {

	private Context mContext;
	private LayoutInflater mInflater;
	private ArrayList<GeneralItemWrapper> mOrderItems;
	private boolean mIsComplex;

	public interface Callback {
		public void onSelectionChanged(int selectedCount);
	}
	private Callback mCallback;
	
	public FoodOrderItemAdapter(Context context, ArrayList<GeneralItemWrapper> items, Callback callback) {
		super(context, 0, items);
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mCallback = callback;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutParams lp;
		View v = convertView;
		
		final ViewHolder holder;
		final GeneralItemWrapper item = getItem(position);
		
		if (v == null) {
			v = mInflater.inflate(R.layout.cms_food_order_item, null,
					false);
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);
			Helper.setAppFonts(holder.button);
			
			holder.button.setTextColor(Color.parseColor("#ffffff"));
			holder.checker = (Button) v.findViewById(R.id.clickBtn);

			if (item.isChecked) {
				holder.checker.setBackgroundResource(R.drawable.checkmark_on);
			} else {
				holder.checker.setBackgroundResource(R.drawable.checkmark_off);
			}

			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		holder.button.setText(MainApplication.getLabel(item.item.getTitleId()));
		
		OnClickListener l = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				item.isChecked = !item.isChecked;
				if (item.isChecked) {
					holder.checker.setBackgroundResource(R.drawable.checkmark_on);
					item.quantity = 1;
				} else {
					holder.checker.setBackgroundResource(R.drawable.checkmark_off);
					item.quantity = 0;
					item.uncheckSelfAndChildren();
				}
				int selectedItems = 0;
				for (int i = 0; i < getCount(); i++) {
					if (getItem(i).isChecked) {
						selectedItems++;
					}
				}
				mCallback.onSelectionChanged(selectedItems);
			}
		};
		holder.button.setOnClickListener(l);
		holder.checker.setOnClickListener(l);

		if (item.isChecked) {
			holder.checker.setBackgroundResource(R.drawable.checkmark_on);
		} else {
			holder.checker.setBackgroundResource(R.drawable.checkmark_off);
		}
		
		return v;
	}
	static class ViewHolder {
		Button button;
		Button checker;
	}
}
