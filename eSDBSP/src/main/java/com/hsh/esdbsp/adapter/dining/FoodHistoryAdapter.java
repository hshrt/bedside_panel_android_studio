package com.hsh.esdbsp.adapter.dining;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.adapter.dining.FoodHistoryAdapter.FoodHistory;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.model.Hotel;

public class FoodHistoryAdapter extends ArrayAdapter<FoodHistory> {

	private Context mContext;
	private LayoutInflater mInflater;
	ArrayList<GeneralItem> tempArray;
	
	public FoodHistoryAdapter(Context context, int resource,
			List<FoodHistory> objects) {
		super(context, resource, objects);
		mContext = context;
		mInflater = LayoutInflater.from(context);
		tempArray = GlobalValue.getInstance().getAllItemArray();
	}

	
	
	@Override
	public int getItemViewType(int position) {
		return (getItem(position).header != null) ? 0 : 1;
	}

	@Override
	public int getViewTypeCount() {
		return 2;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		FoodHistory foodHistory = getItem(position);
		
		if (convertView == null) {
			viewHolder = new ViewHolder();
			switch(getItemViewType(position)) {
			case 0:
				convertView = mInflater.inflate(R.layout.cms_food_history_header, null, false);
				viewHolder.itemName = (MyTextView) convertView.findViewById(R.id.item_name);
				break;
			case 1:
				convertView = mInflater.inflate(R.layout.cms_food_history_item, null, false);
				viewHolder.qtyBadge = (MyTextView) convertView.findViewById(R.id.qty_badge);
				viewHolder.itemName = (MyTextView) convertView.findViewById(R.id.item_name);
				viewHolder.optionsName = (MyTextView) convertView.findViewById(R.id.options_name);
				viewHolder.deliveryTime = (MyTextView) convertView.findViewById(R.id.deliveryTime);
				viewHolder.price = (MyTextView) convertView.findViewById(R.id.price);
				convertView.setTag(viewHolder);
				break;
			}
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		
		if (getItemViewType(position) == 0) {
			viewHolder.itemName.setText(foodHistory.header);
		} else {
			GeneralItemWrapper rootItem = foodHistory.foodIdList.get(0);
			
			viewHolder.qtyBadge.setText(foodHistory.quantity);
			viewHolder.itemName.setText(MainApplication.getLabel(rootItem.getItem().getTitleId()));
			
			String choice = "";
			for (int i = 1; i < foodHistory.foodIdList.size(); i++) {
				if (i != 1) {
					choice += ",\n";
				}
				choice += foodHistory.foodIdList.get(i).getTitleWithParents(rootItem);
				if (foodHistory.foodIdList.get(i).getQuantity() > 1) {
					choice += " x " + foodHistory.foodIdList.get(i).getQuantity();
				}
			}
			viewHolder.optionsName.setText(choice);
			
			viewHolder.deliveryTime.setText(MainApplication.getLabel("Delivery Time") + "\n" + foodHistory.deliveryTime.substring(0, 16));
			
			String dollar = "US $";
			if(GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)){
				dollar = "RMB ";
			}
			if(GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)){
				dollar = "HK $";
			}

			double finalPrice = rootItem.getItem().getPrice();
			for (int i = 1; i < foodHistory.foodIdList.size(); i++) {
				GeneralItemWrapper option = foodHistory.foodIdList.get(i);
				finalPrice += option.getItem().getPrice() * option.getQuantity();
			}
			
			viewHolder.price.setText(dollar + Helper.processPrice(finalPrice * Integer.parseInt(foodHistory.quantity)+""));
			if(finalPrice == 0) {
				viewHolder.price.setText("");
			}

		}
		
		return convertView;
	}


	public static class FoodHistory {
		public String header;
		public String id;
		public String room;
		public ArrayList<GeneralItemWrapper> foodIdList;
		public String deliveryTime;
		public String orderTime;
		public String numOfGuest;
		public String quantity;
		public String status;
		public String lastUpdate;
		public String lastUpdateBy;
		public String item;
		public String choices;
	}
	
	static class ViewHolder {
		MyTextView qtyBadge;
		MyTextView itemName;
		MyTextView optionsName;
		MyTextView deliveryTime;
		MyTextView price;
	}
}
