package com.hsh.esdbsp.adapter.dining;

import java.util.ArrayList;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Hotel;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;

public class FoodOrderComplexItemAdapter extends ArrayAdapter<GeneralItemWrapper> {

	private Context mContext;
	private LayoutInflater mInflater;
	private ArrayList<GeneralItemWrapper> mOrderItems;
	private boolean mIsComplex;

	public interface Callback {
		public void onSelectionChanged(int selectedCount);
	}
	private Callback mCallback;
	
	public FoodOrderComplexItemAdapter(Context context, ArrayList<GeneralItemWrapper> items, ArrayList<GeneralItemWrapper> orderItems, Callback callback) {
		super(context, 0, items);
		mContext = context;
		mInflater = LayoutInflater.from(context);
		mOrderItems = orderItems;
		mCallback = callback;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		
		final ViewHolder holder;
		final GeneralItemWrapper item = getItem(position);
		
		if (v == null) {
			v = mInflater.inflate(R.layout.cms_food_order_complex_item, null, false);
			holder = new ViewHolder();
			holder.button = (Button) v.findViewById(R.id.itemButton);
			holder.button.setTextColor(Color.parseColor("#ffffff"));
			Helper.setAppFonts(holder.button);
			holder.price = (MyTextView) v.findViewById(R.id.price);
			holder.minusBtn = (Button) v.findViewById(R.id.minusBtn);
			Helper.setAppFonts(holder.minusBtn);
			holder.quantity = (MyTextView) v.findViewById(R.id.quantity);
			holder.plusBtn = (Button) v.findViewById(R.id.plusBtn);
			Helper.setAppFonts(holder.plusBtn);
			
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		
		holder.button.setText(MainApplication.getLabel(item.item.getTitleId()));
		holder.minusBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (item.quantity > 0) {
					item.quantity--;
					if (item.isIdInList(mOrderItems) && item.quantity == 0) {
						GeneralItemWrapper toRemove = null;
						for (GeneralItemWrapper i: mOrderItems) {
							if (item.getItem().getItemId().equals(i.getItem().getItemId())) {
								toRemove = i;
							}
						}
						if (toRemove != null) {
							mOrderItems.remove(toRemove);
						}
					}
					mCallback.onSelectionChanged(getTotalQuantity());
					notifyDataSetChanged();
				}
			}
		});
		holder.quantity.setText(Integer.toString(item.quantity));
		holder.plusBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.d("FoodOrderComplexItem", "maxQuantity: " + item.getItem().getMaxChoice());
				if (item.quantity < item.getItem().getMaxChoice()) {
					item.quantity++;
					if (!item.isIdInList(mOrderItems)) {
						mOrderItems.add(item);
					}
					mCallback.onSelectionChanged(getTotalQuantity());
					notifyDataSetChanged();
				}
			}
		});
		String dollarSign = "US $";
		if(GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)){
			dollarSign = "RMB ";				
		}
		if(GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)){
			dollarSign = "HK $";				
		}
		
		holder.price.setText(dollarSign+Helper.processPrice(item.item.getPrice()+""));
		
		return v;
	}
	
	private int getTotalQuantity() {
		int total = 0;
		for(int i = 0; i < getCount(); i++) {
			total += getItem(i).quantity;
		}
		return total;
	}
	
	static class ViewHolder {
		Button button;
		MyTextView price;
		Button minusBtn;
		MyTextView quantity;
		Button plusBtn;
	}
}
