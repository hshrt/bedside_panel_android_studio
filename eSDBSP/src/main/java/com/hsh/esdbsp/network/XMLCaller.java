package com.hsh.esdbsp.network;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.webkit.URLUtil;

import com.google.android.gms.security.ProviderInstaller;
import com.hsh.esdbsp.adapter.CustomBaseAdapter;
import com.hsh.esdbsp.activity.BaseActivity;
import com.hsh.esdbsp.fragment.BaseFragment;
import com.hsh.esdbsp.widget.Log;

/**
 * XmlProvider provides an easy way to fetch and parse an XML document. This class allows
 * the fetching of an XML file over the network without having to worry about hogging up
 * the UI thread or extra implementations needed to do so.
 * 
 */
public class XMLCaller {

	/**
	 * A tag associated with this specific class type. This can be used for debugging and
	 * for logging activities and tasks dealing with objects of this type.
	 */
	public static final String TAG = "< XmlProvider >";
	
	/**
	 * Indicates that no action is needed on failure.
	 */
	public static final int FAIL_MODE_NO_ACTION = 0;

	/**
	 * Indicates that a retry message should be displayed on error.
	 */
	public static final int FAIL_MODE_DISPLAY_RETRY = 1;

	/**
	 * Indicates that an error message should be displayed on error.
	 */
	public static final int FAIL_MODE_DISPLAY_ERROR = 2;
	
	/**
	 * Indicates that an retry message should be displayer on error and tell user this is an timeout Error.
	 */
	public static final int FAIL_MODE_DISPLAY_RETRY_TIMEOUT = 3;
	/**
	 * Indicates that an Parsing Error
	 */
	public static final int FAIL_MODE_PARSE_ERROR = 4;
	
	/**
	 * An interface a class needs to implement if an XML file needs to be retrieved
	 * from the network. These methods will be passed in to the XmlParser class
	 * to be used by the internal processor.  
	 */
	public interface InternetCallbacks {
		/**
		 * Checks whether or not there is a valid Internet connection
		 * @return true if a valid connection exits, false otherwise
		 */
		public boolean hasInternet();
		
		/**
		 * Displays some sort error message indicating the network issue.
		 * @param isPostExecute true if this is called during onPostExecute, false otherwise
		 */
		public void onError(int failMode, boolean isPostExecute);
		
		/**
		 * Allows extra parsing of the XML data. This is where the caching
		 * of the data should also take place, to ensure easy access in case of
		 * network loss.<p>
		 * The AsyncTask will <i>not</i> call <code>setXmlString()</code>
		 * automatically, so that call should be implemented here.
		 * @param xml
		 */
		public void postExecute(String xml);
		
		public void postExecuteWithCellId(String xml, String cellId);
		
	}
	
	private InternetCallbacks mCallbacks;
	private int position;
	private HttpTask mTask;
	private int mFailMode = FAIL_MODE_DISPLAY_RETRY;
	
	private Dialog vDialog;
	
	private Bundle mParams;
	
	private String requestMethod = "post";
	
	//for upload photo in multipart form 
	public Boolean isUploadImage = false;
	
	private String cellId;
	
	String multipart_form_data = "multipart/form-data";
    String twoHyphens = "--";
    String boundary = "****************fD4fH3gL0hK7aI6";    
    String lineEnd = System.getProperty("line.separator"); 

	/**
	 * Initializes the parser to process a network XML file.
	 * @param callbacks an {@link Activity} class that implements the {@link InternetCallbacks} interface
	 * @throws Exception If the callback object is not an instance of the Activity class
	 */
	public XMLCaller(InternetCallbacks callbacks, String _requestMethod, Bundle _param) throws Exception {
		/*if (callbacks != null && !(callbacks instanceof Activity)) {
			throw new Exception("callbacks is of class " + callbacks.getClass() + ", which is not an instance of Activity");
		}*/
		requestMethod = _requestMethod;
		mCallbacks = callbacks;
		
		mTask = null;
		mParams = _param;
		
		if(mParams != null && mParams.getString("isUploadImage")!= null && mParams.getString("isUploadImage").equals("yes")){
			isUploadImage = true;
		}
		
		if(mParams != null && mParams.getString("cellId")!= null){
			cellId = mParams.getString("cellId");
		}
	}
	
	public XMLCaller(InternetCallbacks callbacks, String _requestMethod,Bundle _param, int position) throws Exception {
		/*if (callbacks != null && !(callbacks instanceof Activity)) {
			throw new Exception("callbacks is of class " + callbacks.getClass() + ", which is not an instance of Activity");
		}*/
		
		requestMethod = _requestMethod;
		mCallbacks = callbacks;
		this.position = position;
		mTask = null;
		mParams = _param;
		
		if(mParams != null && mParams.getString("isUploadImage")!= null && mParams.getString("isUploadImage").equals("yes")){
			isUploadImage = true;
		}
		
		if(mParams != null && mParams.getString("cellId")!= null){
			cellId = mParams.getString("cellId");
		}
	}

	/**
	 * Config this option will determine how the AsyncTask will handle any Internet
	 * connection issues.  
	 * @param failMode
	 * @return <code>this</code> to allow chaining
	 */
	public XMLCaller setFailMode(int failMode) {
		mFailMode = failMode;
		return this;
	}
	
	/**
	 * Fetches and sets up the XML file from the network via an AsyncTask. Calling this method 
	 * will require passing in an object that implements the {@link InternetCallbacks}
	 * <code>interface</code> to ensure the proper handling of network issues.<p>
	 * This method will complete the task on a separate thread to prevent a lag on the UI thread 
	 * also creating a ProgressDialog as a visual indication of the network call. If the network 
	 * connection fails for any reason (no Internet access, failed to connect to the server, 
	 * etc.), an error message will be displayed. If the XML file is a non-critical piece of 
	 * data and does not require a failure screen, call <code>setFailMode(true)</code> before 
	 * calling this method (<b>Note:</b> Not yet implemented).<p>
	 * <b>Important:</b> This method should only be called once per attempt due to the 
	 * nature of creating an {@link AsyncTask}. Subsequent calls will cancel the previous
	 * network task and create a new task.  
	 * @param //path the path of the XML file to be fetched
	 * @param //callbacks the implemented interface that will complete the necessary actions
	 * @throws MalformedURLException if <code>path</code> cannot be parsed as as a URL
	 */
	public void fetchOnlineXml(URL url) throws Exception {
		if (mCallbacks == null) {
			throw new MissingCallbacksException("Cannot utilize Internet activities; no InternetCallbacks instance has been set");
		}
		
		if (mTask != null
				&& (mTask.getStatus().equals(AsyncTask.Status.RUNNING) || mTask.getStatus().equals(AsyncTask.Status.FINISHED))) {
			Log.i(TAG, "XMLCaller contructor cancel task");
			mTask.cancel(true);
			mTask = null;
		}
		
		mTask = new HttpTask();
		mTask.execute(url);
		
		if(mCallbacks instanceof BaseFragment){
			((BaseFragment) mCallbacks).setCurrentTask(mTask);
		}else if(mCallbacks instanceof CustomBaseAdapter){
			((CustomBaseAdapter) mCallbacks).setCurrentTask(mTask);
		}
		else if(mCallbacks instanceof BaseActivity){
			((BaseActivity) mCallbacks).setCurrentTask(mTask);
		}
	}

	/**
	 * Sets up the XML document for parsing and data extraction.
	 * @param xml the XML string to be parsed
	 * @throws ParserConfigurationException if the Document parser cannot be created
	 * @throws IOException If any IO error occurs
	 * @throws SAXException If any parsing error occurs
	 */
	public Document fetchParsedXml(String xml) throws ParserConfigurationException, IOException, SAXException {
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(xml.toString()));

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		//dbf.setCoalescing(true);
		return dbf.newDocumentBuilder().parse(is);
	}
	
	/**
	 * Retrieves the value of the provided {@link Node}. This is called as a
	 * static method.
	 * @param node the Node with the value to be extracted
	 * @return the parsed inner text of Node, or null if there is no value
	 */
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	public static String getNodeValue(Node node) {
		if (node != null && node.hasChildNodes()) {
			String value = "";
			for (Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
				if (child.getNodeType() == Node.TEXT_NODE) {
					value += child.getNodeValue();
				} else if (child.getNodeType() == Node.CDATA_SECTION_NODE) {
					value += child.getTextContent();
				}
			}
			
			if (!value.isEmpty()) {
				return value;
			}
		}
		
		return null;
	}
	
	/**
	 * Retrieves the list of attributes as a {@link HashMap}. This is called as a static
	 * method. 
	 * @param node the {@link Node} with the attributes to be extracted
	 * @return a hash map of <code>&lt;Attribute, Value&gt;</code> pairs, or null if no 
	 * attributes exists
	 */
	public static HashMap<String, String> getNodeAttributes(Node node) {
		if (!node.hasAttributes()) {
			return null;
		}
		
		HashMap<String, String> attrList = new HashMap<String, String>();
		NamedNodeMap nodeMap = node.getAttributes();
		for (int i = 0; i < nodeMap.getLength(); i++) {
			Node n = nodeMap.item(i);
			if (n != null && n.getNodeType() == Node.ATTRIBUTE_NODE) {
				attrList.put(n.getNodeName(), n.getNodeValue());
			}
		}
		
		return attrList;
	}
	
	private void createProgressDialogView(Context context) {
		/*vDialog = new Dialog(context, R.style.Dialog_Progress);
		vDialog.setContentView(R.layout.dialog_progress);
		vDialog.setCancelable(false);
		
		TextView msg = (TextView) vDialog.findViewById(R.id.dialog_text);
		Helper.setFonts(msg, context.getResources().getString(R.string.helvetica_bold));
		msg.setText("Loading...");
		vDialog.hide();*/
	}
	private static List<BasicNameValuePair> paramsToList(Bundle params) {
        ArrayList<BasicNameValuePair> formList = new ArrayList<BasicNameValuePair>(params.size());
        
        for (String key : params.keySet()) {
            Object value = params.get(key);
            
            // We can only put Strings in a form entity, so we call the toString()
            // method to enforce. We also probably don't need to check for null here
            // but we do anyway because Bundle.get() can return null.
            if (value != null) formList.add(new BasicNameValuePair(key, value.toString()));
        }
        
        return formList;
    }
	private class HttpTask extends AsyncTask<URL, Void, String> {

		private ProgressDialog mDialog;
		
		public HttpTask() {
			if(mCallbacks instanceof Activity){
				//mDialog = new ProgressDialog((Activity) mCallbacks);
				createProgressDialogView((Activity) mCallbacks);
			}
			/*if(mCallbacks instanceof CustomFragment){
				//mDialog = new ProgressDialog(((Fragment) mCallbacks).getActivity());
				createProgressDialogView(((Fragment) mCallbacks).getActivity());
			}
			if(mCallbacks instanceof IC_base){
				Log.i(TAG, "mcallbacks IC_base");
				mDialog = new ProgressDialog(((IC_base) mCallbacks).getC());
				((IC_base) mCallbacks).showProgress();
			}
			if(mCallbacks instanceof PromotionArea){
				Log.i(TAG, "mcallbacks PromotionArea");
				mDialog = new ProgressDialog(((PromotionArea) mCallbacks).getC());
				((PromotionArea) mCallbacks).showProgress();
			}
			else{
				//mDialog.setCancelable(false);
				//mDialog.setMessage("Loading...");
			}*/
		}
		
		/*private void addImageContent(Image[] files, DataOutputStream output) {
	        for(Image file : files) {
	            StringBuilder split = new StringBuilder();
	            split.append(twoHyphens + boundary + lineEnd);
	            split.append("Content-Disposition: form-data; name=\"" + file.getFormName() + "\"; filename=\"" + file.getFileName() + "\"" + lineEnd);
	            split.append("Content-Type: " + file.getContentType() + lineEnd);
	            split.append(lineEnd);
	            try {
	                // åš™è¸�è•­åš™è¸�è•­åš™è¸�è•­åš™è¸�è•­åš™è¸�è•­åš™è¸�è•­ï¿½ï¿½	                output.writeBytes(split.toString());
	                output.write(file.getData(), 0, file.getData().length);
	                output.writeBytes(lineEnd);
	            } catch (IOException e) {
	                throw new RuntimeException(e);
	            }
	        }
	    }*/
		
		private void addFormField(Set<Map.Entry<Object,Object>> params, DataOutputStream output) {
	        StringBuilder sb = new StringBuilder();
	        for(Map.Entry<Object, Object> param : params) {
	            sb.append(twoHyphens + boundary + lineEnd);
	            sb.append("Content-Disposition: form-data; name=\"" + param.getKey() + "\"" + lineEnd);
	            sb.append(lineEnd);
	            sb.append(param.getValue() + lineEnd);
	        }
	        try {
	            output.writeBytes(sb.toString());// åš™è¸�è•­åš™è¸�è•­åš™è³ªâ€�åš™è¸�è•­åš™è³¢ï¿½ç•¾è››è•­åš™è³£?ï¿½	        } catch (IOException e) {
	            //throw new RuntimeException(e);
	        } catch (Exception e) {}
	    }
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (!mCallbacks.hasInternet()) {
				//mCallbacks.onError(mFailMode, false);
				cancel(false);
				return;
			}
			/*if(mCallbacks instanceof IC_base){
				((IC_base) mCallbacks).showProgress();
			}
			else if(mCallbacks instanceof PromotionArea){
				((PromotionArea) mCallbacks).showProgress();
			}
			else if(mCallbacks instanceof CustomFragment){
				((CustomFragment) mCallbacks).showProgress();
			}
			else{
				//mDialog.show();
				vDialog.show();
			}*/
		}

		@Override
		protected String doInBackground(URL... urls) {
			String xml = "";
			for (URL url : urls) {
				try {
					if(requestMethod == "get"){
						if (url.getProtocol().equals("https")) {

							NoSSLv3SocketFactory sf2 = new NoSSLv3SocketFactory();
							HttpsURLConnection.setDefaultSSLSocketFactory(sf2);

							HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
							urlConnection.setRequestMethod("GET");
							if (urlConnection instanceof HttpsURLConnection) { // 是Https请求
								NoSSLv3SocketFactory sf = new NoSSLv3SocketFactory();
								((HttpsURLConnection) urlConnection).setSSLSocketFactory(sf);
							}
							urlConnection.setConnectTimeout(10000);
							urlConnection.setReadTimeout(60000);

							int responseCode = urlConnection.getResponseCode();
							if (responseCode == 200) { // 请求成功
								InputStream inputStream = urlConnection.getInputStream();
								BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
								StringBuilder result = new StringBuilder();
								String line;
								while((line = reader.readLine()) != null) {
									result.append(line);
								}
								xml = result.toString();

								reader.close();
								inputStream.close();
							}
							urlConnection.disconnect();
//
//							try {
//
//								KeyStore trustStore = KeyStore.getInstance(KeyStore
//										.getDefaultType());
//								trustStore.load(null, null);
//								SSLSocketFactory sf = new SSLSocketFactoryEx(trustStore);
//								sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);  //允许所有主机的验证
//
//								HttpParams params = new BasicHttpParams();
//
//								HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
//								HttpProtocolParams.setContentCharset(params,
//										HTTP.DEFAULT_CONTENT_CHARSET);
//								HttpProtocolParams.setUseExpectContinue(params, true);
//
//								SchemeRegistry registry = new SchemeRegistry();
//								registry.register(new Scheme("http", PlainSocketFactory
//										.getSocketFactory(), 80));
//								registry.register(new Scheme ("https", sf, 443));
//
//								ClientConnectionManager ccm = new ThreadSafeClientConnManager(
//										params, registry);
//
//								httpClient = new DefaultHttpClient(ccm, httpParameters);
//							} catch (KeyStoreException e) {
//								e.printStackTrace();
//							} catch (CertificateException e) {
//								e.printStackTrace();
//							} catch (NoSuchAlgorithmException e) {
//								e.printStackTrace();
//							} catch (KeyManagementException e) {
//								e.printStackTrace();
//							} catch (UnrecoverableKeyException e) {
//								e.printStackTrace();
//							}
						} else {


							HttpGet httpGet = new HttpGet(url.toString());

							//httpGet.addHeader("Authorization", "Basic " + Base64.encodeToString(("cmchan814" + ":" + "4d9f2992dfcda9943f3b3daa4abb16d380281169").getBytes(), Base64.NO_WRAP));

							HttpParams httpParameters = new BasicHttpParams();
							// Set the timeout in milliseconds until a connection is established.
							// The default value is zero, that means the timeout is not used.
							int timeoutConnection = 10000;
							HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
							// Set the default socket timeout (SO_TIMEOUT)
							// in milliseconds which is the timeout for waiting for data.
							int timeoutSocket = 60000;
							HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

							DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
							// Set up the credential provider for that server and port
							CredentialsProvider credentials = new BasicCredentialsProvider();
							credentials.setCredentials(new AuthScope("flightxml.flightaware.com", 80),
									new UsernamePasswordCredentials("cmchan814", "4d9f2992dfcda9943f3b3daa4abb16d380281169"));

							// and tell your client to use it
							httpClient.setCredentialsProvider(credentials);

							HttpResponse httpResponse = httpClient.execute(httpGet);
							HttpEntity httpEntity = httpResponse.getEntity();

							xml = EntityUtils.toString(httpEntity);

							httpGet.abort();
						}
					}
					else if(requestMethod == "post"){
						
						if(isUploadImage){
							try {
					            //ByteArrayOutputStream bos = new ByteArrayOutputStream();
					            //bm.compress(CompressFormat.JPEG, 75, bos);
					            //byte[] data = bos.toByteArray();
								HttpParams httpParameters = new BasicHttpParams();
								// Set the timeout in milliseconds until a connection is established.
								// The default value is zero, that means the timeout is not used. 
								int timeoutConnection = 30000;
								HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
								// Set the default socket timeout (SO_TIMEOUT) 
								// in milliseconds which is the timeout for waiting for data.
								int timeoutSocket = 100000;
								HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
								
					            HttpClient httpClient = new DefaultHttpClient(httpParameters);
					            HttpPost postRequest = new HttpPost(
					            		urls[0].toString());
					            ByteArrayBody bab = new ByteArrayBody(mParams.getByteArray("photodata"), "forest.jpg");
					            // File file= new File("/mnt/sdcard/forest.png");
					            // FileBody bin = new FileBody(file);
					            MultipartEntity reqEntity = new MultipartEntity(
					                    HttpMultipartMode.BROWSER_COMPATIBLE);
					            reqEntity.addPart("photo", bab);
					            reqEntity.addPart("userid", new StringBody(mParams.getString("userid")));
					            reqEntity.addPart("apptoken", new StringBody(mParams.getString("apptoken")));
					            reqEntity.addPart("createtime", new StringBody(mParams.getString("createtime")));
					            reqEntity.addPart("timezone", new StringBody(mParams.getInt("timezone")+""));
					            postRequest.setEntity(reqEntity);
					            
					            Log.i(TAG, "reqEntity = " + reqEntity.toString());
					            //reqEntity.toString()
					            HttpResponse response = httpClient.execute(postRequest);
					            BufferedReader reader = new BufferedReader(new InputStreamReader(
					                    response.getEntity().getContent(), "UTF-8"));
					            String sResponse;
					            StringBuilder s = new StringBuilder();
					 
					            while ((sResponse = reader.readLine()) != null) {
					                s = s.append(sResponse);
					            }
					            System.out.println("Response: " + s);
					            
					            xml = s.toString();
					        } catch (Exception e) {
					            // handle exception here
					            Log.e(e.getClass().getName(), e.getMessage());
					        }
						}
						else{
							Log.i(TAG, "using Post");
							HttpPost httppost = new HttpPost(urls[0].toString());
							
							httppost.addHeader("Authorization", "Basic " + Base64.encodeToString(("cmchan814" + ":" + "4d9f2992dfcda9943f3b3daa4abb16d380281169").getBytes(), Base64.NO_WRAP));
					        
							HttpParams httpParameters = new BasicHttpParams();
							// Set the timeout in milliseconds until a connection is established.
							// The default value is zero, that means the timeout is not used. 
							int timeoutConnection = 10000;
							HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
							// Set the default socket timeout (SO_TIMEOUT) 
							// in milliseconds which is the timeout for waiting for data.
							int timeoutSocket = 100000;
							HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
							if (mParams != null) {
		                        UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(paramsToList(mParams));
		                        Log.i(TAG, "paramsToList = " + paramsToList(mParams));
		                        httppost.setEntity(formEntity);
		                        //postRequest.setHeader("Content-Type", "application/x-www-form-urlencoded");
		                    }
							
							DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
							HttpResponse httpResponse = httpClient.execute(httppost);
							HttpEntity httpEntity = httpResponse.getEntity();
							
							/*// Set up the credential provider for that server and port
							CredentialsProvider credentials = new BasicCredentialsProvider();
							credentials.setCredentials(new AuthScope("flightxml.flightaware.com", 80),
							    new UsernamePasswordCredentials("cmchan814", "4d9f2992dfcda9943f3b3daa4abb16d380281169"));

							// and tell your client to use it
							httpClient.setCredentialsProvider(credentials);*/
							
							xml = EntityUtils.toString(httpEntity);
							
							httppost.abort();
						}
					}
					
				} catch (IOException e) {
					Log.e(TAG, "Error: " + e.getClass());
					if(SocketTimeoutException.class.isInstance(e)){
						mFailMode = FAIL_MODE_DISPLAY_RETRY_TIMEOUT;
					}

					return null;
				}
				
				
			}
			
			return xml;
		}

		@Override
		protected void onCancelled() {
			Log.i("XMLparser","onCancelled");
			super.onCancelled();
			/*if(mCallbacks instanceof IC_base){
				((IC_base) mCallbacks).hideProgress();
			}
			else if(mCallbacks instanceof PromotionArea){
				((PromotionArea) mCallbacks).hideProgress();
			}
			else if(mCallbacks instanceof CustomFragment){
				((CustomFragment) mCallbacks).hideProgress();
			}
			else{
				if(mDialog != null)
					mDialog.dismiss();
				if(vDialog != null)
					vDialog.dismiss();
			}*/
			mCallbacks.onError(mFailMode, false);
		}

		@Override
		protected void onPostExecute(String xml) {
			Log.i("XMLCaller","onPostExecute");
			super.onPostExecute(xml);
			/*if(mCallbacks instanceof IC_base){
				((IC_base) mCallbacks).hideProgress();
			}
			else if(mCallbacks instanceof PromotionArea){
				((PromotionArea) mCallbacks).hideProgress();
			}
			else if(mCallbacks instanceof CustomFragment){
				((CustomFragment) mCallbacks).hideProgress();
			}
			else{
				//mDialog.dismiss();
				vDialog.dismiss();
			}*/
			if (xml == null) {
				mCallbacks.onError(mFailMode, true);
				return;
			}
			
			if(mCallbacks instanceof CustomBaseAdapter){
				if(cellId == null)
					mCallbacks.postExecute(xml);
				else
					mCallbacks.postExecuteWithCellId(xml, cellId);
			}
			else{
				mCallbacks.postExecute(xml);	
			}
		}
	}



	private static class SSLSocketFactoryEx extends SSLSocketFactory {

		SSLContext sslContext = SSLContext.getInstance("TLS");

		public SSLSocketFactoryEx(KeyStore truststore)
				throws NoSuchAlgorithmException, KeyManagementException,
				KeyStoreException, UnrecoverableKeyException {
			super(truststore);

			TrustManager tm = new X509TrustManager() {

				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				@Override
				public void checkClientTrusted(
						java.security.cert.X509Certificate[] chain, String authType)
						throws java.security.cert.CertificateException {

				}

				@Override
				public void checkServerTrusted(
						java.security.cert.X509Certificate[] chain, String authType)
						throws java.security.cert.CertificateException {

				}
			};

			sslContext.init(null, new TrustManager[] { tm }, null);
		}

		@Override
		public Socket createSocket(Socket socket, String host, int port,
								   boolean autoClose) throws IOException, UnknownHostException {
			return sslContext.getSocketFactory().createSocket(socket, host, port,
					autoClose);
		}

		@Override
		public Socket createSocket() throws IOException {
			return sslContext.getSocketFactory().createSocket();
		}
	}

//	public static class TLSSocketFactory extends SSLSocketFactory {
//
//		private SSLSocketFactory internalSSLSocketFactory;
//
//		public TLSSocketFactory(SSLSocketFactory delegate) throws KeyManagementException, NoSuchAlgorithmException {
//			internalSSLSocketFactory = delegate;
//		}
//
//		@Override
//		public String[] getDefaultCipherSuites() {
//			return internalSSLSocketFactory.getDefaultCipherSuites();
//		}
//
//		@Override
//		public String[] getSupportedCipherSuites() {
//			return internalSSLSocketFactory.getSupportedCipherSuites();
//		}
//
//		@Override
//		public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
//			return enableTLSOnSocket(internalSSLSocketFactory.createSocket(s, host, port, autoClose));
//		}
//
//		@Override
//		public Socket createSocket(String host, int port) throws IOException, UnknownHostException {
//			return enableTLSOnSocket(internalSSLSocketFactory.createSocket(host, port));
//		}
//
//		@Override
//		public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException, UnknownHostException {
//			return enableTLSOnSocket(internalSSLSocketFactory.createSocket(host, port, localHost, localPort));
//		}
//
//		@Override
//		public Socket createSocket(InetAddress host, int port) throws IOException {
//			return enableTLSOnSocket(internalSSLSocketFactory.createSocket(host, port));
//		}
//
//		@Override
//		public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
//			return enableTLSOnSocket(internalSSLSocketFactory.createSocket(address, port, localAddress, localPort));
//		}
//
//    /*
//     * Utility methods
//     */
//
//		private static Socket enableTLSOnSocket(Socket socket) {
//			if (socket != null && (socket instanceof SSLSocket)
//					&& isTLSServerEnabled((SSLSocket) socket)) { // skip the fix if server doesn't provide there TLS version
//				((SSLSocket) socket).setEnabledProtocols(new String[]{"TLS_v1_2", "TLS_v1_1"});
//			}
//			return socket;
//		}
//
//		private static boolean isTLSServerEnabled(SSLSocket sslSocket) {
//			System.out.println("__prova__ :: " + sslSocket.getSupportedProtocols().toString());
//			for (String protocol : sslSocket.getSupportedProtocols()) {
//				if (protocol.equals("TLS_v1_1") || protocol.equals("TLS_v1_2")) {
//					return true;
//				}
//			}
//			return false;
//		}
//	}
}
