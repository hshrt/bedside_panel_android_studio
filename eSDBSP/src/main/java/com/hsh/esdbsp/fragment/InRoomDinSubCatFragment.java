package com.hsh.esdbsp.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.BaseActivity;
import com.hsh.esdbsp.adapter.dining.FoodSubCatAdapter;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.GeneralItem;

public class InRoomDinSubCatFragment extends BaseFragment{

	private final String TAG = "InRoomDinSubCatFragment";

	private View baseView;
	private ProgressBar mProgressBar;

	private ListView subCatList;
	private Button backBtn;
	
	private ArrayList<GeneralItem> foodSubCatArray;
	
	private FoodSubCatAdapter foodSubCatAdapter;

	public enum ApiCallType {
		GET_FLIGHT_STATUS, GET_FLIGHT_TRACK_BY_ID, GET_FLIGHT_TRACK_BY_DESTINATION, GET_ALL_AIRLINE, GET_ALL_AIRPORT
	}

	ApiCallType apiCallType;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView fire");
		baseView = inflater.inflate(R.layout.cms_in_room_sub_category_pch, container,
				false);
		BaseActivity parent = (BaseActivity) this.getActivity();
		mProgressBar = parent.getProgressBar();

		initUI();
		
		if(foodSubCatArray == null){
			foodSubCatArray = new ArrayList<GeneralItem>();
		}
		
		foodSubCatAdapter = new FoodSubCatAdapter(this.getActivity(), foodSubCatArray);
		subCatList.setAdapter(foodSubCatAdapter);
		
		return baseView;
	}
	
	public void setupList(ArrayList<GeneralItem> _foodSubCatArray){
		foodSubCatArray.clear();;
		
		for(GeneralItem item : _foodSubCatArray){
    		foodSubCatArray.add(item);
    	}
		
		Log.i(TAG,"fooSubCatArray length = " + foodSubCatArray.size());
		foodSubCatAdapter.setCurrentPosition(0);
    	foodSubCatAdapter.notifyDataSetChanged();
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}
	
	private void setupLabel(){
		
	}

	private void initUI() {
		subCatList = (ListView) baseView.findViewById(R.id.list);
		backBtn = (Button) baseView.findViewById(R.id.backBtn);		
		backBtn.setVisibility(View.INVISIBLE);
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mProgressBar.setVisibility(View.GONE);
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@Override
	public void onResume() {

		super.onResume();
	}

	@Override
	public void onPause() {

		super.onPause();
		cancelCurrentTask();
	}

	@Override
	public void onDestroyView() {
		Log.e(TAG, "onDestroyView");

		cancelCurrentTask();
		super.onDestroyView();

	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestroy");
		cancelCurrentTask();
		super.onDestroy();

	}

}
