package com.hsh.esdbsp.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.BaseActivity;
import com.hsh.esdbsp.activity.DVDActivity;
import com.hsh.esdbsp.activity.DVDBorrowingActivity;
import com.hsh.esdbsp.activity.DVDRecordActivity;
import com.hsh.esdbsp.activity.DemoTVActivity;
import com.hsh.esdbsp.activity.IBahnMovieActivity;
import com.hsh.esdbsp.activity.IBahnTVActivity;
import com.hsh.esdbsp.activity.LangActivity;
import com.hsh.esdbsp.activity.MovieActivity;
import com.hsh.esdbsp.activity.MusicActivity;
import com.hsh.esdbsp.activity.RadioActivity;
import com.hsh.esdbsp.activity.RoomConditionActivity;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.activity.TVActivity;
import com.hsh.esdbsp.activity.TVAppActivity;
import com.hsh.esdbsp.activity.WeatherActivity;
import com.hsh.esdbsp.activity.WorldWeatherActivity;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.activity.MessageActivity;
import com.hsh.esdbsp.activity.ServiceAirportActivity;
import com.hsh.esdbsp.activity.ServiceHouseKeepingActivity;
import com.hsh.esdbsp.activity.ServiceInRoomDinningActivity;
import com.hsh.esdbsp.activity.ServiceMainActivity;
import com.hsh.esdbsp.global.CheckMessageUpdateManager;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.BatteryChecker;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.model.Hotel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TopBarFragment extends BaseFragment {

    private final String TAG = "TopBarFragment";

    private View baseView;

    private String titleId;
    private String type = "";
    private int choice;
    private ArrayList<GeneralItem> choiceList;
    private boolean isIRD = false;

    private Boolean hideBackBtn; //should hide backBtn or not
    private Boolean invisbleBackBtn;
    private String hightLightChoice; // a string to store when button should be hightlighted

    private Button backBtn;
    private TextView title;
    private TextView headRC;
    private TextView headService;
    private TextView headDining;
    private TextView head_TV;
    private TextView headConcierge;
    private MyTextView headReviewOrder;
    private MyTextView headOrderHistory;

    private TextView msg_Text;
    private TextView langText;
    private TextView weatherText;

    private ImageView msgImage;
    private ImageView msgRedImage;
    private ImageView langImage;
    private ImageView weatherImage;

    //for flight stat
    private TextView flightInformation;
    private TextView flightTracking;

    private TextView weather;
    private TextView worldweather;
    private TextView roomCondition;

    //battery Image
    private ImageView imgBattery;

    private LinearLayout dynamicButtonContainer;

    // ////////////////////////////////////////////////////

    private BatteryChecker mBatteryChecker = null;
    private boolean isAlarmOn = false;
    private boolean isAlarmRinging = false;
    private String alarmSetTime = null;

    // for bsp sleep
    private WakeLock mWakeLock;
    private PowerManager powerManager;
    private boolean backMainTimerDisabled = false;

    private boolean isMsgRedShowing = false;

    // ////////////////////////////////////////////////////

    static Handler timeoutHandler = new Handler();

    private boolean isMessageOn = false;
    private boolean isRoomControl = false;
    private long timeoutInterval;

    private SharedPreferences settings;
    private int messageListenerPosition = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView fire");

        if (RoomControlActivity.class.isInstance(TopBarFragment.this.getActivity())) {
            isRoomControl = true;
            timeoutInterval = MainApplication.getConfig().getDimScreenInterval();
        } else {
            isRoomControl = false;
            timeoutInterval = MainApplication.getConfig().getBackToRoomControlInterval();
        }

        settings = MainApplication.getContext().getSharedPreferences(MainApplication.PREFS_NAME, 0);

        baseView = inflater.inflate(R.layout.top_bar, container, false);

        titleId = this.getArguments().getString("titleId");
        hideBackBtn = this.getArguments().getBoolean("hideBackBtn");
        invisbleBackBtn = this.getArguments().getBoolean("invisbleBackBtn");
        hightLightChoice = this.getArguments().getString("hightLightChoice");
        isIRD = this.getArguments().getBoolean("isIRD");

        if (this.getArguments().containsKey("type")) {
            type = this.getArguments().getString("type");
        }
        if (this.getArguments().containsKey("choiceList")) {
            choiceList = this.getArguments().getParcelableArrayList("choiceList");
        }
        choice = this.getArguments().getInt("choice", -1);

        Log.i(TAG, "titleId = " + titleId);

        //this is a handle to make sure the screen is always on for activity which are not room control
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        powerManager = (PowerManager) getActivity().getSystemService(MainApplication.getContext().POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getActivity().getClass().getName());
        mWakeLock.acquire();

        initBatteryListener();

        loadPageItem();

        //speical deal with room control page
        if (!isRoomControl) {
            loadPageLabel();
        }

        int pos = MainApplication.getMAS().getCurRoom();
        if ((this.getString(R.string.specialhandle).equalsIgnoreCase("pbj1360"))) {

            String maslrBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.maslr_base_dev) : this.getString(R.string.maslr_base);
            String masdrBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.masdr_base_dev) : this.getString(R.string.masdr_base);

            if ((pos == Integer.parseInt(maslrBase)) || (pos == Integer.parseInt(masdrBase))) {
                head_TV.setVisibility(View.GONE);
            }
        }

        //check the original flag blink or not
        initCheckMessageFlag();

        ((BaseActivity) getActivity()).isTopBarInit = true;

        return baseView;
    }

    @Override
    public void onResume() {

        addRunnableCall();

        initCheckMessageFlag();
        addMessageListener();

        super.onResume();
    }

    @Override
    public void onPause() {

        removeRunnableCall();

        if (messageListenerPosition >= 0) {
            CheckMessageUpdateManager.getInstance().removeMessageListener(messageListenerPosition);
            messageListenerPosition = -1;
        }
        super.onPause();
        cancelCurrentTask();
    }

    @Override
    public void onDestroyView() {
        Log.e(TAG, "onDestroyView");

        removeRunnableCall();

        if (messageListenerPosition >= 0) {
            CheckMessageUpdateManager.getInstance().removeMessageListener(messageListenerPosition);
            messageListenerPosition = -1;
        }

        cancelCurrentTask();
        super.onDestroyView();

        ((ViewGroup) baseView).removeAllViews();

        backBtn = null;
        title = null;
        headRC = null;
        headService = null;
        headDining = null;
        head_TV = null;
        headConcierge = null;
        headReviewOrder = null;
        headOrderHistory = null;

        msg_Text = null;
        langText = null;
        weatherText = null;

        msgImage = null;
        msgRedImage = null;
        langImage = null;
        weatherImage = null;

        //for flight stat
        flightInformation = null;
        flightTracking = null;
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        removeBatteryListener();
        cancelCurrentTask();
        if (mWakeLock != null)
            mWakeLock.release();
        super.onDestroy();
    }


    // ////////////////////////////////////////////////////////

    public void addRunnableCall() {
        timeoutHandler.removeCallbacks(timeoutRunnable);
        timeoutHandler.postDelayed(timeoutRunnable, timeoutInterval);
    }

    private void removeRunnableCall() {
        if (timeoutRunnable != null) {
            timeoutHandler.removeCallbacks(timeoutRunnable);
        }
    }

    // ////////////////////////////////////////////////////////

    private Runnable timeoutRunnable = new Runnable() {
        public void run() {

            timeoutHandler.removeCallbacks(timeoutRunnable);

            if (isRoomControl) {
                ((RoomControlActivity) getActivity()).sleepBSP();
            } else {

                Log.i(TAG, "highLightChoice = " + hightLightChoice);
                if ((hightLightChoice == null
                        || (hightLightChoice.compareToIgnoreCase("radio") != 0
                        && hightLightChoice.compareToIgnoreCase("music") != 0
                        && hightLightChoice.compareToIgnoreCase("setting") != 0))
                        && !backMainTimerDisabled) {

                    ((BaseActivity) getActivity()).goMain();
                }
            }
        }
    };

    // ////////////////////////////////////////////////////////


    public void loadPageLabel() {

        try {

            // change all label
            if (hightLightChoice != null && (hightLightChoice.compareToIgnoreCase("weather") == 0 || hightLightChoice.compareToIgnoreCase("world weather") == 0
                    || hightLightChoice.compareToIgnoreCase("room condition") == 0)) {
                Log.i(TAG, "weather case");
                weather.setVisibility(View.VISIBLE);
                worldweather.setVisibility(View.VISIBLE);
                roomCondition.setVisibility(View.VISIBLE);

                if (this.getString(R.string.forDemoRoom).equalsIgnoreCase("0")) {
                    roomCondition.setVisibility(View.GONE);
                }

                weather.setText(Html.fromHtml(MainApplication.getLabel("Weather").toUpperCase()));
                worldweather.setText(Html.fromHtml(MainApplication.getLabel("World Weather").toUpperCase()));
                roomCondition.setText(Html.fromHtml(MainApplication.getLabel("Room Condition").toUpperCase()));

                flightInformation.setVisibility(View.GONE);
                flightTracking.setVisibility(View.GONE);

                title.setVisibility(View.GONE);
                headRC.setVisibility(View.GONE);
                headService.setVisibility(View.GONE);
                headDining.setVisibility(View.GONE);
                head_TV.setVisibility(View.GONE);
                headConcierge.setVisibility(View.GONE);

                msg_Text.setVisibility(View.VISIBLE);
                langText.setVisibility(View.VISIBLE);
                weatherText.setVisibility(View.VISIBLE);

                if (hightLightChoice.compareToIgnoreCase("weather") == 0) {
                    weather.setBackgroundResource(R.drawable.actionbar_selected);
                    worldweather.setBackgroundResource(0);
                    roomCondition.setBackgroundResource(0);
                } else if (hightLightChoice.compareToIgnoreCase("world weather") == 0) {
                    worldweather.setBackgroundResource(R.drawable.actionbar_selected);
                    weather.setBackgroundResource(0);
                    roomCondition.setBackgroundResource(0);

                } else if (hightLightChoice.compareToIgnoreCase("room condition") == 0) {
                    roomCondition.setBackgroundResource(R.drawable.actionbar_selected);
                    worldweather.setBackgroundResource(0);
                    weather.setBackgroundResource(0);
                }

                weather.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        MainApplication.playClickSound(headRC);
                        Intent intent = new Intent(TopBarFragment.this
                                .getActivity().getApplicationContext(),
                                WeatherActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();

                    }
                });
                worldweather.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        MainApplication.playClickSound(headRC);
                        Intent intent = new Intent(TopBarFragment.this
                                .getActivity().getApplicationContext(),
                                WorldWeatherActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();

                    }
                });
                roomCondition.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        MainApplication.playClickSound(headRC);
                        Log.i(TAG, "go room condition page");
                        Intent intent = new Intent(TopBarFragment.this
                                .getActivity().getApplicationContext(),
                                RoomConditionActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();

                    }
                });

            }
            //for case of Airport
            else if (titleId != null && titleId.compareToIgnoreCase("Flight") == 0) {

                flightInformation.setVisibility(View.VISIBLE);
                flightTracking.setVisibility(View.VISIBLE);

                title.setVisibility(View.GONE);
                headRC.setVisibility(View.GONE);
                headService.setVisibility(View.GONE);
                headDining.setVisibility(View.GONE);
                head_TV.setVisibility(View.GONE);
                headConcierge.setVisibility(View.GONE);


                flightInformation.setText(Html.fromHtml(MainApplication.getLabel("flightstats.name.label").toUpperCase()));
                flightTracking.setText(Html.fromHtml(MainApplication.getLabel("flighttrack.name.label").toUpperCase()));

                if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
                    flightTracking.setVisibility(View.GONE);
                }

            } else if (titleId != null && titleId.compareToIgnoreCase("TV/AUDIO") == 0) {

                title.setVisibility(View.GONE);
                headRC.setVisibility(View.GONE);
                headService.setVisibility(View.GONE);
                headDining.setVisibility(View.GONE);
                head_TV.setVisibility(View.GONE);
                headConcierge.setVisibility(View.GONE);


                //String[] itemList = {"TV","Radio"};
                String[] itemList = {"TV", "Movie", "Radio", "Music", "Multimedia"};
                String[] itemListPCH = {"TV", "Radio", "Music"};
                String[] itemList2 = {"TV", "Movie", "Radio", "Music"}; // special handle for pbj av room
                String[] itemListPBH = {"Radio"};
                String[] itemListPHK = {"TV", "Movie", "Radio", "Music"};
                String[] itemListPBJ = {"TV", "Movie", "Radio", "Music", "Multimedia"};
                String[] itemListPSH = {"TV", "Movie", "Radio", "Music", "Multimedia"};

                String[] itemListDemo = {"TV", "TV Apps", "Radio", "Music"};

                String[] itemList_DVD = {"TV", "Movie", "DVD", "Radio", "Music", "Multimedia"};
                String[] itemListPCH_DVD = {"TV", "Radio", "DVD", "Music"};
                String[] itemList2_DVD = {"TV", "Movie", "DVD", "Radio", "Music"}; // special handle for pbj av room
                String[] itemListPBH_DVD = {"DVD", "Radio"};
                String[] itemListPHK_DVD = {"TV", "Movie", "DVD", "Radio", "Music"};
                String[] itemListPBJ_DVD = {"TV", "Movie", "DVD", "Radio", "Music", "Multimedia"};
                String[] itemListPSH_DVD = {"TV", "Movie", "DVD", "Radio", "Music", "Multimedia"};

                String[] itemListDemo_DVD = {"TV", "DVD", "TV Apps", "Radio", "Music"};

                //String[] itemList2  = {"TV","Radio"};
                //String[] itemList2  = {"TV","Radio","Movie", "Multimedia"};

                // special handle for pbj av room
                int pos = MainApplication.getMAS().getCurRoom();
                //Log.v("topbar","pos="+pos);

                boolean isDVDEnable = MainApplication.getLabel("service.dvdborrowingsystem.enabled").equals("1") ? true : false;

                if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
                    if (isDVDEnable) {
                        itemList = itemListPCH_DVD;
                    } else {
                        itemList = itemListPCH;
                    }
                } else if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                    if (isDVDEnable) {
                        itemList = itemListPBH_DVD;
                    } else {
                        itemList = itemListPBH;
                    }
                } else if (this.getString(R.string.forDemoRoom).equalsIgnoreCase("1")) {
                    Log.d("topbar", "use itemListDemo");
                    if (isDVDEnable) {
                        itemList = itemListDemo_DVD;
                    } else {
                        itemList = itemListDemo;
                    }
                } else if (GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                    if (isDVDEnable) {
                        itemList = itemListPHK_DVD;
                    } else {
                        itemList = itemListPHK;
                    }
                } else if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
                    if (isDVDEnable) {
                        itemList = itemListPBJ_DVD;
                    } else {
                        itemList = itemListPBJ;
                    }
                } else if (GlobalValue.getInstance().getHotel().equals(Hotel.SHANGHAI)) {
                    if (isDVDEnable) {
                        itemList = itemListPSH_DVD;
                    } else {
                        itemList = itemListPSH;
                    }
                }

                dynamicButtonContainer.removeAllViews();
                Log.d("topbar", "pos=" + itemList.length);
                resetHighLight();
                for (int x = 0; x < itemList.length; x++) {
                    Log.d("topbar", itemList[x]);
                    MyTextView textView = new MyTextView(this.getActivity());
                    textView.setTag(itemList[x]);
                    //textView.setText(MainApplication.getLabel(itemList[x]).toUpperCase());

                    textView.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel(itemList[x]).toUpperCase())));

                    LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
                    llp.setMargins(Helper.getPixelFromDp(30), 0, 0, 0);
                    llp.gravity = Gravity.CENTER;
                    textView.setLayoutParams(llp);

                    textView.setGravity(Gravity.CENTER);
                    textView.setTextSize(Helper.getPixelFromDp((int) Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize)));
                    Helper.setAppFonts(textView);

                    dynamicButtonContainer.addView(textView);

                    textView.setBackgroundColor(getResources().getColor(R.color.black));


                    if (choice == x) {
                        textView.setBackgroundResource(R.drawable.actionbar_selected);
                    }

                    textView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub

                            if (v.getTag() != null) {
                                Log.i(TAG, "TAG = " + v.getTag());
                            }

                            if (v.getTag() == null) {
                                return;
                            }

                            if (v.getTag().toString().equalsIgnoreCase("TV")) {


                                //MainApplication.playClickSound(baseView);
                                Class tvActivityClass = TVActivity.class;
                                if ("1".equals(getString(R.string.forDemoRoom))) {

                                    tvActivityClass = DemoTVActivity.class;
                                    Log.d("TAG", "using demo activity");

                                } else if ("ibahn".equals(getString(R.string.tvSystem))) {
                                    tvActivityClass = IBahnTVActivity.class;
                                }

                                Intent intent = new Intent(TopBarFragment.this
                                        .getActivity().getApplicationContext(),
                                        tvActivityClass);

                                MainApplication.getCurrentActivity().startActivity(intent);
                                MainApplication.getCurrentActivity().finish();
                            } else if (v.getTag().toString().equalsIgnoreCase("Radio")) {
                                //MainApplication.playClickSound(baseView);
                                Log.i(TAG, "current activity = " + MainApplication.getCurrentActivity().getClass().toString());
                                if (false == MainApplication.getCurrentActivity() instanceof RadioActivity) {
                                    Intent intent = new Intent(TopBarFragment.this
                                            .getActivity().getApplicationContext(),
                                            RadioActivity.class);

                                    MainApplication.getCurrentActivity().startActivity(intent);
                                    MainApplication.getCurrentActivity().finish();
                                }
                            } else if (v.getTag().toString().equalsIgnoreCase("Music")) {
                                //MainApplication.playClickSound(baseView);
                                Intent intent = new Intent(TopBarFragment.this
                                        .getActivity().getApplicationContext(),
                                        MusicActivity.class);

                                MainApplication.getCurrentActivity().startActivity(intent);
                                MainApplication.getCurrentActivity().finish();
                            } else if (v.getTag().toString().equalsIgnoreCase("Movie")) {
                                //MainApplication.playClickSound(head_TV);
                                Class movieActivityClass = MovieActivity.class;
                                if ("ibahn".equals(getString(R.string.movieSystem))) {
                                    movieActivityClass = IBahnMovieActivity.class;
                                }
                                Intent intent = new Intent(TopBarFragment.this
                                        .getActivity().getApplicationContext(),
                                        movieActivityClass);

                                MainApplication.getCurrentActivity().startActivity(
                                        intent);
                                MainApplication.getCurrentActivity().finish();
                            } else if (v.getTag().toString().equalsIgnoreCase("Multimedia")) {
                                //MainApplication.playClickSound(head_TV);
                                Intent intent = new Intent(TopBarFragment.this
                                        .getActivity().getApplicationContext(),
                                        DVDActivity.class);

                                MainApplication.getCurrentActivity().startActivity(
                                        intent);
                                MainApplication.getCurrentActivity().finish();
                            } else if (v.getTag().toString().equalsIgnoreCase("TV Apps")) {
                                //MainApplication.playClickSound(head_TV);
                                Intent intent = new Intent(TopBarFragment.this
                                        .getActivity().getApplicationContext(),
                                        TVAppActivity.class);

                                MainApplication.getCurrentActivity().startActivity(
                                        intent);
                                MainApplication.getCurrentActivity().finish();
                            } else if (v.getTag().toString().equalsIgnoreCase("DVD")) {
                                //MainApplication.playClickSound(head_TV);
                                Intent intent = new Intent(TopBarFragment.this
                                        .getActivity().getApplicationContext(),
                                        DVDBorrowingActivity.class);

                                MainApplication.getCurrentActivity().startActivity(
                                        intent);
                                MainApplication.getCurrentActivity().finish();
                            }

                        }
                    });

                }
            } else if (titleId != null && titleId.compareToIgnoreCase("REVIEW ORDER") == 0) {
                title.setVisibility(View.GONE);
                headRC.setVisibility(View.GONE);
                headService.setVisibility(View.GONE);
                headDining.setVisibility(View.GONE);
                head_TV.setVisibility(View.GONE);
                headConcierge.setVisibility(View.GONE);
                headReviewOrder.setVisibility(View.VISIBLE);
                headReviewOrder.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel(titleId).toUpperCase())));

                headReviewOrder.setBackgroundResource(R.drawable.actionbar_selected);
            } else if (titleId != null && isIRD) {
                title.setVisibility(View.VISIBLE);
                headRC.setVisibility(View.GONE);
                headService.setVisibility(View.GONE);
                headDining.setVisibility(View.GONE);
                head_TV.setVisibility(View.GONE);
                headConcierge.setVisibility(View.GONE);
                title.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel(titleId).toUpperCase())));

                headOrderHistory.setVisibility(View.VISIBLE);

                if (this.getString(R.string.hasInRoomDiningOrder).equalsIgnoreCase("0")) {
                    headOrderHistory.setVisibility(View.GONE);
                }

                if (GlobalValue.getInstance().checkIfThereIsOneCanOrder() == false) {
                    headOrderHistory.setVisibility(View.GONE);
                }

                headOrderHistory.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel("Order History").toUpperCase())));
            } else if (titleId != null && titleId.compareToIgnoreCase("DVD") == 0) {

                title.setVisibility(View.GONE);
                headRC.setVisibility(View.GONE);
                headService.setVisibility(View.GONE);
                headDining.setVisibility(View.GONE);
                head_TV.setVisibility(View.GONE);
                headConcierge.setVisibility(View.GONE);


                String[] itemList = {"DVD", "RECORD"};

                dynamicButtonContainer.removeAllViews();
                Log.d("topbar", "pos=" + itemList.length);

                for (int x = 0; x < itemList.length; x++) {
                    Log.d("topbar", itemList[x]);
                    MyTextView textView = new MyTextView(this.getActivity());
                    textView.setTag(x);

                    textView.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel(itemList[x]).toUpperCase())));

                    LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
                    llp.setMargins(Helper.getPixelFromDp(30), 0, 0, 0);
                    llp.gravity = Gravity.CENTER;
                    textView.setLayoutParams(llp);

                    textView.setGravity(Gravity.CENTER);

                    textView.setTextSize(Helper.getPixelFromDp((int) Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize)));
                    Helper.setAppFonts(textView);

                    dynamicButtonContainer.addView(textView);

                    textView.setBackgroundColor(getResources().getColor(R.color.black));


                    if (choice == x) {
                        textView.setBackgroundResource(R.drawable.actionbar_selected);
                    }

                    textView.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            if (v.getTag() != null) {
                                Log.i(TAG, "TAG = " + v.getTag());
                                choice = (int) v.getTag();
                            }

                            if (v.getTag() == null) {
                                return;
                            }

                            if ((int) v.getTag() == 0) {
                                MainApplication.playClickSound(baseView);
                                Class dVDBorrwoingActivityClass = DVDBorrowingActivity.class;
                                Intent intent = new Intent(TopBarFragment.this
                                        .getActivity().getApplicationContext(),
                                        dVDBorrwoingActivityClass);

                                MainApplication.getCurrentActivity().startActivity(
                                        intent);
                                MainApplication.getCurrentActivity().finish();

                            } else if ((int) v.getTag() == 1) {
                                MainApplication.playClickSound(baseView);
                                Class dVDRecordActivityClass = DVDRecordActivity.class;
                                Intent intent = new Intent(TopBarFragment.this
                                        .getActivity().getApplicationContext(),
                                        dVDRecordActivityClass);

                                MainApplication.getCurrentActivity().startActivity(
                                        intent);
                                MainApplication.getCurrentActivity().finish();
                            }
                        }

                    });

                }
            } else if (titleId != null) {
                title.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel(titleId).toUpperCase())));
                //title.setText("                ");
                title.setBackgroundResource(R.drawable.actionbar_selected);

                headRC.setVisibility(View.GONE);
                headService.setVisibility(View.GONE);
                headDining.setVisibility(View.GONE);
                head_TV.setVisibility(View.GONE);
                headConcierge.setVisibility(View.GONE);


            } else {
                title.setVisibility(View.GONE);

                headRC.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel("RC")).toUpperCase()));

                headService.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel("Services")
                        .toUpperCase())));
                headDining.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel("dining")
                        .toUpperCase())));
                head_TV.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel("TV").toUpperCase() + " / " + MainApplication.getLabel("AUDIO").toUpperCase())));
                headConcierge.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel("concierge")
                        .toUpperCase())));

                if (this.getActivity().getClass().isInstance(MessageActivity.class)) {
                    headRC.setTextSize(40);
                }


                //for PBH, there is TV function
                if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                    //head_TV.setVisibility(View.GONE);
                }

                if (this.getString(R.string.hastvpage).equalsIgnoreCase("notv")) {
                    head_TV.setVisibility(View.GONE);
                }
            }

            if (type.compareToIgnoreCase("Guest request") == 0) {
                title.setVisibility(View.GONE);
                headRC.setVisibility(View.GONE);
                headService.setVisibility(View.GONE);
                headDining.setVisibility(View.GONE);
                head_TV.setVisibility(View.GONE);
                headConcierge.setVisibility(View.GONE);


                if (choiceList != null) {
                    dynamicButtonContainer.removeAllViews();
                    for (int x = 0; x < choiceList.size(); x++) {


                        GeneralItem item = choiceList.get(x);

                        MyTextView tv = new MyTextView(this.getActivity());
                        tv.setTag(x);
                        tv.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel(item.getTitleId()).toUpperCase())));
                        LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
                        llp.setMargins(Helper.getPixelFromDp(30), 0, 0, 0);
                        llp.gravity = Gravity.CENTER;
                        tv.setLayoutParams(llp);

                        tv.setGravity(Gravity.CENTER);

                        tv.setTextSize(Helper.getPixelFromDp((int) Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize)));
                        Helper.setAppFonts(tv);

                        dynamicButtonContainer.addView(tv);

                        if (choice == x) {
                            tv.setBackgroundResource(R.drawable.actionbar_selected);
                        }

                        tv.setOnClickListener(new OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                ServiceHouseKeepingActivity shkActivity = (ServiceHouseKeepingActivity) (TopBarFragment.this.getActivity());
                                Log.i(TAG, "tag = " + (int) v.getTag());
                                shkActivity.refreshWithChoice((int) v.getTag());
                                choice = (int) v.getTag();

                                v.setBackgroundResource(R.drawable.actionbar_selected);

                            }
                        });
                    }
                }
            }


            //Log.i(TAG, "backBtn visiblity = " + backBtn.getVisibility());


            if ((MainApplication.getMAS().getData("data_language").equalsIgnoreCase("P") || DataCacheManager.getInstance().getLang().toString().equalsIgnoreCase("P"))
                    ) {

                int portuguesefontsize = getResources().getInteger(R.integer.top_bar_portuguese_font_size);

                headRC.setTextSize(portuguesefontsize);
                headService.setTextSize(portuguesefontsize);
                headDining.setTextSize(portuguesefontsize);
                head_TV.setTextSize(portuguesefontsize);
                headConcierge.setTextSize(portuguesefontsize);
            }

            //if there is backBtn here, we will set the Font Size smaller for the "Room Control, Service, Dining, TV/Audio, Concierge"
            if (backBtn.getVisibility() == View.VISIBLE) {

                int smallfontsize = getResources().getInteger(R.integer.top_bar_smaller_font_size);

                headRC.setTextSize(smallfontsize);
                headService.setTextSize(smallfontsize);
                headDining.setTextSize(smallfontsize);
                head_TV.setTextSize(smallfontsize);
                headConcierge.setTextSize(smallfontsize);
            }

            Helper.setAppFonts(title);
            Helper.setAppFonts(headRC);
            Helper.setAppFonts(headService);
            Helper.setAppFonts(headDining);
            Helper.setAppFonts(head_TV);
            Helper.setAppFonts(headConcierge);
            Helper.setAppFonts(msg_Text);
            Helper.setAppFonts(langText);
            Helper.setAppFonts(weatherText);
            Helper.setAppFonts(flightInformation);
            Helper.setAppFonts(flightTracking);
            Helper.setAppFonts(weather);
            Helper.setAppFonts(worldweather);
            Helper.setAppFonts(roomCondition);

            msg_Text.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel("Messages").toUpperCase())));
            langText.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel("Lang").toUpperCase())));
            weatherText.setText(Html.fromHtml(Helper.enlargeFirstLetter(MainApplication.getLabel("Weather").toUpperCase())));

        } catch (Exception e) {
            Log.i(TAG, "set label error = ");
            e.printStackTrace();
        }
        ;
    }

    private void resetHighLight() {
        headService.setBackgroundResource(0);
        head_TV.setBackgroundResource(0);
        headConcierge.setBackgroundResource(0);
        headDining.setBackgroundResource(0);
        headRC.setBackgroundResource(0);
    }

    private void loadPageItem() {

        try {

            imgBattery = (ImageView) baseView.findViewById(R.id.headBattery);
            dynamicButtonContainer = (LinearLayout) baseView.findViewById(R.id.dynamicButtonContainer);
            langText = (TextView) baseView.findViewById(R.id.langText);
            msg_Text = (TextView) baseView.findViewById(R.id.msg_Text);
            weatherText = (TextView) baseView.findViewById(R.id.weatherText);
            langImage = (ImageView) baseView.findViewById(R.id.langImage);
            msgImage = (ImageView) baseView.findViewById(R.id.msgImage);
            msgRedImage = (ImageView) baseView.findViewById(R.id.msgRedImage);
            weatherImage = (ImageView) baseView.findViewById(R.id.weatherImage);

            if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
                langImage.setImageResource(R.drawable.usa_bw);
            }
            if (GlobalValue.getInstance().getHotel().equals(Hotel.LONDON)) {
                langImage.setImageResource(R.drawable.lang);
            }

            weather = (TextView) baseView.findViewById(R.id.weather);
            worldweather = (TextView) baseView.findViewById(R.id.worldweather);
            roomCondition = (TextView) baseView.findViewById(R.id.roomCondition);

            worldweather.setBackgroundResource(0);

            flightInformation = (TextView) baseView.findViewById(R.id.flightInformation);
            flightTracking = (TextView) baseView.findViewById(R.id.flightTracking);

            flightInformation.setBackgroundResource(R.drawable.actionbar_selected);
            flightTracking.setBackgroundResource(0);

            flightInformation.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    ServiceAirportActivity s = (ServiceAirportActivity) TopBarFragment.this.getActivity();
                    s.onFlightInfoPressed();

                    flightInformation.setBackgroundResource(R.drawable.actionbar_selected);
                    flightTracking.setBackgroundResource(0);
                }
            });

            flightTracking.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    ServiceAirportActivity s = (ServiceAirportActivity) TopBarFragment.this.getActivity();
                    s.onFlightTrackPressed();
                    // TopBarFragment.this.getActivity().onBackPressed();

                    flightTracking.setBackgroundResource(R.drawable.actionbar_selected);
                    flightInformation.setBackgroundResource(0);
                }
            });

            backBtn = (Button) baseView.findViewById(R.id.backBtn);

            if (hideBackBtn) {
                backBtn.setVisibility(View.GONE);
            }

            if (invisbleBackBtn) {
                //backBtn.setVisibility(View.INVISIBLE);
                //backBtn.setEnabled(false);
                //((ImageView)baseView.findViewById(R.id.backBtnImage)).setVisibility(View.INVISIBLE);
            }

            backBtn.setOnClickListener(new OnClickListener() {


                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    if (RadioActivity.class.isInstance(TopBarFragment.this.getActivity())) {
                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), RoomControlActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();
                    } else if (TVActivity.class.isInstance(TopBarFragment.this.getActivity()) ||
                            DemoTVActivity.class.isInstance(TopBarFragment.this.getActivity()) ||
                            IBahnTVActivity.class.isInstance(TopBarFragment.this.getActivity())) {

                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), RoomControlActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();

                    } else if (DVDActivity.class.isInstance(TopBarFragment.this.getActivity())) {

                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), RoomControlActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();

                    } else if (MusicActivity.class.isInstance(TopBarFragment.this.getActivity())) {

                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), RoomControlActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();

                    } else if (MovieActivity.class.isInstance(TopBarFragment.this.getActivity()) ||
                            IBahnMovieActivity.class.isInstance(TopBarFragment.this.getActivity())) {

                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), RoomControlActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();

                    } else if (TVAppActivity.class.isInstance(TopBarFragment.this.getActivity())) {

                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), RoomControlActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();

                    } else if (DVDBorrowingActivity.class.isInstance(TopBarFragment.this.getActivity())) {

                        Class tvActivityClass = TVActivity.class;
                        if ("1".equals(getString(R.string.forDemoRoom))) {
                            tvActivityClass = DemoTVActivity.class;
                        } else if ("ibahn".equals(getString(R.string.tvSystem))) {
                            tvActivityClass = IBahnTVActivity.class;
                        }

                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), tvActivityClass);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();

                    } else if (DVDRecordActivity.class.isInstance(TopBarFragment.this.getActivity())) {

                        Class tvActivityClass = TVActivity.class;
                        if ("1".equals(getString(R.string.forDemoRoom))) {
                            tvActivityClass = DemoTVActivity.class;
                        } else if ("ibahn".equals(getString(R.string.tvSystem))) {
                            tvActivityClass = IBahnTVActivity.class;
                        }

                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), tvActivityClass);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();
                    }
                    /*else if(ServiceGeneralItemActivity.class.isInstance(TopBarFragment.this.getActivity())){
                        ((ServiceGeneralItemActivity)(TopBarFragment.this.getActivity())).clearFragment();
					}*/

                    Log.i(TAG, "Class = " + TopBarFragment.this.getActivity().getClass().toString());
                    TopBarFragment.this.getActivity().onBackPressed();
                    // TopBarFragment.this.getActivity().onBackPressed();

                }
            });

            title = (TextView) baseView.findViewById(R.id.title);
            headService = (TextView) baseView.findViewById(R.id.headService);

            headDining = (TextView) baseView.findViewById(R.id.headDining);
            headDining.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        if (MainApplication.getCurrentActivity() instanceof ServiceMainActivity) {
                            ServiceMainActivity a = (ServiceMainActivity) MainApplication.getCurrentActivity();
                            a.refreshData(1);
                            hightLightChoice = "dining";
                            resetHighLight();
                            headDining.setBackgroundResource(R.drawable.actionbar_selected);
                        } else {
                            MainApplication.playClickSound(baseView);
                            Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), ServiceMainActivity.class);
                            intent.putExtra("rootId", 1);
                            MainApplication.getCurrentActivity().startActivity(intent);
                            MainApplication.getCurrentActivity().finish();
                        }

                    } catch (Exception e) {
                    }
                }
            });
            // ////////////////////////////////////////////////////////

            headRC = (TextView) baseView.findViewById(R.id.headRoomControl);
            headRC.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (isRoomControl) {
                        ((RoomControlActivity) getActivity()).showRC();
                    } else {
                        MainApplication.playClickSound(headRC);
                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), RoomControlActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();
                    }
                }

            });

            headService.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (MainApplication.getCurrentActivity() instanceof ServiceMainActivity) {
                        ServiceMainActivity a = (ServiceMainActivity) MainApplication.getCurrentActivity();
                        a.refreshData(0);
                        hightLightChoice = "service";
                        resetHighLight();
                        headService.setBackgroundResource(R.drawable.actionbar_selected);
                    } else {
                        MainApplication.playClickSound(baseView);
                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), ServiceMainActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();
                    }
                }

            });

            headReviewOrder = (MyTextView) baseView.findViewById(R.id.headReviewOrder);

            head_TV = (TextView) baseView.findViewById(R.id.head_TV);
            head_TV.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (MainApplication.getCurrentActivity() instanceof RadioActivity) {

                    } else {
                        MainApplication.playClickSound(baseView);
                        Class tvActivityClass = TVActivity.class;

                        if ("1".equals(getString(R.string.forDemoRoom))) {
                            tvActivityClass = DemoTVActivity.class;
                        } else if ("ibahn".equals(getString(R.string.tvSystem))) {
                            tvActivityClass = IBahnTVActivity.class;
                        }

                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), tvActivityClass);

                        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                            intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), RadioActivity.class);
                        }
                        Activity LastActivity = MainApplication.getCurrentActivity();
                        MainApplication.getCurrentActivity().startActivity(intent);
                        LastActivity.finish();
                    }
                }

            });

            headConcierge = (TextView) baseView.findViewById(R.id.headConcierge);
            headConcierge.setVisibility(View.VISIBLE);
            headConcierge.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (MainApplication.getCurrentActivity() instanceof ServiceMainActivity) {
                        ServiceMainActivity a = (ServiceMainActivity) MainApplication.getCurrentActivity();
                        a.refreshData(2);
                        hightLightChoice = "concierge";
                        resetHighLight();
                        headConcierge.setBackgroundResource(R.drawable.actionbar_selected);
                    } else {
                        MainApplication.playClickSound(headConcierge);
                        Intent intent = new Intent(TopBarFragment.this.getActivity().getApplicationContext(), ServiceMainActivity.class);
                        intent.putExtra("rootId", 2);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();
                    }
                }

            });
            headOrderHistory = (MyTextView) baseView.findViewById(R.id.headOrderHistory);
            resetHighLight();
            if (isIRD) {
                title.setBackgroundResource(R.drawable.actionbar_selected);
                title.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        ServiceInRoomDinningActivity s = (ServiceInRoomDinningActivity) TopBarFragment.this.getActivity();
                        s.onTopBarMainPressed();
                        title.setBackgroundResource(R.drawable.actionbar_selected);
                        headOrderHistory.setBackgroundResource(0);
                    }
                });
                headOrderHistory.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ServiceInRoomDinningActivity s = (ServiceInRoomDinningActivity) TopBarFragment.this.getActivity();
                        s.onTopBarHistoryPressed();
                        headOrderHistory.setBackgroundResource(R.drawable.actionbar_selected);
                        title.setBackgroundResource(0);
                    }
                });
            }


            msgRedImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainApplication.playClickSound(baseView);
                    Intent intent = new Intent(MainApplication.getContext(), MessageActivity.class);
                    MainApplication.getCurrentActivity().startActivity(intent);
                }
            });

            msgImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainApplication.playClickSound(baseView);
                    Intent intent = new Intent(MainApplication.getContext(), MessageActivity.class);
                    MainApplication.getCurrentActivity().startActivity(intent);
                }

            });


            langImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    MainApplication.playClickSound(baseView);
                    if (MainApplication.getCurrentActivity() instanceof LangActivity) {

                    } else {
                        Intent intent = new Intent(MainApplication.getContext(), LangActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);
                        MainApplication.getCurrentActivity().finish();
                    }
                }

            });

            // ///////////////


            weatherImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    MainApplication.playClickSound(baseView);
                    if (MainApplication.getCurrentActivity() instanceof WeatherActivity) {

                    } else {
                        Intent intent = new Intent(MainApplication.getContext(), WeatherActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);

                    }
                }

            });

            // /////////////////////////////////////////////////////////

            if (hightLightChoice.compareToIgnoreCase("service") == 0) {
                headService.setBackgroundResource(R.drawable.actionbar_selected);
            } else if (hightLightChoice.compareToIgnoreCase("rc") == 0) {
                headRC.setBackgroundResource(R.drawable.actionbar_selected);
            } else if (hightLightChoice.compareToIgnoreCase("tv") == 0) {
                head_TV.setBackgroundResource(R.drawable.actionbar_selected);
            } else if (hightLightChoice.compareToIgnoreCase("concierge") == 0) {
                headConcierge.setBackgroundResource(R.drawable.actionbar_selected);
            } else if (hightLightChoice.compareToIgnoreCase("dining") == 0) {
                headDining.setBackgroundResource(R.drawable.actionbar_selected);
            }

            // /////////////////////////////////////////////////////////

        } catch (Exception e) {
        }

    }

    private void initCheckMessageFlag() {
        setMsgIcon(CheckMessageUpdateManager.getInstance().getIsMsgOn());
    }

    public void stopMainTimer() {
        timeoutHandler.removeCallbacks(timeoutRunnable);
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void resetBackMainTimer() {
        Log.i(TAG, "resetBackMainTimer fire");
        timeoutHandler.removeCallbacks(timeoutRunnable);
        timeoutHandler.postDelayed(timeoutRunnable, timeoutInterval);
        //this is a handle to make sure the screen is always on for activity which are not room control
        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void disableBackMainTimer() {
        backMainTimerDisabled = true;
    }

    public void enableBackMainTimer() {
        backMainTimerDisabled = false;
    }

    private void initBatteryListener() {
        if (mBatteryChecker == null) {
            mBatteryChecker = new BatteryChecker(new BatteryChecker.OnBatteryChangedListener() {
                @Override
                public void onBatteryChanged(int myBatteryDiv) {
                    if (imgBattery != null && isAdded()) {
                        imgBattery.setImageDrawable((getResources().getDrawable(getResources().getIdentifier(
                                "drawable/battery_" + myBatteryDiv,
                                "drawable",
                                TopBarFragment.this.getActivity().getPackageName()))));
                    }
                }

                @Override
                public void onPowerPlug(boolean plug) {
                    if (isRoomControl) {
                        ((RoomControlActivity) getActivity()).wakeBSP();
                    }
                }
            });
        }
    }

    private void removeBatteryListener() {
        if (mBatteryChecker != null) {
            mBatteryChecker.unloadReceiver();
            mBatteryChecker = null;
        }
    }

    public BatteryChecker getBatteryChecker() {
        return mBatteryChecker;
    }

    public void setAlarmSetting(boolean isAlarmOn, String hour, String minute, String snooze, String time, String settime, String curtime) {

        if (this.isAlarmOn != isAlarmOn) {
            this.isAlarmOn = isAlarmOn;
            checkAlarm(curtime);
        } else {
            //snooze
            this.isAlarmOn = isAlarmOn;
            if (isAlarmOn) {
                if (this.alarmSetTime != null && !this.alarmSetTime.equalsIgnoreCase(settime)) {
                    if (isAlarmRinging) {
                        isAlarmRinging = false;
                        if (isRoomControl) {
                            ((RoomControlActivity) getActivity()).disableAlarm();
                        }
                    }
                }
            }
        }
        this.alarmSetTime = settime;
    }

    public void checkAlarm(String currentTime) {

        if (isAlarmOn) {
            if (alarmSetTime != null) {
                if (alarmSetTime.equalsIgnoreCase(currentTime)) {
                    if (!isAlarmRinging) {
                        isAlarmRinging = true;
                        if (isRoomControl) {
                            ((RoomControlActivity) getActivity()).enableAlarm();
                        } else {
                            ((BaseActivity) getActivity()).goMain();
                        }
                    }
                } else {
                    isAlarmRinging = false;
                    if (isRoomControl) {
                        ((RoomControlActivity) getActivity()).disableAlarm();
                    }
                }
            }

        } else {
            if (isAlarmRinging) {
                isAlarmRinging = false;
                if (isRoomControl) {
                    ((RoomControlActivity) getActivity()).disableAlarm();
                }
            }
        }
    }

    public void setMessage(boolean haveMessage) {

        if (isMessageOn == haveMessage) {
            return;
        }
        setMsgIcon(haveMessage);
        isMessageOn = haveMessage;
    }

    public void setFax(boolean haveFax) {
        if (isMessageOn == haveFax) {
            return;
        }

        setMsgIcon(haveFax);
        isMessageOn = haveFax;
    }

    public void addMessageListener() {
        messageListenerPosition = CheckMessageUpdateManager.getInstance().addMessageListener(new CheckMessageUpdateManager.MessageListener() {
            @Override
            public void onMessageIconChanged(boolean isOn) {
                setMsgIcon(isOn);
            }

            @Override
            public void onPopupMessage(JSONArray messageObjects) {
                if(isRoomControl) {
                    ((RoomControlActivity) getActivity()).handlePopupMessage(messageObjects);
                }
            }
        });
    }

    private void setMsgIcon(boolean isMsg){
        if(isMsg) {
            if(!isMsgRedShowing) {
                msgRedImage.setVisibility(View.VISIBLE);
                Animation myFadeInAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.blink);
                msgRedImage.startAnimation(myFadeInAnimation);
                isMsgRedShowing = true;
            }
        } else {
            if(isMsgRedShowing) {
                msgRedImage.clearAnimation();
                msgRedImage.setVisibility(View.GONE);
                isMsgRedShowing = false;
            }
        }
    }

}
