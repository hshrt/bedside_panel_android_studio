package com.hsh.esdbsp.fragment;

import java.util.Locale;

import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.BaseActivity;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.SeekBar.OnSeekBarChangeListener;
import tv.danmaku.ijk.media.player.IMediaPlayer;
import tv.danmaku.ijk.media.player.IMediaPlayer.OnCompletionListener;
import tv.danmaku.ijk.media.player.IMediaPlayer.OnPreparedListener;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;
import tv.danmaku.ijk.media.widget.VideoView;

public class VideoPopupFragment extends Fragment {

    private static final int FADE_OUT = 1;
    private static final int SHOW_PROGRESS = 2;

    private ViewGroup mBackground;
    private Button mCloseBtn;
	private VideoView mVideoView;
	private ViewGroup mMediaController;
	private ImageButton mPauseButton;
	private SeekBar mProgress;
	private TextView mEndTime, mCurrentTime;
	private ImageButton mMuteButton;
	private SeekBar mVolume;
	private SettingsContentObserver mSettingsContentObserver;

	private boolean mCloseAtEnd = true;
	
	private long mDuration;	
	private boolean mShowing;
	private boolean mDragging;
    private boolean mInstantSeeking = true;
    private static final int sDefaultTimeout = 3000;

    private PopupWindow mUglyHackPopupWindow;
	private AudioManager mAM;
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
		View view = inflater.inflate(R.layout.video_popup, null, false);
		mBackground = (ViewGroup) view.findViewById(R.id.fullscreen_bg);
		mBackground.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		mCloseBtn = (Button) view.findViewById(R.id.close_btn);
		mVideoView = (VideoView) view.findViewById(R.id.video);
		mVideoView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mVideoView.setZOrderOnTop(true);
		mMediaController = (ViewGroup) view.findViewById(R.id.media_ctrl);
		mPauseButton = (ImageButton) view.findViewById(R.id.mediacontroller_pause);
		mProgress = (SeekBar) view.findViewById(R.id.mediacontroller_progress);
		mCurrentTime = (TextView) view.findViewById(R.id.mediacontroller_time_current);
		mEndTime = (TextView) view.findViewById(R.id.mediacontroller_time_total);
		mMuteButton = (ImageButton) view.findViewById(R.id.mediacontroller_mute);
		mVolume = (SeekBar) view.findViewById(R.id.mediacontroller_volume);
        return view;
    }

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		mAM = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
		IjkMediaPlayer.loadLibrariesOnce(null);
		mBackground.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View arg0, MotionEvent arg1) {
				return true;
			}
		});
		mCloseBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				close();
			}
		});
		mPauseButton.setOnClickListener(mPauseListener);
		mProgress.setOnSeekBarChangeListener(mSeekListener);
		mProgress.setThumbOffset(1);
		mProgress.setMax(1000);

		mVolume.setMax(mAM.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
		mVolume.setProgress(mAM.getStreamVolume(AudioManager.STREAM_MUSIC));
		mVolume.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) 
            {
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) 
            {
            }

            @Override
            public void onProgressChanged(SeekBar arg0, int progress, boolean arg2) 
            {
            	mAM.setStreamVolume(AudioManager.STREAM_MUSIC,
                        progress, 0);
            	if (progress > 0) {
            		mMuteButton.setImageResource(android.R.drawable.ic_lock_silent_mode_off);
            	}
            }
        });
		mSettingsContentObserver = new SettingsContentObserver(getActivity(), new Handler());
		getActivity()
			.getApplicationContext()
			.getContentResolver()
			.registerContentObserver(android.provider.Settings.System.CONTENT_URI, true, mSettingsContentObserver);
		mMuteButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (mAM.getStreamVolume(AudioManager.STREAM_MUSIC) > 0) {
					mAM.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
					mVolume.setProgress(0);
					mMuteButton.setImageResource(android.R.drawable.ic_lock_silent_mode);
				}
			}
		});
		if (mAM.getStreamVolume(AudioManager.STREAM_MUSIC) > 0) {
			mMuteButton.setImageResource(android.R.drawable.ic_lock_silent_mode_off);
		} else {
			mMuteButton.setImageResource(android.R.drawable.ic_lock_silent_mode);
		}
		
		mVideoView.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View view, MotionEvent event) {
				show(0);
				return false;
			}
		});
		mVideoView.setOnPreparedListener(new OnPreparedListener() {
			
			@Override
			public void onPrepared(IMediaPlayer arg0) {
				disableBackMainTimer();
			}
		});
		mVideoView.setOnCompletionListener(new OnCompletionListener() {
			
			@Override
			public void onCompletion(IMediaPlayer arg0) {
				if (mCloseAtEnd) {
					close();
				}
			}
		});

		Bundle args = getArguments();
		if (args != null && args.containsKey("url")) {
			mVideoView.setVideoPath(args.getString("url"));
		}
		if (args != null && args.containsKey("closeAtEnd")) {
			mCloseAtEnd = args.getBoolean("closeAtEnd");
		}
		mVideoView.start();
		Log.d("RichMedia", "hwaccel: " + mVideoView.isHardwareAccelerated());
		show(0);
		
		// An ugly hack for Samsung Galaxy Tab 10.1
		// The videoView will disappear/make the whole screen black on this device. But if 
		// a popup window is layered on the screen the VideoView will behave normally.
		FrameLayout frameLayout = new FrameLayout(getActivity());
		frameLayout.setLayoutParams(new LayoutParams(100, 100));
		mUglyHackPopupWindow = new PopupWindow(frameLayout);
		mUglyHackPopupWindow.showAtLocation(mBackground, 0, 10, 10);
	}
	
	public void show() {
		show(sDefaultTimeout);
	}
	
	public void show(int timeout) {
		if (!mShowing) {
			mShowing = true;
			mMediaController.setVisibility(View.VISIBLE);
		}
		mHandler.sendEmptyMessage(SHOW_PROGRESS);
		
        if (timeout != 0) {
            mHandler.removeMessages(FADE_OUT);
            mHandler.sendMessageDelayed(mHandler.obtainMessage(FADE_OUT),
                    timeout);
        }
	}
	
	public void hide() {
		if (mShowing) {
			mMediaController.setVisibility(View.INVISIBLE);
			mShowing = false;
		}
	}
	
	private void close() {
		getActivity()
			.getSupportFragmentManager()
			.beginTransaction()
			.remove(VideoPopupFragment.this)
			.commit();
	}
	
    @Override
	public void onDetach() {
    	super.onDetach();
		mHandler.removeCallbacksAndMessages(null);
		mVideoView.stopPlayback();
		if (mUglyHackPopupWindow != null) {
			mUglyHackPopupWindow.dismiss();
		}
		enableBackMainTimer();
		getActivity().getApplicationContext().getContentResolver().unregisterContentObserver(mSettingsContentObserver);
	}

	private View.OnClickListener mPauseListener = new View.OnClickListener() {
        public void onClick(View v) {
            doPauseResume();
            show(0);
        }
    };

    private void doPauseResume() {
    	if (mVideoView.getMediaPlayer() == null) {
    		return;
    	}
        if (mVideoView.getMediaPlayer().isPlaying()) {
        	mVideoView.getMediaPlayer().pause();
        	enableBackMainTimer();
        } else {
        	mVideoView.getMediaPlayer().start();
            disableBackMainTimer();
        }
        updatePausePlay();
    }

    private void updatePausePlay() {
        if (mPauseButton == null || mVideoView.getMediaPlayer() == null) {
            return;
        }
        if (mVideoView.getMediaPlayer().isPlaying()) {
            mPauseButton.setImageResource(android.R.drawable.ic_media_pause);
        } else {
            mPauseButton.setImageResource(android.R.drawable.ic_media_play);
        }
    }
	
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            long pos;
            switch (msg.what) {
            case FADE_OUT:
                hide();
                break;
            case SHOW_PROGRESS:
                pos = setProgress();
                if (!mDragging && mShowing) {
                    msg = obtainMessage(SHOW_PROGRESS);
                    sendMessageDelayed(msg, 1000 - (pos % 1000));
                    updatePausePlay();
                }
                break;
            }
        }
    };
    
    private long setProgress() {
        if (mVideoView == null || mDragging)
            return 0;

        int position = mVideoView.getCurrentPosition();
        int duration = mVideoView.getDuration();
        if (mProgress != null) {
            if (duration > 0) {
                long pos = 1000L * position / duration;
                mProgress.setProgress((int) pos);
            }
            int percent = mVideoView.getBufferPercentage();
            mProgress.setSecondaryProgress(percent * 10);
        }

        mDuration = duration;

        if (mEndTime != null)
            mEndTime.setText(generateTime(mDuration));
        if (mCurrentTime != null)
            mCurrentTime.setText(generateTime(position));

        return position;
    }
    
    private static String generateTime(long position) {
        int totalSeconds = (int) ((position / 1000.0)+0.5);

        int seconds = totalSeconds % 60;
        int minutes = (totalSeconds / 60) % 60;
        int hours = totalSeconds / 3600;

        if (hours > 0) {
            return String.format(Locale.US, "%02d:%02d:%02d", hours, minutes,
                    seconds).toString();
        } else {
            return String.format(Locale.US, "%02d:%02d", minutes, seconds)
                    .toString();
        }
    }
    
    private Runnable lastRunnable;
    private OnSeekBarChangeListener mSeekListener = new OnSeekBarChangeListener() {
        public void onStartTrackingTouch(SeekBar bar) {
            mDragging = true;
            show(3600000);
            mHandler.removeMessages(SHOW_PROGRESS);
            if (mInstantSeeking)
                mAM.setStreamMute(AudioManager.STREAM_MUSIC, true);
        }

        public void onProgressChanged(SeekBar bar, int progress,
                boolean fromuser) {
            if (!fromuser)
                return;

            final long newposition = (mDuration * progress) / 1000;
            String time = generateTime(newposition);
            if (mInstantSeeking) {
                mHandler.removeCallbacks(lastRunnable);
                lastRunnable = new Runnable() {
                    @Override
                    public void run() {
                    	mVideoView.getMediaPlayer().seekTo(newposition);
                    }
                };
                mHandler.postDelayed(lastRunnable, 200);
            }
            if (mCurrentTime != null)
                mCurrentTime.setText(time);
        }

        public void onStopTrackingTouch(SeekBar bar) {
            if (!mInstantSeeking)
                mVideoView.getMediaPlayer().seekTo((mDuration * bar.getProgress()) / 1000);
            show(0);
            mHandler.removeMessages(SHOW_PROGRESS);
            mAM.setStreamMute(AudioManager.STREAM_MUSIC, false);
            mDragging = false;
            mHandler.sendEmptyMessageDelayed(SHOW_PROGRESS, 1000);
        }
    };

    private void enableBackMainTimer() {

		if (getActivity() instanceof BaseActivity) {
			((BaseActivity) getActivity()).enableBackMainTimer();
		}
    }
    
    private void disableBackMainTimer() {
		if (getActivity() instanceof BaseActivity) {
			((BaseActivity) getActivity()).disableBackMainTimer();
		}
    }

    public class SettingsContentObserver extends ContentObserver {
        int previousVolume;
        Context context;

        public SettingsContentObserver(Context c, Handler handler) {
            super(handler);
            context=c;

            AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            previousVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return super.deliverSelfNotifications();
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);

            AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
            int currentVolume = audio.getStreamVolume(AudioManager.STREAM_MUSIC);

            if (currentVolume != previousVolume) {
            	if (mVolume != null) {
            		mVolume.setProgress(currentVolume);
            	}
            }
            previousVolume = currentVolume;
        }
    }
}
