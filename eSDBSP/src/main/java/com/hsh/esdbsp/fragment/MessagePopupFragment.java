package com.hsh.esdbsp.fragment;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.view.MyTextView;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MessagePopupFragment extends Fragment {

	private Handler timeoutHandler = new Handler();
	private LinearLayout mMessageDialogLayout;
	private MyTextView mMessageDialogLabel;
	private ImageView mMessageDialogClose;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		
		Bundle args = getArguments();
		
		View view = inflater.inflate(R.layout.error_message_layout, container, false);
		mMessageDialogLayout = (LinearLayout) view.findViewById(R.id.errorMessageDialogLayout);
		mMessageDialogLabel = (MyTextView) view.findViewById(R.id.errorMessageDialogLabel);
		mMessageDialogClose = (ImageView) view.findViewById(R.id.errorMessageDialogCloseImage);
		
		mMessageDialogLayout.setVisibility(View.VISIBLE);
		mMessageDialogLayout.setClickable(true);
		mMessageDialogClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				MessagePopupFragment.this.close();
			}
		});
		
		if (args.containsKey("message")) {
			mMessageDialogLabel.setText(MainApplication.getLabel(args.getString("message")));
			mMessageDialogLabel.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 40.0f);
		}
		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (getArguments().containsKey("timeout")) {
			timeoutHandler.postDelayed(new Runnable() {
				
				@Override
				public void run() {
					MessagePopupFragment.this.close();
				}
			}, getArguments().getInt("timeout"));
		}
	}

	private void close() {
		timeoutHandler.removeCallbacksAndMessages(null);
		getActivity()
			.getSupportFragmentManager()
			.beginTransaction()
			.remove(MessagePopupFragment.this)
			.commit();
	}
}