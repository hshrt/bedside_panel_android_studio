package com.hsh.esdbsp.fragment;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.BaseActivity;
import com.hsh.esdbsp.activity.RoomControlActivity;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.view.MySpinnerAdapter;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Command;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.Hotel;

public class BottomBarFragment extends BaseFragment {

    private final String TAG = "BottomBarFragment";
    private View baseView;
    private TextView footRoomNum;
    private Context context;

    Handler pendingChangeHandler = null;
    private int pendingChangeInterval = 2000;
    boolean isUIChanging = false;
    // /////////////////////////////////////////////
    // / copy from rc

    static int myHeadClockHour;
    static int myHeadClockMin;
    static String myHeadClockSign; // am pm

    TextView rcHeadClock;
    TextView rcHeadClockSign;

    TextView rcVCLabel;
    LinearLayout rcVC;
    boolean isValetOn = false;

    TextView rcMURLabel;
    LinearLayout rcMUR;
    boolean isMurOn = false;

    TextView rcDNDLabel;
    LinearLayout rcDND;
    boolean isDndOn = false;

    boolean isAlarmOn = false;
    boolean isRoomControl = false;
    // /////////////////////////////////////////////

    LinearLayout rcAlarmBottomBarButton;

    Spinner footRoomArea; // zone
    MySpinnerAdapter dataAdapter; // for the spinner

    // ////////////////////////////////////////////

    int suite = 0;
    boolean isRefreshFootRoomAreaNeeded = false;
    boolean isFootRoomAreaInited = false;

    List<String> list = new ArrayList<String>();
    List<Integer> listImg = new ArrayList<Integer>();
    List<Integer> listMAS = new ArrayList<Integer>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView fire");
        this.context = getContext();
        baseView = inflater.inflate(R.layout.bottom_bar, container, false);

        footRoomArea = (Spinner) baseView.findViewById(R.id.footRoomArea);

        if (MainApplication.getCurrentActivity() instanceof RoomControlActivity) {
            isRoomControl = true;
            isRefreshFootRoomAreaNeeded = true;

            try {
                suite = Integer.parseInt(MainApplication.getMAS().getData("data_roomtype") + "") - 1;
            } catch (Exception ex) {
                suite = 0;
            }

        } else {
            footRoomArea.setVisibility(View.INVISIBLE);
        }

        loadFindView();
        ((BaseActivity) getActivity()).isBottomBarInit = true;

        if (!isRoomControl) {
            loadRCLabel();
        }

        return baseView;
    }


    @Override
    public void onPause() {
        super.onPause();
        cancelCurrentTask();
    }

    @Override
    public void onDestroyView() {
        Log.e(TAG, "onDestroyView_" + getActivity().getClass().getSimpleName());

        cancelCurrentTask();
        super.onDestroyView();

        ((ViewGroup) baseView).removeAllViews();
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        cancelCurrentTask();
        super.onDestroy();
    }

    // //////////////////////////////////////////////////////////////////////////
    //

    public void rcVC_logic() {
        if (!isValetOn) {
            rcVC.setActivated(false);
        } else {
            rcVC.setActivated(true);
            rcDND.setActivated(false);
        }
    }

    public void rcMUR_logic() {
        if (!isMurOn) {
            rcMUR.setActivated(false);
        } else {
            rcMUR.setActivated(true);
            rcDND.setActivated(false);
        }
    }

    public void rcDND_logic() {
        if (!isDndOn) {
            rcDND.setActivated(false);
        } else {
            rcDND.setActivated(true);
            rcVC.setActivated(false);
            rcMUR.setActivated(false);
        }
    }

    public void rcAlarm_logic() {
        if (!isAlarmOn) {
            rcAlarmBottomBarButton.setActivated(false);
        } else {
            rcAlarmBottomBarButton.setActivated(true);
        }
    }

    // ////////////////////////////////////////////////////////////////////////

    public void loadRCLabel() {

        rcVCLabel.setText(MainApplication.getLabel("VC"));
        rcMURLabel.setText(MainApplication.getLabel("MUR"));
        rcDNDLabel.setText(MainApplication.getLabel("DND"));
        setRoomNumber(MainApplication.getMAS().getData("data_myroom"));

        if (MainApplication.getMAS().getData("data_language") == "R"
                || DataCacheManager.getInstance().getLang() == "R"
                || MainApplication.getMAS().getData("data_language") == "G"
                || DataCacheManager.getInstance().getLang() == "G"
                || MainApplication.getMAS().getData("data_language") == "S"
                || DataCacheManager.getInstance().getLang() == "S"
                || MainApplication.getMAS().getData("data_language") == "F"
                || DataCacheManager.getInstance().getLang() == "F"
                || MainApplication.getMAS().getData("data_language") == "P"
                || DataCacheManager.getInstance().getLang() == "P") {
            int fontsize = getResources().getInteger(R.integer.bottom_bar_custom_font_size);

            rcMURLabel.setTextSize(fontsize);
            rcDNDLabel.setTextSize(fontsize);

            int roomfontsize = getResources().getInteger(R.integer.bottom_bar_roomnum_font_size);
            footRoomNum.setTextSize(roomfontsize);
        }
    }

    private void loadFindView() {

        footRoomNum = (TextView) baseView.findViewById(R.id.footRoomNum);
        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            rcHeadClockSign = (TextView) baseView.findViewById(R.id.headClockSign_pch);
        }
        rcHeadClockSign.setText("");
        rcHeadClock = (TextView) baseView.findViewById(R.id.headClock);
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            rcHeadClock = (TextView) baseView.findViewById(R.id.headClock_pch);
        } else {
            rcHeadClock = (TextView) baseView.findViewById(R.id.headClock);
        }
        Helper.setTimeDigiFonts(rcHeadClock);
        Helper.setTimeDigiFonts(rcHeadClockSign);
        // vc and dnd


        rcVCLabel = (TextView) baseView.findViewById(R.id.rcVCLabel);
        rcVC = (LinearLayout) baseView.findViewById(R.id.rcVC);
        rcVC.setClickable(true);
        rcVC.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                isUIChanging = true;
                rcVC.setPressed(true);
                MainApplication.playClickSound(rcVC);

                if (isValetOn) {
                    isValetOn = false;
                    Command.Other.ValetOff();
                } else {
                    isValetOn = true;
                    isDndOn = false;
                    Command.Other.ValetOn();
                }
                rcVC_logic();
                rcVC.setPressed(false);
                isUIChanging = false;
                stackPendingChange();
                // for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                map.put("TurnOn", (isValetOn ? 1 : 0) + "");
                FlurryAgent.logEvent("Valet", map);
            }

        });


        rcMURLabel = (TextView) baseView.findViewById(R.id.rcMURLabel);
        rcMUR = (LinearLayout) baseView.findViewById(R.id.rcMUR);
        rcMUR.setClickable(true);
        rcMUR.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                isUIChanging = true;
                rcMUR.setPressed(true);
                MainApplication.playClickSound(rcMUR);

                if (isMurOn) {
                    isMurOn = false;
                    Command.Other.MUROff();
                } else {
                    isMurOn = true;
                    isDndOn = false;
                    Command.Other.MUROn();
                }
                // handleSleep();
                rcMUR_logic();
                rcMUR.setPressed(false);
                isUIChanging = false;
                stackPendingChange();
                // for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                map.put("TurnOn", (isMurOn ? 1 : 0) + "");
                FlurryAgent.logEvent("MakeUpRoom", map);

            }
        });


        rcDNDLabel = (TextView) baseView.findViewById(R.id.rcDNDLabel);
        rcDND = (LinearLayout) baseView.findViewById(R.id.rcDND);
        rcDND.setClickable(true);
        rcDND.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                isUIChanging = true;
                rcDND.setPressed(true);
                MainApplication.playClickSound(rcDND);
                if (isDndOn) {
                    isDndOn = false;
                    Command.Other.DNDOff();
                } else {
                    isDndOn = true;
                    isValetOn = false;
                    isMurOn = false;
                    Command.Other.DNDOn();
                }
                // handleSleep();
                rcDND_logic();
                rcDND.setPressed(false);
                isUIChanging = false;
                stackPendingChange();

                // for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                map.put("TurnOn", (isDndOn ? 1 : 0) + "");
                FlurryAgent.logEvent("PrivacyPlease", map);

            }
        });


        rcAlarmBottomBarButton = (LinearLayout) baseView.findViewById(R.id.rcAlarmBottomBarButton);
        if (isRoomControl) {
            rcAlarmBottomBarButton.setVisibility(View.VISIBLE);
            rcAlarmBottomBarButton.setClickable(true);
            rcAlarmBottomBarButton.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                        MainApplication.playClickSound(rcAlarmBottomBarButton);
                        ((RoomControlActivity) getActivity()).onAlarmBottomBarButtonClicked();
                    }
                    return true;
                }
            });
        } else {
            rcAlarmBottomBarButton.setVisibility(View.GONE);
        }

        Helper.setAppFonts(rcVCLabel);
        Helper.setAppFonts(rcMURLabel);
        Helper.setAppFonts(rcDNDLabel);
        Helper.setAppFonts(footRoomNum);

        setRoomNumber(MainApplication.getMAS().getData("data_myroom"));
        setTimeBySystem();
        setValet((MainApplication.getMAS().getData("data_valet")).equals("ON") ? true : false);
        setMur((MainApplication.getMAS().getData("data_mur")).equals("ON") ? true : false);
        setDnd((MainApplication.getMAS().getData("data_dnd")).equals("ON") ? true : false);
        setAlarm((MainApplication.getMAS().getData("data_alarm")).equals("ON") ? true : false);
    }

    // ///////////////////////////////////////////////////////////////////////////////


    public void setRoomNumber(String roomNumber) {
        footRoomNum.setText(Html.fromHtml(MainApplication.getLabel("Room") + " " + MainApplication.getMAS().getData("data_myroom")));
    }

    public void setTimeBySystem() {

        if(!setTime(MainApplication.getMAS().getData("data_curtime"))) {

            if (rcHeadClock != null && rcHeadClockSign != null) {
                //get clock from system time, for no MAS issue
                DateFormat df = new SimpleDateFormat("h:mm");
                String date = df.format(Calendar.getInstance().getTime());

                String _tmpHeadClock[] = date.split(":");

                String hr = _tmpHeadClock[0];
                String min = _tmpHeadClock[1];

                rcHeadClock.setText(hr + ":" + min);

                df = new SimpleDateFormat("h:mm a", Locale.US);
                date = df.format(Calendar.getInstance().getTime());

                String tmpString[] = date.split(" ");
                rcHeadClockSign.setText(tmpString[1].toLowerCase());
            }
        }
    }

    public boolean setTime(String timeString) {
        boolean success = false;

        if (timeString != null && timeString.length() > 0) {
            try {
                String tmpHeadClock[] = timeString.split(":");
                myHeadClockHour = Integer.parseInt(tmpHeadClock[0]);
                myHeadClockMin = Integer.parseInt(tmpHeadClock[1]);
                if (myHeadClockHour == 0) {
                    myHeadClockHour = 12;
                    myHeadClockSign = "am";
                } else if (myHeadClockHour == 12) {
                    myHeadClockHour = myHeadClockHour;
                    myHeadClockSign = "pm";
                } else if (myHeadClockHour > 12) {
                    myHeadClockHour = myHeadClockHour - 12;
                    myHeadClockSign = "pm";
                } else {
                    myHeadClockSign = "am";
                }
                rcHeadClock.setText(MainApplication.twoDigitMe(myHeadClockHour) + ":" + MainApplication.twoDigitMe(myHeadClockMin));
                rcHeadClockSign.setText(myHeadClockSign);
                success = true;
            } catch (Exception ex) {

            }
        }
        return success;
    }

    public void setValet(boolean isValetOn) {
        this.isValetOn = isValetOn;
        rcVC_logic();
    }

    public void setMur(boolean isMur) {
        this.isMurOn = isMur;
        rcMUR_logic();
    }

    public void setDnd(boolean isDnd) {
        this.isDndOn = isDnd;
        rcDND_logic();
    }

    public void setAlarm(boolean isAlarmOn) {
        this.isAlarmOn = isAlarmOn;
        rcAlarm_logic();
    }

    // ////////////////////////////////////////////////////////

    public void setSuite(int suite) {
        this.suite = suite;
    }

    public void createFootRoomArea() {

        // get back the mas value once
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int previouslyStartedmas = prefs.getInt(previouslyStartedmasString, Integer.parseInt(masBase));

        int mySelected = 0;

        list.clear();

        switch (suite) {
            case 0:
                break;

            case 1:
                if (previouslyStartedmas == Integer.parseInt(masBase)) {
                    mySelected = 0;
                } else {
                    mySelected = 1;
                }

                list.add(MainApplication.getLabel("Bedroom Control"));
                listImg.add(R.drawable.bedrm_icon_off);
                listMAS.add(Integer.parseInt(masBase));

                list.add(MainApplication.getLabel("Living Room Control"));
                listImg.add(R.drawable.living_rm_off);
                listMAS.add(Integer.parseInt(maslrBase));
                break;

            case 2:
                if (previouslyStartedmas == Integer.parseInt(masBase)) {
                    mySelected = 0;
                } else if (previouslyStartedmas == Integer.parseInt(maslrBase)) {
                    mySelected = 1;
                } else {
                    mySelected = 2;
                }

                list.add(MainApplication.getLabel("Bedroom Control"));
                listImg.add(R.drawable.bedrm_icon_off);
                listMAS.add(Integer.parseInt(masBase));

                list.add(MainApplication.getLabel("Living Room Control"));
                listImg.add(R.drawable.living_rm_off);
                listMAS.add(Integer.parseInt(maslrBase));

                list.add(MainApplication.getLabel("Dining Room Control"));
                listImg.add(R.drawable.dining_rm_icon_off);
                listMAS.add(Integer.parseInt(masdrBase));

                break;
            case 3:
                if (previouslyStartedmas == Integer.parseInt(masBase)) {
                    mySelected = 0;
                } else if (previouslyStartedmas == Integer.parseInt(maslrBase)) {
                    mySelected = 2;
                } else if (previouslyStartedmas == Integer.parseInt(massbBase)) {
                    mySelected = 1;
                } else {
                    mySelected = 3;
                }

                list.add(MainApplication.getLabel("Bedroom Control"));
                listImg.add(R.drawable.bedrm_icon_off);
                listMAS.add(Integer.parseInt(masBase));

                list.add(MainApplication.getLabel("Second Bedroom Control"));
                listImg.add(R.drawable.bedrm_icon_off);
                listMAS.add(Integer.parseInt(massbBase));

                list.add(MainApplication.getLabel("Living Room Control"));
                listImg.add(R.drawable.living_rm_off);
                listMAS.add(Integer.parseInt(maslrBase));

                list.add(MainApplication.getLabel("Dining Room Control"));
                listImg.add(R.drawable.dining_rm_icon_off);
                listMAS.add(Integer.parseInt(masdrBase));
                break;


            case 10:
                if (previouslyStartedmas == Integer.parseInt(masBase)) {
                    mySelected = 0;
                } else if (previouslyStartedmas == Integer.parseInt(maslrBase)) {
                    mySelected = 1;
                } else {
                    mySelected = 2;
                }

                list.add(MainApplication.getLabel("Bedroom Control"));
                listImg.add(R.drawable.bedrm_icon_off);
                listMAS.add(Integer.parseInt(masBase));

                list.add(MainApplication.getLabel("Living Room Control"));
                listImg.add(R.drawable.living_rm_off);
                listMAS.add(Integer.parseInt(maslrBase));

                list.add(MainApplication.getLabel("Study Room Control"));
                listImg.add(R.drawable.study_rm_icon_off);
                listMAS.add(Integer.parseInt(massrBase));

                break;


            case 11: // PBJ add 10
                if (previouslyStartedmas == Integer.parseInt(masBase)) {
                    mySelected = 0;
                } else if (previouslyStartedmas == Integer.parseInt(maslrBase)) {
                    mySelected = 2;
                } else if (previouslyStartedmas == Integer.parseInt(massbBase)) {
                    mySelected = 1;
                } else {
                    mySelected = 3;
                }


                list.add(MainApplication.getLabel("Bedroom Control"));
                listImg.add(R.drawable.bedrm_icon_off);
                listMAS.add(Integer.parseInt(masBase));

                list.add(MainApplication.getLabel("Second Bedroom Control"));
                listImg.add(R.drawable.bedrm_icon_off);
                listMAS.add(Integer.parseInt(massbBase));

                list.add(MainApplication.getLabel("Living Room Control"));
                listImg.add(R.drawable.living_rm_off);
                listMAS.add(Integer.parseInt(maslrBase));

                list.add(MainApplication.getLabel("Study Room Control"));
                listImg.add(R.drawable.study_rm_icon_off);
                listMAS.add(Integer.parseInt(massrBase));

                break;

            case 12:
                if (previouslyStartedmas == Integer.parseInt(masBase)) {
                    mySelected = 0;
                } else if (previouslyStartedmas == Integer.parseInt(maslrBase)) {
                    mySelected = 1;
                } else if (previouslyStartedmas == Integer.parseInt(massrBase)) {
                    mySelected = 2;
                } else {
                    mySelected = 3;
                }

                list.add(MainApplication.getLabel("Bedroom Control"));
                listImg.add(R.drawable.bedrm_icon_off);
                listMAS.add(Integer.parseInt(masBase));

                list.add(MainApplication.getLabel("Living Room Control"));
                listImg.add(R.drawable.living_rm_off);
                listMAS.add(Integer.parseInt(maslrBase));

                list.add(MainApplication.getLabel("Study Room Control"));
                listImg.add(R.drawable.study_rm_icon_off);
                listMAS.add(Integer.parseInt(massrBase));

                list.add(MainApplication.getLabel("Theatre Room Control"));
                listImg.add(R.drawable.av_rm_icon_off);
                listMAS.add(Integer.parseInt(masavBase));

                break;

            case 19: // pbj PS
                if (previouslyStartedmas == Integer.parseInt(masBase)) {
                    mySelected = 0;
                } else if (previouslyStartedmas == Integer.parseInt(massbBase)) {
                    mySelected = 1;
                } else if (previouslyStartedmas == Integer.parseInt(massrBase)) {
                    mySelected = 2;
                } else if (previouslyStartedmas == Integer.parseInt(maslrBase)) {
                    mySelected = 3;
                } else {
                    mySelected = 4;
                }

                list.add(MainApplication.getLabel("Bedroom Control"));
                listImg.add(R.drawable.bedrm_icon_off);
                listMAS.add(Integer.parseInt(masBase));

                list.add(MainApplication.getLabel("Second Bedroom Control"));
                listImg.add(R.drawable.bedrm_icon_off);
                listMAS.add(Integer.parseInt(massbBase));

                list.add(MainApplication.getLabel("Family Room Control"));
                listImg.add(R.drawable.family_rm_off);
                listMAS.add(Integer.parseInt(massrBase));

                list.add(MainApplication.getLabel("Living Room Control"));
                listImg.add(R.drawable.living_rm_off);
                listMAS.add(Integer.parseInt(maslrBase));

                list.add(MainApplication.getLabel("Dining Room Control"));
                listImg.add(R.drawable.dining_rm_off);
                listMAS.add(Integer.parseInt(masdrBase));

                break;


            default:
                break;
        }

        if (suite == 0) {
            footRoomArea.setVisibility(View.INVISIBLE);
        } else {
            footRoomArea.setVisibility(View.VISIBLE);

            if (dataAdapter == null) {
                dataAdapter = new MySpinnerAdapter(context, android.R.layout.simple_spinner_item, list, listImg);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                footRoomArea.setAdapter(dataAdapter);
            } else {
                dataAdapter.notifyDataSetChanged();
            }

            footRoomArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
//                    if (!isAlarmRinging) {
                    MainApplication.getMAS().setSelectedArea(pos);
                    MainApplication.getMAS().setCurRoom(listMAS.get(pos));
                    ((RoomControlActivity) getActivity()).switchControlLayout(MainApplication.getMAS().getCurRoom());
//                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }

            });

            android.util.Log.v(this.getClass().getSimpleName(), "zone select=" + mySelected + ", my record = " + previouslyStartedmas);

            /// new add to select

            MainApplication.getMAS().setSelectedArea(mySelected);
            footRoomArea.setSelection(MainApplication.getMAS().getSelectedArea(), true);
            // footRoomArea.setSelection(MainApplication.getMAS().getCurRoom());
        }
    }

    public void rcFootRoomArea_logic() {

        if (MainApplication.getScreenSta() == 1) { // on screen
            if (isRefreshFootRoomAreaNeeded) {
                isRefreshFootRoomAreaNeeded = false;
                isFootRoomAreaInited = true;
                createFootRoomArea();
            }
        }

        if (suite == 0) {
            footRoomArea.setVisibility(View.INVISIBLE);
        } else {
            footRoomArea.setVisibility(View.VISIBLE);
        }
    }

    public void setRefreshFootRoomArea(boolean isNeed) {
        isRefreshFootRoomAreaNeeded = isNeed;
    }

    private void stackPendingChange() {
        if (pendingChangeHandler == null) {
            pendingChangeHandler = new Handler();
        }
        pendingChangeHandler.postDelayed(pendingChangeRunnable, pendingChangeInterval);
    }

    private Runnable pendingChangeRunnable = new Runnable() {
        public void run() {

            if (!isUIChanging) {
                pendingChangeHandler.removeCallbacks(pendingChangeRunnable);
                pendingChangeHandler = null;

                isValetOn = (MainApplication.getMAS().getData("data_valet")).equals("ON") ? true : false;
                isMurOn = (MainApplication.getMAS().getData("data_mur")).equals("ON") ? true : false;
                isDndOn = (MainApplication.getMAS().getData("data_dnd")).equals("ON") ? true : false;

                rcVC_logic();
                rcMUR_logic();
                rcDND_logic();

            } else {
                pendingChangeHandler.postDelayed(pendingChangeRunnable, pendingChangeInterval);
            }
        }
    };

    public void hideSpinnerDropDown() {
        try {
            Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
            method.setAccessible(true);
            method.invoke(footRoomArea);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
