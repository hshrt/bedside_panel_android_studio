package com.hsh.esdbsp.fragment;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.adapter.dining.FoodOrderComplexItemAdapter;
import com.hsh.esdbsp.adapter.dining.FoodOrderItemAdapter;
import com.hsh.esdbsp.adapter.dining.GeneralItemWrapper;
import com.hsh.esdbsp.model.FoodOrderData;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.FoodOrder;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.model.Hotel;

public class InRoomDinOrderFragment extends Fragment implements FoodOrderItemAdapter.Callback, FoodOrderComplexItemAdapter.Callback {

    private MyTextView tipsText;
    private MyTextView extensionText;
    private TextView timeAvailText;

    //UI for the popup detail View
    private LinearLayout detailBox; // detail View root container
    private MyTextView foodTitle;
    private MyTextView foodPrice;
    private WebView foodDes;

    private Button closeBtn, closeBtn2, closeBtn4;

    // UI For order options
    private Button orderPlusBtn, orderMinusBtn;
    private Button orderItemBtn;
    private LinearLayout optionBox;
    private MyTextView foodTitle2;
    private MyTextView setNumberTitle, setPromptTitle;
    private MyTextView optionTitle;
    private GridView optionItem_gridView;
    private Button optionItemBackBtn, optionItemBackBtn3;
    private Button optionItemOkBtn, optionItemOkBtn3;
    private MyTextView orderQtyDesc;
    private MyTextView orderQtyText;
    private RelativeLayout bottomBtnContainer;
    private int orderQty;

    private LinearLayout setMsgBox;

    private GeneralItem timeItem;

    private ArrayList<GeneralItemWrapper> pages;
    private int currentPage;
    private int currentSetNumber;
    private ArrayList<ArrayDeque<GeneralItemWrapper>> optionSetStack;
    private ArrayList<ArrayDeque<Integer>> currentPageStack;
    private boolean inSetNumberPromptPage;
    private GeneralItemWrapper rootItem;
    private ArrayList<GeneralItemWrapper> orderItem;
    private ArrayList<GeneralItemWrapper> options;
    private ArrayList<GeneralItemWrapper> adapterData;

    private String startTime;
    private String startTime2;
    private String endTime;
    private String endTime2;


    public interface Callback {
        public void onPlaceOrder();

        public void onClose(Fragment fragment);
    }

    private Callback callback;
    private boolean isAdd;

    public static InRoomDinOrderFragment newInstance(String id, Callback callback) {
        InRoomDinOrderFragment f = new InRoomDinOrderFragment();

        Bundle args = new Bundle();
        args.putString("id", id);
        f.setArguments(args);
        f.isAdd = true;
        f.callback = callback;

        return f;
    }

    public static InRoomDinOrderFragment newInstance(int position, int day, int hour, int min, Callback callback) {
        InRoomDinOrderFragment f = new InRoomDinOrderFragment();
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);
        f.isAdd = false;
        f.callback = callback;

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.cms_food_order, container, false);

        optionItem_gridView = (GridView) view.findViewById(R.id.optionItem_gridView);
        optionItemBackBtn = (Button) view.findViewById(R.id.optionItemBackBtn);
        optionItemBackBtn3 = (Button) view.findViewById(R.id.optionItemBackBtn3);
        optionItemOkBtn = (Button) view.findViewById(R.id.optionItemOkBtn);
        optionItemOkBtn3 = (Button) view.findViewById(R.id.optionItemOkBtn3);

        tipsText = (MyTextView) view.findViewById(R.id.tips);
        extensionText = (MyTextView) view.findViewById(R.id.extensionText);
        timeAvailText = (MyTextView) view.findViewById(R.id.timeAvailText);

        bottomBtnContainer = (RelativeLayout) view.findViewById(R.id.bottomBtnContainer);

        if (this.getString(R.string.hasInRoomDiningOrder).equalsIgnoreCase("0")) {
            bottomBtnContainer.setVisibility(View.GONE);
        }

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
            //timeAvailText.setVisibility(View.VISIBLE);
            tipsText.setVisibility(View.VISIBLE);
        } else {
            //tipsText.setVisibility(View.INVISIBLE);
        }

        extensionText.setText(Html.fromHtml(MainApplication.getLabel("inroom.extension")));

        foodTitle = (MyTextView) view.findViewById(R.id.foodTitle);
        foodPrice = (MyTextView) view.findViewById(R.id.foodPrice);
        foodDes = (WebView) view.findViewById(R.id.foodDes);

        foodDes.setBackgroundColor(0x00000000);
        foodDes.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
        foodDes.setHorizontalScrollBarEnabled(true);
        foodDes.getSettings().setDefaultFontSize(20);

        foodDes.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);

        // disable scroll on touch
        /*
        foodDes.setOnTouchListener(new View.OnTouchListener() {
	
		    public boolean onTouch(View v, MotionEvent event) {
		      return (event.getAction() == MotionEvent.ACTION_MOVE);
		    }
		});
		*/

        //foodDes.setScrollContainer(false);
        detailBox = (LinearLayout) view.findViewById(R.id.detailBox);
        optionBox = (LinearLayout) view.findViewById(R.id.optionBox);
        foodTitle2 = (MyTextView) view.findViewById(R.id.foodTitle2);
        setNumberTitle = (MyTextView) view.findViewById(R.id.setNumberTitle);
        setPromptTitle = (MyTextView) view.findViewById(R.id.setPromptTitle);
        optionTitle = (MyTextView) view.findViewById(R.id.optionTitle);
        closeBtn = (Button) view.findViewById(R.id.closeBtn);
        closeBtn2 = (Button) view.findViewById(R.id.closeBtn2);
        closeBtn4 = (Button) view.findViewById(R.id.closeBtn4);

        OnClickListener closeBtnOnClickListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                InRoomDinOrderFragment.this.close();
            }
        };
        closeBtn.setOnClickListener(closeBtnOnClickListener);
        closeBtn2.setOnClickListener(closeBtnOnClickListener);
        closeBtn4.setOnClickListener(closeBtnOnClickListener);

        orderQtyDesc = (MyTextView) view.findViewById(R.id.orderQtyDesc);
        orderQtyDesc.setText(MainApplication.getLabel("Quantity"));
        orderItemBtn = (Button) view.findViewById(R.id.orderItemBtn);
        orderItemBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clickOnOrderItem();
            }
        });
        Helper.setAppFonts(orderItemBtn);
        orderPlusBtn = (Button) view.findViewById(R.id.orderPlusBtn);
        orderPlusBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                changeOrderQty(1);
            }
        });
        orderMinusBtn = (Button) view.findViewById(R.id.orderMinusBtn);
        orderMinusBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                changeOrderQty(-1);
            }
        });
        OnClickListener optionItemBackBtnListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                clickOnOrderItemBack();
            }
        };
        optionItemBackBtn.setOnClickListener(optionItemBackBtnListener);
        optionItemBackBtn3.setOnClickListener(optionItemBackBtnListener);
        OnClickListener optionItemOkBtnListener = new OnClickListener() {

            @Override
            public void onClick(View v) {
                clickOnOrderItemOk();
            }
        };
        optionItemOkBtn.setOnClickListener(optionItemOkBtnListener);
        optionItemOkBtn3.setOnClickListener(optionItemOkBtnListener);
        optionItemOkBtn.setText(MainApplication.getLabel("Next"));
        Helper.setAppFonts(optionItemOkBtn);
        orderQtyText = (MyTextView) view.findViewById(R.id.orderQtyText);

        setMsgBox = (LinearLayout) view.findViewById(R.id.setMsgBox);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String id;
        if (isAdd) {
            id = getArguments().getString("id");
            options = new ArrayList<>();
            orderQty = 1;
            rootItem = new GeneralItemWrapper(getItem(id));
            rootItem.fillChildren();
        } else {
            int position = getArguments().getInt("position");
            if (FoodOrderData.getInstance().getFoodOrder().size() > position) {
                FoodOrder detail = FoodOrderData.getInstance().getFoodOrder().get(position);
                rootItem = detail.getFood();
                orderQty = detail.getOrderQty();
                if (rootItem.getOptionSet().size() > 0) {
                    orderQtyDesc.setVisibility(View.INVISIBLE);
                    orderMinusBtn.setVisibility(View.INVISIBLE);
                    orderQtyText.setVisibility(View.INVISIBLE);
                    orderPlusBtn.setVisibility(View.INVISIBLE);
                }
            }
        }

        pages = rootItem.getOptionSet();
        //Toast.makeText(getActivity(), "has " + pages.size() + " pages", Toast.LENGTH_LONG).show();
        clickOnFood();
        foodTitle2.setText(MainApplication.getLabel(rootItem.getItem().getTitleId()));

        orderQtyText.setText(Integer.toString(orderQty));
        timeItem = getParent(getParent(rootItem.getItem()));
        startTime = timeItem.getStartTime().substring(0, 5);
        if (startTime.startsWith("0")) {
            startTime = startTime.substring(1);
        }
        endTime = timeItem.getEndTime().substring(0, 5);
        if (endTime.startsWith("0")) {
            endTime = endTime.substring(1);
        }
        startTime2 = null;
        if (timeItem.getStartTime2() != null) {
            startTime2 = timeItem.getStartTime2().substring(0, 5);
        }

        if (startTime2 != null && startTime2.startsWith("0")) {
            startTime2 = startTime2.substring(1);
        }

        endTime2 = null;
        if (timeItem.getEndTime2() != null) {
            endTime2 = timeItem.getEndTime2().substring(0, 5);
        }
        if (endTime2 != null && endTime2.startsWith("0")) {
            endTime2 = endTime2.substring(1);
        }
        try {
            String availableFromTo = null;
            if ((startTime2 != null && !"0:00".equals(startTime2)) ||
                    (endTime2 != null && !"0:00".equals(endTime2))) {
                availableFromTo = String.format(
                        MainApplication.getLabel("inroom.availablefromto2"),
                        startTime, endTime, startTime2, endTime2);
            } else {
                availableFromTo = String.format(
                        MainApplication.getLabel("inroom.availablefromto"),
                        startTime, endTime);

                if ("0:00".equals(startTime) && "0:00".equals(endTime) && !((rootItem.getItem().getCanOrder() != 1 || getParent(rootItem.getItem()).getCanOrder() != 1) && this.getString(R.string.mustShowOrder).equalsIgnoreCase("0"))) {
                    availableFromTo = "";
                    extensionText.setVisibility(View.INVISIBLE);
                }
            }
            tipsText.setText(Html.fromHtml(availableFromTo + "<br/><br/>" + MainApplication.getLabel("inroom.remind")
            ));


            Log.i("inRoom", "Parent Label = " + MainApplication.getLabel(getParent(getParent(rootItem.getItem())).getTitleId()));

            //for PHK case, PHK minibar is inside IRD page and it doesn't charge 10% service fee, so need hide the tips text
            if (MainApplication.getLabel(getParent(getParent(rootItem.getItem())).getTitleId()).equalsIgnoreCase("Minibar")) {
                tipsText.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clickOnFood() {

        detailBox.setVisibility(View.VISIBLE);
        optionBox.setVisibility(View.INVISIBLE);

        //for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        map.put("Choice", MainApplication.getEngLabel(rootItem.getItem().getTitleId()));
        FlurryAgent.logEvent("DinItem", map);

        foodTitle.setText(MainApplication.getLabel(rootItem.getItem().getTitleId()));

        String dollarSign = "US $";
        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
            dollarSign = "RMB ";
        }
        if (GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            dollarSign = "HK $";
        }

        foodPrice.setText(dollarSign + Helper.processPrice(rootItem.getItem().getPrice() + ""));

        if (rootItem.getItem().getPrice() == 0) {
            foodPrice.setText("");
        }


        String pish = "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/minion.otf\")}body {font-family: MyFont; color:#fff}</style></head><body>";
        String pas = "</body></html>";
        String myHtmlString = pish + MainApplication.getLabel(rootItem.getItem().getDescriptionId()) + pas;

        foodDes.loadDataWithBaseURL(null, myHtmlString, "text/html", "UTF-8", null);

        orderItemBtn.setText(MainApplication.getLabel("Next"));

        if ((rootItem.getItem().getCanOrder() != 1 || getParent(rootItem.getItem()).getCanOrder() != 1) && this.getString(R.string.mustShowOrder).equalsIgnoreCase("0")) {
            bottomBtnContainer.setVisibility(View.GONE);
            extensionText.setVisibility(View.VISIBLE);
        } else {
            extensionText.setVisibility(View.INVISIBLE);
        }
    }

    private void clickOnOrderItem() {
        currentSetNumber = 0;
        currentPage = 0;

        currentPageStack = new ArrayList<>();
        optionSetStack = new ArrayList<>();

        for (int i = 0; i < orderQty; i++) {
            ArrayDeque<GeneralItemWrapper> stack = new ArrayDeque<>();

            if (isAdd) {
                GeneralItemWrapper item = new GeneralItemWrapper(rootItem.getItem());
                item.fillChildren();
                stack.add(item);
            } else {
                stack.add(rootItem);
            }
            currentPageStack.add(new ArrayDeque<Integer>());
            optionSetStack.add(stack);
        }

        setNumberTitle.setText(String.format(MainApplication.getLabel("inroom.setnumbertitle"), currentSetNumber + 1));

        if (rootItem.getOptionSet().size() == 0) {
            placeOrder();
        } else if (orderQty > 1) {
            showSetNumberPage();
        } else {
            showOptionPage();
        }
    }

    public void clickOnOrderItemOk() {

        if (orderQty > 1 && inSetNumberPromptPage) {
            // Set Number Page -> Option page
            inSetNumberPromptPage = false;
            showOptionPage();
        } else if (optionSetStack.get(currentSetNumber).size() > 1 || currentPage < optionSetStack.get(currentSetNumber).peekFirst().getOptionSet().size()) {
            // Look for option set in option set first
            for (GeneralItemWrapper i : adapterData) {

                if (i.isChecked() && (i.getOptions().size() > 0 || i.getOptionSet().size() > 0)) {

                    // Go down one level
                    optionSetStack.get(currentSetNumber).add(i);
                    currentPageStack.get(currentSetNumber).add(currentPage);
                    currentPage = 0;
                    showOptionPage();
                    return;
                }
            }

            if (optionSetStack.get(currentSetNumber).size() > 1) {
                // Go back to first level
                while (optionSetStack.get(currentSetNumber).size() > 1) {
                    optionSetStack.get(currentSetNumber).pollLast();
                    currentPage = currentPageStack.get(currentSetNumber).pollLast();
                }

                // Check if at last page again
                if (currentPage < optionSetStack.get(currentSetNumber).peekFirst().getOptionSet().size() - 1) {
                    currentPage++;
                    showOptionPage();
                } else {
                    if (currentSetNumber < orderQty - 1) {
                        currentPageStack.get(currentSetNumber).add(currentPage);
                        currentSetNumber++;
                        setNumberTitle.setText(String.format(MainApplication.getLabel("inroom.setnumbertitle"), currentSetNumber + 1));
                        currentPage = 0;
                        showSetNumberPage();
                    } else {
                        placeOrder();
                    }
                }
                return;
            }

            // If not, move to next (can be next option page, next set or delivery time page)
            if (currentPage < optionSetStack.get(currentSetNumber).peekFirst().getOptionSet().size() - 1) {
                currentPage++;
                showOptionPage();
            } else {
                if (currentSetNumber < orderQty - 1) {
                    currentPageStack.get(currentSetNumber).add(currentPage);
                    setNumberTitle.setText(String.format(MainApplication.getLabel("inroom.setnumbertitle"), currentSetNumber + 1));
                    currentSetNumber++;
                    currentPage = 0;
                    showSetNumberPage();
                } else {
                    placeOrder();
                }
            }
        } else {
            // End of first level option set
            if (currentSetNumber < orderQty - 1) {
                currentPageStack.get(currentSetNumber).add(currentPage);
                setNumberTitle.setText(String.format(MainApplication.getLabel("inroom.setnumbertitle"), currentSetNumber + 1));
                currentSetNumber++;
                currentPage = 0;
                showSetNumberPage();
            } else {
                placeOrder();
            }
        }
    }

    public void clickOnOrderItemBack() {

        if (inSetNumberPromptPage) {
            inSetNumberPromptPage = false;
            if (currentSetNumber == 0) {
                showDetailPage();
            } else {
                // Return to last option of last set number
                currentSetNumber--;
                setNumberTitle.setText(String.format(MainApplication.getLabel("inroom.setnumbertitle"), currentSetNumber + 1));
                currentPage = currentPageStack.get(currentSetNumber).pollLast();
                showOptionPage();
            }
        } else {
            if (optionSetStack.get(currentSetNumber).size() > 1) {
                // Pop up one level
                optionSetStack.get(currentSetNumber).pollLast();
                currentPage = currentPageStack.get(currentSetNumber).pollLast();
                showOptionPage();
            } else if (currentPage > 0) {
                currentPage--;
                showOptionPage();
            } else if (orderQty > 1) {
                showSetNumberPage();
            } else if (currentPage == 0 && optionSetStack.get(currentSetNumber).size() == 1) {
                showDetailPage();
            }
        }
    }

    private void showDetailPage() {
        detailBox.setVisibility(View.VISIBLE);
        optionBox.setVisibility(View.INVISIBLE);
        setMsgBox.setVisibility(View.INVISIBLE);
    }

    private void showSetNumberPage() {
        setMsgBox.setVisibility(View.VISIBLE);
        detailBox.setVisibility(View.INVISIBLE);
        optionBox.setVisibility(View.INVISIBLE);
        setPromptTitle.setText(String.format(MainApplication.getLabel("inroom.setnumberprompt"), currentSetNumber + 1));
        inSetNumberPromptPage = true;
    }


    private void showOptionPage() {
        optionBox.setVisibility(View.VISIBLE);
        detailBox.setVisibility(View.INVISIBLE);
        setMsgBox.setVisibility(View.INVISIBLE);

        GeneralItemWrapper currentOptionSet;
        if (optionSetStack.get(currentSetNumber).size() == 1) {
            currentOptionSet = optionSetStack.get(currentSetNumber).peekLast().getOptionSet().get(currentPage);
        } else {
            currentOptionSet = optionSetStack.get(currentSetNumber).peekLast();
        }

        int min = currentOptionSet.getItem().getMinChoice();
        int max = currentOptionSet.getItem().getMaxChoice();
        Log.d("Choices", "deliveryMin = " + min + ", max = " + max);
        String optionTitleStr = "";
        if (currentOptionSet.getItem().getComplexOption() != 1) {
            optionTitleStr = " - ";
            try {
                if (min != 0 && max != 0 && min != max) {
                    optionTitleStr += String.format(MainApplication.getLabel("INROOM_ORDER_MIN_MAX"), min, max);
                } else if (min == 0 && max != 0) {
                    optionTitleStr += String.format(MainApplication.getLabel("INROOM_ORDER_MAX"), max);
                } else {
                    optionTitleStr += String.format(MainApplication.getLabel("INROOM_ORDER_MIN"), min);
                }
            } catch (IllegalFormatException e) {
                optionTitleStr = "";
            }
        }
        optionTitle.setText(MainApplication.getLabel(currentOptionSet.getItem().getTitleId()) + optionTitleStr);
        if (MainApplication.getEngLabel(currentOptionSet.getItem().getTitleId()).toLowerCase().contains("please select")) {
            //optionTitle.setText(MainApplication.getLabel(pages.get(currentPage).getTitleId()));
        }

        setNumberTitle.setText(String.format(MainApplication.getLabel("inroom.setnumbertitle"), currentSetNumber + 1));

        boolean isComplexOption = currentOptionSet.getItem().getComplexOption() == 1;
        int selectedCount = 0;
        if (isComplexOption) {
            FoodOrderComplexItemAdapter adapter = new FoodOrderComplexItemAdapter(getActivity(), pages.get(currentPage).getOptions(), options, (FoodOrderComplexItemAdapter.Callback) this);
            optionItem_gridView.setAdapter(adapter);
            optionItem_gridView.setNumColumns(1);
        } else {
            adapterData = new ArrayList<>();
            adapterData.addAll(currentOptionSet.getOptionSet());
            adapterData.addAll(currentOptionSet.getOptions());
            for (GeneralItemWrapper i : adapterData) {
                if (i.isChecked()) {
                    selectedCount++;
                }
            }
            FoodOrderItemAdapter adapter = new FoodOrderItemAdapter(getActivity(), adapterData, this);
            optionItem_gridView.setAdapter(adapter);
            optionItem_gridView.setNumColumns(2);
        }
        onSelectionChanged(selectedCount);
    }

    @Override
    public void onSelectionChanged(int selectedCount) {
        boolean choiceCountValid = false;
        if (pages.get(currentPage).getItem().getComplexOption() == 1) {
            if (rootItem.getItem().getPrice() > 0.0) {
                optionItemOkBtn.setEnabled(true);
            } else {
                if (currentPage == pages.size() - 1) {
                    optionItemOkBtn.setEnabled(options.size() > 0);
                } else {
                    optionItemOkBtn.setEnabled(true);
                }
            }
        } else {
            int min = pages.get(currentPage).getItem().getMinChoice();
            int max = pages.get(currentPage).getItem().getMaxChoice();
            Log.d("Choices", "deliveryMin = " + min + ", max = " + max);
            if (min != 0 && max != 0) {
                choiceCountValid = selectedCount >= min && selectedCount <= max;
            } else if (min == 0 && max != 0) {
                choiceCountValid = selectedCount <= max;
            } else {
                choiceCountValid = selectedCount >= min;
            }
            optionItemOkBtn.setEnabled(choiceCountValid);
        }
    }

    public void placeOrder() {
        close();

        if (isAdd) {

            boolean isOrderExistBefore = (FoodOrderData.getInstance().getFoodOrder().size() > 0);

            if (rootItem.getOptionSet().size() >= 1) {
                for (int i = 0; i < orderQty; i++) {
                    GeneralItemWrapper orderItem = optionSetStack.get(i).getFirst();
                    FoodOrder orderDetail = new FoodOrder(orderItem, 1);
                    FoodOrderData.getInstance().getFoodOrder().add(orderDetail);
                }
            } else {
                FoodOrder orderDetail = new FoodOrder(rootItem, orderQty);
                FoodOrderData.getInstance().getFoodOrder().add(orderDetail);
            }

            if (!isOrderExistBefore) {
                FoodOrderData.getInstance().setStartTime(startTime);
                FoodOrderData.getInstance().setEndTime(endTime);
                FoodOrderData.getInstance().setStartTime2(startTime2);
                FoodOrderData.getInstance().setEndTime2(endTime2);
            } else if (FoodOrderData.getInstance().getStartTime().equals("0:00") && FoodOrderData.getInstance().getEndTime().equals("0:00")) {
                FoodOrderData.getInstance().setStartTime(startTime);
                FoodOrderData.getInstance().setEndTime(endTime);
                FoodOrderData.getInstance().setStartTime2(startTime2);
                FoodOrderData.getInstance().setEndTime2(endTime2);
            }

        } else {
            FoodOrder orderDetail = new FoodOrder(rootItem, orderQty);
            FoodOrderData.getInstance().getFoodOrder().set(getArguments().getInt("position"), orderDetail);
        }
        callback.onPlaceOrder();
    }

    public void close() {
        callback.onClose(this);
    }

    public void changeOrderQty(int delta) {
        orderQty = orderQty + delta;
        if (orderQty > rootItem.getItem().getMaxQuantity()) {
            orderQty = rootItem.getItem().getMaxQuantity();
        }
        if (orderQty < 1) {
            orderQty = 1;
        }
        orderQtyText.setText(Integer.toString(orderQty));
    }

    public void clickClose(View v) {
        close();
    }

    private GeneralItem getItem(String id) {
        for (GeneralItem item : GlobalValue.getInstance().getAllItemArray()) {
            if (item.getItemId().equalsIgnoreCase(id)) {
                return item;
            }
        }
        return null;
    }

    private ArrayList<GeneralItem> getChildren(String id) {
        ArrayList<GeneralItem> result = new ArrayList<>();
        for (GeneralItem item : GlobalValue.getInstance().getAllItemArray()) {
            if (item.getParentId().equalsIgnoreCase(id)) {
                result.add(item);
            }
        }
        return result;
    }

    private GeneralItem getParent(GeneralItem item) {
        for (GeneralItem i : GlobalValue.getInstance().getAllItemArray()) {
            if (i.getItemId().equals(item.getParentId())) {
                return i;
            }
        }
        return null;
    }

}
