package com.hsh.esdbsp.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.BaseActivity;
import com.hsh.esdbsp.adapter.dining.PreviewOrderAdapter;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.GeneralItem;

public class InRoomPreviewFragment extends BaseFragment{

	private final String TAG = "InRoomPreviewFragment";

	private View baseView;
	private ProgressBar mProgressBar;

	private ListView previewList;
	private Button backBtn;
	
	private ArrayList<GeneralItem> foodItemArray;
	
	private PreviewOrderAdapter previewOrderAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView fire");
		baseView = inflater.inflate(R.layout.cms_in_room_preview_pch, container,
				false);
		BaseActivity parent = (BaseActivity) this.getActivity();
		mProgressBar = parent.getProgressBar();

		initUI();
		
		if(foodItemArray == null){
			foodItemArray = new ArrayList<GeneralItem>();
		}		
		
		//foodItemArray = this.getArguments().getParcelableArrayList("foodItemArray");
		GeneralItem n1 = new GeneralItem("id");
		n1.setTitleId("Continental Breakfast");
		n1.setPrice(100);
		GeneralItem n2= new GeneralItem("id2");
		n2.setTitleId("The Peninsula Breakfast");
		n2.setPrice(200);
		GeneralItem n3 = new GeneralItem("id3");
		n3.setTitleId("Continental Breakfast");
		n3.setPrice(300);
		GeneralItem n4 = new GeneralItem("id3");
		n4.setTitleId("Continental Breakfast");
		n4.setPrice(400);
		GeneralItem n5 = new GeneralItem("id3");
		n5.setTitleId("Continental Breakfast");
		n5.setPrice(500);
		GeneralItem n6 = new GeneralItem("id3");
		n6.setTitleId("Continental Breakfast");
		n6.setPrice(600);
		
		foodItemArray.add(n1);
		foodItemArray.add(n2);
		foodItemArray.add(n3);
		foodItemArray.add(n4);
		foodItemArray.add(n5);
		foodItemArray.add(n6);
		
		previewOrderAdapter = new PreviewOrderAdapter(this.getActivity(), foodItemArray);
		previewOrderAdapter.setParentName(TAG);
		previewList.setAdapter(previewOrderAdapter);
		
		return baseView;
	}
	
	public void setupList(ArrayList<GeneralItem> _foodSubCatArray){
		foodItemArray.clear();;
		
		for(GeneralItem item : _foodSubCatArray){
			foodItemArray.add(item);
    	}
		
		Log.i(TAG,"fooSubCatArray length = " + foodItemArray.size());
		previewOrderAdapter.setCurrentPosition(0);
		previewOrderAdapter.notifyDataSetChanged();
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	private void initUI() {
		previewList = (ListView) baseView.findViewById(R.id.list);
		/*backBtn = (Button) baseView.findViewById(R.id.backBtn);
		
		backBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				InRoomOptionFragment.this.getActivity().getSupportFragmentManager().popBackStack();
			}
		});*/
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mProgressBar.setVisibility(View.GONE);
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@Override
	public void onResume() {

		super.onResume();
	}

	@Override
	public void onPause() {

		super.onPause();
		cancelCurrentTask();
	}

	@Override
	public void onDestroyView() {
		Log.e(TAG, "onDestroyView");

		cancelCurrentTask();
		super.onDestroyView();

	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestroy");
		cancelCurrentTask();
		super.onDestroy();

	}

}
