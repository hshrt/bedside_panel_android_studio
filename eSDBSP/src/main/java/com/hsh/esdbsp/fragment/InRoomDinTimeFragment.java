package com.hsh.esdbsp.fragment;

import java.util.Calendar;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class InRoomDinTimeFragment extends Fragment {
	
	private ImageButton dayUpBtn, dayDownBtn, hourUpBtn, hourDownBtn, minUpBtn, minDownBtn;
	private Button closeBtn, confirmBtn;
	private TextView selectDeliveryTime, notAvailableText;
	private TextView dayTxt, hourTxt, minTxt, dayHeaderTxt, hourHeaderTxt, minHeaderTxt;
	private int day, hour, min;
	private Callback callback;
	
	public interface Callback {
		public void onTimeConfirm(Fragment fragment, int day, int hour, int min);
	}
	
	public static InRoomDinTimeFragment newInstance(Callback callback) {
		InRoomDinTimeFragment f = new InRoomDinTimeFragment();
		f.callback = callback;
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.cms_food_order_time, container, false);
		dayUpBtn = (ImageButton) view.findViewById(R.id.dayUpBtn);
		dayUpBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeDay(1 - day);
			}
		});
		dayDownBtn = (ImageButton) view.findViewById(R.id.dayDownBtn);
		dayDownBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeDay(1 - day);
			}
		});
		hourUpBtn = (ImageButton) view.findViewById(R.id.hourUpBtn);
		hourUpBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeTime(-1, 0);
			}
		});
		hourDownBtn = (ImageButton) view.findViewById(R.id.hourDownBtn);
		hourDownBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeTime(1, 0);
			}
		});
		minUpBtn = (ImageButton) view.findViewById(R.id.minUpBtn);
		minUpBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeTime(0, -10);
			}
		});
		minDownBtn = (ImageButton) view.findViewById(R.id.minDownBtn);
		minDownBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeTime(0, 10);
			}
		});
		selectDeliveryTime = (TextView) view.findViewById(R.id.selectDeliveryTime);
		selectDeliveryTime.setText(MainApplication.getLabel("INROOM_ORDER_TIME_SELECT"));
		notAvailableText = (TextView) view.findViewById(R.id.notAvailableText);
		notAvailableText.setText(MainApplication.getLabel("INROOM_ORDER_TIME_NA"));
		notAvailableText.setVisibility(View.INVISIBLE);
		dayHeaderTxt = (TextView) view.findViewById(R.id.dayHeaderText);
		dayHeaderTxt.setText(MainApplication.getLabel("INROOM_ORDER_DAY"));
		hourHeaderTxt = (TextView) view.findViewById(R.id.hourHeaderTxt);
		hourHeaderTxt.setText(MainApplication.getLabel("INROOM_ORDER_HOUR"));
		minHeaderTxt = (TextView) view.findViewById(R.id.minHeaderTxt);
		minHeaderTxt.setText(MainApplication.getLabel("INROOM_ORDER_MINUTE"));
		dayTxt = (TextView) view.findViewById(R.id.dayText);
		hourTxt = (TextView) view.findViewById(R.id.hourTxt);
		minTxt = (TextView) view.findViewById(R.id.minTxt);
		closeBtn = (Button) view.findViewById(R.id.closeBtn);
		closeBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
				ft.remove(InRoomDinTimeFragment.this);
				ft.commit();
			}
		});
		confirmBtn = (Button) view.findViewById(R.id.confirmBtn);
		confirmBtn.setText(MainApplication.getLabel("Confirm"));
		confirmBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				callback.onTimeConfirm(InRoomDinTimeFragment.this, day, hour, min);
			}
		});
		changeTime(0, 0);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		day = getArguments().getInt("day");
		hour = getArguments().getInt("hour");
		min = getArguments().getInt("min");
		changeDay(day);
		changeTime(0, 0);
	}

	private void changeDay(int newDay) {
		day = newDay;
		if (day == 0) {
			dayTxt.setText(MainApplication.getLabel("Today"));
		} else if (day == 1) {
			dayTxt.setText(MainApplication.getLabel("Tomorrow"));
		}
		checkTimeValid();
	}
	
	private void changeTime(int hourDelta, int minDelta) {
		hour += hourDelta;
		min += minDelta;
		if (hour < 0) {
			hour = 23;
		}
		if (hour > 23) {
			hour = 0;
		}
		if (min < 0) {
			min = 50;
		}
		if (min >= 60) {
			min = 0;
		}
		hourTxt.setText(String.format("%02d", hour));
		minTxt.setText(String.format("%02d", min));
		checkTimeValid();
	}
	
	private void checkTimeValid() {
		int currentTime = 0;
		String tmpHeadClock[] = MainApplication.getMAS()
				.getData("data_curtime").split(":");
		if (tmpHeadClock.length == 2) {
			currentTime = Integer.parseInt(tmpHeadClock[0]) * 60 + Integer.parseInt(tmpHeadClock[1]); 
		} else {
			Calendar c = Calendar.getInstance();
			currentTime = c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE);
		}
		if (currentTime > day * 1440 + hour * 60 + min) {
			notAvailableText.setVisibility(View.VISIBLE);
			confirmBtn.setEnabled(false);
		} else {
			notAvailableText.setVisibility(View.INVISIBLE);
			confirmBtn.setEnabled(true);
		}
	}
}
