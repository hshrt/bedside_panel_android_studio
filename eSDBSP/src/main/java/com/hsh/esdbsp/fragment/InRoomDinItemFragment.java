package com.hsh.esdbsp.fragment;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.BaseActivity;
import com.hsh.esdbsp.adapter.dining.FoodSubCatAdapter;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.GeneralItem;

public class InRoomDinItemFragment extends BaseFragment{

	private final String TAG = "InRoomDinItemFragment";

	private View baseView;
	private ProgressBar mProgressBar;

	private ListView foodList;
	private Button backBtn;
	
	private ArrayList<GeneralItem> foodItemArray;
	
	private FoodSubCatAdapter foodItemAdapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView fire");
		baseView = inflater.inflate(R.layout.cms_in_room_sub_category_pch, container,
				false);
		BaseActivity parent = (BaseActivity) this.getActivity();
		mProgressBar = parent.getProgressBar();

		initUI();
		
		if(foodItemArray == null){
			foodItemArray = new ArrayList<GeneralItem>();
		}
		
		
		foodItemArray = this.getArguments().getParcelableArrayList("foodItemArray");
		
		foodItemAdapter = new FoodSubCatAdapter(this.getActivity(), foodItemArray);
		foodItemAdapter.setParentName(TAG);
		foodList.setAdapter(foodItemAdapter);
		
		return baseView;
	}
	
	public void setupList(ArrayList<GeneralItem> _foodSubCatArray){
		foodItemArray.clear();;
		
		for(GeneralItem item : _foodSubCatArray){
			foodItemArray.add(item);
    	}
		
		Log.i(TAG,"fooSubCatArray length = " + foodItemArray.size());
		foodItemAdapter.setCurrentPosition(0);
		foodItemAdapter.notifyDataSetChanged();
	}


	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}

	private void initUI() {
		foodList = (ListView) baseView.findViewById(R.id.list);
		backBtn = (Button) baseView.findViewById(R.id.backBtn);
		
		backBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				InRoomDinItemFragment.this.getActivity().getSupportFragmentManager().popBackStack();
			}
		});
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mProgressBar.setVisibility(View.GONE);
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	@Override
	public void onResume() {

		super.onResume();
	}

	@Override
	public void onPause() {

		super.onPause();
		cancelCurrentTask();
	}

	@Override
	public void onDestroyView() {
		Log.e(TAG, "onDestroyView");

		cancelCurrentTask();
		super.onDestroyView();

	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestroy");
		cancelCurrentTask();
		super.onDestroy();

	}

}
