package com.hsh.esdbsp.fragment;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.adapter.flight.AirlineAdapter;
import com.hsh.esdbsp.adapter.flight.AirportAdapter;
import com.hsh.esdbsp.adapter.flight.FlightRouteAdapter;
import com.hsh.esdbsp.activity.BaseActivity;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.Airline;
import com.hsh.esdbsp.model.Airport;
import com.hsh.esdbsp.model.FlightInfo;
import com.hsh.esdbsp.network.ApiRequest;
import com.hsh.esdbsp.network.XMLCaller;
import com.hsh.esdbsp.parser.GetAllAirlineParser;
import com.hsh.esdbsp.parser.GetAllAirlineParser.GetAllAirlineParserInterface;
import com.hsh.esdbsp.parser.GetAllAirportParser;
import com.hsh.esdbsp.parser.GetAllAirportParser.GetAllAirportParserInterface;
import com.hsh.esdbsp.parser.GetFlightStatusParser;
import com.hsh.esdbsp.parser.GetFlightStatusParser.GetFlightStatusParserInterface;
import com.hsh.esdbsp.parser.GetFlightTrackParser;

public class FlightTrackFragment extends BaseFragment implements
		GetFlightTrackParser.GetFlightTrackParserInterface,GetFlightStatusParserInterface ,GetAllAirportParserInterface,GetAllAirlineParserInterface{

	private final String TAG = "FlightTrackFragment";
	private View baseView;
	private ProgressBar mProgressBar;

	private Button byFlightNoBtn;
	private Button byDestinationBtn;
	private Button searchBtn;
	private EditText field1; // can represent airline or origin
	private EditText field2; // can represent Flight No. or Destination

	private GoogleMap map;
	private SupportMapFragment mMapFragment;
	
	private boolean mapShown = false; 

	private LinearLayout mapContainer;
	private LinearLayout mapPopUI;
	private LinearLayout container;
	
	private LinearLayout keyboard_container;
	
	private LinearLayout errorMessageDialogLayout;
	private ImageView errorMessageDialogCloseImage;
	private MyTextView errorMessageDialogLabel;
	
	private ListView choiceList;
	
	private AirlineAdapter airlineAdapter;
	private AirportAdapter airportAdapter;
	
	private ArrayList<Airline> airlineArray;
	private ArrayList<Airport> airportArray;
	
	private RelativeLayout flightRoutePopupContainer;
	private ListView flightRouteList;
	private ArrayList<FlightInfo> flightInfoArray;
	private ArrayList<String> latlonArray;
	private FlightRouteAdapter flightRouteAdapter;
	
	private int currentAirlineListPosition = 0;
	private int currentListPosition;
	private int currentListPosition_field2;  
	
	private Airline airline;
	private Airport origin;
	private Airport destination;
	
	//popup UI
	private TextView flightsRouteTitle;
	private TextView flightsRouteNumLabel;
	private TextView flightsRouteAirlineLabel;
	private TextView flightsRouteDepartureLabel;
	private TextView flightsRouteArrivalLabel;
	private TextView flightsRouteStatusLabel;
	
	private MyTextView numericKeyboard_1Button;
	private MyTextView numericKeyboard_2Button;
	private MyTextView numericKeyboard_3Button;
	private MyTextView numericKeyboard_4Button;
	private MyTextView numericKeyboard_5Button;
	private MyTextView numericKeyboard_6Button;
	private MyTextView numericKeyboard_7Button;
	private MyTextView numericKeyboard_8Button;
	private MyTextView numericKeyboard_9Button;
	private MyTextView numericKeyboard_0Button;
	private ImageView numericKeyboard_backspaceButton;
	
	private ImageView closeBtn;
	
	private MyTextView flightsMap_ActualLegend;
	private MyTextView flightsMap_PlanLegend;
	public enum Mode {
		BY_FLIGHT_NO, BY_DESTINATION
	}
	
	Mode mode;
	
	public enum Focus {
		FIELD1, FIELD2
	}
	
	Focus currentFocus;

	public enum ApiCallType {
		GET_FLIGHT_STATUS, GET_FLIGHT_TRACK_BY_ID, GET_FLIGHT_TRACK_BY_DESTINATION, GET_ALL_AIRLINE, GET_ALL_AIRPORT
	}

	ApiCallType apiCallType;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.d(TAG, "onCreateView fire");
		baseView = inflater.inflate(R.layout.airport_flight_track, container,
				false);
		BaseActivity parent = (BaseActivity) this.getActivity();
		mProgressBar = parent.getProgressBar();

		initUI();

		byFlightNoBtn.performClick();
		
		if (flightInfoArray == null) {
			flightInfoArray = new ArrayList<FlightInfo>();
		}
		
		if (airlineArray == null) {airlineArray = new ArrayList<Airline>();}

		if (airportArray == null) {airportArray = new ArrayList<Airport>();}

		
		if (airlineAdapter == null) {
			airlineAdapter = new AirlineAdapter(this.getActivity(), airlineArray);
			choiceList.setAdapter(airlineAdapter);
		}

		if (airportAdapter == null) {
			airportAdapter = new AirportAdapter(this.getActivity(), airportArray);
		}
		
		if( flightRouteAdapter == null){
			flightRouteAdapter = new FlightRouteAdapter(this.getActivity(), flightInfoArray);
			flightRouteList.setAdapter(flightRouteAdapter);
		}
		
		getAllAirline();
		
		flightRouteList.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				//showMap(flightInfoArray.get(position));
				getFlightStatus(flightInfoArray.get(position).getFlightId());
			}});
		

		mMapFragment = new SupportMapFragment();
		FragmentTransaction fragmentTransaction = getChildFragmentManager()
				.beginTransaction();
		fragmentTransaction.add(R.id.map, mMapFragment);
		fragmentTransaction.commit();
		Log.i("debug", "mMapFragment " + mMapFragment);

		mMapFragment.getMapAsync(new OnMapReadyCallback() {

			@Override
			public void onMapReady(GoogleMap map) {
				FlightTrackFragment.this.map = map;
				if (map != null) {
					map.animateCamera(CameraUpdateFactory.zoomTo(map
							.getMaxZoomLevel() - 5));
					Log.i("debug", "map " + map);

				}
			}
			
		});

		return baseView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

	}
	
	private void setupLabel(){
		byFlightNoBtn.setText(MainApplication.getLabel("flighttracks.byflight.label"));
		byDestinationBtn.setText(MainApplication.getLabel("flighttracks.bydestination.label"));
		searchBtn.setText(MainApplication.getLabel("flighttracks.search.label"));
		
		field1.setHint(MainApplication.getLabel("flighttracks.selectairline.hint"));
		field2.setHint(MainApplication.getLabel("flighttracks.enterflight.hint"));
		
		flightsRouteTitle.setText(MainApplication.getLabel("flighttracks.selectflight.label"));
		flightsRouteNumLabel.setText(MainApplication.getLabel("flightstats.flight.label"));
		flightsRouteAirlineLabel.setText(MainApplication.getLabel("flightstats.airline.label"));
		flightsRouteDepartureLabel.setText(MainApplication.getLabel("flightstats.departures.label"));
		flightsRouteArrivalLabel.setText(MainApplication.getLabel("flightstats.arrivals.label"));
		flightsRouteStatusLabel.setText(MainApplication.getLabel("flightstats.status.label"));
	}

	private void initUI() {
		byFlightNoBtn = (Button) baseView.findViewById(R.id.byFlightNoBtn);
		byDestinationBtn = (Button) baseView.findViewById(R.id.byDestinationBtn);
		searchBtn = (Button) baseView.findViewById(R.id.searchingBtn);

		field1 = (EditText) baseView.findViewById(R.id.field1);
		field2 = (EditText) baseView.findViewById(R.id.field2);
		
		flightRoutePopupContainer = (RelativeLayout) baseView.findViewById(R.id.flightRoutePopupContainer);
		flightRouteList = (ListView) baseView.findViewById(R.id.flightsRouteSelectorListView);
		
		flightsRouteTitle = (TextView) baseView.findViewById(R.id.flightsRouteTitle);
		flightsRouteNumLabel = (TextView) baseView.findViewById(R.id.flightsRouteNumLabel);
		flightsRouteAirlineLabel = (TextView) baseView.findViewById(R.id.flightsRouteAirlineLabel);
		flightsRouteDepartureLabel = (TextView) baseView.findViewById(R.id.flightsRouteDepartureLabel);
		flightsRouteArrivalLabel = (TextView) baseView.findViewById(R.id.flightsRouteArrivalLabel);
		flightsRouteStatusLabel = (TextView) baseView.findViewById(R.id.flightsRouteStatusLabel);
		
		closeBtn = (ImageView) baseView.findViewById(R.id.flightsRouteDialogCloseImage);
		mapPopUI = (LinearLayout) baseView.findViewById(R.id.map_popUI);
		
		errorMessageDialogLayout = (LinearLayout) baseView.findViewById(R.id.errorMessageDialogLayout);
		errorMessageDialogCloseImage = (ImageView) baseView.findViewById(R.id.errorMessageDialogCloseImage);
		errorMessageDialogLabel = (MyTextView) baseView.findViewById(R.id.errorMessageDialogLabel);
		
		
		keyboard_container = (LinearLayout) baseView.findViewById(R.id.keyboard_container);
		
		
		numericKeyboard_1Button = (MyTextView) baseView.findViewById(R.id.numericKeyboard_1Button);
		numericKeyboard_2Button = (MyTextView) baseView.findViewById(R.id.numericKeyboard_2Button);
		numericKeyboard_3Button = (MyTextView) baseView.findViewById(R.id.numericKeyboard_3Button);
		numericKeyboard_4Button = (MyTextView) baseView.findViewById(R.id.numericKeyboard_4Button);
		numericKeyboard_5Button = (MyTextView) baseView.findViewById(R.id.numericKeyboard_5Button);
		numericKeyboard_6Button = (MyTextView) baseView.findViewById(R.id.numericKeyboard_6Button);
		numericKeyboard_7Button = (MyTextView) baseView.findViewById(R.id.numericKeyboard_7Button);
		numericKeyboard_8Button = (MyTextView) baseView.findViewById(R.id.numericKeyboard_8Button);
		numericKeyboard_9Button = (MyTextView) baseView.findViewById(R.id.numericKeyboard_9Button);
		numericKeyboard_0Button = (MyTextView) baseView.findViewById(R.id.numericKeyboard_0Button);
		numericKeyboard_backspaceButton = (ImageView) baseView.findViewById(R.id.numericKeyboard_backspaceButton);
		
		//map top up UI
		flightsMap_ActualLegend = (MyTextView) baseView.findViewById(R.id.flightsMap_ActualLegend);
		flightsMap_PlanLegend = (MyTextView) baseView.findViewById(R.id.flightsMap_PlanLegend);
		
		flightsMap_PlanLegend.setText(MainApplication.getLabel("flighttracks.planroute.legend"));
		flightsMap_ActualLegend.setText(MainApplication.getLabel("flighttracks.actualroute.legend"));
		
		field1.setInputType(0);
		//field2.setInputType(0);
		
		setupLabel();
		
		Helper.setAppFonts(byFlightNoBtn);
		Helper.setAppFonts(byDestinationBtn);
		Helper.setAppFonts(searchBtn);
		
		Helper.setAppFonts(flightsRouteTitle);
		Helper.setAppFonts(flightsRouteNumLabel);
		Helper.setAppFonts(flightsRouteAirlineLabel);
		Helper.setAppFonts(flightsRouteDepartureLabel);
		Helper.setAppFonts(flightsRouteArrivalLabel);
		Helper.setAppFonts(flightsRouteStatusLabel);
		
		Helper.setAppFonts(field1);
		Helper.setAppFonts(field2);
		
		mapContainer = (LinearLayout) baseView.findViewById(R.id.map);
		container = (LinearLayout) baseView.findViewById(R.id.container);
		
		choiceList = (ListView) baseView.findViewById(R.id.choiceList);
		
		choiceList.setVisibility(View.GONE);
 		
		mode = Mode.BY_FLIGHT_NO;
		
		closeBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				flightRoutePopupContainer.setVisibility(View.GONE);
			}
		});
		
		errorMessageDialogCloseImage.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				errorMessageDialogLayout.setVisibility(View.GONE);
			}
		});

		byFlightNoBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mode = Mode.BY_FLIGHT_NO;
				
				//for Flurry log
				final Map<String, String> map = new HashMap<String, String>();
				map.put("Room", MainApplication.getMAS().getData("data_myroom"));		
				FlurryAgent.logEvent("ByFlightNo", map);
				
				byFlightNoBtn.setBackgroundResource(R.drawable.button_on);
				byDestinationBtn.setBackgroundResource(R.drawable.base_btn_selector);
				
				choiceList.setVisibility(View.INVISIBLE);
				
				field1.setHint(MainApplication.getLabel("flighttracks.selectairline.hint"));
				field2.setHint(MainApplication.getLabel("flighttracks.enterflight.hint"));
				
				field1.setText("");
				field2.setText("");
				
				field1.clearFocus();
				field2.clearFocus();
				
				field2.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_NUMBER);
				
				field1.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						choiceList.setVisibility(View.VISIBLE);
						Log.i(TAG, "field 1 clicked, airline");
						Log.i(TAG, "currentAirlineListPosition = " + currentAirlineListPosition);
						airlineAdapter.setCurrentPosition(currentAirlineListPosition);
						
			    		choiceList.setAdapter(airlineAdapter);
			    		choiceList.setSelection(currentAirlineListPosition);
			    		currentFocus = Focus.FIELD1;
			    		
			    		keyboard_container.setVisibility(View.GONE);
					}
					
				});
				field1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				    @Override
				    public void onFocusChange(View v, boolean hasFocus) {
				    	//field1.clearFocus();
				    	Log.i(TAG,"damner delete field 1");
				    	
				    	if(hasFocus){
				    		
				    		
				    	}
				    }
				});
				
				field2.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						choiceList.setVisibility(View.INVISIBLE);
			    		choiceList.setAdapter(null);
			    		
			    		keyboard_container.setVisibility(View.VISIBLE);
					}
					
				});
				
				field2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				    @Override
				    public void onFocusChange(View v, boolean hasFocus) {
				    	//field1.clearFocus();
				    	Log.i(TAG,"damner delete field2");
				    	
				    	if(hasFocus){
				    		
				    		
				    	}
				    }
				});
			}
		});
		
		byDestinationBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mode = Mode.BY_DESTINATION;
				
				//for Flurry log
				final Map<String, String> map = new HashMap<String, String>();
				map.put("Room", MainApplication.getMAS().getData("data_myroom"));		
				FlurryAgent.logEvent("ByDestination", map);
				
				//never has keyboard in byDestination Mode
				keyboard_container.setVisibility(View.GONE);
				
				byFlightNoBtn.setBackgroundResource(R.drawable.base_btn_selector);
				byDestinationBtn.setBackgroundResource(R.drawable.button_on);
				
				choiceList.setVisibility(View.INVISIBLE);
				
				field1.setHint(MainApplication.getLabel("flighttracks.origin.hint"));
				field2.setHint(MainApplication.getLabel("flighttracks.destination.hint"));
				
				field1.clearFocus();
				field2.clearFocus();
				
				field1.setText("");
				field2.setText("");
				
				field2.setInputType(0);
				
				
				
				field1.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						choiceList.setVisibility(View.VISIBLE);
			    		airportAdapter.setCurrentPosition(currentListPosition);
			    		choiceList.setAdapter(airportAdapter);
			    		choiceList.setSelection(currentListPosition);
			    		currentFocus = Focus.FIELD1;
					}
					
				});
				
				field1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				    @Override
				    public void onFocusChange(View v, boolean hasFocus) {
				    	//field1.clearFocus();
				    	Log.i(TAG,"damner delete field 1");
				    	
				    	if(hasFocus){
				    		
				    		/*choiceList.setVisibility(View.VISIBLE);
				    		airportAdapter.setCurrentPosition(currentListPosition);
				    		choiceList.setAdapter(airportAdapter);
				    		currentFocus = Focus.FIELD1;*/
				    	}
				    }
				});
				
				//make the native keyboard dont show out
				field2.setInputType(0);
				
				field2.setOnClickListener(new OnClickListener(){

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						choiceList.setVisibility(View.VISIBLE);
			    		airportAdapter.setCurrentPosition(currentListPosition_field2);
			    		choiceList.setAdapter(airportAdapter);
			    		choiceList.setSelection(currentListPosition_field2);
			    		currentFocus = Focus.FIELD2;
					}
					
				});
				
				field2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
				    @Override
				    public void onFocusChange(View v, boolean hasFocus) {
				    	//field1.clearFocus();
				    	Log.i(TAG,"damner delete field2");
				    	
				    	if(hasFocus){
				    		
				    		
				    	}
				    }
				});
			}
		});
		
		searchBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				
				if(mode == Mode.BY_FLIGHT_NO){
					
					if(!field1.getText().toString().equalsIgnoreCase("")&&!field2.getText().toString().equalsIgnoreCase("")){
						getFlightStatus(airline.getIATA(), field2.getText().toString());
						
						//for Flurry log
						final Map<String, String> map = new HashMap<String, String>();
						map.put("Room", MainApplication.getMAS().getData("data_myroom"));
						map.put("Airline", airline.getName());
						map.put("FlightNo", field2.getText().toString());
						FlurryAgent.logEvent("SearchButton", map);
					}
				}
				else if(mode == Mode.BY_DESTINATION){
					if(!field1.getText().toString().equalsIgnoreCase("")&&!field2.getText().toString().equalsIgnoreCase("")
							&& origin != null && destination != null){
						getFlightsFromRoute(origin.getIATA(),destination.getIATA());
						
						//for Flurry log
						final Map<String, String> map = new HashMap<String, String>();
						map.put("Room", MainApplication.getMAS().getData("data_myroom"));
						map.put("Origin", origin.getName());
						map.put("Destination", destination.getName());
						FlurryAgent.logEvent("SearchButton", map);
					}
				}
			}
		});
		
		OnClickListener numberButtonListener = new OnClickListener(){

			@Override
			public void onClick(View v) {
				
				switch(v.getId()){
					case R.id.numericKeyboard_1Button:
						field2.setText(field2.getText()+"1");
						break;
					case R.id.numericKeyboard_2Button:
						field2.setText(field2.getText()+"2");
						break;
					case R.id.numericKeyboard_3Button:
						field2.setText(field2.getText()+"3");
						break;
					case R.id.numericKeyboard_4Button:
						field2.setText(field2.getText()+"4");
						break;
					case R.id.numericKeyboard_5Button:
						field2.setText(field2.getText()+"5");
						break;
					case R.id.numericKeyboard_6Button:
						field2.setText(field2.getText()+"6");
						break;
					case R.id.numericKeyboard_7Button:
						field2.setText(field2.getText()+"7");
						break;
					case R.id.numericKeyboard_8Button:
						field2.setText(field2.getText()+"8");
						break;
					case R.id.numericKeyboard_9Button:
						field2.setText(field2.getText()+"9");
						break;
					case R.id.numericKeyboard_0Button:
						field2.setText(field2.getText()+"0");
						break;
					case R.id.numericKeyboard_backspaceButton:
						if(field2.getText().length()>0){
							field2.setText(field2.getText().toString().substring(0,field2.getText().toString().length()-1));
						}
						break;
				}
			}
		};
		
		numericKeyboard_1Button.setOnClickListener(numberButtonListener);
		numericKeyboard_2Button.setOnClickListener(numberButtonListener);
		numericKeyboard_3Button.setOnClickListener(numberButtonListener);
		numericKeyboard_4Button.setOnClickListener(numberButtonListener);
		numericKeyboard_5Button.setOnClickListener(numberButtonListener);
		numericKeyboard_6Button.setOnClickListener(numberButtonListener);
		numericKeyboard_7Button.setOnClickListener(numberButtonListener);
		numericKeyboard_8Button.setOnClickListener(numberButtonListener);
		numericKeyboard_9Button.setOnClickListener(numberButtonListener);
		numericKeyboard_0Button.setOnClickListener(numberButtonListener);
		numericKeyboard_backspaceButton.setOnClickListener(numberButtonListener);
	}
	
	public void onAirlinePressed(int position, Airline _airline){
		if( currentFocus== Focus.FIELD1){
			field1.setText(_airline.getName());
			airline = _airline;
			
			currentAirlineListPosition = position;
			Log.i(TAG, "current Airline position = " + currentAirlineListPosition);
		}
		else if(currentFocus== Focus.FIELD2){
			field2.setText(_airline.getName());
		}
		choiceList.setVisibility(View.INVISIBLE);
	}
	public void onAirportPressed(int position, Airport _airport){
		if( currentFocus== Focus.FIELD1){
			field1.setText(_airport.getName());
			origin = _airport;
			
			currentListPosition = position;
		}
		else if(currentFocus== Focus.FIELD2){
			field2.setText(_airport.getName());
			destination = _airport;
			
			currentListPosition_field2 = position;
		}
		choiceList.setVisibility(View.INVISIBLE);
		
		
	}
	
	/*Google Map related Function*/

	private static void zoomToCoverAllMarkers(ArrayList<LatLng> latLngList,
			GoogleMap googleMap) {
		LatLngBounds.Builder builder = new LatLngBounds.Builder();
		/*
		 * for (Marker marker : markers) {
		 * builder.include(marker.getPosition()); }
		 */
		for (LatLng marker : latLngList) {
			builder.include(marker);
		}

		LatLngBounds bounds = builder.build();
		int padding = 30; // offset from edges of the map in pixels
		CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
		googleMap.moveCamera(cu);
		googleMap.animateCamera(cu);
	}

	private void moveCameraAndDraw(ArrayList<LatLng> latLngList, int color) {
		// Instantiates a new Polyline object and adds points to define a
		// rectangle
		PolylineOptions rectOptions = new PolylineOptions();

		for (LatLng latLng : latLngList) {
			rectOptions.add(latLng);
		}
		
		rectOptions.width(13).color(color).geodesic(true);

		// Get back the mutable Polyline
		Polyline polyline = map.addPolyline(rectOptions);
	}
	
private void showMap(FlightInfo flightInfo){
		
		flightRoutePopupContainer.setVisibility(View.GONE);
		container.setVisibility(View.GONE);
		mapContainer.setVisibility(View.VISIBLE);
		mapPopUI.setVisibility(View.VISIBLE);

		//get the origin airport and Destination airport lon/lat
		final ArrayList<LatLng> originDestinationArray = new ArrayList<LatLng>();
		
		String[] airport1 = latlonArray.get(0).split(",");
		LatLng l = new LatLng(Double.parseDouble(airport1[0]),Double.parseDouble(airport1[1]));
		
		Log.i(TAG,"longitude = " + l.longitude + "");
		Log.i(TAG,"latitude = " + l.latitude + "");
		
		String[] airport2 = latlonArray.get(1).split(",");
		LatLng l2 = new LatLng(Double.parseDouble(airport2[0]),Double.parseDouble(airport2[1]));
		
		Log.i(TAG,"longitude = " + l2.longitude + "");
		Log.i(TAG,"latitude = " + l2.latitude + "");
		
		originDestinationArray.add(l);
		originDestinationArray.add(l2);

		// zoomToCoverAllMarkers(array, map);
		
		
		//get the actual flight point
		String[] lats = flightInfo.getLats() != null ? flightInfo.getLats().split(","):null;
		String[] lons = flightInfo.getLons() != null ? flightInfo.getLons().split(","):null;

		final ArrayList<LatLng> flightPointArray = new ArrayList<LatLng>();
		
		if(lats != null){
			for (int x = 0; x < lats.length; x++) {
				Log.i(TAG, "lats = " + lats[x]);
				Log.i(TAG, "lons = " + lons[x]);
				
				try{
				LatLng latLng = new LatLng(Double.parseDouble(lats[x]),
						Double.parseDouble(lons[x]));
				flightPointArray.add(latLng);
				}
				catch(Exception e){
					Log.i(TAG, "lat lon error, we will skip this point");
				}
				//Log.i(TAG, "lat = " + latLng.latitude);
				//Log.i(TAG, "lon = " + latLng.longitude);
			}
		}
		
		Log.i(TAG, "flightPointArray length = " + flightPointArray.size());
		
		//get the planned flight point
		String[] plan_lats = flightInfo.getPlaned_lats() != null ? flightInfo.getPlaned_lats().split(","):null;
		String[] plan_lons = flightInfo.getPlaned_lons() != null ? flightInfo.getPlaned_lons().split(","):null;

		final ArrayList<LatLng> planedFlightPointArray = new ArrayList<LatLng>();
		
		if(plan_lats != null){
			for (int x = 0; x < plan_lats.length; x++) {
				LatLng latLng = new LatLng(Double.parseDouble(plan_lats[x]),
						Double.parseDouble(plan_lons[x]));
				planedFlightPointArray.add(latLng);
				//Log.i(TAG, "lat = " + latLng.latitude);
				//Log.i(TAG, "lon = " + latLng.longitude);
			}
		}
		Log.i(TAG, "planedFlightPointArray length = " + planedFlightPointArray.size());
		
		
		Handler handlerTimer = new Handler();
		handlerTimer.postDelayed(new Runnable() {
			public void run() {
				
				//set correct boundary for google map
				zoomToCoverAllMarkers(originDestinationArray, map);
				
				try{
					Log.i(TAG, "size = " + flightInfoArray.size());
					LatLng origin = originDestinationArray.get(0);
					
					//title(flightInfoArray.get(0).getCarrierCode() + " " + flightInfoArray.get(0).getDepartureAirportCode()
					
					Marker originMarker = map.addMarker(new MarkerOptions()
					                          .position(origin));
					
					LatLng des = originDestinationArray.get(1);
					Marker desMarker = map.addMarker(new MarkerOptions()
					                          .position(des));
				}
				catch(Exception e){
					e.printStackTrace();
				}
				
				//draw planned route
				//moveCameraAndDraw(planedFlightPointArray,Color.parseColor("#4c0000ff"));
				//draw actual route
				moveCameraAndDraw(flightPointArray,Color.parseColor("#66ff0000"));

			}
		}, 1000);

		
		mapShown = true;
	}

	/*API request*/

	private void getFlightStatus(String airlineCode, String planeCode) {
		//mProgressBar.setCancelable(false);
		//mProgressBar.setMessage(this.getString(R.string.loading));
		mProgressBar.setVisibility(View.VISIBLE);
		

		Log.i(TAG, "getFlightInfo start");
		apiCallType = ApiCallType.GET_FLIGHT_STATUS;

		try {

			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();
			
			String year = MainApplication.getMAS().getData("year");
			String month = MainApplication.getMAS().getData("month");
			String day = MainApplication.getMAS().getData("day");
			
			if(year.length()==0){
				year = today.year+"";
				month = today.month+1+"";
				day = today.monthDay+"";
			}

			URL url = null;
			// https://api.flightstats.com/flex/flightstatus/rest/v2/json/airport/status/HKG/arr/2014/10/11/23?appId=1b5bbebb&appKey=0bd343eb43861dbd7c82996a3f8d07ac&utc=false&numHours=1&maxFlights=5

			// https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/tracks/CX/439/arr/2014/10/21?appId=24e1354d&appKey=c26fac2248afb16a9b10995584e564b4&hourOfDay=0&utc=false&numHours=24&includeFlightPlan=true&maxPositions=10"
			// https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/track/CX/439/arr/2014/10/21/?appId=24e1354d&appKey=c26fac2248afb16a9b10995584e564b4&hourOfDay=0&utc=false&numHours=24&includeFlightPlan=true&maxPositions=10
			url = new URL(
					this.getString(R.string.flightstat_api_base)
							+ "flight/"
							+ "tracks/"
							+ airlineCode
							+ "/"
							+ planeCode
							+ "/"
							+ this.getString(R.string.arrival)
							+ "/"
							+ year
							+ "/"
							+ month
							+ "/"
							+ day
							+ "/"
							+ "?appId="
							+ this.getString(R.string.flightstat_app_id)
							+ "&appKey="
							+ this.getString(R.string.flightstat_app_token)
							+ "&hourOfDay=0&utc=false&numHours=24&includeFlightPlan=true&maxPositions=1000");

			Log.e(TAG, "url = " + url);

			/* String query = URLEncoder.encode("apples oranges", "utf-8"); */

			Bundle bundle = new Bundle();

			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {

			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}
	
	private void getFlightStatus(String flightId) {
		
		mProgressBar.setVisibility(View.VISIBLE);

		Log.i(TAG, "getFlightInfo start");
		apiCallType = ApiCallType.GET_FLIGHT_STATUS;

		try {

			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();

			URL url = null;
			
			
			url = new URL(
					this.getString(R.string.flightstat_api_base)
							+ "flight/"
							+ "track/"
							+ flightId
							+ "/"
							+ "?appId="
							+ this.getString(R.string.flightstat_app_id)
							+ "&appKey="
							+ this.getString(R.string.flightstat_app_token)
							+ "&includeFlightPlan=true&maxPositions=1000");

			Log.e(TAG, "url = " + url);

			/* String query = URLEncoder.encode("apples oranges", "utf-8"); */

			Bundle bundle = new Bundle();

			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {

			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}

	private void getFlightsFromRoute(String depAirport, String arrAirport) {
		
		mProgressBar.setVisibility(View.VISIBLE);

		Log.i(TAG, "getFlightsFromRoute start");
		apiCallType = ApiCallType.GET_FLIGHT_TRACK_BY_DESTINATION;

		try {

			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();

			String apiType = "";
			int startHour = 0;

			URL url = null;
			
			String year = MainApplication.getMAS().getData("year");
			String month = MainApplication.getMAS().getData("month");
			String day = MainApplication.getMAS().getData("day");
			
			if(year.length()==0){
				year = today.year+"";
				month = today.month+1+"";
				day = today.monthDay+"";
			}
			
			//https://api.flightstats.com/flex/flightstatus/rest/v2/json/route/status/HKG/HND/arr/2014/10/29?appId=24e1354d&appKey=c26fac2248afb16a9b10995584e564b4&hourOfDay=11&numHours=24&utc=false&maxFlights=10
			
			// https://api.flightstats.com/flex/flightstatus/rest/v2/json/airport/status/HKG/arr/2014/10/11/23?appId=1b5bbebb&appKey=0bd343eb43861dbd7c82996a3f8d07ac&utc=false&numHours=1&maxFlights=5

			// https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/tracks/CX/439/arr/2014/10/21?appId=24e1354d&appKey=c26fac2248afb16a9b10995584e564b4&hourOfDay=0&utc=false&numHours=24&includeFlightPlan=true&maxPositions=10"
			// https://api.flightstats.com/flex/flightstatus/rest/v2/json/flight/track/CX/439/arr/2014/10/21/?appId=24e1354d&appKey=c26fac2248afb16a9b10995584e564b4&hourOfDay=0&utc=false&numHours=24&includeFlightPlan=true&maxPositions=10
			url = new URL(
					this.getString(R.string.flightstat_api_base)
							+ "route/"
							+ "status/"
							+ depAirport
							+ "/"
							+ arrAirport
							+ "/"
							+ this.getString(R.string.arrival)
							+ "/"
							+ year
							+ "/"
							+ month
							+ "/"
							+ day
							+ "/"
							+ "?appId="
							+ this.getString(R.string.flightstat_app_id)
							+ "&appKey="
							+ this.getString(R.string.flightstat_app_token)
							+ "&hourOfDay=0&utc=false&numHours=24&maxFlights=100");

			Log.e(TAG, "url = " + url);

			/* String query = URLEncoder.encode("apples oranges", "utf-8"); */

			Bundle bundle = new Bundle();

			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {

			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}
	
	private void getAllAirline() {
		mProgressBar.setVisibility(View.VISIBLE);

		Log.i(TAG, "getAllAirline start");
		apiCallType = ApiCallType.GET_ALL_AIRLINE;

		try {
			URL url = null;
			
			url = new URL(
					MainApplication.getCmsApiBase()+this.getString(R.string.get_all_airline));


			Log.e(TAG, "url = " + url);

			/* String query = URLEncoder.encode("apples oranges", "utf-8"); */

			Bundle bundle = new Bundle();

			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {

			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}
	
	private void getAllAirport() {
		mProgressBar.setVisibility(View.VISIBLE);

		Log.i(TAG, "getAllAirport start");
		apiCallType = ApiCallType.GET_ALL_AIRPORT;

		try {
			URL url = null;
			
			url = new URL(
					MainApplication.getCmsApiBase()+this.getString(R.string.get_all_airport));


			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();

			ApiRequest.request(this, url, "get", bundle);

		} catch (MalformedURLException e) {

			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mProgressBar.setVisibility(View.GONE);
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}

	@Override
	public void postExecute(String json) {

		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);

		mProgressBar.setVisibility(View.GONE);
		if (apiCallType == ApiCallType.GET_FLIGHT_STATUS) {
			GetFlightTrackParser parser = new GetFlightTrackParser(json, this);
			parser.startParsing();
		}
		else if (apiCallType == ApiCallType.GET_FLIGHT_TRACK_BY_DESTINATION) {
			GetFlightStatusParser parser = new GetFlightStatusParser(json, this);
			parser.startParsing();
		}
		else if (apiCallType == ApiCallType.GET_ALL_AIRPORT) {
			GetAllAirportParser parser = new GetAllAirportParser(json, this);
			parser.startParsing();
		}
		else if (apiCallType == ApiCallType.GET_ALL_AIRLINE) {
			GetAllAirlineParser parser = new GetAllAirlineParser(json, this);
			parser.startParsing();
		}
	}

	@Override
	public void onPause() {

		super.onPause();
		cancelCurrentTask();
	}

	@Override
	public void onDestroyView() {
		Log.e(TAG, "onDestroyView");

		cancelCurrentTask();

		if (mMapFragment != null) {
			mMapFragment.onDestroyView();
			mMapFragment.onDestroy();
			mMapFragment = null;
		}
		if (map != null) {
			map.clear();
			map = null;
		}

		super.onDestroyView();

	}

	@Override
	public void onDestroy() {
		Log.e(TAG, "onDestroy");
		cancelCurrentTask();
		super.onDestroy();

	}

	@Override
	public void onGetFlightTrackParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onGetFlightTrackFinishParsing(
			ArrayList<FlightInfo> flightArrivalArray) {
		// TODO Auto-generated method stub

		Log.i(TAG, "total = " + flightArrivalArray.size());

		int numOfFlight = flightArrivalArray.size();
		if (numOfFlight == 0) {
			
			// show there is no flight
			showError(MainApplication.getLabel("flighttracks.noactiveflights.message"));
			
		} else if (numOfFlight == 1) {
			// direct go to map
			flightInfoArray.clear();
			flightInfoArray.addAll(flightArrivalArray);
			showMap(flightArrivalArray.get(0));
		} else if (numOfFlight > 1) {
			// show option flight list
			flightInfoArray.clear();
			flightInfoArray.addAll(flightArrivalArray);
			
			flightRoutePopupContainer.setVisibility(View.VISIBLE);
			flightRouteAdapter.notifyDataSetChanged();
			
		}
	}

	@Override
	public void onGetFlightTrackError() {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void onGetFlightStatusParsingError(int failMode, boolean isPostExecute) {
		showError(MainApplication.getLabel("flighttracks.noactiveflights.message"));
	}

	@Override
	public void onGetFlightStatusFinishParsing(
			ArrayList<FlightInfo> _flightArrivalArray) {
		// TODO Auto-generated method stub
		mProgressBar.setVisibility(View.GONE);

		flightInfoArray.clear();
		flightInfoArray.addAll(_flightArrivalArray);
		
		Log.i(TAG, flightInfoArray.toString());
		Log.i(TAG, flightInfoArray.size()+"");
		
		ArrayList<FlightInfo> tempArray = new ArrayList<FlightInfo>();
		
		for(FlightInfo flightInfo : flightInfoArray){
			if(flightInfo.getStatus().equalsIgnoreCase("active")){
				tempArray.add(flightInfo);
			}
		}
		
		flightInfoArray.clear();
		flightInfoArray.addAll(tempArray);
		
		
		// Now sort by the flight info base on date.
		Collections.sort(flightInfoArray, new Comparator<FlightInfo>() {
			public int compare(FlightInfo one, FlightInfo other) {
				return one.getArrivalDate().compareTo(other.getArrivalDate());
			}
		});
		
		if(flightInfoArray.size() == 0){
			// show there is no flight
			showError(MainApplication.getLabel("flighttracks.noactiveflights.message"));
		}
		else if(flightInfoArray.size() == 1){
			getFlightStatus(flightInfoArray.get(0).getFlightId());
		}
		else if(flightInfoArray.size()> 0){
			flightRoutePopupContainer.setVisibility(View.VISIBLE);
			flightRouteAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onGetFlightStatusError() {
		showError(MainApplication.getLabel("flighttracks.noactiveflights.message"));
	}
	
	private void showError(String msg){
		errorMessageDialogLayout.setVisibility(View.VISIBLE);
		errorMessageDialogLabel.setText(msg);
	}
	
	public void hideMap(){
		mapShown = false;
		mapContainer.setVisibility(View.GONE);
		mapPopUI.setVisibility(View.GONE);
		map.clear();
		container.setVisibility(View.VISIBLE);
	}

	public boolean isMapShown() {
		return mapShown;
	}

	public void setMapShown(boolean mapShown) {
		this.mapShown = mapShown;
	}

	@Override
	public void onGetAirportsLatLon(ArrayList<String> _latlonArray) {
		// TODO Auto-generated method stub
		latlonArray = _latlonArray;
	}

	@Override
	public void onGetAirlineParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetAirlineFinishParsing(ArrayList<Airline> _airlineArray) {
		
		airlineArray.clear();
		airlineArray.addAll(_airlineArray);
		airlineAdapter.notifyDataSetChanged();
		
		getAllAirport();
	}

	@Override
	public void onGetAirlineError() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetAirportParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetAirportFinishParsing(ArrayList<Airport> _airportArray) {
		
		mProgressBar.setVisibility(View.GONE);
		
		airportArray.clear();
		airportArray.addAll(_airportArray);
		airportAdapter.notifyDataSetChanged();
	}

	@Override
	public void onGetAirportError() {
		// TODO Auto-generated method stub
		
	}
}
