package com.hsh.esdbsp.fragment;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.activity.InitActivity;
import com.hsh.esdbsp.widget.Log;

import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.network.XMLCaller;

public class BaseFragment extends Fragment implements XMLCaller.InternetCallbacks{
	public ProgressBar progressBar;
	protected TextView errorText;
	protected Button retryBtn;
	protected Button cancelBtn;
	protected AsyncTask<URL, Void, String> currentTask;

	protected String masBase;
	protected String maslrBase = "";
	protected String massrBase = "";
	protected String massbBase = "";
	protected String masavBase = "";
	protected String masdrBase = "";
	protected String masorBase = "";
	protected String previouslyStartedmasString;
	
	//The errorPopup Container
	public RelativeLayout errorPopup;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		masBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.mas_base_dev) : this.getString(R.string.mas_base);
		previouslyStartedmasString = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.pref_previously_startedmas_dev) : this.getString(R.string.pref_previously_startedmas);

		maslrBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.maslr_base_dev) : this.getString(R.string.maslr_base);
		massrBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.massr_base_dev) : this.getString(R.string.massr_base);
		massbBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.massb_base_dev) : this.getString(R.string.massb_base);
		masavBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.masav_base_dev) : this.getString(R.string.masav_base);
		masdrBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.masdr_base_dev) : this.getString(R.string.masdr_base);
		masorBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.masor_base_dev) : this.getString(R.string.masor_base);
	}

	public void hideError(){
		errorPopup.setVisibility(View.GONE);
	}
	public void showError(){
		errorPopup.setVisibility(View.VISIBLE);
	}
	public void  hideProgress(){
		progressBar.setVisibility(View.GONE);
	}
	public void showProgress(){
		progressBar.setVisibility(View.VISIBLE);
	}
	protected int getOrientation(){
		return this.getActivity().getResources().getConfiguration().orientation;
	}
	public void setCurrentTask(AsyncTask<URL, Void, String> task){
		currentTask = task;
	}
	protected void cancelCurrentTask(){
		if (currentTask != null && (currentTask.getStatus().equals(AsyncTask.Status.RUNNING) || currentTask.getStatus().equals(AsyncTask.Status.FINISHED))) {
			currentTask.cancel(true);
			Log.i("BaseFragment", "cancel currentTask");
			currentTask = null;
		}
	}
	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}
	@Override
	public void onError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void postExecute(String xml) {
		// TODO Auto-generated method stub
		hideProgress();
	}
	@Override
	public void postExecuteWithCellId(String xml, String cellId) {
		// TODO Auto-generated method stub
		
	}
}
