package com.hsh.esdbsp.fragment;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.adapter.dvd.DVDMovieListAdapter;
import com.hsh.esdbsp.adapter.dvd.DVDMovieListAdapter.ItemClickListener;
import com.hsh.esdbsp.dialog.DVDMovieDetailDialog;
import com.hsh.esdbsp.dialog.DVDMovieDetailDialog.BorrowClickListener;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.DVDMovieListItem;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class DVDGenreListFragment extends BaseFragment {

	private String TAG = this.getClass().getSimpleName();
	private AsyncHttpClient client;
	int genreId;
	RecyclerView recyclerView;
	
    public static DVDGenreListFragment newInstance(int genreId) {
    	DVDGenreListFragment fragment = new DVDGenreListFragment();
        Bundle args = new Bundle();
        args.putInt("GENREID", genreId);
        fragment.setArguments(args);
        return fragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        genreId = getArguments() != null ? getArguments().getInt("GENREID") : -1 ;

    }
  
   
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
                                                                                                                                                                                                                                                                                                                      
        final View view = inflater.inflate(R.layout.fragment_dvd_genrelist, null);

		final TextView noMovieText = (TextView) view.findViewById(R.id.tvNoMovie);
        noMovieText.setText(MainApplication.getLabel("dvd.nomovie"));
        final RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rvMovielist);
        int numberOfColumns = 5;
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        
		client = new AsyncHttpClient();
    	client.setMaxRetriesAndTimeout(0, 10000);

    	RequestParams params;
        params = new RequestParams();
             
        String currAPILang = "en";
        String currLang = MainApplication.getMAS().getData("data_language") == "" ? "E" : MainApplication.getMAS().getData("data_language");
        
        if(currLang.equals("E")) {
        	currAPILang = "en";
        } else if(currLang.equals("F")) {
        	currAPILang = "fr";
        } else if(currLang.equals("CT")) {
        	currAPILang = "zh_hk";
        } else if(currLang.equals("CS")) {
        	currAPILang = "zh_cn";
        } else if(currLang.equals("J")) {
        	currAPILang = "jp";
        } else if(currLang.equals("G")) {
        	currAPILang = "de";
        } else if(currLang.equals("K")) {
        	currAPILang = "ko";
        } else if(currLang.equals("R")) {
        	currAPILang = "ru";
        } else if(currLang.equals("P")) {
        	currAPILang = "pt";
        } else if(currLang.equals("A")) {
        	currAPILang = "ar";
        } else if(currLang.equals("S")) {
        	currAPILang = "es";
        }
		params.add("lang", currAPILang);
	
		
		String url;
		
		if(genreId == -1) {
			url = MainApplication.getApiBase() + getString(R.string.dvd_borrowing_api)
			+ getString(R.string.get_latest_movielist);
    	} else {
    		url = MainApplication.getApiBase() + getString(R.string.dvd_borrowing_api)
    		+ getString(R.string.get_movielist);
    		 params.add("genreId", Integer.toString(genreId));
    	}

		
  
    	Log.i(TAG, "URL = " + url);
    	
    	client.get(url, params, new AsyncHttpResponseHandler() {

    		@Override
    		public void onStart() {
    			// called before request is started
    		}

    		@Override
    		public void onSuccess(int arg0, Header[] headers, byte[] response) {
    			String s = new String(response);
    			Log.i(TAG, "success response = " + s);

    			JSONObject restObject;
    			
    			try {
    				restObject = new JSONObject(s);

    				JSONArray jArray = restObject.getJSONArray("data");
    		
    				int length = jArray.length();
    				
    				
    				if(length < 1) {
    					recyclerView.setVisibility(View.GONE);
    					noMovieText.setVisibility(View.VISIBLE);
    					
    				} else {
    					recyclerView.setVisibility(View.VISIBLE);
    					noMovieText.setVisibility(View.GONE);
    					
	    				final List<DVDMovieListItem> movieList = new ArrayList<DVDMovieListItem>();
	    		
	    				for(int i=0; i<length;i++) {
	    					DVDMovieListItem dVDMovieListItem = new DVDMovieListItem();
	    					dVDMovieListItem.setMovieId(jArray.getJSONObject(i).getString("movieId"));
	    					dVDMovieListItem.setTitle(jArray.getJSONObject(i).getString("movieTitle"));
	        				dVDMovieListItem.setPosterUrl(jArray.getJSONObject(i).getString("poster"));
	        				movieList.add(dVDMovieListItem);
	    				}
	    		  		

	    				DVDMovieListAdapter dVDMovieListAdapter = new DVDMovieListAdapter(getContext(), movieList);
	    				dVDMovieListAdapter.setClickListener(new ItemClickListener() {

							@Override
							public void onItemClick(View view, int position) {
								final DVDMovieDetailDialog dVDMovieDetailDialog = new DVDMovieDetailDialog(getActivity(), movieList.get(position).getMovieId());
								dVDMovieDetailDialog.setOnClickListener(new BorrowClickListener() {

									@Override
									public void onBorrowClicked(final String movieId) {
										
										RequestParams params;
								        params = new RequestParams();
								        params.add("configId", "max_borrow_per_room");
								       
								 
								        String configUrl = MainApplication.getApiBase() + getString(R.string.dvd_borrowing_api)
						    			+ getString(R.string.get_config);

								        Log.i(TAG, "URL = " + configUrl);
								        
										client.get(configUrl, params, new AsyncHttpResponseHandler() {

								    		@Override
								    		public void onSuccess(int arg0, Header[] headers, byte[] response) {
								    			String s = new String(response);
								    			Log.i(TAG, "success response = " + s);
								    			
								    			JSONObject restObject;
												try {
																		
													final int maxBorrowCount = new JSONObject(s).getJSONArray("data").getJSONObject(0).getInt("value");
													
													RequestParams params;
											        params = new RequestParams();
											        params.add("room", MainApplication.getMAS().getData("data_myroom"));
											       
											        String maxBorrowUrl = MainApplication.getApiBase() + getString(R.string.dvd_borrowing_api)
									    			+ getString(R.string.get_inuse_count);

											        Log.i(TAG, "URL = " + maxBorrowUrl);
											        
													client.get(maxBorrowUrl, params, new AsyncHttpResponseHandler() {

											    		@Override
											    		public void onSuccess(int arg0, Header[] headers, byte[] response) {
											    			String s = new String(response);
											    			Log.i(TAG, "success response = " + s);
											    			
											    			try {	
																int currentBorrowCount = new JSONObject(s).getJSONArray("data").getJSONObject(0).getInt("totalNum");					
												    			
																if (currentBorrowCount < maxBorrowCount ) {
																	
																	RequestParams params;
															        params = new RequestParams();
															        params.add("movieId", movieId);
															        params.add("roomId", MainApplication.getMAS().getData("data_myroom"));

															        String url = MainApplication.getApiBase() + getString(R.string.dvd_borrowing_api)
													    			+ getString(R.string.add_borrow_record);
	
															        Log.i(TAG, "URL = " + url);
															        
																	client.post(url, params, new AsyncHttpResponseHandler() {
	
															    		@Override
															    		public void onSuccess(int arg0, Header[] headers, byte[] response) {
															    			String s = new String(response);
															    			Log.i(TAG, "success response = " + s);
															    			dVDMovieDetailDialog.popupDialog(MainApplication.getLabel("dvd.success"));
															    			dVDMovieDetailDialog.dismiss();
															    		}
	
															    		@Override
															    		public void onFailure(int arg0, Header[] headers, byte[] response,Throwable e) {
															    			dVDMovieDetailDialog.popupDialog(MainApplication.getLabel("dvd.failed"));
															    		}
																	});
																} else {
																	dVDMovieDetailDialog.popupDialog(MainApplication.getLabel("dvd.exceedquota"));
																}
																
															} catch (JSONException e) {
																// TODO Auto-generated catch block
																e.printStackTrace();
															}									
											    		}

											    		@Override
											    		public void onFailure(int arg0, Header[] headers, byte[] response,Throwable e) {
											    			dVDMovieDetailDialog.popupDialog(MainApplication.getLabel("dvd.failed"));
											    		}
													});
	
													
												} catch (JSONException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}

								    		}

								    		@Override
								    		public void onFailure(int arg0, Header[] headers, byte[] response,Throwable e) {
								    			dVDMovieDetailDialog.popupDialog(MainApplication.getLabel("dvd.failed"));
								    		}
										});
									}
									
								});
								dVDMovieDetailDialog.show();  
										
								//Grab the window of the dialog, and change the width
								WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
								Window window = dVDMovieDetailDialog.getWindow();
								lp.copyFrom(window.getAttributes());
								
								DisplayMetrics displayMetrics = new DisplayMetrics();
								getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
								int height = displayMetrics.heightPixels;
								int width = displayMetrics.widthPixels;
												
								int dialogHeight = (int) ((int)height * 0.8);
								int dialogWidth = (int) ((int)width * 0.8);
					
						
								dVDMovieDetailDialog.getWindow().setLayout(dialogWidth, dialogHeight);
								dVDMovieDetailDialog.show();  
							}
	    					
	    				});
	    		        recyclerView.setAdapter(dVDMovieListAdapter);
    				}


    			} catch (JSONException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}


    		}

    		@Override
    		public void onFailure(int arg0, Header[] headers, byte[] response,
    				Throwable e) {

    			if (response != null) {
    				String s = new String(response);
    				Log.i(TAG, s);
    			}

    			Log.i(TAG, "Stop Retry");
    			recyclerView.setVisibility(View.GONE);
				noMovieText.setVisibility(View.VISIBLE);

    		}

    		@Override
    		public void onRetry(int retryNo) {
    			// called when request is retried
    			Log.i(TAG, "retrying number = " + retryNo);
    		}

    	});

       
        return view;
   
	}
}
