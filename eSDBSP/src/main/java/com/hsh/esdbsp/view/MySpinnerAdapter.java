
package com.hsh.esdbsp.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class MySpinnerAdapter extends ArrayAdapter<String> {
    // Initialise custom font, for example:
    Typeface font = Typeface.createFromAsset(getContext().getAssets(), "minion.otf");
    List<String> items;
    List<Integer> imgs;

    // (In reality I used a manager which caches the Typeface objects)
    // Typeface font = FontManager.getInstance().getFont(getContext(), BLAMBOT);

    public MySpinnerAdapter(Context context, int resource, List<String> items, List<Integer> imgs) {
        super(context, resource, items);
        this.items = items;
        this.imgs = imgs;
    }

    // Affects default (closed) state of the spinner
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

/*        TextView view = (TextView) super.getView(position, convertView, parent);
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimension(R.dimen.rc_bottombar_button_TextView_textSize22));
        view.setTypeface(font);
        view.setPadding(10, 5, 10, 5);
        //view.setGravity(Gravity.CENTER);
        return view;*/

        return getCustomView(position, convertView, parent);
    }

    // Affects opened state of the spinner
    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
/*        TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        view.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimension(R.dimen.rc_bottombar_button_TextView_textSize27));
        view.setTypeface(font);
        view.setPadding(10, 5, 10, 5);
        //view.setGravity(Gravity.CENTER);
        return view;*/

        return getCustomView(position, convertView, parent);
    }


    public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = MainApplication.getCurrentActivity().getLayoutInflater();

        View row = inflater.inflate(R.layout.spinner_row, parent, false);
        TextView label = (TextView) row.findViewById(R.id.text);

        try {
            label.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimension(R.dimen.rc_bottombar_button_TextView_textSize16));
        } catch (Exception e) {
            label.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) 16.0);
        }

        label.setTypeface(font);
        label.setPadding(10, 5, 10, 5);
        label.setText(items.get(position));

        ImageView icon = (ImageView) row.findViewById(R.id.image);
        icon.setImageResource(imgs.get(position));

        return row;
    }

}