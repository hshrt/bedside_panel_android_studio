package com.hsh.esdbsp.view;

public class SettingsOptionItem{
	public final String title;

	public SettingsOptionItem(String title) {
		this.title = title;
	}

	public boolean isSection() {
		return false;
	}
}
