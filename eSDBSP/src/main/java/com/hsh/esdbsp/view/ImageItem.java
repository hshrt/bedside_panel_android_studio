package com.hsh.esdbsp.view;


public class ImageItem {
	private int image;
	private String imageStr;
	
	private String title;
	private String cmd;

	public ImageItem(int image, String title, String cmd) {
		super();
		this.image = image;
		this.title = title;
		this.cmd = cmd;
	}	
	
	public ImageItem(String image, String title, String cmd) {
		super();
		this.imageStr = image;
		this.title = title;
		this.cmd = cmd;
	}	
	
	public String getImageStr() {
		return imageStr;
	}

	public int getImage() {
		return image;
	}

	public String getTitle() {
		return title;
	}
	
	public String getCmd() {
		return cmd;
	}	

	public void setTitle(String title) {
		this.title = title;
	}
}