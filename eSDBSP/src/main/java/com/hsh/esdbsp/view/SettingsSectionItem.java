package com.hsh.esdbsp.view;

public class SettingsSectionItem{

	private final String title;
	
	public SettingsSectionItem(String title) {
		this.title = title;
	}
	
	public String getTitle(){
		return title;
	}

	public boolean isSection() {
		return true;
	}
}
