package com.hsh.esdbsp.activity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.adapter.MainMenuItemAdapter;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Dictionary;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.network.ApiRequest;
import com.hsh.esdbsp.network.XMLCaller;
import com.hsh.esdbsp.parser.GetAllDictParser;
import com.hsh.esdbsp.parser.GetAllDictParser.GetAllDictParserInterface;
import com.hsh.esdbsp.parser.GetGeneralItemParser;
import com.hsh.esdbsp.parser.GetGeneralItemParser.GetGeneralItemParserInterface;

public class ServiceMainActivity extends BaseActivity implements GetGeneralItemParserInterface, GetAllDictParserInterface {

    private GridView menu_gridView;

    private MainMenuItemAdapter mAdapter;

    private ArrayList<GeneralItem> itemArray;

    private int rootId;

    private boolean isInited = false;

    // ////////////////////////////////////////////////////

    static Handler guiHandler = new Handler();
    static int guiDelay = 500;

    String myCMD = "";

    public enum ApiCallType {
        GET_ALL_ITEMS, GET_ALL_DICT
    }

    ApiCallType apiCallType;

    static final int ACTIVATION_REQUEST = 47; // identifies our request id
    DevicePolicyManager devicePolicyManager;
    ComponentName myDeviceAdmin;

    // ////////////////////////////////////////////////////////

    @Override
    protected void onStart() {
        Log.i(TAG, "onStart");
        GlobalValue.getInstance().setAllowMoveTop(true);
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.i(TAG, "OnResume");

        addRunnableCall();
        Window mWindow = MainApplication.getCurrentActivity().getWindow();
        WindowManager.LayoutParams lp = mWindow.getAttributes();
        lp.screenBrightness = (float) 1.0f;
        mWindow.setAttributes(lp);
        super.onResume();

        if(isInited) {
            if (bottomBarFragment != null && isBottomBarInit) {
                bottomBarFragment.loadRCLabel();
            }

            if (topBarFragment != null && isTopBarInit) {
                topBarFragment.loadPageLabel();
            }

            initUI();
            refreshData(rootId);
        } else {
            isInited = true;
        }
    }

    @Override
    protected void onPause() {
        Log.i(TAG, "onPause");
        removeRunnableCall();
        super.onPause();
    }

    @Override
    protected void onDestroy() {

        Log.i(TAG, "OnDestroy");
        removeRunnableCall();
        topBarFragment = null;
        System.gc();
        super.onDestroy();
    }

    // ////////////////////////////////////////////////////////

    private void addRunnableCall() {
        guiHandler.removeCallbacks(guiRunnable);
        guiHandler.postDelayed(guiRunnable, guiDelay);
    }

    private void removeRunnableCall() {
        guiHandler.removeCallbacks(guiRunnable);
    }


    // ////////////////////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "Service Main fire");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.cms_main);

        // for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        FlurryAgent.logEvent("Service", map);

        mDialog = new ProgressDialog(this);

        Helper.showInternetWarningToast();

        initUI();

        rootId = getIntent().getIntExtra("rootId", 0);

        String highLightChoice = "service";

        if (rootId == 1) {
            highLightChoice = "dining";
        } else if (rootId == 2) {
            highLightChoice = "concierge";
        }

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", true);
        bundle.putBoolean("invisbleBackBtn", true);
        bundle.putString("hightLightChoice", highLightChoice);
        initTopBar(bundle);
        initBottomBar();

        if (itemArray == null) {
            itemArray = new ArrayList<GeneralItem>();
        }

        // subItemArray = GlobalValue.getInstance().getAllItemArray();
        if (GlobalValue.getInstance().getAllItemArray() == null) {
            GlobalValue.getInstance().setAllItemArray(
                    new ArrayList<GeneralItem>());
        } else {
            ArrayList<GeneralItem> generalItemArray = GlobalValue.getInstance().getAllItemArray();

            for (GeneralItem item : generalItemArray) {
                if (item.getParentId().equalsIgnoreCase(rootId + "")) {
                    itemArray.add(item);
                }
            }
        }

        if (mAdapter == null) {
            mAdapter = new MainMenuItemAdapter(this, itemArray);
        }

        menu_gridView.setAdapter(mAdapter);
    }

    public void refreshData(int _rootId) {

        rootId = _rootId;

        ArrayList<GeneralItem> generalItemArray = GlobalValue.getInstance().getAllItemArray();

        itemArray.clear();
        for (GeneralItem item : generalItemArray) {
            if (item.getParentId().equalsIgnoreCase(_rootId + "")) {
                itemArray.add(item);
            }
        }


        if (mAdapter == null) {
            mAdapter = new MainMenuItemAdapter(this, itemArray);
        }

        menu_gridView.setAdapter(mAdapter);
    }

    private Runnable guiRunnable = new Runnable() {

        public void run() {
            guiHandler.removeCallbacks(guiRunnable);
            guiHandler.postDelayed(guiRunnable, guiDelay);

        }
    };

    public void updateUI() {

        Log.i(TAG, "updateUI fire");
        if (GlobalValue.getInstance().getAllItemArray() == null) {
            GlobalValue.getInstance().setAllItemArray(
                    new ArrayList<GeneralItem>());
        } else {
            ArrayList<GeneralItem> generalItemArray = GlobalValue.getInstance().getAllItemArray();

            itemArray.clear();

            for (GeneralItem item : generalItemArray) {
                if (item.getParentId().equalsIgnoreCase("0")) {
                    itemArray.add(item);
                }
            }
        }

        if (mAdapter == null) {
            mAdapter = new MainMenuItemAdapter(this, itemArray);
        }

        mAdapter.notifyDataSetChanged();
    }


    public void onItemclick(GeneralItem item) {
        String type = item.getType();
        String title = MainApplication.getEngLabel(item.getTitleId());
        Log.i(TAG, "type = " + type);
        Log.i(TAG, "title = " + title);

        // for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        map.put("Choice", MainApplication.getEngLabel(item.getTitleId()));
        FlurryAgent.logEvent("ServiceMainMenuChoice", map);

        if (title.compareToIgnoreCase("Earth TV") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, EarthTVActivity.class);
            intent.putExtra("parentId", item.getItemId());
            intent.putExtra("titleId", item.getTitleId());
            startActivityForResult(intent, 0);

        } else if (type.compareToIgnoreCase("Information (with top sub menu)") == 0
                || type.compareToIgnoreCase("Information (with two sub menu)") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, ServiceInRoomDinningActivity.class);
            intent.putExtra("parentId", item.getItemId());
            intent.putExtra("titleId", item.getTitleId());
            startActivityForResult(intent, 0);

        } else if (type.compareToIgnoreCase("Information (with sub menu)") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, ServiceGeneralMenuActivity.class);
            intent.putExtra("parentId", item.getItemId());
            intent.putExtra("titleId", item.getTitleId());
            startActivityForResult(intent, 0);

        } else if (type.compareToIgnoreCase("Guest request") == 0
                || title.compareToIgnoreCase("HouseKeeping") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, ServiceHouseKeepingActivity.class);
            intent.putExtra("parentId", item.getItemId());
            intent.putExtra("titleId", item.getTitleId());
            intent.putExtra("type", type);
            intent.putExtra("choice", 0);
            startActivityForResult(intent, 0);

        } else if (title.compareToIgnoreCase("Priority Check-Out") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, ExpressCheckoutActivity.class);
            intent.putExtra("parentId", item.getItemId());
            intent.putExtra("titleId", item.getTitleId());
            intent.putExtra("type", type);
            startActivityForResult(intent, 0);

        } else if (type.compareToIgnoreCase("PressReader") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, NewsActivity.class);
            intent.putExtra("titleId", item.getTitleId());
            intent.putExtra("url", "http://www.pressreader.com/");
            intent.putExtra("hasWebControl", true);
            startActivityForResult(intent, 0);

//            String mSchema = "pressreader://pressreader.com";
//            GlobalValue.getInstance().setAllowMoveTop(false);
//            startActivity(
//                    new Intent(Intent.ACTION_VIEW, Uri.parse(mSchema).buildUpon()
//                            .appendPath("shared")
//                            .appendQueryParameter("exit_button", MainApplication.getLabel("Exit"))
//                            .appendQueryParameter("language", Helper.changeLangCode(DataCacheManager.getInstance().getLang()))
//                            .appendQueryParameter("wakelock", "on")
//                            .build()));

        } else if (title.compareToIgnoreCase("news") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, NewsActivity.class);
            intent.putExtra("parentId", item.getItemId());
            intent.putExtra("titleId", item.getTitleId());
            startActivityForResult(intent, 0);

        } else if (type.compareToIgnoreCase("NY Times") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, NewsActivity.class);
            intent.putExtra("titleId", item.getTitleId());
            intent.putExtra("url", "http://www.nytimes.com/");
            intent.putExtra("hasWebControl", true);
            startActivityForResult(intent, 0);

        } else if (type.compareToIgnoreCase("Web") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, NewsActivity.class);
            intent.putExtra("titleId", item.getTitleId());
            intent.putExtra("url", item.getCommand());
            intent.putExtra("hasWebControl", true);

            if (item.getCommand().equalsIgnoreCase("https://hsh.blazeloop.com")) {
                intent.putExtra("hasWebControl", false);
            }
            startActivityForResult(intent, 0);

        } else if (title.compareToIgnoreCase("airport") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, ServiceAirportActivity.class);
            startActivityForResult(intent, 0);

        } else if (title.compareToIgnoreCase("Earth TV") == 0 || type.compareToIgnoreCase("Earth TV") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, EarthTVActivity.class);
            startActivityForResult(intent, 0);

        } else if (title.compareToIgnoreCase("FX Currency") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, FxActivity.class);
            intent.putExtra("titleId", item.getTitleId());
            startActivityForResult(intent, 0);

        } else if (type.compareToIgnoreCase("Mini Bar") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, ServiceInfoActivity.class);
            intent.putExtra("parentId", item.getItemId());
            intent.putExtra("titleId", item.getTitleId());
            startActivityForResult(intent, 0);

        } else if (type.compareToIgnoreCase("Information") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, ServiceGeneralItemActivity.class);
            intent.putExtra("parentId", item.getItemId());
            intent.putExtra("titleId", item.getTitleId());
            startActivityForResult(intent, 0);

        } else if (type.compareToIgnoreCase("Restaurant") == 0 || type.compareToIgnoreCase("Spa/Restaurant") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, ServiceSpaRestActivity.class);
            intent.putExtra("parentId", item.getItemId());
            intent.putExtra("titleId", item.getTitleId());
            intent.putExtra("type", type);
            startActivityForResult(intent, 0);

        } else if (type.compareToIgnoreCase("Spa") == 0) {

            Intent intent = new Intent(ServiceMainActivity.this, ServiceSpaRestActivity.class);
            intent.putExtra("parentId", item.getItemId());
            intent.putExtra("titleId", item.getTitleId());
            intent.putExtra("type", type);
            startActivityForResult(intent, 0);
        }

    }

    private void initUI() {
        menu_gridView = (GridView) findViewById(R.id.menu_gridView);
    }

    private void getAllItems() {
        mDialog.setCancelable(false);
        mDialog.setMessage(MainApplication.getLabel("loading"));
        showDialog();
        Log.i(TAG, "getAllItems start");
        apiCallType = ApiCallType.GET_ALL_ITEMS;

        if (MainApplication.useLocalFile) {
            // Reading text file from assets folder
            StringBuffer sb = new StringBuffer();
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(getAssets().open(
                        "items.json")));
                String temp;
                while ((temp = br.readLine()) != null)
                    sb.append(temp);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close(); // stop reading
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            String myjsonstring = sb.toString();

            postExecute(myjsonstring);
        }

        try {
            URL url = null;

            url = new URL(MainApplication.getCmsApiBase()
                    + this.getString(R.string.get_item_list));
            Log.e(TAG, "url = " + url);

            Bundle bundle = new Bundle();
            bundle.putInt("getAll", 1);
            ApiRequest.request(this, url, "post", bundle);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        } catch (Exception e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        }
    }

    private void getAllDict() {
        mDialog.setCancelable(false);
        mDialog.setMessage(MainApplication.getLabel("loading"));
        showDialog();
        Log.i(TAG, "getAllDict start");
        apiCallType = ApiCallType.GET_ALL_DICT;

        if (MainApplication.useLocalFile) {
            // Reading text file from assets folder
            StringBuffer sb = new StringBuffer();
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(getAssets().open(
                        "dict.json")));
                String temp;
                while ((temp = br.readLine()) != null)
                    sb.append(temp);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close(); // stop reading
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            String myjsonstring = sb.toString();

            postExecute(myjsonstring);
        }

        try {
            URL url = null;

            url = new URL(MainApplication.getCmsApiBase()
                    + this.getString(R.string.get_lang_key));
            Log.e(TAG, "url = " + url);

            Bundle bundle = new Bundle();
            bundle.putInt("getAll", 1);
            ApiRequest.request(this, url, "post", bundle);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        } catch (Exception e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        }
    }

    @Override
    public boolean hasInternet() {
        return Helper.hasInternet(false);
    }

    @Override
    public void onError(int failMode, boolean isPostExecute) {
        mDialog.dismiss();
        Log.i(TAG, "failMode" + failMode);
        Log.i(TAG, "onerror");
    }

    @Override
    public void postExecute(String json) {

        Log.i(TAG, "postExecute");
        Log.i("XMLContent", "api xml =" + json);

        if (apiCallType == ApiCallType.GET_ALL_ITEMS) {
            GetGeneralItemParser parser = new GetGeneralItemParser(json, this);
            parser.startParsing();
        } else if (apiCallType == ApiCallType.GET_ALL_DICT) {
            GetAllDictParser parser = new GetAllDictParser(json, this);
            parser.startParsing();

            String filename = "dict.json";
            FileOutputStream outputStream;

            try {
                outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
                outputStream.write(json.getBytes());
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            int ch;
            StringBuffer fileContent = new StringBuffer("");
            FileInputStream fis;
            try {
                fis = this.openFileInput("dict.json");
                try {
                    while ((ch = fis.read()) != -1)
                        fileContent.append((char) ch);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

			/*
			 * String data = new String(fileContent);
			 * 
			 * Log.i(TAG, "i am data = " + data);
			 * 
			 * GlobalValue.getInstance().setDict(data);
			 */

        }
    }

    @Override
    public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute) {
        // TODO Auto-generated method stub
        mDialog.dismiss();
    }

    @Override
    public void onGetGeneralItemFinishParsing(ArrayList<GeneralItem> generalItemArray) {
        // TODO Auto-generated method stub
        mDialog.dismiss();
        GlobalValue.getInstance().getAllItemArray().clear();
        GlobalValue.getInstance().getAllItemArray().addAll(generalItemArray);

        itemArray.clear();
        // subItemArray.addAll(generalItemArray);

        for (GeneralItem item : generalItemArray) {
            if (item.getParentId().equalsIgnoreCase("0")) {
                itemArray.add(item);
            }
        }

        Log.i(TAG, "the subItemArray = " + itemArray.toString());

        getAllDict();
    }

    @Override
    public void onGetGeneralItemError() {
        mDialog.dismiss();
    }

    @Override
    public void onGetDictParsingError(int failMode, boolean isPostExecute) {
        mDialog.dismiss();
    }

    @Override
    public void onGetDictFinishParsing(ArrayList<Dictionary> dictArray) {
        mDialog.dismiss();

        GlobalValue.getInstance().getAllDictArray().clear();
        GlobalValue.getInstance().getAllDictArray().addAll(dictArray);

        hideDialog();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onGetDictError() {
        mDialog.dismiss();
    }

    public int getRootId() {
        return rootId;
    }

    public void setRootId(int rootId) {
        this.rootId = rootId;
    }

    @Override
    protected void onLanguageChanged(String language) {
        initUI();
        refreshData(rootId);
    }



}
