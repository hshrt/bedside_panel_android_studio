package com.hsh.esdbsp.activity;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.MessagePopupFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.fragment.VideoPopupFragment;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.adapter.GeneralItemAdapter;
import com.hsh.esdbsp.adapter.GeneralItemSpaRestAdapter;
import com.hsh.esdbsp.view.EnlargeImageView;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.network.ApiRequest;
import com.hsh.esdbsp.network.XMLCaller;
import com.hsh.esdbsp.parser.GetGeneralItemParser;
import com.hsh.esdbsp.parser.GetGeneralItemParser.GetGeneralItemParserInterface;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ServiceSpaRestActivity extends BaseActivity implements GetGeneralItemParserInterface {

    private ListView leftMenuListView;
    private TextView descriptionText;

    private GeneralItemAdapter mAdapter;
    private GeneralItemSpaRestAdapter itemAdapter;

    private ImageButton richMediaPlayBtn;
    private Button scaleUpBtn;
    private Button scaleDownBtn;
    private Button arrowBtn;
    private Button printBtn;

    private Button menuBtn;
    private Button closeBtn;

    private FrameLayout scroller;

    private Boolean scrollerClose = false;

    private int textSize = 25;

    private int animation_duration = 500;

    private EnlargeImageView bgPhoto;

    private String parentId;
    private String titleId;

    private ArrayList<GeneralItem> leftItemArray;
    private ArrayList<GeneralItem> topItemArray;
    private ArrayList<GeneralItem> itemArray;

    private ArrayList<TextView> textViewArray;

    // a key for what is the current topic of this page and what to get from DB
    private String objectString;

    private LinearLayout listContainer;

    private RelativeLayout menuContainer;
    private LinearLayout titleContainer;
    private ListView itemList;
    private LinearLayout itemContainer;

    public enum ApiCallType {
        GET_GENERAL_ITEMS
    }

    ApiCallType apiCallType;

    private AsyncHttpClient client;
    private int retryNum = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, TAG + "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cms_spa_restaurant);

        mDialog = new ProgressDialog(this);
        textSize = (int) getResources().getDimension(R.dimen.default_text_size);

        initUI();

        leftMenuListView.setVerticalScrollBarEnabled(false);

        Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                leftMenuListView.setVerticalScrollBarEnabled(true);
            }
        };
        handler.postDelayed(r, 1000);

        parentId = getIntent().getStringExtra("parentId");
        titleId = getIntent().getStringExtra("titleId");

        Bundle bundle = new Bundle();
        bundle.putString("titleId", titleId);
        initTopBar(bundle);
        initBottomBar();

        descriptionText.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        if (leftItemArray == null) {
            leftItemArray = new ArrayList<GeneralItem>();
        }

        if (topItemArray == null) {
            topItemArray = new ArrayList<GeneralItem>();
        }
        if (itemArray == null) {
            itemArray = new ArrayList<GeneralItem>();
        }
        if (textViewArray == null) {
            textViewArray = new ArrayList<TextView>();
        }

        // get itemFromCacheArray
        ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();

        for (GeneralItem item : tempArray) {
            if (item.getItemId().equalsIgnoreCase(parentId)) {
                leftItemArray.add(item);
            }
        }

        for (GeneralItem item : tempArray) {
            if (item.getParentId().equalsIgnoreCase(parentId)) {
                topItemArray.add(item);
            }
        }

        Log.i(TAG, "topItemArray length = " + topItemArray.size());

        setupTitleBar();

        Log.i(TAG, "length = " + leftItemArray.size());

        if (mAdapter == null) {
            mAdapter = new GeneralItemAdapter(this, leftItemArray);
            leftMenuListView.setAdapter(mAdapter);
        }

        if (itemAdapter == null) {
            itemAdapter = new GeneralItemSpaRestAdapter(this, itemArray);
            itemList.setAdapter(itemAdapter);
        }

        mAdapter.notifyDataSetChanged();

        if (topItemArray.size() > 0) {
            TextView v = textViewArray.get(0);
            v.performClick();
        } else {
            menuBtn.setVisibility(View.GONE);
        }

        try {
            changeDescriptionText(0);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "descriptionText webView Error: " + e.toString());
        }

    }

    private void initUI() {
        listContainer = (LinearLayout) findViewById(R.id.listContainer);
        listContainer.setVisibility(View.GONE);
        leftMenuListView = (ListView) findViewById(R.id.list);
        //leftMenuListView.setVisibility(View.GONE);

        descriptionText = (TextView) findViewById(R.id.descriptionText);
        Helper.setMinionFonts(descriptionText);
        richMediaPlayBtn = (ImageButton) findViewById(R.id.playBtn);
        richMediaPlayBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videoURLGetter != null) {
                    videoURLGetter.cancel(true);
                }
                videoURLGetter = new VideoURLGetter();
                videoURLGetter.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, leftItemArray.get(0).getVideoId());
            }
        });


        scaleUpBtn = (Button) findViewById(R.id.scaleUpBtn);
        scaleUpBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textSize < 40) {
                    textSize += 5;
                }
                descriptionText.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                // for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("ServiceZoomIn", map);
            }
        });

        scaleDownBtn = (Button) findViewById(R.id.scaleDownBtn);
        scaleDownBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textSize > (int) getResources().getDimension(R.dimen.default_text_size)) {
                    textSize -= 5;
                }
                descriptionText.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                // for Flurry log
                final Map<String, String> map2 = new HashMap<String, String>();
                map2.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("ServiceZoomOut", map2);
            }
        });


        printBtn = (Button) findViewById(R.id.printBtn);
        printBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                print();
            }
        });


        menuBtn = (Button) findViewById(R.id.menuBtn);
        menuBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                menuContainer.setVisibility(View.VISIBLE);
                scroller.setVisibility(View.GONE);
                arrowBtn.setVisibility(View.GONE);
                scaleDownBtn.setVisibility(View.GONE);
                scaleUpBtn.setVisibility(View.GONE);
            }
        });


        closeBtn = (Button) findViewById(R.id.closeBtn);
        closeBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                menuContainer.setVisibility(View.GONE);
                scroller.setVisibility(View.VISIBLE);
                arrowBtn.setVisibility(View.VISIBLE);
                scaleDownBtn.setVisibility(View.VISIBLE);
                scaleUpBtn.setVisibility(View.VISIBLE);
            }
        });


        menuContainer = (RelativeLayout) findViewById(R.id.menuContainer);
        titleContainer = (LinearLayout) findViewById(R.id.titleContainer);
        itemList = (ListView) findViewById(R.id.itemList);
        itemContainer = (LinearLayout) findViewById(R.id.itemContainer);
        bgPhoto = (EnlargeImageView) findViewById(R.id.bgPhoto);

        Helper.setAppFonts(menuBtn);

        menuBtn.setText(MainApplication.getLabel("mm.menu"));

        leftMenuListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.i(TAG, "wei!");
                // descriptionText.setText(Html.fromHtml(MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),null,
                // new MyTagHandler()).toString());
                // descriptionText.setHtmlFromString(MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),true);
                // descriptionText.setHtmlFromString(MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),
                // false);
                // descriptionText.loadData(MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),
                // "text/html", null);
                // descriptionText.loadDataWithBaseURL("file:///android_asset/style.css",
                // MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),
                // "text/html","utf-8", null);

				/*
                 * String pish =
				 * "<html><head><style type=\"text/css\">@font-face {font-family: MyFont;src: url(\"file:///android_asset/minion.otf\")}body {font-family: MyFont; color:#fff}</style></head><body>"
				 * ; String pas = "</body></html>"; String myHtmlString = pish +
				 * MainApplication
				 * .getLabel(leftItemArray.get(position).getDescriptionId()) +
				 * pas;
				 * 
				 * descriptionText.loadDataWithBaseURL(null,myHtmlString,
				 * "text/html", "UTF-8", null);
				 */
            }

        });

        scroller = (FrameLayout) findViewById(R.id.scroller);
        arrowBtn = (Button) findViewById(R.id.arrowBtn);

        // arrowBtn.setVisibility(View.INVISIBLE);

        arrowBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Display display = getWindowManager().getDefaultDisplay();
                int width = display.getWidth();

                // to differentiate the high density display galaxy tab S2 and
                // other tablet, 1 = normal, 2 = high density
                int divider = 1;

                if (width == 2048 || GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                    divider = 2;
                }

                Log.i(TAG,
                        "scroll distance ="
                                + Helper.getPixelFromDpNormalScale((int) getResources()
                                .getDimension(R.dimen.scroller_width)));

                // for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                map.put("Close", scrollerClose == true ? 1 + "" : 0 + "");
                FlurryAgent.logEvent("ServiceItemDescription", map);

                if (!scrollerClose) {
                    ObjectAnimator scrollerAnim = ObjectAnimator.ofFloat(
                            scroller,
                            "translationX",
                            0,
                            Helper.getPixelFromDpNormalScale((int) getResources()
                                    .getDimension(R.dimen.scroller_width))
                                    / divider);
                    ObjectAnimator arrowAnim = ObjectAnimator.ofFloat(
                            arrowBtn,
                            "translationX",
                            0,
                            Helper.getPixelFromDpNormalScale((int) getResources()
                                    .getDimension(R.dimen.scroller_width))
                                    / divider);
                    scrollerAnim.setDuration(animation_duration);
                    scrollerAnim.start();
                    arrowAnim.setDuration(animation_duration);
                    arrowAnim.addListener(new AnimatorListener() {

                        @Override
                        public void onAnimationStart(Animator animation) {
                            scaleDownBtn.setVisibility(View.GONE);
                            scaleUpBtn.setVisibility(View.GONE);
                            printBtn.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            arrowBtn.setBackgroundResource(R.drawable.info_open);

                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }
                    });
                    arrowAnim.start();
                    scrollerClose = true;
                } else {
                    ObjectAnimator scollerAnim = ObjectAnimator.ofFloat(
                            scroller,
                            "translationX",
                            Helper.getPixelFromDpNormalScale((int) getResources()
                                    .getDimension(R.dimen.scroller_width)
                                    / divider), 0);
                    ObjectAnimator arrowAnim = ObjectAnimator.ofFloat(
                            arrowBtn,
                            "translationX",
                            Helper.getPixelFromDpNormalScale((int) getResources()
                                    .getDimension(R.dimen.scroller_width)
                                    / divider), 0);
                    scollerAnim.setDuration(animation_duration);
                    scollerAnim.start();
                    arrowAnim.setDuration(animation_duration);
                    arrowAnim.addListener(new AnimatorListener() {

                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            arrowBtn.setBackgroundResource(R.drawable.info_close);
                            scaleDownBtn.setVisibility(View.VISIBLE);
                            scaleUpBtn.setVisibility(View.VISIBLE);
                            if (leftItemArray.get(0).getPrint()
                                    .equalsIgnoreCase("1")) {
                                printBtn.setVisibility(View.VISIBLE);
                            } else {
                                printBtn.setVisibility(View.GONE);
                            }


                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }
                    });
                    arrowAnim.start();
                    scrollerClose = false;
                    scroller.scrollTo(0, 0);
                }
            }
        });

        //auto click menu button (request by Maxi (PBJ) )
        //menuBtn.performClick();
    }

    private void setupTitleBar() {
        for (int x = 0; x < topItemArray.size(); x++) {
            MyTextView textView = new MyTextView(this);
            textView.setTag(x);
            textView.setText(MainApplication.getLabel(topItemArray.get(x).getTitleId()).toUpperCase());
            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.FILL_PARENT);
            llp.setMargins(Helper.getPixelFromDp(30), 0, 0, 0);
            llp.gravity = Gravity.CENTER;
            textView.setLayoutParams(llp);

            textView.setGravity(Gravity.CENTER);
            Log.i(TAG, "return size = " + Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize));
            Log.i(TAG, "return size2 = " + Helper.getPixelFromDp((int) Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize)));
            textView.setTextSize(Helper.getPixelFromDp((int) Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize)));
            Helper.setAppFonts(textView);

            titleContainer.addView(textView);
            textViewArray.add(textView);

            textView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    for (TextView t : textViewArray) {
                        t.setBackgroundResource(0);
                    }

                    v.setBackgroundResource(R.drawable.actionbar_selected);

                    int choice = (int) v.getTag();
                    Log.i(TAG, "choice =" + choice);

                    GeneralItem g = topItemArray.get(choice);

                    String rootId = g.getItemId();

                    ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();

                    itemArray.clear();

                    for (GeneralItem item : tempArray) {
                        if (item.getParentId().equalsIgnoreCase(rootId)) {
                            itemArray.add(item);
                        }
                    }
                    itemContainer.removeAllViews();
                    setupItemLayout();

                    //subItemAdapter.notifyDataSetChanged();
                }
            });

        }
    }

    private void setupItemLayout() {
        for (int x = 0; x < itemArray.size(); x++) {
//            MyTextView textView = new MyTextView(this);
//            textView.setText(Html.fromHtml("<u>" + MainApplication.getLabel(itemArray.get(x).getTitleId()) + "</u>"));
//            LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
//            llp.setMargins(Helper.getPixelFromDp(30), 0, 0, 0);
//            textView.setLayoutParams(llp);
//
//            textView.setGravity(Gravity.LEFT | Gravity.CENTER);
//            Log.i(TAG, "return size = " + Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize));
//            Log.i(TAG, "return size2 = " + Helper.getPixelFromDp((int) Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize)));
//            textView.setTextSize(Helper.getPixelFromDp((int) Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize)));
//            Helper.setAppFonts(textView);
//
//            itemContainer.addView(textView);


            if (itemArray.get(x).getImageName() != null && itemArray.get(x).getImageName().length() > 0) {

                String imagePath = MainApplication.getUploadApiBase() + itemArray.get(x).getImageName() + "_m.jpg";

                ImageView imageView = new ImageView(this);
                LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
                llp.setMargins(Helper.getPixelFromDp(30), 0, Helper.getPixelFromDp(30), Helper.getPixelFromDp(30));
                imageView.setLayoutParams(llp);
                imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                imageView.setAdjustViewBounds(true);

                ImageLoader.getInstance().displayImage(imagePath, imageView);
                itemContainer.addView(imageView);
            }

            MyTextView desView = new MyTextView(this);
            desView.setText(Html.fromHtml(MainApplication.getLabel(itemArray.get(x).getDescriptionId())));
            LinearLayout.LayoutParams llp2 = new LinearLayout.LayoutParams(
                    LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
            llp2.setMargins(Helper.getPixelFromDp(30), 0, Helper.getPixelFromDp(30), Helper.getPixelFromDp(10));
            desView.setLayoutParams(llp2);
            desView.setGravity(Gravity.LEFT | Gravity.CENTER);
            Log.i(TAG, "return size = " + Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize));
            Log.i(TAG, "return size2 = " + Helper.getPixelFromDp((int) Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize)));
            desView.setTextSize(Helper.getPixelFromDp((int) Helper.getDimenFromResource(R.dimen.topBarDynamicTextSize)));
            Helper.setAppFonts(desView);

            itemContainer.addView(desView);
        }
    }

    public void changeDescriptionText(int position) {

        // for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        map.put("Choice", MainApplication.getEngLabel(leftItemArray.get(position).getTitleId()));
        FlurryAgent.logEvent("ServiceItemChoice", map);

        String pish = "<html><head></head><body>";
        String pas = "</body></html>";

		/*
         * This section make sure the html align the right hand side for the
		 * language Arabic
		 */
        String currLang = "";
        currLang = MainApplication.getMAS().getData("data_language") == "" ? "E"
                : MainApplication.getMAS().getData("data_language");

        if (currLang.equalsIgnoreCase("")) {
            currLang = "E";
        }

        if (currLang.equalsIgnoreCase("A") || DataCacheManager.getInstance().getLang().equalsIgnoreCase("A")) {
            pish += "<span dir=\"rtl\">";
            pas = "</span>" + pas;
        }
		/* *********************** */

        String myHtmlString = pish + MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()) + pas;

        descriptionText.setText(Html.fromHtml(myHtmlString));

        Log.i(TAG, "html text = " + myHtmlString);

        String content = MainApplication.getLabel(leftItemArray.get(position).getDescriptionId());
        //a defensive way to make sure not blank content shown
        if (content == null || content.length() == 0) {
            menuBtn.performClick();
            closeBtn.setVisibility(View.GONE);
        }

        if (leftItemArray.get(position).getImageName() != null
                && !leftItemArray.get(position).getImageName()
                .equalsIgnoreCase("")
                && !leftItemArray.get(position).getImageName()
                .equalsIgnoreCase("null")) {

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.bbg)
                    .showImageForEmptyUri(R.drawable.bbg)
                    .showImageOnFail(R.drawable.bbg).cacheInMemory(true)
                    .cacheOnDisk(true).considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565).build();

            String imageLink = MainApplication.getUploadApiBase()
                    + leftItemArray.get(position).getImageName() + "_m.jpg";
            ImageLoader.getInstance().displayImage(imageLink, bgPhoto, options);
        }

        if (leftItemArray.get(position).getPrint().equalsIgnoreCase("1")) {
            printBtn.setVisibility(View.VISIBLE);
        } else {
            printBtn.setVisibility(View.GONE);
        }

        if (null != leftItemArray.get(position).getVideoId() &&
                !"".equals(leftItemArray.get(position).getVideoId())) {
            richMediaPlayBtn.setVisibility(View.VISIBLE);
            String imageLink = MainApplication.getUploadApiBase()
                    + leftItemArray.get(position).getVideoThumbnail() + "_s.jpg";
            ImageLoader.getInstance().loadImage(imageLink, new ImageLoadingListener() {

                @Override
                public void onLoadingComplete(String arg0, View arg1, Bitmap arg2) {
                    richMediaPlayBtn.setBackgroundDrawable(new BitmapDrawable(getResources(), arg2));
                }

                @Override
                public void onLoadingCancelled(String arg0, View arg1) {
                }

                @Override
                public void onLoadingFailed(String arg0, View arg1, FailReason arg2) {
                }

                @Override
                public void onLoadingStarted(String arg0, View arg1) {
                }
            });
        } else {
            richMediaPlayBtn.setVisibility(View.GONE);
        }
        // descriptionText.loadDataWithBaseURL("file:///android_asset/style.css",
        // MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),
        // "text/html","utf-8", null);
        ;
    }

    private void getGeneralItems() {
        mDialog.setCancelable(false);
        mDialog.setMessage(MainApplication.getLabel("loading"));
        showDialog();
        Log.i(TAG, "getGeneralItems start");
        apiCallType = ApiCallType.GET_GENERAL_ITEMS;

        if (MainApplication.useLocalFile) {
            // Reading text file from assets folder
            StringBuffer sb = new StringBuffer();
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(getAssets().open(
                        objectString + ".txt")));
                String temp;
                while ((temp = br.readLine()) != null)
                    sb.append(temp);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close(); // stop reading
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            String myjsonstring = sb.toString();

            postExecute(myjsonstring);
        }

        try {
            URL url = null;

            url = new URL(MainApplication.getApiBase()
                    + this.getString(R.string.get_table) + "?table="
                    + objectString);
            Log.e(TAG, "url = " + url);

            Bundle bundle = new Bundle();
            ApiRequest.request(this, url, "get", bundle);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        } catch (Exception e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        }
    }

    @Override
    public boolean hasInternet() {
        return Helper.hasInternet(false);
    }

    @Override
    public void onError(int failMode, boolean isPostExecute) {
        mDialog.dismiss();
        Log.i(TAG, "failMode" + failMode);
        Log.i(TAG, "onerror");
    }

    @Override
    public void postExecute(String json) {

        Log.i(TAG, "postExecute");
        Log.i("XMLContent", "api xml =" + json);

        hideDialog();
        if (apiCallType == ApiCallType.GET_GENERAL_ITEMS) {
            GetGeneralItemParser parser = new GetGeneralItemParser(json, this);
            parser.startParsing();
        }
    }

    @Override
    public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute) {
        // TODO Auto-generated method stub
        mDialog.dismiss();
    }

    @Override
    public void onGetGeneralItemFinishParsing(
            ArrayList<GeneralItem> generalItemArray) {
        // TODO Auto-generated method stub
        mDialog.dismiss();

        leftItemArray.clear();
        leftItemArray.addAll(generalItemArray);
        Log.i(TAG, leftItemArray.toString());
        mAdapter.notifyDataSetChanged();

        // descriptionText.setText(leftItemArray.get(0).getDescription());

        if (mAdapter.getFirstButton() != null) {
            // mAdapter.getFirstButton().performClick();
        }
    }

    @Override
    public void onGetGeneralItemError() {
        mDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cms_service_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    private VideoURLGetter videoURLGetter;

    private class VideoURLGetter extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            if (params == null || params.length < 1 || params[0] == null
                    || "".equals(params[0])) {
                return null;
            }

            URL url = null;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(MainApplication.getCmsApiBase()
                        + "video/getOnlineVideo.php");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(
                        urlConnection.getInputStream());
                String response = "";
                int c = in.read();
                while (c != -1) {
                    response += (char) c;
                    c = in.read();
                }
                in.close();

                // Process response
                JSONObject responseJson = new JSONObject(response);
                JSONArray contentJson = responseJson.getJSONArray("content");
                for (int i = 0; i < contentJson.length(); i++) {
                    String id = contentJson.getJSONObject(i).getString("id");
                    if (params[0].equals(id)) {
                        return contentJson.getJSONObject(i).getString(
                                "playlist_name");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("RichMedia", "postExecute result: " + result);
            VideoPopupFragment fragment = new VideoPopupFragment();
            Bundle args = new Bundle();
            args.putString("url", getString(R.string.video_base) + result);
            fragment.setArguments(args);
            if (!isFinishing() && !isDestroyed()) {
                getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment).commitAllowingStateLoss();
            }
        }
    }

    private void print() {
        Log.i(TAG, "print fire");

        MessagePopupFragment fragment = new MessagePopupFragment();
        Bundle args = new Bundle();
        args.putString("message", "print.popup.message");
        //args.putInt("timeout", 10000);
        fragment.setArguments(args);
        getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment).commit();

        client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(0, 6000000);

        String printIP = this.getString(R.string.printServer);
        // http://172.30.6.220
        // String printIP = "http://192.168.11.61";
        String url = printIP
                + "/printme.php?url="
                + "%22"
                + MainApplication.getApiBase()
                + "cms"
                + GlobalValue.getInstance().getHotel()
                + "/api/print/print.php?itemId="
                + leftItemArray.get(0).getItemId()
                + "%26lang="
                + Helper.changeLangCodePrint(MainApplication.getMAS().getData("data_language").toString()) + "%22&room="
                + MainApplication.getMAS().getData("data_myroom");

        Log.i(TAG, "URL = " + url);

        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int arg0, Header[] headers, byte[] response) {
                String s = new String(response);
                Log.i(TAG, "success response = " + s);
                Toast t = Toast.makeText(ServiceSpaRestActivity.this, "print command sent", Toast.LENGTH_SHORT);
                t.show();
                // every request should have 3 time retry
                retryNum = 0;
            }

            @Override
            public void onFailure(int arg0, Header[] headers, byte[] response, Throwable e) {

                if (response != null) {
                    String s = new String(response);
                    Log.i(TAG, s);
                }

                Log.i(TAG, "Error = " + e.toString());

                if (retryNum < 2) {
                    retryNum++;
                } else {
                    retryNum = 0;
                    Log.i(TAG, "Stop Retry");
                }
            }

            @Override
            public void onRetry(int retryNo) {
                Log.i(TAG, "retrying number = " + retryNo);
            }

        });
    }

    @Override
    protected void onLanguageChanged(String language) {
        initUI();

        // get itemFromCacheArray
        ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();

        leftItemArray.clear();
        for (GeneralItem item : tempArray) {
            if (item.getItemId().equalsIgnoreCase(parentId)) {
                leftItemArray.add(item);
            }
        }

        topItemArray.clear();
        for (GeneralItem item : tempArray) {
            if (item.getParentId().equalsIgnoreCase(parentId)) {
                topItemArray.add(item);
            }
        }

        setupTitleBar();

        mAdapter.notifyDataSetChanged();
        itemAdapter.notifyDataSetChanged();

        if (topItemArray.size() > 0) {
            TextView v = textViewArray.get(0);
            v.performClick();
        } else {
            menuBtn.setVisibility(View.GONE);
        }

        try {
            changeDescriptionText(0);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "descriptionText webView Error: " + e.toString());
        }
    }

}