package com.hsh.esdbsp.activity;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.view.*;
import com.hsh.esdbsp.adapter.GridViewAdapter;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.widget.Command;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class DemoTVActivity extends BaseActivity {

    String myLOG = "DemoTVActivity";
    //-----------
    MyTextView footRoom;
    MyGridView gridView;
    GridViewAdapter customGridAdapter;

    int isMute = 0;
    int isTVon = 0;
    ImageView tvMute;
    ImageView tvMinus;
    ImageView tvPlus;

    TextView tvONLabel;
    LinearLayout tvON;

    TextView tvOFFLabel;
    LinearLayout tvOFF;

    Button powerBtn, arrowUpBtn, backBtn, arrowDownBtn, arrowLeftBtn, arrowRightBtn, okBtn, movieBtn;
    Button chMinusBtn, chPlusBtn, soundMinusBtn, soundMuteBtn, soundPlusBtn;

    Button playPauseBtn, stopBtn, rewindBtn, forwardBtn, lastBtn, nextBtn;

    //-----------

    //////////////////////////////////////////////////////

    static Handler guiHandler = new Handler();
    static int guiDelay = 500;
    static int guiDelayLong = 5 * 1000;

    //////////////////////////////////////////////////////

    static Handler tvChannelHandler = new Handler();
    static int tvChannelDelay = 500;

    //////////////////////////////////////////////////////

    @Override
    protected void onResume() {
        addRunnableCall();
        super.onResume();
    }

    @Override
    protected void onPause() {
        removeRunnableCall();
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        removeRunnableCall();
        super.onDestroy();
    }

//////////////////////////////////////////////////////////


    private void addRunnableCall() {
        guiHandler.removeCallbacks(guiRunnable);
        guiHandler.postDelayed(guiRunnable, guiDelay);
    }

    private void removeRunnableCall() {
        guiHandler.removeCallbacks(guiRunnable);
    }


//////////////////////////////////////////////////////////


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.tv_demo);

        FragmentManager fragmentManager = getSupportFragmentManager();

        Bundle bundle = new Bundle();
        // bundle.putString("titleId", "");
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("titleId", "TV/AUDIO");
        bundle.putString("hightLightChoice", "tv");
        bundle.putInt("choice", 0);
        initTopBar(bundle);
        initBottomBar();

        if (MainApplication.getTVGrid() == null) {
            new Thread(new Runnable() {
                public void run() {
                    loadPageItem();
                    setupTVControl();
                }
            }).start();
        } else {
            loadPageItem();
            setupTVControl();
        }

        addRunnableCall();
        loadPageLabel();
    }

    private void setupTVControl() {
        Log.i(myLOG, "SetupTVCONTROL" + GlobalValue.getInstance().getHotel());
        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
            backBtn.setVisibility(View.VISIBLE);
        }
    }

    private void loadPageLabel() {
        footRoom.setText(Html.fromHtml(MainApplication.getLabel("Room") + " " + MainApplication.getMAS().getData("data_myroom")));
    }


    private void loadPageItem() {

        MainApplication.brightUp();
        footRoom = (MyTextView) findViewById(R.id.footRoomNum);
        footRoom.setText("");

        loadTVFixItem();

        tvChannelHandler.removeCallbacks(tvChannelRunnable);
        tvChannelHandler.postDelayed(tvChannelRunnable, tvChannelDelay);
    }

    private ArrayList getChannelData() {

        @SuppressWarnings("rawtypes")        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();


        for (int i = 1; i <= 8; i++) {

            if (i > 73 && i < 79) {
                // skip
            } else if (i == 82) {
                // skip direct tv
            } else if ((i >= 84) && (i <= 86)) {
                // first three -- requested by Chris , 20151106
            } else {
                imageItems.add(getChannelDataSub(i));
            }
        }
        return imageItems;
    }

    private ImageItem getChannelDataSub(int i) {
        ImageItem myIMG = null;

        String imageID;
        String imageID2;

        String j = "00";

        if (i < 10) {
            j = "0" + i;
        } else {
            j = "" + i;
        }

        imageID = "tv" + j + "_selector";

        int resID = getResources().getIdentifier(imageID, "drawable", getPackageName());
        myIMG = new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i);

        return myIMG;
    }


/////////////////////////////////////////////////////////////////////////////////////	


    private ArrayList getChannelDataPBJ() {

        @SuppressWarnings("rawtypes")        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();

        try {
            for (int i = 1; i <= 39; i++) {

                String imageID;
                String imageID2;

                String j = "00";

                if (i < 10) {
                    j = "0" + i;
                } else {
                    j = "" + i;
                }

                imageID = "pbjtv" + j + "_selector";

                int resID = getResources().getIdentifier(imageID, "drawable", getPackageName());
                imageItems.add(new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i));

            }


        } catch (Exception e) {
        }

        return imageItems;
    }


/////////////////////////////////////////////////////////////////////////////////////	


    private ArrayList getChannelDataPNY() {

        @SuppressWarnings("rawtypes")        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();

        try {


            for (int i = 1; i <= 105; i++) {

                String imageID;
                String imageID2;

                String j = "00";

                if (i < 10) {
                    j = "0" + i;
                } else {
                    j = "" + i;
                }

                imageID = "pnytv" + j + "_selector";

                int resID = getResources().getIdentifier(imageID, "drawable", getPackageName());
                imageItems.add(new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i));

            }


        } catch (Exception e) {
        }

        return imageItems;
    }


//////////////////////////////////////////////////////////////////////////////////////////


    private ArrayList getChannelDataPLNModel() {

        @SuppressWarnings("rawtypes")        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();

        try {


            for (int i = 1; i <= 8; i++) {

                String imageID;
                String imageID2;

                String j = "00";

                if (i < 10) {
                    j = "0" + i;
                } else {
                    j = "" + i;
                }

                imageID = "plnmtv" + j + "_selector";

                int resID = getResources().getIdentifier(imageID, "drawable", getPackageName());
                imageItems.add(new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i));

            }


        } catch (Exception e) {
        }

        return imageItems;
    }


//////////////////////////////////////////////////////////////////////////////////////////


    private ArrayList getChannelDataPHK() {

        @SuppressWarnings("rawtypes")        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();

        try {


            for (int i = 1; i <= 4; i++) {

                String imageID;
                String imageID2;

                String j = "00";

                if (i < 10) {
                    j = "0" + i;
                } else {
                    j = "" + i;
                }

                imageID = "phktv" + j + "_selector";

                int resID = getResources().getIdentifier(imageID, "drawable", getPackageName());
                imageItems.add(new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i));

            }


        } catch (Exception e) {
        }

        return imageItems;
    }


//////////////////////////////////////////////////////////////////////////////////////////

    private void loadTVFixItem() {

        powerBtn = (Button) findViewById(R.id.power_btn);
        powerBtn = (Button) findViewById(R.id.on_btn);
        powerBtn.setVisibility(View.VISIBLE);
        powerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isTVon = (isTVon + 1) % 2;
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.OFF);
            }
        });

        arrowUpBtn = (Button) findViewById(R.id.arrow_up);
        arrowUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.UP);
            }
        });

        arrowDownBtn = (Button) findViewById(R.id.arrow_down);
        arrowDownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.DOWN);
            }
        });

        arrowLeftBtn = (Button) findViewById(R.id.arrow_left);
        arrowLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.LEFT);
            }
        });

        arrowRightBtn = (Button) findViewById(R.id.arrow_right);
        arrowRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.RIGHT);
            }
        });

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            backBtn = (Button) findViewById(R.id.bottomBackBtn);
            backBtn.setVisibility(View.VISIBLE);
            backBtn.setText("Back");
            Helper.setAppFonts(backBtn);
        } else {
            backBtn = (Button) findViewById(R.id.back_btn);
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.BACK);
            }
        });


        movieBtn = (Button) findViewById(R.id.movieBtn);
        movieBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.MOVIE);
            }
        });

        okBtn = (Button) findViewById(R.id.ok_button);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.OK);
            }
        });

        chMinusBtn = (Button) findViewById(R.id.ch_minus);
        chMinusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.CHANNELDOWN);
            }
        });

        chPlusBtn = (Button) findViewById(R.id.ch_plus);
        chPlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.CHANNELUP);
            }
        });

        soundMinusBtn = (Button) findViewById(R.id.sound_minus);
        soundMinusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.VOLUMNDOWN);
            }
        });

        soundMuteBtn = (Button) findViewById(R.id.sound_mute);
        soundMuteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.MUTE);
            }
        });

        soundPlusBtn = (Button) findViewById(R.id.sound_plus);
        soundPlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.VOLUMNUP);
            }
        });

        playPauseBtn = (Button) findViewById(R.id.playPauseBtn);
        playPauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.PLAYPAUSE);
            }
        });

        stopBtn = (Button) findViewById(R.id.stopBtn);
        stopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.STOP);
            }
        });

        rewindBtn = (Button) findViewById(R.id.rewindBtn);
        rewindBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.REWIND10);
            }
        });

        forwardBtn = (Button) findViewById(R.id.forwardBtn);
        forwardBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.FASTFORWARD10);
            }
        });

        lastBtn = (Button) findViewById(R.id.lastBtn);
        lastBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.LAST);
            }
        });

        nextBtn = (Button) findViewById(R.id.nextBtn);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.DemoTV.RemoteButton(Command.RemoteButton.NEXT);
            }
        });

    }


    private void loadTVItem() {

        gridView = (MyGridView) findViewById(R.id.gridView);
        if (MainApplication.getTVGrid() == null) {
            if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
                customGridAdapter = new GridViewAdapter(this, R.layout.tv_demo_channel, getChannelData());
                MainApplication.setTVGrid(customGridAdapter);
            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
                customGridAdapter = new GridViewAdapter(this, R.layout.tv_demo_channel, getChannelDataPBJ());
                MainApplication.setTVGrid(customGridAdapter);
            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                customGridAdapter = new GridViewAdapter(this, R.layout.tv_demo_channel, getChannelDataPHK());
                MainApplication.setTVGrid(customGridAdapter);
            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                customGridAdapter = new GridViewAdapter(this, R.layout.tv_demo_channel, getChannelDataPNY());
                MainApplication.setTVGrid(customGridAdapter);
            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.LONDON)) {
                customGridAdapter = new GridViewAdapter(this, R.layout.tv_demo_channel, getChannelDataPLNModel());
                MainApplication.setTVGrid(customGridAdapter);
            }

        } else {
            customGridAdapter = MainApplication.getTVGrid();
        }

        gridView.setAdapter(customGridAdapter);
        gridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                Command.Other.DirectSTBCommand(Integer.toString(position + 1));
            }
        });
    }


    private Runnable tvChannelRunnable = new Runnable() {

        public void run() {
            tvChannelHandler.removeCallbacks(tvChannelRunnable);
            loadTVItem();
        }
    };


    private Runnable guiRunnable = new Runnable() {

        public void run() {
            guiHandler.removeCallbacks(guiRunnable);
            loadPageLabel();
            guiHandler.postDelayed(guiRunnable, guiDelay);
        }
    };


}
