package com.hsh.esdbsp.activity;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.comm.*;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.view.*;
import com.hsh.esdbsp.adapter.ibahn.IBahnTvChannelAdapter;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.model.IBahnMovie;
import com.hsh.esdbsp.model.IBahnTvChannel;
import com.hsh.esdbsp.widget.Command;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class IBahnTVActivity extends BaseActivity {

    String myLOG = "IBahnTVActivity";

    MyGridView gridView;
    IBahnTvChannelAdapter customGridAdapter;

    Button onBtn, offBtn, arrowUpBtn, backBtn, arrowDownBtn, arrowLeftBtn, arrowRightBtn, okBtn;
    Button chMinusBtn, chPlusBtn, soundMinusBtn, soundMuteBtn, soundPlusBtn;
    ImageButton redBtn, greenBtn, yellowBtn;

    Button playPauseBtn, stopBtn, rewindBtn, forwardBtn, lastBtn, nextBtn;

    LinearLayout control_layer1, control_layer2, control_layer3;

    String myCMD;

    String iBahnParamRoom = "";
    String iBahnParamBox = "";
    String iBahnParamToken = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.tv_ibahn);

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("titleId", "TV/AUDIO");
        bundle.putString("hightLightChoice", "tv");
        bundle.putInt("choice", 0);
        initTopBar(bundle);
        initBottomBar();


        if (MainApplication.getTVGrid() == null) {
            new Thread(new Runnable() {
                public void run() {
                    loadPageItem();
                    setupTVControl();
                }
            }).start();
        } else {
            loadPageItem();
            setupTVControl();
        }

        new IBahnSetup().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    private class IBahnSetup extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            iBahnParamRoom = MainApplication.getMAS().getData("data_myroom").replaceAll("^0+", "");
            Log.d("setupIBahnParam", "Zone = " + MainApplication.getMAS().getSelectedArea());
            String currentZone = Integer.toString(MainApplication.getMAS().getSelectedArea());
            String urlString = MainApplication.getCmsApiBase();
            urlString += "ibahn/getToken.php?room=" + iBahnParamRoom;
            URL url = null;
            HttpURLConnection urlConnection = null;

            try {
                url = new URL(urlString);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String response = "";
                int c = in.read();
                while (c != -1) {
                    response += (char) c;
                    c = in.read();
                }
                Log.d("getToken", response);
                in.close();
                JSONObject responseJson = new JSONObject(response);
                if (responseJson.getInt("status") == 1) {
                    JSONArray dataArray = responseJson.getJSONArray("data");
                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject dataObj = dataArray.getJSONObject(i);
                        if (dataObj.has("zone") && currentZone.equals(dataObj.getString("zone"))) {
                            iBahnParamBox = dataObj.getString("box");
                            iBahnParamToken = dataObj.getString("token");
                            break;
                        } else if (currentZone.equals(dataObj.getString("box"))) {
                            iBahnParamBox = dataObj.getString("box");
                            iBahnParamToken = dataObj.getString("token");
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            Log.d("setupIBahnParam", iBahnParamRoom + ", " + iBahnParamBox + ", " + iBahnParamToken);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            IBahnComm comm = new IBahnComm();
            comm.getTVChannels(iBahnParamRoom, iBahnParamBox, iBahnParamToken, new IBahnComm.Callback() {

                @Override
                public void onGetTvChannelResult(List<IBahnTvChannel> tvChannelResult) {
                    loadTVItem(tvChannelResult);
                }

                @Override
                public void onGetMovieResult(List<IBahnMovie> movieResult) {
                    // TODO Auto-generated method stub

                }
            });
        }
    }

    private void setupTVControl() {
        Log.i(myLOG, "SetupTVCONTROL" + GlobalValue.getInstance().getHotel());

        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            backBtn.setVisibility(View.VISIBLE);
            control_layer1.setVisibility(View.GONE);
            control_layer2.setVisibility(View.GONE);
            control_layer3.setVisibility(View.GONE);
        }
    }

    private void loadPageItem() {
        MainApplication.brightUp();
        loadTVFixItem();
    }

//////////////////////////////////////////////////////////////////////////////////////////

    private void loadTVFixItem() {


        onBtn = (Button) findViewById(R.id.on_btn);
        onBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.ON, iBahnParamRoom, iBahnParamBox, iBahnParamToken);

            }
        });

        offBtn = (Button) findViewById(R.id.offBtn);
        offBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.OFF, iBahnParamRoom, iBahnParamBox, iBahnParamToken);

            }
        });

        arrowUpBtn = (Button) findViewById(R.id.arrow_up);
        arrowUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.UP, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        arrowDownBtn = (Button) findViewById(R.id.arrow_down);
        arrowDownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.DOWN, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        arrowLeftBtn = (Button) findViewById(R.id.arrow_left);
        arrowLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.LEFT, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        arrowRightBtn = (Button) findViewById(R.id.arrow_right);
        arrowRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.RIGHT, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
            backBtn = (Button) findViewById(R.id.bottomBackBtn);
            backBtn.setText("Back");
            Helper.setAppFonts(backBtn);
        } else {
            backBtn = (Button) findViewById(R.id.back_btn);
        }

        okBtn = (Button) findViewById(R.id.ok_button);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.OK, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        redBtn = (ImageButton) findViewById(R.id.red_button);
        redBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.RED, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        greenBtn = (ImageButton) findViewById(R.id.green_button);
        greenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.GREEN, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        yellowBtn = (ImageButton) findViewById(R.id.yellow_button);
        yellowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.YELLOW, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        chMinusBtn = (Button) findViewById(R.id.ch_minus);
        chMinusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.CHANNELDOWN, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        chPlusBtn = (Button) findViewById(R.id.ch_plus);
        chPlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.CHANNELUP, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        soundMinusBtn = (Button) findViewById(R.id.sound_minus);
        soundMinusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.VOLUMNDOWN, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        soundMuteBtn = (Button) findViewById(R.id.sound_mute);
        soundMuteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.MUTE, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        soundPlusBtn = (Button) findViewById(R.id.sound_plus);
        soundPlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnTV.RemoteButton(Command.RemoteButton.VOLUMNUP, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        control_layer1 = (LinearLayout) findViewById(R.id.control_layer1);
        control_layer2 = (LinearLayout) findViewById(R.id.control_layer2);
        control_layer3 = (LinearLayout) findViewById(R.id.control_layer3);

    }

    private void loadTVItem(final List<IBahnTvChannel> items) {
        gridView = (MyGridView) findViewById(R.id.gridView);
        customGridAdapter = new IBahnTvChannelAdapter(this, 0, items);
        gridView.setAdapter(customGridAdapter);
        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                String channel = customGridAdapter.getItem(position).getNumber();
                Command.Other.DirectCommand("SET%20TV%20" + channel , true);
            }

        });
    }

}
