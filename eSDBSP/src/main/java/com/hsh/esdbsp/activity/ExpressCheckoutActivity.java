package com.hsh.esdbsp.activity;

import java.util.ArrayList;

import org.apache.http.Header;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class ExpressCheckoutActivity extends BaseActivity {

	private FrameLayout scroller;

	private Boolean scrollerClose = false;

	private String parentId;
	private String titleId;

	private MyTextView check_out_intro_text;
	private Button itemButton1, itemButton2, itemButton3;
	private Button clickBtn1, clickBtn2, clickBtn3, checkoutButton;
	
	private ArrayList<Button> buttonList;

	private RelativeLayout detailBox;
	private MyTextView message;
	private Button closeBtn;
	
	private Boolean[] clickArr ;
	
	String[] commandList = {"CONOFOLIO","COWFOLIO","COWBAGGAGE"};
	
	private int[] callArr;
	
	private int currentIndex;
	private int retryNum = 0;

	// a key for what is the current topic of this page and what to get from DB
	private String objectString;

	public enum ApiCallType {
		GET_GENERAL_ITEMS
	}

	ApiCallType apiCallType;
	
	private AsyncHttpClient client;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, TAG + "onCreate");
		super.onCreate(savedInstanceState);

		setContentView(R.layout.express_checkout);

		mDialog = new ProgressDialog(this);
		
		clickArr = new Boolean[2];
		for(int x = 0; x < 2 ; x++){
			clickArr[x] = false;
		}

		initUI();

		parentId = getIntent().getStringExtra("parentId");
		titleId = getIntent().getStringExtra("titleId");

		FragmentManager fragmentManager = getSupportFragmentManager();

		Bundle bundle = new Bundle();
		bundle.putString("titleId", titleId);
		initTopBar(bundle);
		initBottomBar();

	}

	private void initUI() {
		check_out_intro_text = (MyTextView) findViewById(R.id.check_out_intro_text);

		itemButton1 = (Button) findViewById(R.id.itemButton1);
		itemButton2 = (Button) findViewById(R.id.itemButton2);
		clickBtn1 = (Button) findViewById(R.id.clickBtn1);
		clickBtn2 = (Button) findViewById(R.id.clickBtn2);
		
		message = (MyTextView) findViewById(R.id.message);		
		closeBtn = (Button) findViewById(R.id.closeBtn);
		
		clickBtn1.setTag(0);
		clickBtn2.setTag(1);
		
		progressBar = (ProgressBar) findViewById(R.id.progressIndicator);
		
		buttonList = new ArrayList<Button>();
		buttonList.add(clickBtn1);
		buttonList.add(clickBtn2);
		
		checkoutButton = (Button) findViewById(R.id.checkoutButton);

		check_out_intro_text.setText(MainApplication.getLabel("checkout_intro"));
		itemButton1.setText(MainApplication.getLabel("check_out_item1"));
		itemButton2.setText(MainApplication.getLabel("check_out_item2"));
		
		itemButton1.setTag(0);
		itemButton2.setTag(10);

		checkoutButton.setText(MainApplication.getLabel("CHECKOUT"));

		detailBox = (RelativeLayout) findViewById(R.id.detailBox);
		message = (MyTextView) findViewById(R.id.message);
		
		Helper.setAppFonts(itemButton1);
		Helper.setAppFonts(itemButton2);
		Helper.setAppFonts(checkoutButton);

        OnClickListener clickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				int number = (int)v.getTag();
				if(clickArr[number]){
					clickArr[number] = false;
					v.setBackgroundResource(R.drawable.checkmark_off);
				}
				else{
					clickArr[number] = true;
					v.setBackgroundResource(R.drawable.checkmark_on);
				}
			}
		};
		
		clickBtn1.setOnClickListener(clickListener);
		clickBtn2.setOnClickListener(clickListener);
		
		OnClickListener itemClickListener = new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				int number = (int)v.getTag();
				number = number/10;
				Button b = buttonList.get(number);
				
				if(clickArr[number]){
					clickArr[number] = false;
					b.setBackgroundResource(R.drawable.checkmark_off);
				}
				else{
					clickArr[number] = true;
					b.setBackgroundResource(R.drawable.checkmark_on);
				}
				
				/*if(number == 0 && clickArr[0] == true){
					clickArr[2] = false;
					buttonList.get(2).setBackgroundResource(R.drawable.checkmark_off);
				}
				
				if(number == 2 && clickArr[2] == true){
					clickArr[0] = false;
					buttonList.get(0).setBackgroundResource(R.drawable.checkmark_off);
				}*/
			}
		};
		
		itemButton1.setOnClickListener(itemClickListener);
		itemButton2.setOnClickListener(itemClickListener);
		
		checkoutButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				
				if (clickArr[0]) {
					requestService(1);
				}
				else{
					requestService(0);
				}
				if (clickArr[1]) {
					requestService(2);
				}

				
			}
		});
		
		closeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				detailBox.setVisibility(View.GONE);		
			}
		});
	}
	
private void requestService(final int index){
	
		Log.i(TAG, "index = " + index);
		
		showProgress();
		
		client = new AsyncHttpClient();
		
		client.setMaxRetriesAndTimeout(0, 10000);
		
		String url = "http://"+MainApplication.getMAS().getMASPath()+"/mas.php?cmd=SET%20"+commandList[index]+"%20ON";
		
		Log.i(TAG, "URL = " + url);
		
		client.get(url, new AsyncHttpResponseHandler() {

		    @Override
		    public void onStart() {
		        // called before request is started
		    }
		    
		    @Override
			public void onSuccess(int arg0, Header[] headers, byte[] response) {
				String s = new String(response);			
				Log.i(TAG,"success response = " +  s);
				
				//every request should have 3 time retry
				retryNum = 0;
				
				String msg = MainApplication.getLabel("CHECKOUT_THANKS");
				
				if(clickArr[0]){
					msg += " " + MainApplication.getLabel("CHECKOUT_INVOICE");
				}
				if(clickArr[1]){
					msg += " " + MainApplication.getLabel("CHECKOUT_BAGGAGE");
				}
				
				msg+=MainApplication.getLabel("CHECKOUT_END");
				
				showMessage(msg);
				
				hideProgress();
				
				
			}
		    
			@Override
			public void onFailure(int arg0, Header[] headers, byte[] response,
					Throwable e) {
				
				if(response != null){
					String s = new String(response);	
					Log.i(TAG, s);
				}

				Log.i(TAG, "Stop Retry");
				hideProgress();
				
				//notice the user check the network
				showMessage(MainApplication.getLabel("checkout.error.msg"));
			}

		    @Override
		    public void onRetry(int retryNo) {
		        // called when request is retried
		    	
		    	Log.i(TAG,"retrying number = " + retryNo);
			}
		
		});
	}

	private void showMessage(String msg){
		
		
		message.setText(msg);
		detailBox.setVisibility(View.VISIBLE);
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mDialog.dismiss();
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}

	public String getTitleId() {
		return titleId;
	}

	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}

	@Override
	protected void onLanguageChanged(String language) {
		initUI();
	}

}