package com.hsh.esdbsp.activity;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.adapter.MovieItemAdapter;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Movie;
import com.hsh.esdbsp.widget.Command;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

public class MovieActivity extends BaseActivity {

	private GridView movieGridView;

	private ArrayList<Movie> movieItems;

	private MovieItemAdapter mAdapter;

	private ImageView detailPhoto;
	private Button closeBtn;
	private RelativeLayout popContainer;
	
	private Button watchBtn;
	
	private AsyncHttpClient client;
	
	String myCMDBaseIRC = "SET%20IRC%20";
	
	String CMDChangeMovie = "TC4D";
	
	String CMDSourceMovie = "SET%20MOVIEMODE";
	
	int currentChoice;
	
	
	int isMute = 0;
	int isTVon = 0; 
	
	Button powerBtn, arrowUpBtn,backBtn,arrowDownBtn,arrowLeftBtn, arrowRightBtn, okBtn;
	Button chMinusBtn, chPlusBtn, soundMinusBtn, soundMuteBtn, soundPlusBtn;
	Button playPauseBtn, stopBtn, rw10Btn, ff10Btn, rw60Btn, ff60Btn;

	String myCMD;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.movies);

		FragmentManager fragmentManager = getSupportFragmentManager();

		Bundle bundle = new Bundle();
		// bundle.putString("titleId", "");
		bundle.putBoolean("hideBackBtn", false);
		bundle.putString("titleId", "TV/AUDIO");
		bundle.putString("hightLightChoice", "movie");
		bundle.putInt("choice", 1);
		initTopBar(bundle);
		initBottomBar();

		movieGridView = (GridView) this.findViewById(R.id.movie_grid_view);
		detailPhoto = (ImageView) this.findViewById(R.id.detailPhoto);
		
		popContainer = (RelativeLayout) this.findViewById(R.id.popContainer);
		closeBtn = (Button) this.findViewById(R.id.closeBtn);
		watchBtn = (Button) this.findViewById(R.id.watchBtn);
		

		//call movie hash
		getMovieHash();
		if (movieItems == null) {
			movieItems = new ArrayList<Movie>();
		}

		for (int x = 0; x < 50; x++) {
			Movie m = new Movie(x + "");
			movieItems.add(m);
		}

		if (mAdapter == null) {
			mAdapter = new MovieItemAdapter(this, movieItems);
			movieGridView.setAdapter(mAdapter);
		}

		mAdapter.notifyDataSetChanged();
		
		movieGridView.setOnItemClickListener(new OnItemClickListener() {


			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				popContainer.setVisibility(View.VISIBLE);
				currentChoice = position;
				setDetailPhoto(position);
			}
		});
		
		closeBtn.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				popContainer.setVisibility(View.GONE);
			}
			
		});
		
		watchBtn.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				Command.Movie.Watch(octTOHex(currentChoice));
			}
		});
		
		watchBtn.setText(MainApplication.getLabel("watch"));
		
		Helper.setAppFonts(watchBtn);
		
		setupRemoteControl();
	}
	
	private void getMovieHash(){
		Log.i(TAG, "getMovieHash fire");
		
		client = new AsyncHttpClient();
		client.setMaxRetriesAndTimeout(0, 10000);

		String url = MainApplication.getCmsApiBase()
				+ getString(R.string.get_movie_hash);

		Log.i(TAG, "URL = " + url);

		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int arg0, Header[] headers, byte[] response) {
				String s = new String(response);
				Log.i(TAG, "success response = " + s);

				JSONObject restObject;
				try {
					restObject = new JSONObject(s);

					JSONArray jArray = restObject.getJSONArray("data");
			
					int length = jArray.length();
					String movieHashKey = jArray.get(0).toString();
					Log.i(TAG, "HashKey = " + movieHashKey);
					if(movieHashKey.equalsIgnoreCase(settings.getString("movieHashKey", ""))){
						//no need update
						Log.i(TAG, "no need update");
					}
					else{
						//there is update
											
						//clear all the image cache related to the PBJ movie
						for(int x = 1; x<=50;x++){
							String imageLink = MainApplication.getApiBase() + "cmspbj/"
									+ "movie/" + "big" + x + "_" + "e" + "." + "jpg";
							
							DiskCacheUtils.removeFromCache(imageLink, ImageLoader.getInstance().getDiskCache());    
							MemoryCacheUtils.removeFromCache(imageLink, ImageLoader.getInstance().getMemoryCache());
							
							imageLink = MainApplication.getApiBase() + "cmspbj/"
									+ "movie/" + "big" + x + "_" + "c" + "." + "jpg";
							
							DiskCacheUtils.removeFromCache(imageLink, ImageLoader.getInstance().getDiskCache());    
							MemoryCacheUtils.removeFromCache(imageLink, ImageLoader.getInstance().getMemoryCache());
							
							imageLink = MainApplication.getApiBase() + "cmspbj/"
									+ "movie/" + "small" + x  + "." + "png";
							
							DiskCacheUtils.removeFromCache(imageLink, ImageLoader.getInstance().getDiskCache());    
							MemoryCacheUtils.removeFromCache(imageLink, ImageLoader.getInstance().getMemoryCache());
						}
						
						
						mAdapter.notifyDataSetChanged();
						settings.edit().putString("movieHashKey", movieHashKey).commit();
					}
					

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}

			@Override
			public void onFailure(int arg0, Header[] headers, byte[] response,
					Throwable e) {

				if (response != null) {
					String s = new String(response);
					Log.i(TAG, s);
				}

				Log.i(TAG, "Stop Retry");
				//hideProgress();

			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried
				Log.i(TAG, "retrying number = " + retryNo);
			}

		});
	}
	
	private void setupRemoteControl(){
		powerBtn = (Button)findViewById(R.id.power_btn);
		powerBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.AVOff();
			}
		});

		arrowUpBtn = (Button)findViewById(R.id.arrow_up);
		arrowUpBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.UP);
			}
		});

		arrowDownBtn = (Button)findViewById(R.id.arrow_down);
		arrowDownBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.DOWN);
			}
		});

		arrowLeftBtn = (Button)findViewById(R.id.arrow_left);
		arrowLeftBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.LEFT);
			}
		});

		arrowRightBtn = (Button)findViewById(R.id.arrow_right);
		arrowRightBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.RIGHT);
			}
		});

		backBtn = (Button)findViewById(R.id.back_btn);
		backBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.BACK);
			}
		});

		okBtn = (Button)findViewById(R.id.ok_button);
		okBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.OK);
			}
		});
		
		chMinusBtn = (Button)findViewById(R.id.ch_minus);
		chMinusBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.CHANNELDOWN);
			}
		});

		chPlusBtn = (Button)findViewById(R.id.ch_plus);
		chPlusBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.CHANNELUP);
			}
		});

		soundMinusBtn = (Button)findViewById(R.id.sound_minus);
		soundMinusBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.VOLUMNDOWN);
			}
		});

		soundMuteBtn = (Button)findViewById(R.id.sound_mute);
		soundMuteBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.MUTE);
			}
		});

		soundPlusBtn = (Button)findViewById(R.id.sound_plus);
		soundPlusBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.VOLUMNUP);
			}
		});
		 
		playPauseBtn = (Button)findViewById(R.id.playPauseBtn);
		playPauseBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.PLAYPAUSE);
			}
		});

		stopBtn = (Button)findViewById(R.id.stopBtn);
		stopBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.STOP);
			}
		});

		rw10Btn = (Button)findViewById(R.id.rw10Btn);
		rw10Btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.REWIND10);
			}
		});

		ff10Btn = (Button)findViewById(R.id.ff10Btn);
		ff10Btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.FASTFORWARD10);
			}
		});

		rw60Btn = (Button)findViewById(R.id.rw60Btn);
		rw60Btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.REWIND60);
			}
		});

		ff60Btn = (Button)findViewById(R.id.ff60Btn);
		ff60Btn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				MainApplication.playClickSound(v);
				Command.Movie.RemoteButton(Command.RemoteButton.FASTFORWARD60);
			}
		});
	}
	
	private String octTOHex(int i){
		//String s = Integer.toHexString(Integer.parseInt(i+"", 8));
		//Log.i(TAG, "calculated hex = " + s);
		
		//Integer decnum = Integer.parseInt(i+"", 16);
		String hexnum = Integer.toHexString(i+1);
		
		Log.i(TAG, "hexnum.length() = " + hexnum.length());
		
		if(hexnum.length()==1){
			hexnum = "0"+hexnum;
		}
		Log.i(TAG, "calculated hex = " + hexnum);
		return hexnum;
		
	}

	private void setDetailPhoto(int position) {
		DisplayImageOptions options = new DisplayImageOptions.Builder()
				.showImageOnLoading(R.drawable.thumb_transparent)
				.showImageForEmptyUri(R.drawable.thumb_transparent)
				.showImageOnFail(R.drawable.thumb_transparent)
				.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();

		String lang = "e";

		String currLang = MainApplication.getMAS().getData("data_language") == "" ? "E" : MainApplication.getMAS().getData("data_language");
		if (currLang.equalsIgnoreCase("CS")
				|| DataCacheManager.getInstance().getLang().equalsIgnoreCase("CS")
				|| currLang.equalsIgnoreCase("CT")
				|| DataCacheManager.getInstance().getLang().equalsIgnoreCase("CT")) {
			lang = "c";
		}
		
		String imageLink = MainApplication.getApiBase() + "cmspbj/" + "movie/" + "big" + +(position + 1) + "_" + lang + "." + "jpg";
		Log.i(TAG, "imageLink = " + imageLink);

		ImageLoader.getInstance().displayImage(imageLink, detailPhoto, options);
	}

}
