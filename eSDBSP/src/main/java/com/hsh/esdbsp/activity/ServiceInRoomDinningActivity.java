package com.hsh.esdbsp.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.InRoomDinOrderFragment;
import com.hsh.esdbsp.fragment.MessagePopupFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.adapter.dining.FoodAdapter;
import com.hsh.esdbsp.adapter.dining.FoodCatAdapter;
import com.hsh.esdbsp.adapter.dining.FoodHistoryAdapter;
import com.hsh.esdbsp.adapter.dining.FoodHistoryAdapter.FoodHistory;
import com.hsh.esdbsp.adapter.dining.FoodSubCatAdapter;
import com.hsh.esdbsp.adapter.dining.GeneralItemWrapper;
import com.hsh.esdbsp.model.FoodOrderData;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.ImageWidget;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.FoodOrder;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.network.ApiRequest;
import com.hsh.esdbsp.network.XMLCaller;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ServiceInRoomDinningActivity extends BaseActivity implements InRoomDinOrderFragment.Callback {

    private String TAG = "ServiceInRoomDinningActivity";
    private ListView leftMenuListView;
    private ListView rightMenuListView;
    private LinearLayout top_list_container;
    private LinearLayout mainLayout;
    private FrameLayout historyLayout;
    private ListView historyList;
    private MyTextView historyEmptyText;

    private FoodCatAdapter foodCatAdapter;

    private FoodSubCatAdapter foodSubCatAdapter;

    private FoodAdapter foodAdapter;

    private ArrayList<GeneralItem> allItemArray;

    private ArrayList<GeneralItem> foodCatArray;

    private ArrayList<GeneralItem> foodSubCatArray;//this is array storing all foodSubCat

    private ArrayList<GeneralItem> filter_foodSubCatArray;//this array storing foodSubCat for selected Food Category

    private ArrayList<GeneralItem> foodArray;
    private ArrayList<GeneralItem> filter_foodArray, sub_foodArray;

    private ArrayList<TextView> catMenuArray;

    private Button subItemBackBtn;

    private ImageView topFoodImage;

    private String parentId;
    private String titleId;

    private LinearLayout layout_drink_container;

    private int layoutType;

    //store it the cater the need of showing level 4 (sub_item)
    private int currentSubCatIndex;

    private ViewGroup header;

    private HorizontalScrollView horizontal_scrollView;

    private AsyncHttpClient client;

    public enum ApiCallType {
        GET_FOOD_CAT, GET_FOOD_SUB_CAT, GET_FOOD
    }

    ApiCallType apiCallType;

    GeneralItem currentFood;

    FrameLayout optionFragmentContainer;

    Button reviewOrderBtn;
    Button callBtn;

    RelativeLayout rightTopBar;
    TextView itemListTitle;

    ArrayList<FoodOrder> orderDetail = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cms_two_column_food);

        mDialog = new ProgressDialog(this);

        initUI();

        parentId = getIntent().getStringExtra("parentId");
        titleId = getIntent().getStringExtra("titleId");

        FragmentManager fragmentManager = getSupportFragmentManager();

        Bundle bundle = new Bundle();
        bundle.putString("titleId", titleId);
        bundle.putBoolean("isIRD", true);
        initTopBar(bundle);
        initBottomBar();

        parentId = getIntent().getStringExtra("parentId");

        if (foodCatArray == null) {
            foodCatArray = new ArrayList<GeneralItem>();
        }

        if (foodSubCatArray == null) {
            foodSubCatArray = new ArrayList<GeneralItem>();
        }

        if (foodArray == null) {
            foodArray = new ArrayList<GeneralItem>();
        }

        if (filter_foodSubCatArray == null) {
            filter_foodSubCatArray = new ArrayList<GeneralItem>();
        }

        if (filter_foodArray == null) {
            filter_foodArray = new ArrayList<GeneralItem>();
        }

        if (sub_foodArray == null) {
            sub_foodArray = new ArrayList<GeneralItem>();
        }

        //get itemFromCacheArray
        allItemArray = GlobalValue.getInstance().getAllItemArray();

        for (GeneralItem item : allItemArray) {
            Log.i(TAG, "temp Arary item = " + item.getItemId());
            if (item.getParentId().equalsIgnoreCase(parentId)) {
                foodCatArray.add(item);
                Log.i(TAG, "Add ITEM");
            }
        }
        foodCatAdapter = new FoodCatAdapter(this, foodCatArray);

        //top_list.setAdapter(foodCatAdapter);

        foodSubCatAdapter = new FoodSubCatAdapter(this, filter_foodSubCatArray);

        leftMenuListView.setAdapter(foodSubCatAdapter);

        foodAdapter = new FoodAdapter(this, filter_foodArray);
        rightMenuListView.setAdapter(foodAdapter);


        rightMenuListView.setVerticalScrollBarEnabled(false);
        leftMenuListView.setVerticalScrollBarEnabled(false);
        horizontal_scrollView.setHorizontalScrollBarEnabled(false);

        Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                rightMenuListView.setVerticalScrollBarEnabled(true);
                leftMenuListView.setVerticalScrollBarEnabled(true);
                horizontal_scrollView.setHorizontalScrollBarEnabled(true);
            }
        };
        handler.postDelayed(r, 1000);

        setUpTopList();
        //getFoodCat();
    }

    private void initUI() {
        leftMenuListView = (ListView) findViewById(R.id.list);
        rightMenuListView = (ListView) findViewById(R.id.right_list3);

        horizontal_scrollView = (HorizontalScrollView) findViewById(R.id.horizontal_scrollView);
        top_list_container = (LinearLayout) findViewById(R.id.top_list_container);

        LayoutInflater inflater = getLayoutInflater();
        header = (ViewGroup) inflater.inflate(R.layout.cms_food_list_header, rightMenuListView, false);
        if (rightMenuListView.getHeaderViewsCount() != 1) {
            rightMenuListView.addHeaderView(header, null, false);
        }

        topFoodImage = (ImageView) findViewById(R.id.topFoodImage);

        //topFoodImage = (EnlargeImageView)findViewById(R.id.food_bg);
        //topFoodImage.setVisibility(View.INVISIBLE);

        layout_drink_container = (LinearLayout) findViewById(R.id.layout_drink_container);

        optionFragmentContainer = (FrameLayout) findViewById(R.id.optionFragmentContainer);

        reviewOrderBtn = (Button) findViewById(R.id.review_order);
        reviewOrderBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clickOnReviewOrder();
            }
        });

        callBtn = (Button) findViewById(R.id.callBtn);
        callBtn.setText(MainApplication.getLabel("Call"));

        if (!"1".equals(MainApplication.getEngLabel("inroom.dinning.callbutton.show"))) {
            callBtn.setVisibility(View.GONE);
        }

        callBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clickToCall();
            }
        });

        Helper.setAppFonts(reviewOrderBtn);

        mainLayout = (LinearLayout) findViewById(R.id.foodMain);
        historyLayout = (FrameLayout) findViewById(R.id.foodOrderHistory);
        historyList = (ListView) findViewById(R.id.orderHistoryList);
        historyEmptyText = (MyTextView) findViewById(R.id.orderHistoryListEmpty);
        historyEmptyText.setText(MainApplication.getLabel("INROOM_ORDER_EMPTY"));

        rightTopBar = (RelativeLayout) findViewById(R.id.rightTopBar);
        itemListTitle = (TextView) findViewById(R.id.itemListTitle);

        subItemBackBtn = (Button) findViewById(R.id.subItemBackBtn);
        subItemBackBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clickOnFoodSubCat(currentSubCatIndex, false);
                //subItemBackBtn.setVisibility(View.GONE);
            }
        });

        onPlaceOrder();
    }

    private void setUpTopList() {

        top_list_container.removeAllViews();

        catMenuArray = new ArrayList<TextView>();

        String firstTag = "";
        for (int x = 0; x < foodCatArray.size(); x++) {
            final TextView tv = new TextView(this);
            tv.setText(MainApplication.getLabel(foodCatArray.get(x).getTitleId()).toUpperCase());
            tv.setTag(foodCatArray.get(x).getItemId());

            if (x == 0) {
                firstTag = foodCatArray.get(x).getItemId();
            }
            catMenuArray.add(tv);


            int topMargin = 3;
            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
                topMargin = 10;
            }

            if (GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                tv.setTextSize(22);
                layout.setMargins(6, 0, 36, 0);
                layout.gravity = Gravity.CENTER;
                tv.setLayoutParams(layout);
            } else {
                LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
                tv.setTextSize(30);
                layout.setMargins(6, topMargin, 36, 3);
                layout.gravity = Gravity.CENTER;
                tv.setLayoutParams(layout);
            }

            Helper.setAppFonts(tv);

            tv.setTextColor(getResources().getColor(R.color.white));
            ;

            if (x == 0) {
                tv.setTextColor(getResources().getColor(R.color.bsp_gold));
            }

            tv.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    onClickCatButton(tv.getTag().toString(), true);

                    for (int x = 0; x < catMenuArray.size(); x++) {
                        TextView t = catMenuArray.get(x);
                        t.setTextColor(getResources().getColor(R.color.white));
                    }

                    tv.setTextColor(getResources().getColor(R.color.bsp_gold));
                }
            });
            top_list_container.addView(tv);
        }
        if (firstTag.length() != 0) {
            this.onClickCatButton(firstTag, true);
        }
    }

    public void clickOnFood(int position) {
        currentFood = filter_foodArray.get(position);

        if (!haveChild(currentFood)) {


            if (isItemSameTimePeriodwithOrder(getParent(getParent(currentFood)))) {
                optionFragmentContainer.setVisibility(View.VISIBLE);
                Fragment fragment = InRoomDinOrderFragment.newInstance(currentFood.getItemId(), this);
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.add(R.id.optionFragmentContainer, fragment);
                ft.commit();
            } else {
                MessagePopupFragment fragment = new MessagePopupFragment();
                Bundle args = new Bundle();
                args.putString("message", "inroom.extension.msg");
                args.putInt("timeout", 10000);
                fragment.setArguments(args);
                getSupportFragmentManager().beginTransaction()
                        .add(android.R.id.content, fragment).commit();
            }
        } else {
            rightTopBar.setVisibility(View.VISIBLE);
            itemListTitle.setText(MainApplication.getLabel(currentFood.getTitleId()));
            Helper.setAppFonts(itemListTitle);
            showSubFoodList(currentFood);
        }
    }

    public boolean haveChild(GeneralItem i) {
        boolean result = false;

        Log.i(TAG, "i.getItemId() = " + i.getItemId());

        for (GeneralItem food : allItemArray) {
            if (food.getParentId().equalsIgnoreCase(i.getItemId())) {
                result = true;
            }
        }
        Log.i(TAG, "Result = " + result);

        return result;
    }

    @Override
    public void onClose(Fragment fragment) {
        optionFragmentContainer.setVisibility(View.GONE);
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }

    public void showSubFoodList(GeneralItem i) {
        filter_foodArray.clear();

        for (GeneralItem food : allItemArray) {
            if (food.getParentId().equalsIgnoreCase(i.getItemId())) {
                filter_foodArray.add(food);
            }
        }

        foodAdapter.notifyDataSetChanged();
    }

    public void clickOnFoodSubCat(int position, boolean log) {

        currentSubCatIndex = position;

        //remove the middle back btn
        rightTopBar.setVisibility(View.GONE);

        String first_sub_cat_id = "";

        first_sub_cat_id = filter_foodSubCatArray.get(0).getItemId().toString();

        //first_sub_cat_id = "1";

        //**this is just temporary solution for hardcode data, will remove
        if (position % 2 == 0) {
            first_sub_cat_id = "1";
        } else {
            first_sub_cat_id = "2";
        }
        //**
        //layout_drink_container
        //int layoutType = filter_foodSubCatArray.get(position).getLayout();

        Log.i(TAG, "layout type = " + layoutType);

        GeneralItem currentItem = null;
        if (filter_foodSubCatArray.size() > position) {
            currentItem = filter_foodSubCatArray.get(position);
        }

        //horizontal food layout
        if (layoutType == 0) {
            layout_drink_container.setVisibility(View.GONE);

            topFoodImage = (ImageView) findViewById(R.id.topFoodImage);

            rightMenuListView = (ListView) findViewById(R.id.right_list3);
            rightMenuListView.setVisibility(View.VISIBLE);

            if (rightMenuListView.getHeaderViewsCount() != 1) {
                rightMenuListView.addHeaderView(header, null, false);
            }
            foodAdapter.setLayoutType(layoutType);

            rightMenuListView.setAdapter(foodAdapter);

            rightTopBar = (RelativeLayout) findViewById(R.id.rightTopBar);
            rightTopBar.setVisibility(View.GONE);
            itemListTitle = (TextView) findViewById(R.id.itemListTitle);

            subItemBackBtn = (Button) findViewById(R.id.subItemBackBtn);
            subItemBackBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    clickOnFoodSubCat(currentSubCatIndex, false);
                    //subItemBackBtn.setVisibility(View.GONE);
                }
            });
            reviewOrderBtn.setVisibility(View.GONE);
            reviewOrderBtn = (Button) findViewById(R.id.review_order);
            reviewOrderBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    clickOnReviewOrder();
                }
            });

            onPlaceOrder();

        }

        //vertical food layout
        else if (layoutType == 1) {
            layout_drink_container.setVisibility(View.VISIBLE);

            topFoodImage = (ImageView) findViewById(R.id.food_bg);

            rightMenuListView.setVisibility(View.GONE);
            rightMenuListView = (ListView) findViewById(R.id.right_list3_2);
            rightMenuListView.setVisibility(View.VISIBLE);

            rightMenuListView.removeHeaderView(header);

            foodAdapter.setLayoutType(layoutType);

            rightMenuListView.setAdapter(foodAdapter);

            rightTopBar = (RelativeLayout) findViewById(R.id.rightTopBar_2);
            rightTopBar.setVisibility(View.GONE);
            itemListTitle = (TextView) findViewById(R.id.itemListTitle_2);

            subItemBackBtn = (Button) findViewById(R.id.subItemBackBtn_2);
            subItemBackBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    clickOnFoodSubCat(currentSubCatIndex, false);
                    //subItemBackBtn.setVisibility(View.GONE);
                }
            });

            reviewOrderBtn.setVisibility(View.GONE);
            reviewOrderBtn = (Button) findViewById(R.id.review_order_2);
            reviewOrderBtn.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    clickOnReviewOrder();
                }
            });
            onPlaceOrder();
        }

        filter_foodArray.clear();

        if (currentItem != null) {
            for (GeneralItem food : allItemArray) {
                if (food.getParentId().equalsIgnoreCase(currentItem.getItemId())) {
                    filter_foodArray.add(food);
                }
            }
        }

        foodAdapter.notifyDataSetChanged();

        //set up image
        //topFoodImage.setImageResource(R.drawable.bbg);

        if (currentItem.getImageName() != null && !currentItem.getImageName().equalsIgnoreCase("") && !currentItem.getImageName().equalsIgnoreCase("null")) {
            ImageWidget.loadImage(topFoodImage, currentItem.getImageName(), currentItem.getExt(), ImageWidget.SIZE_M);
        }

        if (log) {
            //for Flurry log
            final Map<String, String> map = new HashMap<String, String>();
            map.put("Room", MainApplication.getMAS().getData("data_myroom"));
            map.put("Choice", MainApplication.getEngLabel(currentItem.getTitleId()));
            FlurryAgent.logEvent("DinSubCategory", map);
        }
    }

    private void onClickCatButton(String tag, boolean log) {
        Log.i(TAG, tag);

        //remove the middle back btn
        rightTopBar.setVisibility(View.GONE);

        filter_foodSubCatArray.clear();

        //can correponding layout type of current food cat
        for (GeneralItem item : foodCatArray) {
            if (item.getItemId().equalsIgnoreCase(tag)) {
                layoutType = item.getLayout();
                Log.i(TAG, "layout type = " + layoutType);

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                map.put("Choice", MainApplication.getEngLabel(item.getTitleId()));
                FlurryAgent.logEvent("DinCategory", map);
            }
        }

        for (GeneralItem foodSubCategory : allItemArray) {
            if (foodSubCategory.getParentId().equalsIgnoreCase(tag)) {
                filter_foodSubCatArray.add(foodSubCategory);
            }
        }

        foodSubCatAdapter.setCurrentPosition(0);
        foodSubCatAdapter.notifyDataSetChanged();

        leftMenuListView.setSelectionAfterHeaderView();


        if (filter_foodSubCatArray.size() == 0) {
            return;
        }

        if (foodSubCatAdapter.getFirstButton() != null) {
            //foodSubCatAdapter.getFirstButton().performClick();
        } else {
            Handler handler = new Handler();
            final Runnable r = new Runnable() {
                public void run() {
                    //foodSubCatAdapter.getFirstButton().performClick();
                }
            };
            handler.postDelayed(r, 2000);
        }

        String first_sub_cat_id = filter_foodSubCatArray.get(0).getItemId().toString();
        first_sub_cat_id = "1";


        filter_foodArray.clear();
        for (GeneralItem food : foodArray) {
            if (food.getParentId().equalsIgnoreCase(filter_foodSubCatArray.get(0).getItemId())) {
                filter_foodArray.add(food);
            }
        }

        foodAdapter.notifyDataSetChanged();

        clickOnFoodSubCat(0, log);
    }

    private void getFoodCat() {
        mDialog.setCancelable(false);
        mDialog.setMessage(MainApplication.getLabel("loading"));
        showDialog();
        Log.i(TAG, "getFoodCat start");
        apiCallType = ApiCallType.GET_FOOD_CAT;

        if (MainApplication.useLocalFile) {
            // Reading text file from assets folder
            StringBuffer sb = new StringBuffer();
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(getAssets().open(
                        "food_category" + ".txt")));
                String temp;
                while ((temp = br.readLine()) != null)
                    sb.append(temp);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close(); // stop reading
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            String myjsonstring = sb.toString();

            postExecute(myjsonstring);
        }

        try {
            URL url = null;

            url = new URL(MainApplication.getApiBase()
                    + this.getString(R.string.get_table) + "?table=" + "food_category");
            Log.e(TAG, "url = " + url);

            Bundle bundle = new Bundle();
            ApiRequest.request(this, url, "get", bundle);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        } catch (Exception e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        }
    }

    private void getFoodSubCat() {
        mDialog.setCancelable(false);
        mDialog.setMessage(MainApplication.getLabel("loading"));
        showDialog();
        Log.i(TAG, "getFoodCat start");

        apiCallType = ApiCallType.GET_FOOD_SUB_CAT;

        if (MainApplication.useLocalFile) {
            // Reading text file from assets folder
            StringBuffer sb = new StringBuffer();
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(getAssets().open(
                        "food_sub_category" + ".txt")));
                String temp;
                while ((temp = br.readLine()) != null)
                    sb.append(temp);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close(); // stop reading
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            String myjsonstring = sb.toString();

            postExecute(myjsonstring);
        }

        try {
            URL url = null;

            url = new URL(MainApplication.getApiBase()
                    + this.getString(R.string.get_table) + "?table=" + "food_sub_category");
            Log.e(TAG, "url = " + url);

            Bundle bundle = new Bundle();
            ApiRequest.request(this, url, "get", bundle);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        } catch (Exception e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        }
    }

    private void getFood() {
        mDialog.setCancelable(false);
        mDialog.setMessage(MainApplication.getLabel("loading"));
        showDialog();
        Log.i(TAG, "getFoodCat start");

        apiCallType = ApiCallType.GET_FOOD;

        if (MainApplication.useLocalFile) {
            // Reading text file from assets folder
            StringBuffer sb = new StringBuffer();
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(getAssets().open(
                        "food" + ".txt")));
                String temp;
                while ((temp = br.readLine()) != null)
                    sb.append(temp);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close(); // stop reading
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            String myjsonstring = sb.toString();

            postExecute(myjsonstring);
        }

        try {
            URL url = null;

            url = new URL(MainApplication.getApiBase()
                    + this.getString(R.string.get_table) + "?table=" + "food");
            Log.e(TAG, "url = " + url);

            Bundle bundle = new Bundle();
            ApiRequest.request(this, url, "get", bundle);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        } catch (Exception e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        }
    }

    @Override
    public boolean hasInternet() {
        return Helper.hasInternet(false);
    }

    @Override
    public void onError(int failMode, boolean isPostExecute) {
        mDialog.dismiss();
        Log.i(TAG, "failMode" + failMode);
        Log.i(TAG, "onerror");
    }

    @Override
    public void postExecute(String json) {

        Log.i(TAG, "postExecute");
        Log.i("XMLContent", "api xml =" + json);

        hideDialog();
        /*if (apiCallType == ApiCallType.GET_FOOD_CAT) {
            GetFoodCatParser parser = new GetFoodCatParser(json, this, "food_category");
			parser.startParsing();
		}
		else if(apiCallType == ApiCallType.GET_FOOD_SUB_CAT) {
			GetFoodSubCatParser parser = new GetFoodSubCatParser(json, this, "food_sub_category");
			parser.startParsing();
		}
		else if(apiCallType == ApiCallType.GET_FOOD) {
			GetFoodParser parser = new GetFoodParser(json, this, "food");
			parser.startParsing();
		}*/
    }

    @Override
    public void onPlaceOrder() {
        int count = FoodOrderData.getInstance().getFoodOrder().size();
        if (count > 0) {
            reviewOrderBtn.setVisibility(View.VISIBLE);
            reviewOrderBtn.setText(MainApplication.getLabel("Review order") + " (" + count + ")");
        } else {
            reviewOrderBtn.setVisibility(View.GONE);
            FoodOrderData.getInstance().setStartTime(null);
            FoodOrderData.getInstance().setEndTime(null);
            FoodOrderData.getInstance().setStartTime2(null);
            FoodOrderData.getInstance().setEndTime2(null);
        }
    }

    public void clickOnReviewOrder() {
        Intent intent = new Intent(this, ServiceInRoomDinningConfirmActivity.class);
        intent.putExtra("orderDetail", orderDetail);
        startActivityForResult(intent, 1);
    }

    public void clickOnCallPhone() {
        Intent intent = new Intent(this, ServiceInRoomDinningConfirmActivity.class);
        intent.putExtra("orderDetail", orderDetail);
        startActivityForResult(intent, 1);
    }

    private void clickToCall() {

        Log.i(TAG, "click to Call fire");

        MessagePopupFragment fragment = new MessagePopupFragment();
        Bundle args = new Bundle();
        args.putString("message", "inroom.dinning.callbutton.message");
        args.putInt("timeout", 10000);
        fragment.setArguments(args);
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, fragment).commit();

        client = new AsyncHttpClient();

        client.setMaxRetriesAndTimeout(0, 10000);

        String url = "http://" + MainApplication.getMAS().getMASPath() + "/mas.php?cmd=" + "SET%20DIAL%20" + MainApplication.getLabel("roomServiceKey");

        Log.i(TAG, "URL = " + url);

        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int arg0, Header[] headers, byte[] response) {
                String s = new String(response);
                Log.i(TAG, "success response = " + s);

            }

            @Override
            public void onFailure(int arg0, Header[] headers, byte[] response, Throwable e) {
                Log.i(TAG, "Error = " + e.toString());
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                Log.i(TAG, "retrying number = " + retryNo);
            }

        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            onPlaceOrder(); // Update Review Order button text
        }
    }

    public void onTopBarMainPressed() {
        mainLayout.setVisibility(View.VISIBLE);
        historyLayout.setVisibility(View.GONE);
    }

    public void onTopBarHistoryPressed() {
        mainLayout.setVisibility(View.GONE);
        historyLayout.setVisibility(View.VISIBLE);
        loadHistory();
    }

    public void loadHistory() {
        String roomId = MainApplication.getMAS().getData("data_myroom");
        if (roomId == null || "".equals(roomId)) {
            roomId = "9999";
        }
        AsyncHttpClient client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(0, 10000);
        String url = MainApplication.getCmsApiBase() + "ird/getOrder.php?room=" + roomId + "&device=BSP";

        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onFailure(int arg0, Header[] headers, byte[] response, Throwable arg3) {
                arg3.printStackTrace();
            }

            @Override
            public void onSuccess(int arg0, Header[] headers, byte[] response) {

                String json = new String(response);
                ArrayList<FoodHistory> result = new ArrayList<>();
                JSONObject foodHistory;
                String lastOrderDate = "";
                try {
                    foodHistory = new JSONObject(json);
                    JSONArray dataArray = foodHistory.getJSONArray("data");
                    if (dataArray != null) {
                        for (int i = 0; i < dataArray.length() && i < 20; i++) {
                            JSONObject data = dataArray.getJSONObject(i);
                            FoodHistory f = new FoodHistory();
                            f.id = data.getString("id");
                            f.room = data.getString("room");

                            f.foodIdList = new ArrayList<>();
                            String[] itemArray = data.getString("foodIdList").split(",");

                            GeneralItemWrapper rootItem = null;
                            String[] firstIdAndQty = itemArray[0].split(":");
                            String firstId = firstIdAndQty[0];
                            int firstQty = firstIdAndQty.length > 1 ? Integer.parseInt(firstIdAndQty[1]) : 0;
                            for (GeneralItem item : allItemArray) {
                                if (item.getItemId().equalsIgnoreCase(firstId)) {
                                    rootItem = new GeneralItemWrapper(item);
                                    rootItem.setQuantity(firstQty);
                                    rootItem.fillChildren();
                                    f.foodIdList.add(rootItem);
                                }
                            }

                            for (int j = 1; j < itemArray.length; j++) {
                                String[] idAndQty = itemArray[j].split(":");
                                String id = idAndQty[0];
                                if (id.lastIndexOf('/') > 0) {
                                    id = id.substring(id.lastIndexOf('/') + 1);
                                }
                                int qty = idAndQty.length > 1 ? Integer.parseInt(idAndQty[1]) : 0;
                                GeneralItemWrapper item = rootItem.findChild(id);
                                if (item != null) {
                                    item.setQuantity(qty);
                                    f.foodIdList.add(item);
                                } else {
                                    Log.d(TAG, "Got null child? id: " + id);
                                }
                            }

                            f.deliveryTime = data.getString("deliveryTime");
                            f.orderTime = data.getString("orderTime");
                            f.numOfGuest = data.getString("numOfGuest");
                            f.quantity = data.getString("quantity");
                            f.status = data.getString("status");
                            f.lastUpdate = data.getString("lastUpdate");
                            f.lastUpdateBy = data.getString("lastUpdateBy");
                            f.item = data.getString("item");
                            f.choices = data.optString("choices");

                            if (!lastOrderDate.equals(f.orderTime.substring(0, 10))) {
                                lastOrderDate = f.orderTime.substring(0, 10);
                                FoodHistory h = new FoodHistory();
                                h.header = lastOrderDate;
                                result.add(h);
                            }
                            result.add(f);
                        }
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (result.size() > 0) {
                    historyList.setAdapter(new FoodHistoryAdapter(ServiceInRoomDinningActivity.this, 0, result));
                    historyEmptyText.setVisibility(View.INVISIBLE);
                } else {
                    historyList.setAdapter(null);
                    historyEmptyText.setVisibility(View.VISIBLE);
                }
            }

        });
    }

    private boolean isItemSameTimePeriodwithOrder(GeneralItem currentFood) {

        if (FoodOrderData.getInstance().getStartTime() == null || FoodOrderData.getInstance().getEndTime() == null) {
            return true;
        } else {
            //get current food time range
            int currentFoodStartEndTimes[][];
            if ((currentFood.getStartTime2() != null && !"00:00:00".equals(currentFood.getStartTime2())) ||
                    (currentFood.getEndTime2() != null && !"00:00:00".equals(currentFood.getEndTime2()))) {
                // Two available time slots
                currentFoodStartEndTimes = new int[2][2];
                currentFoodStartEndTimes[0][0] = stringTimeToInt(currentFood.getStartTime());
                currentFoodStartEndTimes[0][1] = stringTimeToInt(currentFood.getEndTime());
                currentFoodStartEndTimes[1][0] = stringTimeToInt(currentFood.getStartTime2());
                currentFoodStartEndTimes[1][1] = stringTimeToInt(currentFood.getEndTime2());
            } else {
                currentFoodStartEndTimes = new int[1][2];
                currentFoodStartEndTimes[0][0] = stringTimeToInt(currentFood.getStartTime());
                currentFoodStartEndTimes[0][1] = stringTimeToInt(currentFood.getEndTime());
            }

            //get current order time range
            int startEndTimes[][];
            if ((FoodOrderData.getInstance().getStartTime2() != null && !"0:00".equals(FoodOrderData.getInstance().getStartTime2())) ||
                    (FoodOrderData.getInstance().getEndTime2() != null && !"0:00".equals(FoodOrderData.getInstance().getEndTime2()))) {
                // Two available time slots
                startEndTimes = new int[2][2];
                startEndTimes[0][0] = stringTimeToInt(FoodOrderData.getInstance().getStartTime());
                startEndTimes[0][1] = stringTimeToInt(FoodOrderData.getInstance().getEndTime());
                startEndTimes[1][0] = stringTimeToInt(FoodOrderData.getInstance().getStartTime2());
                startEndTimes[1][1] = stringTimeToInt(FoodOrderData.getInstance().getEndTime2());
            } else {
                startEndTimes = new int[1][2];
                startEndTimes[0][0] = stringTimeToInt(FoodOrderData.getInstance().getStartTime());
                startEndTimes[0][1] = stringTimeToInt(FoodOrderData.getInstance().getEndTime());
            }


            for (int i = 0; i < currentFoodStartEndTimes.length; i++) {
                for (int j = 0; j < startEndTimes.length; j++) {

                    if (currentFoodStartEndTimes[i][0] < startEndTimes[j][1] && currentFoodStartEndTimes[i][1] > startEndTimes[j][0]) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    private int stringTimeToInt(String time) {
        String[] timeParts = time.split(":");
        if (timeParts.length >= 2) {
            return Integer.parseInt(timeParts[0]) * 60 + Integer.parseInt(timeParts[1]);
        } else {
            return -1;
        }
    }

    private GeneralItem getParent(GeneralItem item) {
        for (GeneralItem i : GlobalValue.getInstance().getAllItemArray()) {
            if (i.getItemId().equals(item.getParentId())) {
                return i;
            }
        }
        return null;
    }

	/*@Override
	public void onGetFoodCatParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetFoodCatFinishParsing(ArrayList<GeneralItem> _foodCatArray) {
		
		//mDialog.dismiss();
		
		foodCatArray.clear();
		
		foodCatArray.addAll(_foodCatArray);
		Log.i(TAG, foodCatArray.toString());
		
		
		catMenuArray = new ArrayList<TextView>();
		
		for(int x = 0; x < foodCatArray.size();x++){
			final TextView tv = new TextView(this);
			tv.setText(foodCatArray.get(x).getName().toUpperCase());
			tv.setTag(foodCatArray.get(x).getItemId());
			catMenuArray.add(tv);
			
			LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
			layout.setMargins(6, 3, 36, 3);
			layout.gravity = Gravity.CENTER;
			tv.setLayoutParams(layout);
			tv.setTextSize(30);

			Helper.setFonts(tv, getString(R.string.app_font));
			
			tv.setTextColor(getResources().getColor(R.color.white));;
			
			if(x == 0){
				tv.setTextColor(getResources().getColor(R.color.bsp_gold));
			}
			
			
			tv.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					onClickCatButton(tv.getTag().toString());
					
					for(int x = 0; x < catMenuArray.size();x++){
						TextView t = catMenuArray.get(x);
						t.setTextColor(getResources().getColor(R.color.white));
					}
					
					tv.setTextColor(getResources().getColor(R.color.bsp_gold));
					
					
				}
			});
			top_list_container.addView(tv);
			
			
		}
		
		//foodCatAdapter.notifyDataSetChanged();
		
		getFoodSubCat();
	}

	@Override
	public void onGetFoodCatError() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onGetFoodSubCatParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetFoodSubCatFinishParsing(
			ArrayList<FoodSubCategory> _foodSubCatArray) {
		// TODO Auto-generated method stub
		//mDialog.dismiss();
		
		foodSubCatArray.clear();
		foodSubCatArray.addAll(_foodSubCatArray);
		
		//foodSubCatAdapter.notifyDataSetChanged();
		
		Log.i(TAG, foodCatArray.toString());	
		
		getFood();
	}

	@Override
	public void onGetFoodSubCatError() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onGetFoodParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onGetFoodFinishParsing(ArrayList<Food> _foodArray) {
		mDialog.dismiss();
		
		foodArray.clear();
		foodArray.addAll(_foodArray);
		Log.i(TAG, foodArray.toString());	
		
		onClickCatButton("1");
	}

	@Override
	public void onGetFoodError() {
		// TODO Auto-generated method stub
		
	}*/

    @Override
    protected void onLanguageChanged(String language) {
        initUI();

        foodCatAdapter.notifyDataSetChanged();
        foodSubCatAdapter.notifyDataSetChanged();
        foodAdapter.notifyDataSetChanged();

        rightMenuListView.setVerticalScrollBarEnabled(false);
        leftMenuListView.setVerticalScrollBarEnabled(false);
        horizontal_scrollView.setHorizontalScrollBarEnabled(false);

        Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                rightMenuListView.setVerticalScrollBarEnabled(true);
                leftMenuListView.setVerticalScrollBarEnabled(true);
                horizontal_scrollView.setHorizontalScrollBarEnabled(true);
            }
        };
        handler.postDelayed(r, 1000);

        setUpTopList();
    }

}