
package com.hsh.esdbsp.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.DataCacheManager;

public class HousekeepingModeActivity extends BaseActivity {

    private TextView clockText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.housekeepingmode);
        initUI();
    }

    private void initUI() {
        clockText = (TextView) findViewById(R.id.clockText);
        clockText.setText((MainApplication.getConfig().getHousekeepInterval() / 1000) + "s remaining");

        new CountDownTimer((MainApplication.getConfig().getHousekeepInterval()), 1000) {

            public void onTick(long millisUntilFinished) {
                clockText.setText((millisUntilFinished / 1000) + "s remaining");
            }

            public void onFinish() {
                DataCacheManager.getInstance().setIsCacheInited(false);
                DataCacheManager.getInstance().startLoadData();
                Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                MainApplication.getCurrentActivity().startActivity(intent);
                HousekeepingModeActivity.this.finish();
            }

        }.start();

    }

}
