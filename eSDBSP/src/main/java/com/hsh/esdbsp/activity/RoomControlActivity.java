package com.hsh.esdbsp.activity;

import java.util.HashMap;
import java.util.Map;

import android.app.Dialog;
import android.app.enterprise.knoxcustom.KnoxCustomManager;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.global.Helper;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.ActivityManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ClipDrawable;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.RelativeLayout;

import com.hsh.esdbsp.receiver.ESDDeviceAdminReceiver;
import com.hsh.esdbsp.view.MyNumberPicker;
import com.hsh.esdbsp.view.SlidingPanel;
import com.hsh.esdbsp.view.TwoDigitFormatter;
import com.hsh.esdbsp.global.CheckMessageUpdateManager;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.widget.Command;
import com.splunk.mint.Mint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class RoomControlActivity extends BaseActivity {

    Context context;

    // SAMSUNG
    private KnoxCustomManager kcm;

    ////////////////
    // system admin
    static final int ACTIVATION_REQUEST = 47; // identifies our request id
    DevicePolicyManager devicePolicyManager;
    ComponentName myDeviceAdmin;

    ///////////////////////////////// loadRCLabel/////////////////////

    static float myBrightness = 0.01f; // for testing, normal is 0.01 full dark
    boolean isSleep = false;

    //////////////////////////////////////////////////////

    ImageView envelopeImage;
    TextView messageText;
    RelativeLayout rlBigEnvelope;

    /////////////////////////////////////////////////////

    boolean isMasterSwitchOn = false;
    int bedroomLightLevel = 0;
    boolean isTempInCelsius = true;
    float ff_rcBedroomTemp = 22.0f;
    float ff_rcBedroomTempOrg = 22.0f;
    boolean isNightLightOn = false;
    boolean isCurtainOn = false;
    int int_rcFan = 0;
    boolean isShowingBigEnvolop = false;
    boolean isAlarmSet = false;
    int int_rcSnooze = 0;
    int int_rcSetHour = 12; // will read from setting file
    int int_rcSetHourMAS = 12; // mas value
    int int_rcSetMinute = 0;
    int int_rcSetMinuteMAS = 0; // mas value
    int curroompage = 0;

    ImageView rcMasterSwitch;
    TextView rcMasterSwitchLabel;

    ImageView rcBedroomLightsTop;
    ImageView rcBedroomLightsBase;
    ImageView rcBedroomLightsDisplay;
    ClipDrawable rcBedroomLightsDisplayDrawable;
    ImageView rcBedroomLightsMinus;

    ImageView rcBedroomLightsPlus;
    TextView rcBedroomLightsLabel;

    TextView rcBedroomTempLabel;
    TextView rcBedroomTempDisplay;
    ImageView rcBedroomTempButton;
    ImageView rcBedroomTempType;
    ImageView rcBedroomTempMinus;
    ImageView rcBedroomTempPlus;
    float ff_rcBedroomTempValue = ff_rcBedroomTemp;

    TextView rcNightLightLabel;
    ImageView rcNightLight;
    LinearLayout rcNightLightALL; // special for suite room only

    TextView rcCurtainsLabel;
    ImageView rcCurtainsTop;
    ImageView rcCurtainsBase;
    ImageView rcCurtainsClose;
    ImageView rcCurtainsOpen;

    TextView rcFanLabel;
    ImageView rcFanTop;
    ImageView rcFanBase;
    ImageView rcFanDisplay;
    ClipDrawable rcFanDisplayDrawable;
    ImageView rcFanMinus;
    ImageView rcFanPlus;

    // new add alarm button for pch
    TextView rcAlarmLabel;
    LinearLayout rcAlarm;

    TextView rcSetAlarmLabel;
    LinearLayout rcSetAlarm;

    TextView rcSnoozeLabel;
    LinearLayout rcSnooze;

    TextView rcSetHourLabel;
    LinearLayout rcSetHour;

    TextView rcSetMinuteLabel;
    LinearLayout rcSetMinute;

    // added for the touch wake
    Button btnTouchWake;

    // ViewFlipper vf; // switch between the layout
    final int bedroompage = 0;
    final int otherroompage = 1;

    SlidingPanel controlPanelContainer;
    LinearLayout controlPanel;

    //////////////////////////////////

    String checkCurLang = "E";

    ///////////////////////////////////
    // for the light bar len
    int levelStep = 1100;
    int bedroomlightBase = 2100;
    int fanspeedBase = 3200;
    int ACMin = MainApplication.getConfig().getMinAC();
    int ACMax = MainApplication.getConfig().getMaxAC();

    /////////////////////////////////////////

    // for pch alarm
    MyNumberPicker hrList;
    MyNumberPicker minList;
    MyNumberPicker apmList;

    // for the pop
    RelativeLayout rcBigSnooze;
    RelativeLayout rcBigOff;

    MyTextView rcBigSnoozeLabel;
    MyTextView rcBigOffLabel;

    // pch version
    private LinearLayout errorMessageDialogLayout;
    private ImageView errorMessageDialogCloseImage;
    private MyTextView errorMessageDialogLabel;

    // for pch alarm pop-up
    private Animation alarmPopAnimShow, alarmPopAnimHide;
    SlidingPanel alarmPopup;
    SlidingPanel alarmPopupAlert;

    boolean isAlarmPopup = false;
    public boolean isAlarmRinging = false;
    private MediaPlayer myAlarmPlayer;
    //////////////////////////////////////////////////////

    private int pendingUpdateLayoutInterval = 1500;
    private HashMap<String, Dialog> dialogMap = new HashMap<String, Dialog>();

    //////////////////////////////////////////////////////
    CountDownTimer countDownTimer = null;
    //////////////////////////////////////////////////////

    @Override
    public void onUserInteraction() {
        restartTimeoutCount();
    }

    @Override
    protected void onStart() {
        GlobalValue.getInstance().setAllowMoveTop(true);
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (MainApplication.mWakeLock != null) {
            MainApplication.mWakeLock.release();
        }

        MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
        MainApplication.mWakeLock.acquire();


        if (bottomBarFragment != null && isBottomBarInit) {
            bottomBarFragment.setRefreshFootRoomArea(true);
        }
        wakeBSP();

        switchControlLayout(MainApplication.getMAS().getCurRoom());
        DataCacheManager.getInstance().startLoadData();
        Helper.showInternetWarningToast();

        MainApplication.getMAS().triggerListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (MainApplication.mWakeLock != null) {
            MainApplication.mWakeLock.release();
        }
        MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
        MainApplication.mWakeLock.acquire();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (MainApplication.mWakeLock != null) {
            MainApplication.mWakeLock.release();
        }
        MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
        MainApplication.mWakeLock.acquire();
    }

    ///////////////////////////////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        context = this;

        Mint.addExtraData("room", MainApplication.getMAS().getData("data_myroom"));

        MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, getClass().getName());
        MainApplication.mWakeLock.acquire();

        if (!GlobalValue.getInstance().getHotel().equals(Hotel.LONDON) &&
                (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) || GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS))) {
            // // system admin
            // Initialize Device Policy Manager service and our receiver
            // class
            devicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
            myDeviceAdmin = new ComponentName(this, ESDDeviceAdminReceiver.class);

            // Activate device administration
            if (!devicePolicyManager.isAdminActive(myDeviceAdmin)) {
                Log.v(TAG, "Samsung: enabling application as device admin");
                try {
                    Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
                    intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, myDeviceAdmin);
                    intent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION, "ESD BSP");
                    startActivityForResult(intent, ACTIVATION_REQUEST);

                } catch (Exception e) {
                    Log.v(TAG, "Exception:" + e);
                }
            } else {
                Log.v(TAG, "Samsung: Application already has device admin privileges");
            }
        }

        // set mas base once
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        int previouslyStartedmas = prefs.getInt(previouslyStartedmasString, Integer.parseInt(masBase));
        MainApplication.getMAS().setCurRoom(previouslyStartedmas);

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            curroompage = bedroompage;
            setContentView(R.layout.roomcontrol);
        } else if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            curroompage = bedroompage;
            setContentView(R.layout.roomcontrol);
        } else {
            curroompage = 5;
            setContentView(R.layout.roomcontrol);
        }

        controlPanelContainer = (SlidingPanel) findViewById(R.id.control_panel_container);

        // for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));

        FlurryAgent.logEvent("RoomControl", map);

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", true);
        bundle.putString("hightLightChoice", "rc");
        initTopBar(bundle);
        initBottomBar();

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            alarmPopAnimShow = AnimationUtils.loadAnimation(this, R.anim.popup_show);
            alarmPopAnimHide = AnimationUtils.loadAnimation(this, R.anim.popup_hide);
            curroompage = 1;
        }
    }

    public void handlePopupMessage(JSONArray messageObjects) {

        if (messageObjects == null || messageObjects.length() < 1) {
            for (Map.Entry<String, Dialog> entry : dialogMap.entrySet()) {
                Dialog dialog = entry.getValue();
                dialog.dismiss();
            }
            dialogMap.clear();
        } else {

            for (int x = 0; x < messageObjects.length(); x++) {

                try {
                    JSONObject messageObject = messageObjects.getJSONObject(x);
                    final String messageId = messageObject.getString("id");
                    boolean isMessageRead = (Integer.parseInt(messageObject.getString("read")) == 1) ? true : false;

                    if (dialogMap.get(messageId) != null) {
                        com.hsh.esdbsp.widget.Log.i(TAG, "exist, not add message");
                        // it is read,we gotta find a way to hide it
                        if (isMessageRead) {
                            Dialog dialog = dialogMap.get(messageId);
                            if (dialog != null) {
                                dialog.dismiss();
                            }
                            dialogMap.remove(messageId);
                        }

                    } else {// not exist
                        com.hsh.esdbsp.widget.Log.i(TAG, "not exist, add message");

                        if (!isMessageRead) {
                            String title = messageObject.getString("subject");
                            String description = messageObject.getString("description");

                            final Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
                            dialog.setContentView(R.layout.custom_message_dialog);
                            dialog.getWindow().setBackgroundDrawableResource(R.color.black_70_percent);
                            dialog.setCanceledOnTouchOutside(false);
                            final Button btnDialogTouchWake = (Button) dialog.findViewById(R.id.btnDialogTouchWake);
                            btnDialogTouchWake.setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    btnDialogTouchWake.setVisibility(View.GONE);
                                    wakeBSP();
                                }
                            });

                            if (MainApplication.getScreenSta() == 1) {
                                btnDialogTouchWake.setVisibility(View.GONE);
                            } else {
                                btnDialogTouchWake.setVisibility(View.VISIBLE);
                            }

                            TextView tvTitle = (TextView) dialog.findViewById(R.id.title);
                            tvTitle.setText(Html.fromHtml(title));

                            TextView tvDescription = (TextView) dialog.findViewById(R.id.msg);
                            tvDescription.setText(Html.fromHtml(description));

                            Button okBtn = (Button) dialog.findViewById(R.id.okBtn);
                            okBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                    CheckMessageUpdateManager.getInstance().setMsgIsRead(messageId);
                                    dialogMap.remove(messageId);
                                }
                            });
                            dialog.show();
                            dialogMap.put(messageId, dialog);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    ////////////////////////////////////////////////////////

    private void loadRCLabel() {

        // change all label
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {

            if (rcNightLightLabel != null) {
                rcNightLightLabel.setText(MainApplication.getLabel("NightLight"));
            }

            try {
                if ((MainApplication.getMAS().getCurRoom() == Integer.parseInt(masBase))
                        || (MainApplication.getMAS().getCurRoom() == Integer.parseInt(massbBase))) {

                    if (rcMasterSwitchLabel != null) {
                        rcMasterSwitchLabel.setText(MainApplication.getLabel("MasterSwitch"));
                    }
                }
            } catch (Exception ex) {

            }

            if (rcBedroomLightsLabel != null) {
                rcBedroomLightsLabel.setText(MainApplication.getLabel("BedroomLight"));
            }

            if (rcAlarmLabel != null) {
                rcAlarmLabel.setText(MainApplication.getLabel("SetAlarm"));
            }
        }

        if (rcBedroomTempLabel != null) {
            rcBedroomTempLabel.setText(MainApplication.getLabel("Temperature"));
        }

        if (rcCurtainsLabel != null) {
            rcCurtainsLabel.setText(MainApplication.getLabel("Curtain"));
        }

        if (rcFanLabel != null) {
            rcFanLabel.setText(MainApplication.getLabel("Fan"));
        }

        if (rcSetAlarmLabel != null) {
            rcSetAlarmLabel.setText(MainApplication.getLabel("SetAlarm"));
        }

        if (rcBigOffLabel != null) {
            rcBigOffLabel.setText(MainApplication.getLabel("AlarmOff").toUpperCase());
        }

        if (rcBigSnoozeLabel != null) {
            rcBigSnoozeLabel.setText(MainApplication.getLabel("Snooze").toUpperCase());
        }

        if (bottomBarFragment != null && isBottomBarInit) {
            bottomBarFragment.rcFootRoomArea_logic();
        }

        if (MainApplication.getMAS().getData("data_language") == "R" || DataCacheManager.getInstance().getLang() == "R") {
            int fontsize = getResources().getInteger(R.integer.room_control_page_custom_font_size);
            if (rcMasterSwitchLabel != null && rcFanLabel != null) {
                rcMasterSwitchLabel.setTextSize(fontsize);
                rcFanLabel.setTextSize(fontsize);
            }
        }
    }

    /////////////////////////////////////////////////////////

    private void unlockScreen() {
        Window window = MainApplication.getCurrentActivity().getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        window.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        window.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    /////////////////////////////////////////////////////////


    @SuppressWarnings("deprecation")
    public void wakeBSP() {

        for (Map.Entry<String, Dialog> entry : dialogMap.entrySet()) {
            Dialog dialog = entry.getValue();
            Button btnDialogTouchWake = (Button) dialog.findViewById(R.id.btnDialogTouchWake);
            btnDialogTouchWake.setVisibility(View.GONE);
        }

        if (MainApplication.mWakeLock != null) {
            MainApplication.mWakeLock.release();
        }

        MainApplication.mWakeLock = MainApplication.powerManager.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, getClass().getName());
        MainApplication.mWakeLock.acquire();

        //unlockScreen();

//            Window mWindow = MainApplication.getCurrentActivity().getWindow();
//            WindowManager.LayoutParams lp = mWindow.getAttributes();
//            lp.screenBrightness = (float) 1;
//            mWindow.setAttributes(lp);

        if (isSleep) {
            isSleep = false;
            Command.RoomControl.WakeupPanel();
        }

        MainApplication.setScreenSta(1);

        if (btnTouchWake == null) {
            btnTouchWake = (Button) findViewById(R.id.btnTouchWake);
        }
        btnTouchWake.setVisibility(View.GONE);

        if (!isAlarmRinging) {
            if (bottomBarFragment != null && isBottomBarInit) {
                bottomBarFragment.setRefreshFootRoomArea(true);
            }
        }
        restartTimeoutCount();
    }

    public void sleepBSP() {

        if (isSleep) {
            return;
        }

        isSleep = true;

        MainApplication.setScreenSta(0);

        Log.v(TAG, "************************");
        Log.v(TAG, "sleeping fire");
        Log.v(TAG, "************************");

        ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);

        int sizeStack = am.getRunningTasks(2).size();

        bottomBarFragment.hideSpinnerDropDown();

        for (int i = 0; i < sizeStack; i++) {
            ComponentName cn = am.getRunningTasks(2).get(i).topActivity;
            Log.d(TAG, cn.getClassName());
        }

        // set back the old setting
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        int previouslyStartedmas = prefs.getInt(previouslyStartedmasString, Integer.parseInt(masBase));

        MainApplication.getMAS().setCurRoom(previouslyStartedmas);
        switchControlLayout(MainApplication.getMAS().getCurRoom());

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            try {
                devicePolicyManager.lockNow();
            } catch (Exception ex) {
                Log.d(TAG, "DevicePolicy: " + ex.getMessage());
            }

            if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
                try {
                    kcm = KnoxCustomManager.getInstance();
                    kcm.setWakeUpMode(true);
                } catch (Throwable e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        }

        isAlarmPopup = false;
        alarmPopup.setVisibility(View.GONE);

        loadControlPanelLayout();

        for (Map.Entry<String, Dialog> entry : dialogMap.entrySet()) {
            Dialog dialog = entry.getValue();
            Button btnDialogTouchWake = (Button) dialog.findViewById(R.id.btnDialogTouchWake);
            btnDialogTouchWake.setVisibility(View.VISIBLE);
        }

        if (btnTouchWake == null) {
            btnTouchWake = (Button) findViewById(R.id.btnTouchWake);
        }
        btnTouchWake.setVisibility(View.VISIBLE);
        btnTouchWake.bringToFront();

//            Window mWindow = MainApplication.getCurrentActivity().getWindow();
//            WindowManager.LayoutParams lp = mWindow.getAttributes();
//            lp.screenBrightness = (float) myBrightness;
//            mWindow.setAttributes(lp);

    }

    /////////////////////////////////////////////////////////////////////////////
    public void showRC() {
        isAlarmPopup = false;
        alarmPopup.setVisibility(View.GONE);
        loadControlPanelLayout();
    }

    //////////////////

    private void killAllLayout() {
        if (controlPanelContainer != null) {
            controlPanelContainer.removeAllViews();
        }
    }

    private void loadControlPanelLayout() {

        killAllLayout();
        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            controlPanel = (LinearLayout) getLayoutInflater().inflate(R.layout.roomcontrol_pch_3_pbh, null);
        } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            // if it is pbj and av room , no curtain
            if ((GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) && (MainApplication.getMAS().getCurRoom() == Integer.parseInt(masavBase))) {
                controlPanel = (LinearLayout) getLayoutInflater().inflate(R.layout.roomcontrol_pch_3, null);
            } else {
                if ((settings.getBoolean("noAC", false)) || this.getString(R.string.hotelacshow).equalsIgnoreCase("noac")) {

                    if (curroompage == bedroompage) {
                        controlPanel = (LinearLayout) getLayoutInflater().inflate(R.layout.roomcontrol_pch_4_noac, null);
                    } else {
                        controlPanel = (LinearLayout) getLayoutInflater().inflate(R.layout.roomcontrol_pch_2_noac, null);
                    }

                } else if ((settings.getBoolean("bacnetAC", false)) || this.getString(R.string.hotelacshow).equalsIgnoreCase("bacnetac")) {

                    if (curroompage == bedroompage) {
                        controlPanel = (LinearLayout) getLayoutInflater().inflate(R.layout.roomcontrol_pch_6_bacnet, null);
                    } else {
                        controlPanel = (LinearLayout) getLayoutInflater().inflate(R.layout.roomcontrol_pch_4_bacnet, null);
                    }

                } else if ((settings.getBoolean("hasCurtain", false)) || this.getString(R.string.hotelcurtain).equalsIgnoreCase("okcurtain")) {

                    if (curroompage == bedroompage) {
                        controlPanel = (LinearLayout) getLayoutInflater().inflate(R.layout.roomcontrol_pch_6, null);
                    } else {
                        controlPanel = (LinearLayout) getLayoutInflater().inflate(R.layout.roomcontrol_pch_4, null);
                    }

                } else {

                    if (curroompage == bedroompage) {
                        controlPanel = (LinearLayout) getLayoutInflater().inflate(R.layout.roomcontrol_pch_5, null);
                    } else {
                        controlPanel = (LinearLayout) getLayoutInflater().inflate(R.layout.roomcontrol_pch_3, null);
                    }

                }
            }
        }
        controlPanelContainer.addView(controlPanel);

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            //alarm
            alarmPopup = (SlidingPanel) findViewById(R.id.alarm_panel_popup);
            alarmPopupAlert = (SlidingPanel) findViewById(R.id.alarm_alert_popup);
            rcSetAlarm = (LinearLayout) findViewById(R.id.rcSetAlarm_pch);
            rcBigSnooze = (RelativeLayout) findViewById(R.id.rcBigSnooze_pch);
            rcBigOff = (RelativeLayout) findViewById(R.id.rcBigOff_pch);
            rcSetAlarmLabel = (MyTextView) findViewById(R.id.rcSetAlarmLabel_pch);
            rcBigSnoozeLabel = (MyTextView) findViewById(R.id.rcBigSnoozeLabel_pch);
            rcBigOffLabel = (MyTextView) findViewById(R.id.rcBigOffLabel_pch);

            // if it is pbj and av room , no curtain

            /// PBH new
            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {

                if (curroompage == bedroompage) {

                    rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pbh);
                    rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus_pbh);
                    rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus_pbh);
                    rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pbh);

                    rcFanBase = (ImageView) findViewById(R.id.rcFanBase_pbh);
                    if (rcFanBase != null) {
                        if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                            rcFanBase.setImageResource(R.drawable.fan5_base_selector);
                        } else {
                            rcFanBase.setImageResource(R.drawable.fan_base_selector);
                        }
                    }

                    rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay_pbh);
                    if (rcFanDisplay != null) {
                        if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                            rcFanDisplay.setImageResource(R.drawable.fan5_speed_display);
                        } else {
                            rcFanDisplay.setImageResource(R.drawable.fan_speed_display);
                        }
                    }

                    rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pbh);
                    rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pbh);
                    rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pbh);
                    rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pbh);
                    rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pbh);
                    rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pbh);

                    rcNightLightALL = (LinearLayout) findViewById(R.id.rcNightLightALL_pbh);
                    rcNightLight = (ImageView) findViewById(R.id.rcNightLight_pbh);
                    rcNightLightLabel = (TextView) findViewById(R.id.rcNightLightLabel_pbh);
                }

            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) && (MainApplication.getMAS().getCurRoom() == Integer.parseInt(masavBase))) {

                rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch3);
                rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch3);
                rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch3);
                rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch3);
                rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch3);
                rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch3);

                rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch3);
                rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch3);
                rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch3);
                rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch3);
                rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch3);
                rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch3);

                rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch3);
                rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus_pch3);
                rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus_pch3);
                rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch3);

                rcFanBase = (ImageView) findViewById(R.id.rcFanBase_pch3);
                if (rcFanBase != null) {
                    if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                        rcFanBase.setImageResource(R.drawable.fan5_base_selector);
                    } else {
                        rcFanBase.setImageResource(R.drawable.fan_base_selector);
                    }
                }

                rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay_pch3);
                if (rcFanDisplay != null) {
                    if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                        rcFanDisplay.setImageResource(R.drawable.fan5_speed_display);
                    } else {
                        rcFanDisplay.setImageResource(R.drawable.fan_speed_display);
                    }
                }

            } else {

                // pch 1708 -- mark this have curtain
                if ((settings.getBoolean("noAC", false)) || this.getString(R.string.hotelacshow).equalsIgnoreCase("noac")) {

                    if (curroompage == bedroompage) {

                        rcMasterSwitch = (ImageView) findViewById(R.id.rcMasterSwitch_pch4_noac);
                        rcMasterSwitchLabel = (TextView) findViewById(R.id.rcMasterSwitchLabel_pch4_noac);

                        rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch4_noac);
                        rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch4_noac);
                        rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch4_noac);
                        rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch4_noac);
                        rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch4_noac);
                        rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch4_noac);

                        rcNightLightALL = (LinearLayout) findViewById(R.id.rcNightLightALL_pch4_noac);
                        rcNightLight = (ImageView) findViewById(R.id.rcNightLight_pch4_noac);
                        rcNightLightLabel = (TextView) findViewById(R.id.rcNightLightLabel_pch4_noac);

                        rcCurtainsTop = (ImageView) findViewById(R.id.rcCurtainsTop_pch4_noac);
                        rcCurtainsBase = (ImageView) findViewById(R.id.rcCurtainsBase_pch4_noac);
                        rcCurtainsClose = (ImageView) findViewById(R.id.rcCurtainsClose_pch4_noac);
                        rcCurtainsOpen = (ImageView) findViewById(R.id.rcCurtainsOpen_pch4_noac);
                        rcCurtainsLabel = (TextView) findViewById(R.id.rcCurtainsLabel_pch4_noac);

                    } else {

                        rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch2_noac);
                        rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch2_noac);
                        rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch2_noac);
                        rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch2_noac);
                        rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch2_noac);
                        rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch2_noac);

                        rcCurtainsTop = (ImageView) findViewById(R.id.rcCurtainsTop_pch2_noac);
                        rcCurtainsBase = (ImageView) findViewById(R.id.rcCurtainsBase_pch2_noac);
                        rcCurtainsClose = (ImageView) findViewById(R.id.rcCurtainsClose_pch2_noac);
                        rcCurtainsOpen = (ImageView) findViewById(R.id.rcCurtainsOpen_pch2_noac);
                        rcCurtainsLabel = (TextView) findViewById(R.id.rcCurtainsLabel_pch2_noac);
                    }

                } else if ((settings.getBoolean("bacnetAC", false)) || this.getString(R.string.hotelacshow).equalsIgnoreCase("bacnetac")) {

                    if (curroompage == bedroompage) {

                        rcMasterSwitch = (ImageView) findViewById(R.id.rcMasterSwitch_pch6_bacnet);
                        rcMasterSwitchLabel = (TextView) findViewById(R.id.rcMasterSwitchLabel_pch6_bacnet);
                        rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch6_bacnet);
                        rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch6_bacnet);
                        rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch6_bacnet);
                        rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch6_bacnet);
                        rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch6_bacnet);
                        rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch6_bacnet);

                        rcNightLightALL = (LinearLayout) findViewById(R.id.rcNightLightALL_pch6_bacnet);
                        rcNightLight = (ImageView) findViewById(R.id.rcNightLight_pch6_bacnet);
                        rcNightLightLabel = (TextView) findViewById(R.id.rcNightLightLabel_pch6_bacnet);

                        rcCurtainsTop = (ImageView) findViewById(R.id.rcCurtainsTop_pch6_bacnet);
                        rcCurtainsBase = (ImageView) findViewById(R.id.rcCurtainsBase_pch6_bacnet);
                        rcCurtainsClose = (ImageView) findViewById(R.id.rcCurtainsClose_pch6_bacnet);
                        rcCurtainsOpen = (ImageView) findViewById(R.id.rcCurtainsOpen_pch6_bacnet);
                        rcCurtainsLabel = (TextView) findViewById(R.id.rcCurtainsLabel_pch6_bacnet);

                        rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch6_bacnet);
                        rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch6_bacnet);
                        rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch6_bacnet);
                        rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch6_bacnet);
                        rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch6_bacnet);
                        rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch6_bacnet);

                        rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch6_bacnet);
                        rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch6_bacnet);

                    } else {

                        rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch4_bacnet);
                        rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch4_bacnet);
                        rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch4_bacnet);
                        rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch4_bacnet);
                        rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch4_bacnet);
                        rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch4_bacnet);

                        rcCurtainsTop = (ImageView) findViewById(R.id.rcCurtainsTop_pch4_bacnet);
                        rcCurtainsBase = (ImageView) findViewById(R.id.rcCurtainsBase_pch4_bacnet);
                        rcCurtainsClose = (ImageView) findViewById(R.id.rcCurtainsClose_pch4_bacnet);
                        rcCurtainsOpen = (ImageView) findViewById(R.id.rcCurtainsOpen_pch4_bacnet);
                        rcCurtainsLabel = (TextView) findViewById(R.id.rcCurtainsLabel_pch4_bacnet);

                        rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch4_bacnet);
                        rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch4_bacnet);
                        rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch4_bacnet);
                        rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch4_bacnet);
                        rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch4_bacnet);
                        rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch4_bacnet);

                        rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch4_bacnet);
                        rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch4_bacnet);
                    }

                } else {

                    if ((settings.getBoolean("hasCurtain", false)) || this.getString(R.string.hotelcurtain).equalsIgnoreCase("okcurtain")) {

                        if (curroompage == bedroompage) {

                            rcMasterSwitch = (ImageView) findViewById(R.id.rcMasterSwitch_pch6);
                            rcMasterSwitchLabel = (TextView) findViewById(R.id.rcMasterSwitchLabel_pch6);

                            rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch6);
                            rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch6);
                            rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch6);
                            rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch6);

                            rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch6);
                            rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch6);

                            rcNightLightALL = (LinearLayout) findViewById(R.id.rcNightLightALL_pch6);
                            rcNightLight = (ImageView) findViewById(R.id.rcNightLight_pch6);
                            rcNightLightLabel = (TextView) findViewById(R.id.rcNightLightLabel_pch6);

                            rcCurtainsTop = (ImageView) findViewById(R.id.rcCurtainsTop_pch6);
                            rcCurtainsBase = (ImageView) findViewById(R.id.rcCurtainsBase_pch6);
                            rcCurtainsClose = (ImageView) findViewById(R.id.rcCurtainsClose_pch6);
                            rcCurtainsOpen = (ImageView) findViewById(R.id.rcCurtainsOpen_pch6);
                            rcCurtainsLabel = (TextView) findViewById(R.id.rcCurtainsLabel_pch6);

                            rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch6);
                            rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus_pch6);
                            rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus_pch6);
                            rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch6);

                            rcFanBase = (ImageView) findViewById(R.id.rcFanBase_pch6);
                            if (rcFanBase != null) {
                                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                    rcFanBase.setImageResource(R.drawable.fan5_base_selector);
                                } else {
                                    rcFanBase.setImageResource(R.drawable.fan_base_selector);
                                }
                            }

                            rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay_pch6);
                            if (rcFanDisplay != null) {
                                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                    rcFanDisplay.setImageResource(R.drawable.fan5_speed_display);
                                } else {
                                    rcFanDisplay.setImageResource(R.drawable.fan_speed_display);
                                }
                            }

                            rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch6);
                            rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch6);
                            rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch6);
                            rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch6);
                            rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch6);
                            rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch6);

                        } else {

                            rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch4);
                            rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch4);
                            rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch4);
                            rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch4);
                            rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch4);
                            rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch4);

                            rcCurtainsTop = (ImageView) findViewById(R.id.rcCurtainsTop_pch4);
                            rcCurtainsBase = (ImageView) findViewById(R.id.rcCurtainsBase_pch4);
                            rcCurtainsClose = (ImageView) findViewById(R.id.rcCurtainsClose_pch4);
                            rcCurtainsOpen = (ImageView) findViewById(R.id.rcCurtainsOpen_pch4);
                            rcCurtainsLabel = (TextView) findViewById(R.id.rcCurtainsLabel_pch4);

                            rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch4);
                            rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch4);
                            rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch4);
                            rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch4);
                            rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch4);
                            rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch4);

                            rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch4);
                            rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus_pch4);
                            rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus_pch4);
                            rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch4);


                            rcFanBase = (ImageView) findViewById(R.id.rcFanBase_pch4);
                            if (rcFanBase != null) {
                                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                    rcFanBase.setImageResource(R.drawable.fan5_base_selector);
                                } else {
                                    rcFanBase.setImageResource(R.drawable.fan_base_selector);
                                }
                            }

                            rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay_pch4);
                            if (rcFanDisplay != null) {
                                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                    rcFanDisplay.setImageResource(R.drawable.fan5_speed_display);
                                } else {
                                    rcFanDisplay.setImageResource(R.drawable.fan_speed_display);
                                }
                            }
                        }

                    } else {

                        if (curroompage == bedroompage) {

                            rcMasterSwitch = (ImageView) findViewById(R.id.rcMasterSwitch_pch);
                            rcMasterSwitchLabel = (TextView) findViewById(R.id.rcMasterSwitchLabel_pch);

                            rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch);
                            rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch);
                            rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch);
                            rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch);

                            rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch);
                            rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch);

                            rcNightLightALL = (LinearLayout) findViewById(R.id.rcNightLightALL_pch);
                            rcNightLight = (ImageView) findViewById(R.id.rcNightLight_pch);
                            rcNightLightLabel = (TextView) findViewById(R.id.rcNightLightLabel_pch);

                            rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch);
                            rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch);
                            rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch);
                            rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch);
                            rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch);
                            rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch);

                            rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch);
                            rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus_pch);
                            rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus_pch);
                            rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch);

                            rcFanBase = (ImageView) findViewById(R.id.rcFanBase_pch);
                            if (rcFanBase != null) {
                                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                    rcFanBase.setImageResource(R.drawable.fan5_base_selector);
                                } else {
                                    rcFanBase.setImageResource(R.drawable.fan_base_selector);
                                }
                            }

                            rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay_pch);
                            if (rcFanDisplay != null) {
                                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                    rcFanDisplay.setImageResource(R.drawable.fan5_speed_display);
                                } else {
                                    rcFanDisplay.setImageResource(R.drawable.fan_speed_display);
                                }
                            }

                        } else {

                            rcBedroomLightsLabel = (TextView) findViewById(R.id.rcBedroomLightsLabel_pch3);
                            rcBedroomLightsTop = (ImageView) findViewById(R.id.rcBedroomLightsTop_pch3);
                            rcBedroomLightsBase = (ImageView) findViewById(R.id.rcBedroomLightsBase_pch3);
                            rcBedroomLightsDisplay = (ImageView) findViewById(R.id.rcBedroomLightsDisplay_pch3);

                            rcBedroomLightsMinus = (ImageView) findViewById(R.id.rcBedroomLightsMinus_pch3);
                            rcBedroomLightsPlus = (ImageView) findViewById(R.id.rcBedroomLightsPlus_pch3);

                            rcBedroomTempButton = (ImageView) findViewById(R.id.rcBedroomTempButton_pch3);
                            rcBedroomTempType = (ImageView) findViewById(R.id.rcBedroomTempType_pch3);
                            rcBedroomTempMinus = (ImageView) findViewById(R.id.rcBedroomTempMinus_pch3);
                            rcBedroomTempPlus = (ImageView) findViewById(R.id.rcBedroomTempPlus_pch3);
                            rcBedroomTempLabel = (TextView) findViewById(R.id.rcBedroomTempLabel_pch3);
                            rcBedroomTempDisplay = (TextView) findViewById(R.id.rcBedroomTempDisplay_pch3);

                            rcFanTop = (ImageView) findViewById(R.id.rcFanTop_pch3);
                            rcFanMinus = (ImageView) findViewById(R.id.rcFanMinus_pch3);
                            rcFanPlus = (ImageView) findViewById(R.id.rcFanPlus_pch3);
                            rcFanLabel = (TextView) findViewById(R.id.rcFanLabel_pch3);

                            rcFanBase = (ImageView) findViewById(R.id.rcFanBase_pch3);
                            if (rcFanBase != null) {
                                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                    rcFanBase.setImageResource(R.drawable.fan5_base_selector);
                                } else {
                                    rcFanBase.setImageResource(R.drawable.fan_base_selector);
                                }
                            }

                            rcFanDisplay = (ImageView) findViewById(R.id.rcFanDisplay_pch3);
                            if (rcFanDisplay != null) {
                                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                    rcFanDisplay.setImageResource(R.drawable.fan5_speed_display);
                                } else {
                                    rcFanDisplay.setImageResource(R.drawable.fan_speed_display);
                                }
                            }
                        }
                    }
                }
            }
        }
        initControlPanelItem();

        loadRCLabel();

        showFaxBigEnvelopePopup();
    }

    private void initControlPanelItem() {

        resetLayoutUpdateCounter(200);

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            prepareAlarmSelectionList();
        }

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            // for alarm set message
            errorMessageDialogLayout = (LinearLayout) findViewById(R.id.errorMessageDialogLayout);
            errorMessageDialogCloseImage = (ImageView) findViewById(R.id.errorMessageDialogCloseImage);
            errorMessageDialogLabel = (MyTextView) findViewById(R.id.errorMessageDialogLabel);

            errorMessageDialogCloseImage.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.errorMessageDialogCloseImage:
                            errorMessageDialogLayout.setVisibility(View.GONE);
                            break;

                        default:
                            break;
                    }
                }
            });
        }

        /////////////////////////////////////////////////

        btnTouchWake = (Button) findViewById(R.id.btnTouchWake);
        btnTouchWake.setVisibility(View.GONE);
        btnTouchWake.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                Log.v(TAG, "Touch Wake");
                wakeBSP();
                return false;
            }

        });

        // pch alarm
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {

            if (isAlarmRinging) {
                alarmPopupAlert.setVisibility(View.VISIBLE);
            } else {
                alarmPopupAlert.setVisibility(View.GONE);
            }

            if (isAlarmPopup) {
                alarmPopup.setVisibility(View.VISIBLE);
            } else {
                alarmPopup.setVisibility(View.GONE);
            }
        }

        if ((MainApplication.getMAS().getCurRoom() == Integer.parseInt(masBase)) || (MainApplication.getMAS().getCurRoom() == Integer.parseInt(massbBase))) {

            if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {

                if (rcMasterSwitch != null) {
                    rcMasterSwitch.setClickable(true);
                    rcMasterSwitch.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            resetLayoutUpdateCounter();

                            rcMasterSwitch.setPressed(true);
                            MainApplication.playClickSound(rcMasterSwitch);

                            if (isMasterSwitchOn) {
                                isMasterSwitchOn = false;
                                Command.RoomControl.MasterLightOff();
                            } else {
                                isMasterSwitchOn = true;
                                Command.RoomControl.MasterLightOn();
                            }
                            rcMasterSwitch_logic();
                            rcMasterSwitch.setPressed(false);
                        }

                    });
                }
            }
        }

        if (rcBedroomTempButton != null) {
            rcBedroomTempButton.setClickable(true);
            rcBedroomTempButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    resetLayoutUpdateCounter();
                    rcBedroomTempButton.setPressed(true);
                    MainApplication.playClickSound(rcBedroomTempButton);


                    if (int_rcFan > 0) {

                        if (isTempInCelsius) {
                            isTempInCelsius = false;
                        } else {
                            isTempInCelsius = true;
                        }

                        // for Flurry log
                        final Map<String, String> map = new HashMap<String, String>();
                        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                        map.put("changeToF", (isTempInCelsius ? "0" : "1"));
                        FlurryAgent.logEvent("Temperature", map);

                        if (isTempInCelsius) {
                            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                                ff_rcBedroomTemp = f2c(ff_rcBedroomTemp);
                                ff_rcBedroomTempOrg = ff_rcBedroomTemp;
                            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;
                            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;
                            }

                        } else {
                            ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);
                            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                                ff_rcBedroomTempOrg = ff_rcBedroomTemp;
                            }
                        }
                        Command.RoomControl.TempCF(isTempInCelsius);

                    } else {
                        if (int_rcFan == 0) {
                            if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                int_rcFan = 5;
                            } else {
                                int_rcFan = 3;
                            }
                        } else {
                            int_rcFan = 0;
                        }
                        rcFan_logic();
                        Command.RoomControl.FanSpeed(int_rcFan, RoomControlActivity.this.getString(R.string.dotfan));
                    }
                    rcBedroomTempCF_logic();
                    rcBedroomTempButton.setPressed(false);
                }
            });
        }

        if (rcBedroomTempType != null) {
            rcBedroomTempType.setClickable(true);
            rcBedroomTempType.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    resetLayoutUpdateCounter();

                    rcBedroomTempType.setPressed(true);
                    MainApplication.playClickSound(rcBedroomTempType);


                    if (int_rcFan > 0) {

                        if (isTempInCelsius) {
                            isTempInCelsius = false;
                        } else {
                            isTempInCelsius = true;
                        }

                        // for Flurry log
                        final Map<String, String> map = new HashMap<String, String>();
                        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                        map.put("changeToF", (isTempInCelsius ? "0" : "1"));
                        FlurryAgent.logEvent("DegreeType", map);

                        if (isTempInCelsius) {
                            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                                ff_rcBedroomTemp = f2c(ff_rcBedroomTemp);
                                ff_rcBedroomTempOrg = ff_rcBedroomTemp;
                            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;
                            }
                        } else {
                            ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);
                            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                                ff_rcBedroomTempOrg = ff_rcBedroomTemp;
                            }
                        }
                    }
                    rcBedroomTempCF_logic();
                    Command.RoomControl.TempCF(isTempInCelsius);

                    rcBedroomTempType.setPressed(false);
                }

            });
        }

        if (rcBedroomTempMinus != null) {
            rcBedroomTempMinus.setClickable(true);
            rcBedroomTempMinus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    resetLayoutUpdateCounter();

                    rcBedroomTempMinus.setPressed(true);
                    MainApplication.playClickSound(rcBedroomTempMinus);

                    // for Flurry log
                    final Map<String, String> map = new HashMap<String, String>();
                    map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                    FlurryAgent.logEvent("TempMinus", map);

                    if (int_rcFan > 0) {
                        if (isTempInCelsius) {
                            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                                ff_rcBedroomTemp = (ff_rcBedroomTemp - 0.5f);
                                ff_rcBedroomTempOrg = ff_rcBedroomTemp;
                            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                                ff_rcBedroomTempOrg = (ff_rcBedroomTempOrg - 1.0f);
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;
                            }
                        } else {
                            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                                ff_rcBedroomTemp = (ff_rcBedroomTemp - 1);
                                ff_rcBedroomTempOrg = ff_rcBedroomTemp;
                            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                                ff_rcBedroomTempOrg = (ff_rcBedroomTempOrg - 1);
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;
                                if (!isTempInCelsius) {
                                    ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);
                                }
                            }
                        }

                        if (ff_rcBedroomTempOrg < ACMin) {
                            ff_rcBedroomTempOrg = ACMin;

                            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;
                            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;
                                if (!isTempInCelsius) {
                                    ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);
                                }
                            }
                        }

                        rcBedroomTempCF_logic();
                        Command.RoomControl.TempChange(ff_rcBedroomTempOrg);

                    } else {
                        // special 5 level
                        if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                            int_rcFan = 5;
                        } else {
                            int_rcFan = 3;
                        }
                        rcFan_logic();
                        rcBedroomTempCF_logic();
                        Command.RoomControl.FanToggle();
                    }
                    rcBedroomTempMinus.setPressed(false);
                }

            });
        }

        if (rcBedroomTempPlus != null) {
            rcBedroomTempPlus.setClickable(true);
            rcBedroomTempPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    resetLayoutUpdateCounter();

                    rcBedroomTempPlus.setPressed(true);
                    MainApplication.playClickSound(rcBedroomTempPlus);


                    // for Flurry log
                    final Map<String, String> map = new HashMap<String, String>();
                    map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                    FlurryAgent.logEvent("TempPlus", map);

                    if (int_rcFan > 0) {
                        if (isTempInCelsius) {
                            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                                ff_rcBedroomTemp = (ff_rcBedroomTemp + 0.5f);
                                ff_rcBedroomTempOrg = ff_rcBedroomTemp;
                            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                                ff_rcBedroomTempOrg = (ff_rcBedroomTempOrg + 1.0f);
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;
                            }
                        } else {
                            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                                ff_rcBedroomTemp = (ff_rcBedroomTemp + 1);
                                ff_rcBedroomTempOrg = ff_rcBedroomTemp;
                            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                                ff_rcBedroomTempOrg = (ff_rcBedroomTempOrg + 1);
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;

                                if (!isTempInCelsius) {
                                    ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);
                                }
                            }
                        }

                        if (ff_rcBedroomTempOrg > ACMax) {
                            ff_rcBedroomTempOrg = ACMax;

                            if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;
                            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                                ff_rcBedroomTemp = ff_rcBedroomTempOrg;

                                if (!isTempInCelsius) {
                                    ff_rcBedroomTemp = c2f(ff_rcBedroomTemp);

                                }
                            }
                        }
                        rcBedroomTempCF_logic();
                        Command.RoomControl.TempChange(ff_rcBedroomTempOrg);

                    } else {
                        if (int_rcFan == 0) {
                            // special 5 level
                            if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                int_rcFan = 5;
                            } else {
                                int_rcFan = 3;
                            }
                        } else {
                            int_rcFan = 0;
                        }
                        rcFan_logic();
                        rcBedroomTempCF_logic();
                        Command.RoomControl.FanToggle();

                    }
                    rcBedroomTempPlus.setPressed(false);
                }

            });
        }

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {

            if (rcBedroomLightsDisplay != null) {
                rcBedroomLightsDisplay.setActivated(true);
                rcBedroomLightsDisplayDrawable = (ClipDrawable) rcBedroomLightsDisplay.getDrawable();
                rcBedroomLightsDisplayDrawable.setLevel(bedroomlightBase + levelStep * bedroomLightLevel); // ClipDrawable
            }
            // 0:
            // blank,
            // 10000:
            // whole

            // bedroom light listener
            OnTouchListener bedroomLightOnTouchListener = new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {

                        case MotionEvent.ACTION_DOWN:

                            rcBedroomLightsBase.setPressed(true);
                            rcBedroomLightsTop.setPressed(true);
                            return true;

                        case MotionEvent.ACTION_UP:

                            resetLayoutUpdateCounter();
                            rcBedroomLightsTop.setPressed(true);
                            rcBedroomLightsBase.setPressed(false);
                            MainApplication.playClickSound(rcBedroomLightsTop);

                            if (bedroomLightLevel == 0) {
                                bedroomLightLevel = 5;
                            } else {
                                bedroomLightLevel = 0;
                            }
                            rcBedroomLights_logic();
                            Command.RoomControl.RoomLightToggle();
                            rcBedroomLightsTop.setPressed(false);
                            return true;
                    }
                    return false;
                }
            };

            if (rcBedroomLightsTop != null) {
                rcBedroomLightsTop.setClickable(true);
                rcBedroomLightsTop.setOnTouchListener(bedroomLightOnTouchListener);
            }

            if (rcBedroomLightsBase != null) {
                rcBedroomLightsBase.setClickable(true);
                rcBedroomLightsBase.setOnTouchListener(bedroomLightOnTouchListener);
            }

            if (rcBedroomLightsMinus != null) {
                rcBedroomLightsMinus.setClickable(true);
                rcBedroomLightsMinus.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        resetLayoutUpdateCounter();

                        rcBedroomLightsMinus.setPressed(true);
                        MainApplication.playClickSound(rcBedroomLightsTop);

                        bedroomLightLevel = (bedroomLightLevel - 1);
                        if (bedroomLightLevel < 0) {
                            bedroomLightLevel = 0;
                        }
                        rcBedroomLights_logic();
                        Command.RoomControl.RoomLightChange(bedroomLightLevel);
                        rcBedroomLightsMinus.setPressed(false);
                    }

                });
            }

            if (rcBedroomLightsPlus != null) {
                rcBedroomLightsPlus.setClickable(true);
                rcBedroomLightsPlus.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        resetLayoutUpdateCounter();

                        rcBedroomLightsPlus.setPressed(true);
                        MainApplication.playClickSound(rcBedroomLightsTop);

                        bedroomLightLevel = (bedroomLightLevel + 1);
                        if (bedroomLightLevel > 5) {
                            bedroomLightLevel = 5;
                        }
                        rcBedroomLights_logic();
                        Command.RoomControl.RoomLightChange(bedroomLightLevel);
                        rcBedroomLightsPlus.setPressed(false);
                    }

                });
            }

            ////////////////////////////////////////
        }

        if ((MainApplication.getMAS().getCurRoom() == Integer.parseInt(masBase))
                || (MainApplication.getMAS().getCurRoom() == Integer.parseInt(massbBase))) {

            if (rcNightLight != null) {
                rcNightLight.setClickable(true);
                rcNightLight.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        resetLayoutUpdateCounter();

                        rcNightLight.setPressed(true);
                        MainApplication.playClickSound(rcNightLight);


                        if (isNightLightOn) {
                            isNightLightOn = false;
                            Command.RoomControl.NightLightOff();
                        } else {
                            isNightLightOn = true;
                            Command.RoomControl.NightLightOn();
                        }
                        // for Flurry log
                        final Map<String, String> map = new HashMap<String, String>();
                        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                        map.put("TurnOn", isNightLightOn ? "1" : "0" + "");
                        FlurryAgent.logEvent("Nightlight", map);

                        rcNightLight_logic();
                        rcNightLight.setPressed(false);
                    }

                });
            }

        }

        ////////////////////////////////////////


        if ((settings.getBoolean("bacnetAC", false)) || this.getString(R.string.hotelacshow).equalsIgnoreCase("bacnetac")) {
            //
        } else {

            if (rcFanDisplay != null) {
                rcFanDisplay.setActivated(true);
                rcFanDisplayDrawable = (ClipDrawable) rcFanDisplay.getDrawable();
                rcFanDisplayDrawable.setLevel(fanspeedBase + levelStep * int_rcFan); // ClipDrawable
            }
            // 0:
            // blank,
            // 10000:
            // whole
        }

        if (rcFanTop != null) {
            rcFanTop.setClickable(true);

            rcFanTop.setOnTouchListener(new OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {

                        case MotionEvent.ACTION_DOWN:

                            MainApplication.playClickSound(rcFanTop);
                            rcFanTop.setPressed(true);
                            if ((settings.getBoolean("bacnetAC", false)) || MainApplication.getCurrentActivity().getString(R.string.hotelacshow).equalsIgnoreCase("bacnetac")) {
                            } else {
                                rcFanBase.setPressed(true);
                            }
                            return true;

                        case MotionEvent.ACTION_UP:

                            resetLayoutUpdateCounter();

                            // for Flurry log
                            final Map<String, String> map = new HashMap<String, String>();
                            map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                            map.put("TurnOff", int_rcFan + "");
                            FlurryAgent.logEvent("FanSpeed", map);

                            if (int_rcFan == 0) {
                                // special 5 level
                                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                    int_rcFan = 5;
                                } else {
                                    if ((settings.getBoolean("bacnetAC", false)) || MainApplication.getCurrentActivity().getString(R.string.hotelacshow).equalsIgnoreCase("bacnetac")) {
                                        int_rcFan = 1;
                                    } else {
                                        int_rcFan = 3;
                                    }
                                }
                            } else {
                                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                    int_rcFan = 0;
                                } else {
                                    int_rcFan = 0;
                                }
                            }

                            rcFanTop.setPressed(false);

                            if ((settings.getBoolean("bacnetAC", false)) || MainApplication.getCurrentActivity().getString(R.string.hotelacshow).equalsIgnoreCase("bacnetac")) {
                                rcBedroomTempCF_logic();
                            } else {
                                rcFanBase.setPressed(false);
                                rcFan_logic();
                            }
                            Command.RoomControl.FanSpeed(int_rcFan, RoomControlActivity.this.getString(R.string.dotfan));
                            return true;
                    }
                    return false;
                }
            });
        }


        if ((settings.getBoolean("bacnetAC", false)) || this.getString(R.string.hotelacshow).equalsIgnoreCase("bacnetac")) {
            //
        } else {
            if (rcFanBase != null) {
                rcFanBase.setClickable(true);

                rcFanBase.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {

                            case MotionEvent.ACTION_DOWN:

                                MainApplication.playClickSound(rcFanTop);
                                rcFanTop.setPressed(true);
                                rcFanBase.setPressed(true);
                                return true;

                            case MotionEvent.ACTION_UP:

                                resetLayoutUpdateCounter();

                                if (int_rcFan == 0) {
                                    if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                        int_rcFan = 5;
                                    } else {
                                        int_rcFan = 3;
                                    }
                                } else {
                                    if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                                        int_rcFan = 0;
                                    } else {
                                        int_rcFan = 0;
                                    }
                                }
                                rcFan_logic();
                                Command.RoomControl.FanSpeed(int_rcFan, RoomControlActivity.this.getString(R.string.dotfan));
                                rcFanTop.setPressed(false);
                                rcFanBase.setPressed(false);
                                return true;
                        }
                        return false;
                    }
                });


            }

            if (rcFanMinus != null) {
                rcFanMinus.setClickable(true);
                rcFanMinus.setActivated(true);

                rcFanMinus.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        resetLayoutUpdateCounter();

                        rcFanMinus.setPressed(true);
                        MainApplication.playClickSound(rcFanMinus);

                        // for Flurry log
                        final Map<String, String> map = new HashMap<String, String>();
                        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                        FlurryAgent.logEvent("FanMinus", map);

                        int_rcFan = int_rcFan - 1;
                        if (int_rcFan < 0) {
                            int_rcFan = 0;
                        }
                        rcFan_logic();
                        Command.RoomControl.FanSpeed(int_rcFan, RoomControlActivity.this.getString(R.string.dotfan));
                        rcFanMinus.setPressed(false);
                    }
                });
            }

            if (rcFanPlus != null) {
                rcFanPlus.setClickable(true);
                rcFanPlus.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        resetLayoutUpdateCounter();

                        rcFanPlus.setPressed(true);
                        MainApplication.playClickSound(rcFanPlus);

                        // for Flurry log
                        final Map<String, String> map = new HashMap<String, String>();
                        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                        FlurryAgent.logEvent("FanPlus", map);

                        int_rcFan = int_rcFan + 1;
                        // special 5 level
                        if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                            if (int_rcFan > 5) {
                                int_rcFan = 5;
                            }
                        } else {
                            if (int_rcFan > 3) {
                                int_rcFan = 3;
                            }
                        }
                        rcFan_logic();
                        Command.RoomControl.FanSpeed(int_rcFan, RoomControlActivity.this.getString(R.string.dotfan));
                        rcFanPlus.setPressed(false);
                    }

                });
            }
        }

        int pos = MainApplication.getMAS().getCurRoom();
        if ((GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) && (pos == Integer.parseInt(masavBase))) {
            // skip
        } else {

            if ((settings.getBoolean("hasCurtain", false)) || this.getString(R.string.hotelcurtain).equalsIgnoreCase("okcurtain")) {

                OnTouchListener curtainOnTouchListener = new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        switch (event.getAction()) {

                            case MotionEvent.ACTION_DOWN:

                                rcCurtainsTop.setPressed(true);
                                rcCurtainsBase.setPressed(true);
                                return true;

                            case MotionEvent.ACTION_UP:

                                resetLayoutUpdateCounter();

                                rcCurtainsTop.setPressed(true);
                                rcCurtainsBase.setPressed(true);
                                MainApplication.playClickSound(rcCurtainsTop);

                                if (isCurtainOn) {
                                    isCurtainOn = false;
                                } else {
                                    isCurtainOn = true;
                                }
                                Command.RoomControl.CurtainToggle();
                                rcCurtains_logic();
                                return true;
                        }
                        return false;
                    }
                };

                if (rcCurtainsTop != null) {
                    rcCurtainsTop.setClickable(true);
                    rcCurtainsTop.setOnTouchListener(curtainOnTouchListener);
                }

                if (rcCurtainsBase != null) {
                    rcCurtainsBase.setClickable(true);
                    rcCurtainsBase.setOnTouchListener(curtainOnTouchListener);
                }

                if (rcCurtainsClose != null) {
                    rcCurtainsClose.setClickable(true);
                    rcCurtainsClose.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            resetLayoutUpdateCounter();

                            rcCurtainsClose.setPressed(true);
                            MainApplication.playClickSound(rcCurtainsTop);

                            Command.RoomControl.CurtainChange(false);
                            rcCurtains_logic();
                            rcCurtainsClose.setPressed(false);
                        }

                    });
                }

                if (rcCurtainsOpen != null) {
                    rcCurtainsOpen.setClickable(true);
                    rcCurtainsOpen.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {

                            resetLayoutUpdateCounter();

                            rcCurtainsOpen.setPressed(true);
                            MainApplication.playClickSound(rcCurtainsTop);

                            Command.RoomControl.CurtainChange(true);
                            rcCurtains_logic();
                            rcCurtainsOpen.setPressed(false);
                        }

                    });
                }
            }
        }
        alarmSetting();
    }

    ////////////////////////////////////////////////////////

    public void rcMasterSwitch_logic() {

        if ((MainApplication.getMAS().getCurRoom() == Integer.parseInt(masBase))
                || (MainApplication.getMAS().getCurRoom() == Integer.parseInt(massbBase))) {

            if (!isMasterSwitchOn) {

                if (rcMasterSwitch != null) {
                    rcMasterSwitch.setActivated(false);
                }

                if (rcBedroomLightsTop != null) {
                    rcBedroomLightsTop.setActivated(false);
                }

                if (rcBedroomLightsBase != null) {
                    rcBedroomLightsBase.setActivated(false);
                }

                bedroomLightLevel = 0;
            } else {

                if (rcNightLightLabel != null) {
                    rcNightLightLabel.setTextColor(getResources().getColor(R.color.white));
                }

                if (rcMasterSwitch != null) {
                    rcMasterSwitch.setActivated(true);
                }

                if (rcBedroomLightsTop != null) {
                    rcBedroomLightsTop.setActivated(true);
                }

                if (rcBedroomLightsBase != null) {
                    rcBedroomLightsBase.setActivated(true);
                }

                bedroomLightLevel = 5;

                isNightLightOn = false;

                if (rcNightLight != null) {
                    rcNightLight.setActivated(false);
                }
            }

            if (rcBedroomLightsDisplayDrawable != null) {
                rcBedroomLightsDisplayDrawable.setLevel(bedroomlightBase + levelStep * bedroomLightLevel); // ClipDrawable
            }
        }
    }

    public void rcBedroomLights_logic() {

        if (bedroomLightLevel == 0) {
            // rcBedroomLightsLabel.setTextColor(getResources().getColor(R.color.white));
            // rcMasterSwitchLabel.setTextColor(getResources().getColor(R.color.white));

            if (rcBedroomLightsTop != null) {
                rcBedroomLightsTop.setActivated(false);
            }

            if (rcBedroomLightsBase != null) {
                rcBedroomLightsBase.setActivated(false);
            }

            isMasterSwitchOn = false;

            if (rcMasterSwitch != null) {
                rcMasterSwitch.setActivated(false);
            }

        } else {

            if (rcNightLightLabel != null) {
                rcNightLightLabel.setTextColor(getResources().getColor(R.color.white));
            }

            if (rcBedroomLightsTop != null) {
                rcBedroomLightsTop.setActivated(true);
            }

            if (rcBedroomLightsBase != null) {
                rcBedroomLightsBase.setActivated(true);
            }

            if ((MainApplication.getMAS().getCurRoom() == Integer.parseInt(masBase))
                    || (MainApplication.getMAS().getCurRoom() == Integer.parseInt(massbBase))) {

                isMasterSwitchOn = true;

                if (rcMasterSwitch != null) {
                    rcMasterSwitch.setActivated(true);
                }

                isNightLightOn = false;
                if (rcNightLight != null) {
                    rcNightLight.setActivated(false);
                }
            }
        }

        if (rcBedroomLightsDisplayDrawable != null) {
            rcBedroomLightsDisplayDrawable.setLevel(bedroomlightBase + levelStep * bedroomLightLevel); // ClipDrawable
        }
    }

    public void rcNightLight_logic() {

        if ((MainApplication.getMAS().getCurRoom() == Integer.parseInt(masBase)) || (MainApplication.getMAS().getCurRoom() == Integer.parseInt(massbBase))) {

            if (rcNightLightALL != null) {
                rcNightLightALL.setVisibility(View.VISIBLE);
            }
        } else {
            if (rcNightLightALL != null) {
                rcNightLightALL.setVisibility(View.GONE);
            }
        }

        if ((MainApplication.getMAS().getCurRoom() == Integer.parseInt(masBase)) || (MainApplication.getMAS().getCurRoom() == Integer.parseInt(massbBase))) {

            if (!isNightLightOn) {

                if (rcNightLightLabel != null) {
                    rcNightLightLabel.setTextColor(getResources().getColor(R.color.white));
                }

                if (rcNightLight != null) {
                    rcNightLight.setActivated(false);
                }

            } else {

                if (rcNightLightLabel != null) {
                    rcNightLightLabel.setTextColor(getResources().getColor(R.color.white));
                }

                if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {

                    if (rcMasterSwitchLabel != null) {
                        rcMasterSwitchLabel.setTextColor(getResources().getColor(R.color.white));
                    }

                    if (rcBedroomLightsLabel != null) {
                        rcBedroomLightsLabel.setTextColor(getResources().getColor(R.color.white));
                    }
                }

                if (rcNightLight != null) {
                    rcNightLight.setActivated(true);
                }

                if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {

                    if (rcBedroomLightsTop != null) {
                        rcBedroomLightsTop.setActivated(false);
                    }

                    if (rcBedroomLightsBase != null) {
                        rcBedroomLightsBase.setActivated(false);
                    }

                    isMasterSwitchOn = false;
                    bedroomLightLevel = 0;

                    if (rcMasterSwitch != null) {
                        rcMasterSwitch.setActivated(false);
                    }
                }
            }

            if (rcBedroomLightsDisplayDrawable != null) {
                rcBedroomLightsDisplayDrawable.setLevel(bedroomlightBase + levelStep * bedroomLightLevel); // ClipDrawable
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////

    public static float c2f(float c) {
        float f = 0f;

        // pch
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
            f = Math.round((int) (c * 9 / 5)) + 32;
            return f;
        } else if (GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            f = Math.round((c * 9 / 5)) + 32;
            return f;
        }

        if (c == 15) {
            f = 59.0f;
        } else if (c == 15.5) {
            f = 60.0f;
        } else if (c == 16) {
            f = 61.0f;
        } else if (c == 16.5) {
            f = 62.0f;
        } else if (c == 17) {
            f = 63.0f;
        } else if (c == 17.5) {
            f = 64.0f;
        } else if (c == 18) {
            f = 64.0f;
        } else if (c == 18.5) {
            f = 65.0f;
        } else if (c == 19) {
            f = 66.0f;
        } else if (c == 19.5) {
            f = 67.0f;
        } else if (c == 20) {
            f = 68.0f;
        } else if (c == 20.5) {
            f = 69.0f;
        } else if (c == 21) {
            f = 70.0f;
        } else if (c == 21.5) {
            f = 71.0f;
        } else if (c == 22) {
            f = 72.0f;
        } else if (c == 22.5) {
            f = 73.0f;
        } else if (c == 23) {
            f = 73.0f;
        } else if (c == 23.5) {
            f = 74.0f;
        } else if (c == 24) {
            f = 75.0f;
        } else if (c == 24.5) {
            f = 76.0f;
        } else if (c == 25) {
            f = 77.0f;
        } else if (c == 25.5) {
            f = 78.0f;
        } else if (c == 26) {
            f = 79.0f;
        } else if (c == 26.5) {
            f = 80.0f;
        } else if (c == 27) {
            f = 81.0f;
        } else if (c == 27.5) {
            f = 82.0f;
        } else if (c == 28) {
            f = 82.0f;
        }
        return f;
    }

    public static float f2c(float f) {
        float c = 0;

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            // pch only have c value
            c = Math.round((int) ((f - 32) * 5 / 9));
            return c;
        }

        if (f == 59) {
            c = 15.0f;
        } else if (f == 60) {
            c = 15.5f;
        } else if (f == 61) {
            c = 16.0f;
        } else if (f == 62) {
            c = 17.0f;
        } else if (f == 63) {
            c = 17.5f;
        } else if (f == 64) {
            c = 18.0f;
        } else if (f == 65) {
            c = 18.5f;
        } else if (f == 66) {
            c = 19.0f;
        } else if (f == 67) {
            c = 19.5f;
        } else if (f == 68) {
            c = 20.0f;
        } else if (f == 69) {
            c = 20.5f;
        } else if (f == 70) {
            c = 21.0f;
        } else if (f == 71) {
            c = 21.5f;
        } else if (f == 72) {
            c = 22.0f;
        } else if (f == 73) {
            c = 23.0f;
        } else if (f == 74) {
            c = 23.5f;
        } else if (f == 75) {
            c = 24.0f;
        } else if (f == 76) {
            c = 24.5f;
        } else if (f == 77) {
            c = 25.0f;
        } else if (f == 78) {
            c = 25.5f;
        } else if (f == 79) {
            c = 26.0f;
        } else if (f == 80) {
            c = 26.5f;
        } else if (f == 81) {
            c = 27.0f;
        } else if (f == 82) {
            c = 28.0f;
        }
        return c;
    }

    public void rcBedroomTempCF_logic() {

        if (int_rcFan > 0) {

            if (rcBedroomTempLabel != null) {
                rcBedroomTempLabel.setTextColor(getResources().getColor(R.color.white));
            }

            if (rcBedroomTempButton != null) {
                rcBedroomTempButton.setActivated(true);
            }

            if (isTempInCelsius) {
                // c
                if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                    ACMin = Math.round(MainApplication.getConfig().getMinAC());
                    ACMax = Math.round(MainApplication.getConfig().getMaxAC());
                } else {
                    ACMin = MainApplication.getConfig().getMinAC();
                    ACMax = MainApplication.getConfig().getMaxAC();
                }

                ff_rcBedroomTempValue = ff_rcBedroomTemp;

                if (rcBedroomTempDisplay != null) {
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                        rcBedroomTempDisplay.setText(Html.fromHtml("" + ff_rcBedroomTempValue + "&#176;C"));
                    } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                            GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                            GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                        rcBedroomTempDisplay.setText(Html.fromHtml("" + Math.round(ff_rcBedroomTempValue) + "&#176;C"));
                    }

                    if (ff_rcBedroomTemp <= 20) {
                        rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.blue));
                    } else if (ff_rcBedroomTemp > 25) {
                        rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.red));
                    } else {
                        rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.white));
                    }
                }
            } else {

                ACMin = MainApplication.getConfig().getMinAC();
                ACMax = MainApplication.getConfig().getMaxAC();

                // int_rcBedroomTempValue = Math.round(int_rcBedroomTemp *
                // 9/5) + 32;
                ff_rcBedroomTempValue = ff_rcBedroomTemp;

                if (rcBedroomTempDisplay != null) {

                    if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                        rcBedroomTempDisplay.setText(Html.fromHtml("" + ff_rcBedroomTempValue + "&#176;F"));
                    } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                            GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                            GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                        rcBedroomTempDisplay.setText(Html.fromHtml("" + Math.round(ff_rcBedroomTempValue) + "&#176;F"));
                    }

                    if (ff_rcBedroomTemp <= 68) {
                        rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.blue));
                    } else if (ff_rcBedroomTemp > 77) {
                        rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.red));
                    } else {
                        rcBedroomTempDisplay.setTextColor(getResources().getColor(R.color.white));
                    }
                }

            }
        } else {

            if (rcBedroomTempLabel != null) {
                rcBedroomTempLabel.setTextColor(getResources().getColor(R.color.white));
            }

            if (rcBedroomTempButton != null) {
                rcBedroomTempButton.setActivated(false);
            }

            if (rcBedroomTempDisplay != null) {
                rcBedroomTempDisplay.setText(Html.fromHtml(""));
            }
        }
    }

    public void rcCurtains_logic() {

        if (rcCurtainsTop != null) {
            rcCurtainsTop.setActivated(true);
            rcCurtainsTop.setPressed(false);
        }

        if (rcCurtainsBase != null) {
            rcCurtainsBase.setActivated(true);
            rcCurtainsBase.setPressed(false);
        }

        Handler tmp = new Handler();
        tmp.postDelayed(new Runnable() {
            public void run() {
                if (rcCurtainsTop != null) {
                    rcCurtainsTop.setActivated(false);
                }

                if (rcCurtainsBase != null) {
                    rcCurtainsBase.setActivated(false);
                }
            }
        }, 700);

    }

    /////////////////////////////////////////////////////////////////////
    public void switchControlLayout(int pos) {

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {

            if (pos == Integer.parseInt(masBase)) {
                curroompage = bedroompage;
            } else if (pos == Integer.parseInt(maslrBase)) {
                curroompage = otherroompage;
            } else if (pos == Integer.parseInt(massrBase)) {
                curroompage = otherroompage;
            } else if (pos == Integer.parseInt(masavBase)) {
                curroompage = otherroompage;
            } else if (pos == Integer.parseInt(massbBase)) {
                curroompage = bedroompage;
            } else if (pos == Integer.parseInt(masdrBase)) {
                curroompage = otherroompage;
            }
        }

        // record the mas set
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        int previouslyStartedmas = prefs.getInt(previouslyStartedmasString, Integer.parseInt(masBase));
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(previouslyStartedmasString, pos);
        edit.commit();

        loadControlPanelLayout();

        //wait for dict update (mainly for first launch)
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadRCLabel();

                if (bottomBarFragment != null && isBottomBarInit) {
                    bottomBarFragment.loadRCLabel();
                }

                if (topBarFragment != null && isTopBarInit) {
                    topBarFragment.loadPageLabel();
                }
            }
        }, getIntent().getBooleanExtra("launch", false) ? 2000 : 0);

    }

    public void rcFan_logic() {

        try {
            if ((settings.getBoolean("bacnetAC", false))
                    || MainApplication.getCurrentActivity().getString(R.string.hotelacshow).equalsIgnoreCase("bacnetac")) {

                if (int_rcFan == 0) {
                    rcFanLabel.setTextColor(getResources().getColor(R.color.white));
                    rcFanTop.setActivated(false);
                } else {
                    rcFanLabel.setTextColor(getResources().getColor(R.color.white));
                    rcFanTop.setActivated(true);
                }

            } else {

                if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                    fanspeedBase = 2100;
                } else {
                    fanspeedBase = 3200;
                }

                if (int_rcFan == 0) {
                    rcFanLabel.setTextColor(getResources().getColor(R.color.white));
                    rcFanTop.setActivated(false);
                    rcFanBase.setActivated(false);
                } else {
                    rcFanLabel.setTextColor(getResources().getColor(R.color.white));
                    rcFanTop.setActivated(true);
                    rcFanBase.setActivated(true);
                }

                rcFanDisplayDrawable = (ClipDrawable) rcFanDisplay.getDrawable();
                rcFanDisplayDrawable.setLevel(fanspeedBase + levelStep * int_rcFan); // ClipDrawable
                // 0:
                // blank,
                // 10000:
                // whole
            }
            rcBedroomTempCF_logic();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void rcAlarmGUI_logic() {

        if (!isAlarmSet) {
            rcSetAlarm.setActivated(false);

            if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                    GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                rcAlarm.setActivated(false); // bottom bar button
                alarmPopupAlert.setVisibility(View.GONE);
                isAlarmPopup = false;
            }

        } else {
            rcSetAlarm.setActivated(true);
            if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                    GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
                rcAlarm.setActivated(true);
            }
        }
    }

    ////////////////////////////////////////////////////////


    @Override
    protected void onRoomControlChanged(String temperature, String temperatureCF, String fan, boolean isNightLightOn, String roomLight, boolean isMasterLightOn) {

        if (countDownTimer != null) {
            resetLayoutUpdateCounter();
        } else {

            if ((MainApplication.getMAS().getCurRoom() == Integer.parseInt(masBase)) || (MainApplication.getMAS().getCurRoom() == Integer.parseInt(massbBase))) {
                this.isNightLightOn = isNightLightOn;
                rcNightLight_logic();
            }

            if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                    GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                    GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                try {
                    bedroomLightLevel = Integer.parseInt(roomLight);
                    rcBedroomLights_logic();
                } catch (Exception ex) {
                    bedroomLightLevel = 0;
                    rcBedroomLights_logic();
                }
            }

            try {
                ff_rcBedroomTempOrg = Float.parseFloat(temperature);
            } catch (Exception ex) {
                ff_rcBedroomTempOrg = 0f;
            }

            // just use to calc
            // pch is always use c value

            ff_rcBedroomTemp = ff_rcBedroomTempOrg;


            if (temperatureCF.equalsIgnoreCase("C")) {
                isTempInCelsius = true;
            } else {
                isTempInCelsius = false;

                if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                        GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                    ff_rcBedroomTemp = c2f(ff_rcBedroomTempOrg);

                }
            }
            rcBedroomTempCF_logic();


            if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                try {
                    int_rcFan = Integer.parseInt(fan);
                } catch (Exception ex) {
                    int_rcFan = 0;
                }
            } else {
                if (fan.equalsIgnoreCase("HIGH")) {
                    int_rcFan = 3;
                } else if (fan.equalsIgnoreCase("MID")) {
                    int_rcFan = 2;
                } else if (fan.equalsIgnoreCase("LOW")) {
                    int_rcFan = 1;
                } else if (fan.equalsIgnoreCase("OFF")) {
                    int_rcFan = 0;
                } else {
                    int_rcFan = 0;
                }
            }
            rcFan_logic();
        }
    }

    @Override
    protected void onAlarmChanged(boolean isAlarmOn, String hour, String minute, String snooze, String time, String settime) {

        isAlarmSet = isAlarmOn;

        int_rcSetHour = int_rcSetHourMAS;
        int_rcSetMinute = int_rcSetMinuteMAS;

        try {
            int_rcSetHourMAS = Integer.parseInt(hour);
            int_rcSetMinuteMAS = Integer.parseInt(minute);
            int_rcSnooze = Integer.parseInt(snooze);
        } catch (Exception ex) {
            int_rcSetHourMAS = 12;
            int_rcSetMinuteMAS = 0;
            int_rcSnooze = 5;
        }

        if (int_rcSetHourMAS == 0) {
            int_rcSetHour = 12;
            apmList.setValue(0);
        } else if (int_rcSetHourMAS < 12) {
            apmList.setValue(0);
        } else if (int_rcSetHourMAS == 12) {
            apmList.setValue(1);
        } else {
            int_rcSetHour = int_rcSetHourMAS - 12;
            apmList.setValue(1);
        }

        if (!isAlarmPopup) {
            hrList.setValue(int_rcSetHour);
            minList.setValue(int_rcSetMinute);
            rcAlarmGUI_logic();
        }
    }

    @Override
    protected void onLanguageChanged(String language) {
        if (countDownTimer != null) {
            resetLayoutUpdateCounter();
        } else {
            if (checkCurLang != language) {
                checkCurLang = language;
                if (bottomBarFragment != null && isBottomBarInit) {
                    bottomBarFragment.setRefreshFootRoomArea(true);
                }
            }
        }
        loadRCLabel();
    }

    @Override
    protected void onRoomChanged(String roomNumber, String roomType) {
        if (bottomBarFragment != null && isBottomBarInit) {
            bottomBarFragment.setSuite(Integer.parseInt(roomType + "") - 1);
        }
    }


    //////////////////////////////////////////////////////////

    public void alarmSetting() {

        //// PCH alarm
        rcAlarm = (LinearLayout) findViewById(R.id.rcAlarmBottomBarButton);
        rcAlarm.setClickable(true);

        rcSetAlarm.setClickable(true);
        rcSetAlarm.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                rcSetAlarm.setActivated(true);
                MainApplication.playClickSound(rcSetAlarm);
                rcSetAlarm.setActivated(false);

                if (isAlarmSet) {
                    isAlarmSet = false;
                } else {
                    isAlarmSet = true;
                }

                // for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                map.put("TurnOn", (isAlarmSet ? "1" : "0"));
                FlurryAgent.logEvent("EnableAlarm", map);

                if (!isAlarmSet) {
                    showMessage(MainApplication.getLabel("alarm.thankyou.cancel"));
                    disableAlarm();
                    Command.Alarm.AlarmOff();

                } else {
                    if (apmList.getValue() == 0) {
                        if (hrList.getValue() == 12) {
                            int_rcSetHour = 0;
                        } else {
                            int_rcSetHour = hrList.getValue();
                        }
                    } else {
                        if (hrList.getValue() == 12) {
                            int_rcSetHour = 12;
                        } else {
                            int_rcSetHour = hrList.getValue() + 12;
                        }
                    }
                    int_rcSetHourMAS = int_rcSetHour;
                    int_rcSetMinute = minList.getValue();
                    int_rcSetMinuteMAS = int_rcSetMinute;

                    showMessage(MainApplication.getLabel("alarm.thankyou.msg"));
                    Command.Alarm.AlarmOn(MainApplication.twoDigitMe(int_rcSetHour) + ":" + MainApplication.twoDigitMe(int_rcSetMinute));
                }
                rcAlarmGUI_logic();

            }
        });

        /// for the big button press

        rcBigOff.setClickable(true);
        rcBigOff.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    rcBigOff.setActivated(true);
                    MainApplication.playClickSound(rcBigOff);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    rcBigOff.setActivated(false);

                    isAlarmSet = false;
                    Command.Alarm.AlarmOff();
                    rcAlarmGUI_logic();
                    // resume the rc
                    disableAlarm();
                }

                return true;
            }
        });

        rcBigSnooze.setClickable(true);
        rcBigSnooze.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    rcBigSnooze.setActivated(true);
                    rcBigSnooze.setPressed(true);
                    MainApplication.playClickSound(rcBigSnooze);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    rcBigSnooze.setActivated(false);
                    rcBigSnooze.setPressed(false);

                    // for Flurry log
                    final Map<String, String> map = new HashMap<String, String>();
                    map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                    FlurryAgent.logEvent("Snooze", map);

                    String[] snoozeParts = MainApplication.getMAS().getData("data_alarm_settime").split(":");
                    int snoozeHour = Integer.parseInt(snoozeParts[0]);
                    int snoozeMinute = Integer.parseInt(snoozeParts[1]) + Integer.parseInt(MainApplication.getMAS().getData("data_alarm_snooze"));

                    if (snoozeMinute > 59) {
                        snoozeMinute = snoozeMinute - 60;
                        snoozeHour = snoozeHour + 1;
                        if (snoozeHour > 23) {
                            snoozeHour = 0;
                        }
                    }

                    Command.Alarm.AlarmSnooze(MainApplication.twoDigitMe(snoozeHour) + ":" + MainApplication.twoDigitMe(snoozeMinute));
                    // resume the rc
                    disableAlarm();
                }
                return true;
            }
        });
    }

    // for pch alarm msgbox use
    private void showMessage(String msg) {
        if (errorMessageDialogLayout != null && errorMessageDialogLabel != null) {
            errorMessageDialogLayout.setVisibility(View.VISIBLE);
            errorMessageDialogLabel.setText(msg);
        }
    }

    private void offMessage() {
        if (errorMessageDialogLayout != null) {
            errorMessageDialogLayout.setVisibility(View.GONE);
        }
    }

    /////////////////////////////////////////////////////////////////

    // PCH version alarm

    private void prepareAlarmSelectionList() {

        hrList = (MyNumberPicker) this.findViewById(R.id.hrList);

        hrList.setMinValue(1);
        hrList.setMaxValue(12);
        hrList.setWrapSelectorWheel(false);
        hrList.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        hrList.setScaleX(2f);
        hrList.setScaleY(2f);
        hrList.setFormatter(new TwoDigitFormatter());
        hrList.setShowDividers(0);

        hrList.setOnValueChangedListener(new OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                if (apmList.getValue() == 0) {
                    if (newVal == 12) {
                        int_rcSetHour = 0;
                    } else {
                        int_rcSetHour = newVal;
                    }
                } else {
                    if (newVal == 12) {
                        int_rcSetHour = 12;
                    } else {
                        int_rcSetHour = newVal + 12;
                    }
                }

                int_rcSetHourMAS = int_rcSetHour;

                // int_rcSetAlarm = 1;
                int_rcSetMinute = minList.getValue();
                int_rcSetMinuteMAS = int_rcSetMinute;

                String myAlarm = MainApplication.twoDigitMe(int_rcSetHour) + ":"
                        + MainApplication.twoDigitMe(int_rcSetMinute);

                if (isAlarmSet) {
                    Command.Alarm.AlarmOn(myAlarm);
                }
                rcAlarmGUI_logic();
            }
        });

        minList = (MyNumberPicker) this.findViewById(R.id.minList);
        minList.setMinValue(0);
        minList.setMaxValue(59);
        minList.setWrapSelectorWheel(false);

        minList.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        minList.setScaleX(2f);
        minList.setScaleY(2f);
        minList.setFormatter(new TwoDigitFormatter());

        minList.setShowDividers(0);

        minList.setOnValueChangedListener(new OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                try {

                    if (apmList.getValue() == 0) {
                        if (hrList.getValue() == 12) {
                            int_rcSetHour = 0;
                        } else {
                            int_rcSetHour = hrList.getValue();
                        }
                    } else {
                        if (hrList.getValue() == 12) {
                            int_rcSetHour = 12;
                        } else {
                            int_rcSetHour = hrList.getValue() + 12;
                        }
                    }

                    int_rcSetHourMAS = int_rcSetHour;

                    // int_rcSetAlarm = 1;
                    int_rcSetMinute = newVal;
                    int_rcSetMinuteMAS = int_rcSetMinute;

                    String myAlarm = MainApplication.twoDigitMe(int_rcSetHour) + ":"
                            + MainApplication.twoDigitMe(int_rcSetMinute);

                    if (isAlarmSet) {
                        Command.Alarm.AlarmOn(myAlarm);
                    }
                    rcAlarmGUI_logic();

                } catch (Exception e) {
                }
            }
        });

        apmList = (MyNumberPicker) this.findViewById(R.id.apmList);
        apmList.setMinValue(0);
        apmList.setMaxValue(1);
        apmList.setDisplayedValues(new String[]{"AM", "PM"});

        apmList.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        apmList.setScaleX(2f);
        apmList.setScaleY(2f);

        apmList.setOnValueChangedListener(new OnValueChangeListener() {

            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

                if (newVal == 0) {
                    if (hrList.getValue() == 12) {
                        int_rcSetHour = 0;
                    } else {
                        int_rcSetHour = hrList.getValue();
                    }
                } else {
                    if (hrList.getValue() == 12) {
                        int_rcSetHour = 12;
                    } else {
                        int_rcSetHour = hrList.getValue() + 12;
                    }
                }

                int_rcSetHourMAS = int_rcSetHour;

                // int_rcSetAlarm = 1;
                int_rcSetMinute = minList.getValue();
                int_rcSetMinuteMAS = int_rcSetMinute;

                String myAlarm = MainApplication.twoDigitMe(int_rcSetHour) + ":"
                        + MainApplication.twoDigitMe(int_rcSetMinute);

                if (isAlarmSet) {
                    Command.Alarm.AlarmOn(myAlarm);
                }
                rcAlarmGUI_logic();
            }
        });

        if (int_rcSetHourMAS == 0) {
            int_rcSetHour = 12;
            apmList.setValue(0);

        } else if (int_rcSetHourMAS < 12) {
            apmList.setValue(0);
        } else if (int_rcSetHourMAS == 12) {
            apmList.setValue(1);
        } else {
            int_rcSetHour = int_rcSetHourMAS - 12;
            apmList.setValue(1);
        }

        hrList.setValue(int_rcSetHour);
        minList.setValue(int_rcSetMinute);

    }

    /////////////////////////////////////////////////////////////////


    public void showFaxBigEnvelopePopup() {

        envelopeImage = (ImageView) findViewById(R.id.ivEnvelope);
        messageText = (TextView) findViewById(R.id.tvMessage);
        rlBigEnvelope = (RelativeLayout) findViewById(R.id.rlBigEnvelope);

        messageText.setText(MainApplication.getLabel("alert.fax.big.red.envelope.message"));

        Animation splashAnimation = AnimationUtils.loadAnimation(this, R.anim.splash);

        if (settings.getBoolean("pending_fax", false) && !isShowingBigEnvolop) {
            isShowingBigEnvolop = true;

            if (btnTouchWake == null) {
                btnTouchWake = (Button) findViewById(R.id.btnTouchWake);
            }
            btnTouchWake.setAlpha((float) 0.9);

//            if (MainApplication.getScreenSta() == 0) {
//                Window mWindow = MainApplication.getCurrentActivity().getWindow();
//                WindowManager.LayoutParams lp = mWindow.getAttributes();
//                lp.screenBrightness = (float) myBrightness;
//                mWindow.setAttributes(lp);
//            }

            rlBigEnvelope.setVisibility(View.VISIBLE);
            envelopeImage.startAnimation(splashAnimation);

            rlBigEnvelope.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    isShowingBigEnvolop = false;
                    btnTouchWake.setAlpha(1);

                    try {

                        String dateString = MainApplication.getMAS().getData("year") + "-"
                                + MainApplication.getMAS().getData("month") + "-"
                                + MainApplication.getMAS().getData("day") + " "
                                + MainApplication.getMAS().getData("hour") + ":"
                                + MainApplication.getMAS().getData("minute");


                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        Date date = null;
                        try {
                            date = format.parse(dateString);
                            System.out.println(date);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        if (date != null) {
                            settings.edit().putLong("lastMessageVisitTime", date.getTime()).commit();
                        }

                        Intent intent = new Intent(MainApplication.getContext(), MessageActivity.class);
                        MainApplication.getCurrentActivity().startActivity(intent);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    rlBigEnvelope.setVisibility(View.GONE);
                    envelopeImage.clearAnimation();
                }

            });

        } else if (!isShowingBigEnvolop) {
            rlBigEnvelope.setVisibility(View.GONE);
            envelopeImage.clearAnimation();
        }

    }

    public void hideFaxBigEnvelopePopup() {
        if (isShowingBigEnvolop) {
            envelopeImage = (ImageView) findViewById(R.id.ivEnvelope);
            rlBigEnvelope = (RelativeLayout) findViewById(R.id.rlBigEnvelope);

            rlBigEnvelope.setVisibility(View.GONE);
            envelopeImage.clearAnimation();

            if (btnTouchWake == null) {
                btnTouchWake = (Button) findViewById(R.id.btnTouchWake);
            }
            btnTouchWake.setAlpha(1);

            isShowingBigEnvolop = false;
        }
    }


    public void enableAlarm() {

        wakeBSP();

        if (topBarFragment != null && isTopBarInit) {
            topBarFragment.stopMainTimer();
        }

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {

            isAlarmRinging = true;
            killAllLayout();

            if (alarmPopup != null) {
                alarmPopup.setVisibility(View.GONE);
            }

            alarmPopupAlert.setVisibility(View.VISIBLE);
            alarmPopupAlert.bringToFront();
            rlBigEnvelope.setVisibility(View.GONE);

            AudioManager audioManager = (AudioManager) MainApplication.getCurrentActivity().getSystemService(MainApplication.getContext().AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 20, 0);

            if (myAlarmPlayer != null && myAlarmPlayer.isPlaying()) {
                myAlarmPlayer.stop();
            }

            myAlarmPlayer = MediaPlayer.create(MainApplication.getContext(), MainApplication.getCurrentActivity().getResources().getIdentifier("raw/alarm_sound", "raw", MainApplication.getCurrentActivity().getPackageName()));
            myAlarmPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer arg0) {
                    myAlarmPlayer.start();
                }
            });
            myAlarmPlayer.start();
        }
    }

    public void disableAlarm() {
        // after alarm wake to snooze

        if (topBarFragment != null && isTopBarInit) {
            topBarFragment.resetBackMainTimer();
        }

        if (myAlarmPlayer != null && myAlarmPlayer.isPlaying()) {
            myAlarmPlayer.stop();
        }

        if (isAlarmRinging) {
            isAlarmRinging = false;
            isAlarmPopup = false;
            loadControlPanelLayout();
        }
    }

    public void onAlarmBottomBarButtonClicked() {

        isAlarmPopup = isAlarmPopup ? false : true;

        if (isAlarmPopup) {
            killAllLayout();
            offMessage();
            alarmPopupAlert.setVisibility(View.GONE);
            alarmPopup.setVisibility(View.VISIBLE);
            loadAlarmPopupLayout();

        } else {
            loadControlPanelLayout();
            alarmPopup.setVisibility(View.GONE);
        }
        restartTimeoutCount();
    }

    @Override
    public void showLoading() {
        super.showLoading();
        if (topBarFragment != null && isTopBarInit) {
            topBarFragment.stopMainTimer();
        }
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        if (topBarFragment != null && isTopBarInit) {
            topBarFragment.resetBackMainTimer();
            topBarFragment.loadPageLabel();
        }
        if (bottomBarFragment != null && isBottomBarInit) {
            bottomBarFragment.loadRCLabel();
        }
        loadRCLabel();
    }

    private void loadAlarmPopupLayout() {
        if (isAlarmSet) {
            rcSetAlarm.setActivated(true);
        } else {
            rcSetAlarm.setActivated(false);
        }
    }


    private void resetLayoutUpdateCounter() {
        resetLayoutUpdateCounter(pendingUpdateLayoutInterval);
    }

    private void resetLayoutUpdateCounter(int interval) {

        if (countDownTimer == null) {
            countDownTimer = new CountDownTimer(interval, 100) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    countDownTimer = null;
                    try {
                        if ((MainApplication.getMAS().getCurRoom() == Integer.parseInt(masBase))
                                || (MainApplication.getMAS().getCurRoom() == Integer.parseInt(massbBase))) {
                            isNightLightOn = MainApplication.getMAS().getData("data_nightlight").equalsIgnoreCase("ON");
                            rcNightLight_logic();
                        }
                    } catch (Exception ex) {
                        isNightLightOn = false;
                        rcNightLight_logic();
                    }

                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                            GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                            GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                            try {
                            bedroomLightLevel = Integer.parseInt(MainApplication.getMAS().getData("data_roomlight"));
                            rcBedroomLights_logic();
                        } catch (Exception ex) {
                            bedroomLightLevel = 0;
                            rcBedroomLights_logic();
                        }
                    }

                    try {
                        ff_rcBedroomTempOrg = Float.parseFloat(MainApplication.getMAS().getData("data_temperature"));
                    } catch (Exception ex) {
                        ff_rcBedroomTempOrg = 0f;
                    }
                    ff_rcBedroomTemp = ff_rcBedroomTempOrg;

                    if (MainApplication.getMAS().getData("data_temperature_cf").equalsIgnoreCase("C")) {
                        isTempInCelsius = true;
                    } else {
                        isTempInCelsius = false;

                        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                            ff_rcBedroomTemp = c2f(ff_rcBedroomTempOrg);
                        }
                    }
                    rcBedroomTempCF_logic();

                    if (RoomControlActivity.this.getString(R.string.dotfan).equalsIgnoreCase("5")) {
                        try {
                            int_rcFan = Integer.parseInt(MainApplication.getMAS().getData("data_fan"));
                        } catch (Exception ex) {
                            int_rcFan = 0;
                        }
                    } else {

                        if (MainApplication.getMAS().getData("data_fan").equalsIgnoreCase("HIGH")) {
                            int_rcFan = 3;
                        } else if (MainApplication.getMAS().getData("data_fan").equalsIgnoreCase("MID")) {
                            int_rcFan = 2;
                        } else if (MainApplication.getMAS().getData("data_fan").equalsIgnoreCase("LOW")) {
                            int_rcFan = 1;
                        } else if (MainApplication.getMAS().getData("data_fan").equalsIgnoreCase("OFF")) {
                            int_rcFan = 0;
                        } else {
                            int_rcFan = 0;
                        }
                    }
                    rcFan_logic();


                    isAlarmSet = (MainApplication.getMAS().getData("data_alarm")).equals("ON") ? true : false;
                    int_rcSetHour = int_rcSetHourMAS;
                    int_rcSetMinute = int_rcSetMinuteMAS;
                    try {
                        int_rcSetHourMAS = Integer.parseInt(MainApplication.getMAS().getData("data_alarm_hr"));
                        int_rcSetMinuteMAS = Integer.parseInt(MainApplication.getMAS().getData("data_alarm_min"));
                        int_rcSnooze = Integer.parseInt(MainApplication.getMAS().getData("data_alarm_snooze"));
                    } catch (Exception ex) {
                        int_rcSetHourMAS = 12;
                        int_rcSetMinuteMAS = 0;
                        int_rcSnooze = 5;
                    }

                    if (int_rcSetHourMAS == 0) {
                        int_rcSetHour = 12;
                        apmList.setValue(0);
                    } else if (int_rcSetHourMAS < 12) {
                        apmList.setValue(0);
                    } else if (int_rcSetHourMAS == 12) {
                        apmList.setValue(1);
                    } else {
                        int_rcSetHour = int_rcSetHourMAS - 12;
                        apmList.setValue(1);
                    }
                    hrList.setValue(int_rcSetHour);
                    minList.setValue(int_rcSetMinute);
                    rcAlarmGUI_logic();
                }
            };
            countDownTimer.start();
        } else {
            countDownTimer.cancel();
            countDownTimer = null;
            resetLayoutUpdateCounter();
        }
    }

}
