package com.hsh.esdbsp.activity;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.widget.Command;

public class DVDActivity extends BaseActivity {

    String myCMDBaseDVD = "SET%20DVD%20";
    String myCMDBaseCard = "SET%20DVD%20";

    // ///////////////////////
    // 4 main
    Button dvd_bluray;
    Button dvd_smartdock;
    Button dvd_externalinput;
    Button dvd_cardreader;

    // ///////////////////////
    // --- bluray ----

    LinearLayout dvd_include_bluray;

    Button dvd_open;
    Button dvd_red;
    Button dvd_green;
    Button dvd_yellow;
    Button dvd_purple;
    Button dvd_topmenu;
    Button dvd_popupmenu;
    Button dvd_return;
    Button dvd_option;
    Button dvd_off;
    Button dvd_arrow_up;
    Button dvd_arrow_left;
    Button dvd_square;
    Button dvd_arrow_right;
    Button dvd_arrow_down;
    Button dvd_subtitle;
    Button dvd_audio;
    Button dvd_display;
    Button dvd_home;
    Button dvd_backward;
    Button dvd_forward;
    Button dvd_play;
    Button dvd_previous;
    Button dvd_next;
    Button dvd_pause;
    Button dvd_stop;
    Button dvd_minus;
    // special mute and dismute
    Button dvd_speaker;
    int dvd_speaker_sta = 0; // 0 is not mute
    Button dvd_plus;

    // ///////////////////////

    // ///////////////////////
    // --- cardreader ----

    LinearLayout dvd_include_cardreader;

    Button card_movie;
    Button card_photo;
    Button card_music;
    Button card_play_pause;
    Button card_stop;
    Button card_rotate;
    Button card_arrow_up;
    Button card_arrow_left;
    Button card_square;
    Button card_arrow_right;
    Button card_arrow_down;
    Button card_minus;
    Button card_speaker;
    // use dvd speaker cmd
    Button card_plus;
    Button card_backward;
    Button card_forward;
    Button card_previous;
    Button card_next;
    Button card_menu;
    Button card_back;

    // ///////////////////////
    // --- external ----

    LinearLayout dvd_include_external;
    Button ext_minus;
    Button ext_speaker;
    // use dvd speaker cmd
    Button ext_plus;

    // ///////////////////////
    // --- smartdock ----

    LinearLayout dvd_include_smartdock;
    Button smart_minus;
    Button smart_speaker;
    // use dvd speaker cmd
    Button smart_plus;

    // ////////////////////////////////////////////////////////


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.dvd_all);


        // for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        FlurryAgent.logEvent("Multimedia", map);


        Bundle bundle = new Bundle();
        // bundle.putString("titleId", "");
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("titleId", "TV/AUDIO");
        bundle.putString("hightLightChoice", "radio");
        bundle.putInt("choice", 4);

        if (MainApplication.getLabel("service.dvdborrowingsystem.enabled").equals("1") && GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
            bundle.putInt("choice", 5);
        }
        initTopBar(bundle);
        initBottomBar();


        loadPageItem();

        // send once to bluray
        Command.DVD.Bluray();
    }

    // ////////////////////////////////////////////////////////////////////////////////

    @Override
    protected void onLanguageChanged(String language) {
        loadPageItem();
    }

    private void hideAll() {

        dvd_include_bluray.setVisibility(View.GONE);
        dvd_include_smartdock.setVisibility(View.GONE);
        dvd_include_external.setVisibility(View.GONE);
        dvd_include_cardreader.setVisibility(View.GONE);

    }

    private void loadPageItem() {


        MainApplication.brightUp();

        dvd_bluray = (Button) findViewById(R.id.dvd_bluray);
        dvd_bluray.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.Bluray();
                    hideAll();
                    dvd_include_bluray.setVisibility(View.VISIBLE);
                }
                return false;
            }

        });

        dvd_smartdock = (Button) findViewById(R.id.dvd_smartdock);
        dvd_smartdock.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.SmartDock();
                    hideAll();
                    dvd_include_smartdock.setVisibility(View.VISIBLE);
                }
                return false;
            }

        });

        dvd_externalinput = (Button) findViewById(R.id.dvd_externalinput);
        dvd_externalinput.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.ExternalInput();
                    hideAll();
                    dvd_include_external.setVisibility(View.VISIBLE);
                }
                return false;
            }

        });

        dvd_cardreader = (Button) findViewById(R.id.dvd_cardreader);
        dvd_cardreader.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardReader();
                    hideAll();
                    dvd_include_cardreader.setVisibility(View.VISIBLE);
                }
                return false;
            }

        });


        dvd_include_bluray = (LinearLayout) findViewById(R.id.dvd_include_bluray);
        dvd_open = (Button) findViewById(R.id.dvd_open);
        dvd_open.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDOpen();
                }
                return false;
            }

        });

        dvd_red = (Button) findViewById(R.id.dvd_red);
        dvd_red.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.RED);
                }
                return false;
            }

        });

        dvd_green = (Button) findViewById(R.id.dvd_green);
        dvd_green.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.GREEN);
                }
                return false;
            }

        });

        dvd_yellow = (Button) findViewById(R.id.dvd_yellow);
        dvd_yellow.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.YELLOW);
                }
                return false;
            }

        });

        dvd_purple = (Button) findViewById(R.id.dvd_purple);
        dvd_purple.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.PURPLE);
                }
                return false;
            }

        });

        dvd_topmenu = (Button) findViewById(R.id.dvd_topmenu);
        dvd_topmenu.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.TopMenu();
                }
                return false;
            }

        });

        dvd_popupmenu = (Button) findViewById(R.id.dvd_popupmenu);
        dvd_popupmenu.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.PopupMenu();
                }
                return false;
            }

        });

        dvd_return = (Button) findViewById(R.id.dvd_return);
        dvd_return.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.BACK);
                }
                return false;
            }

        });

        dvd_option = (Button) findViewById(R.id.dvd_option);
        dvd_option.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.Option();
                }
                return false;
            }

        });

        dvd_off = (Button) findViewById(R.id.dvd_off);
        dvd_off.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.OFF);
                }
                return false;
            }

        });

        dvd_arrow_up = (Button) findViewById(R.id.dvd_arrow_up);
        dvd_arrow_up.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.UP);
                }
                return false;
            }

        });

        dvd_arrow_left = (Button) findViewById(R.id.dvd_arrow_left);
        dvd_arrow_left.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.LEFT);
                }
                return false;
            }

        });

        dvd_square = (Button) findViewById(R.id.dvd_square);
        dvd_square.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.SQUARE);
                }
                return false;
            }

        });

        dvd_arrow_right = (Button) findViewById(R.id.dvd_arrow_right);
        dvd_arrow_right.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.RIGHT);
                }
                return false;
            }

        });

        dvd_arrow_down = (Button) findViewById(R.id.dvd_arrow_down);
        dvd_arrow_down.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.DOWN);
                }
                return false;
            }

        });

        dvd_subtitle = (Button) findViewById(R.id.dvd_subtitle);
        dvd_subtitle.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.CC);
                }
                return false;
            }

        });

        dvd_audio = (Button) findViewById(R.id.dvd_audio);
        dvd_audio.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.Audio();
                }
                return false;
            }

        });

        dvd_display = (Button) findViewById(R.id.dvd_display);
        dvd_display.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.Display();
                }
                return false;
            }

        });

        dvd_home = (Button) findViewById(R.id.dvd_home);
        dvd_home.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.Home();
                }
                return false;
            }

        });

        dvd_backward = (Button) findViewById(R.id.dvd_backward);
        dvd_backward.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.REWIND10);
                }
                return false;
            }

        });

        dvd_forward = (Button) findViewById(R.id.dvd_forward);
        dvd_forward.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.FASTFORWARD10);
                }
                return false;
            }

        });

        dvd_play = (Button) findViewById(R.id.dvd_play);
        dvd_play.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.PLAY);
                }
                return false;
            }

        });

        dvd_previous = (Button) findViewById(R.id.dvd_previous);
        dvd_previous.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.LAST);
                }
                return false;
            }

        });

        dvd_next = (Button) findViewById(R.id.dvd_next);
        dvd_next.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.NEXT);
                }
                return false;
            }

        });

        dvd_pause = (Button) findViewById(R.id.dvd_pause);
        dvd_pause.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.PAUSE);
                }
                return false;
            }

        });

        dvd_stop = (Button) findViewById(R.id.dvd_stop);
        dvd_stop.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.STOP);
                }
                return false;
            }

        });

        dvd_minus = (Button) findViewById(R.id.dvd_minus);
        dvd_minus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.VOLUMNDOWN);
                }
                return false;
            }

        });

        dvd_speaker = (Button) findViewById(R.id.dvd_speaker);
        dvd_speaker.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    dvd_speaker_sta = (dvd_speaker_sta + 1) % 2;
                    if (dvd_speaker_sta == 1) {
                        Command.DVD.DVDRemoteButton(Command.RemoteButton.MUTE);
                    } else {
                        Command.DVD.DVDRemoteButton(Command.RemoteButton.UNMUTE);
                    }
                }
                return false;
            }

        });

        dvd_plus = (Button) findViewById(R.id.dvd_plus);
        dvd_plus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.DVDRemoteButton(Command.RemoteButton.VOLUMNUP);
                }
                return false;
            }

        });


        // ///////////////////////////////////
        // card


        dvd_include_cardreader = (LinearLayout) findViewById(R.id.dvd_include_cardreader);

        card_movie = (Button) findViewById(R.id.card_movie);
        card_movie.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardMovie();
                }
                return false;
            }

        });

        card_photo = (Button) findViewById(R.id.card_photo);
        card_photo.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardPhoto();
                }
                return false;
            }

        });

        card_music = (Button) findViewById(R.id.card_music);
        card_music.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardMusic();
                }
                return false;
            }

        });

        card_play_pause = (Button) findViewById(R.id.card_play_pause);
        card_play_pause.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.PLAYPAUSE);
                }
                return false;
            }

        });

        card_stop = (Button) findViewById(R.id.card_stop);
        card_stop.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.STOP);
                }
                return false;
            }

        });

        card_rotate = (Button) findViewById(R.id.card_rotate);
        card_rotate.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRotate();
                }
                return false;
            }

        });

        card_arrow_up = (Button) findViewById(R.id.card_arrow_up);
        card_arrow_up.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.UP);
                }
                return false;
            }

        });

        card_arrow_left = (Button) findViewById(R.id.card_arrow_left);
        card_arrow_left.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.LEFT);
                }
                return false;
            }

        });

        card_square = (Button) findViewById(R.id.card_square);
        card_square.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.SQUARE);
                }
                return false;
            }

        });

        card_arrow_right = (Button) findViewById(R.id.card_arrow_right);
        card_arrow_right.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.RIGHT);
                }
                return false;
            }

        });

        card_arrow_down = (Button) findViewById(R.id.card_arrow_down);
        card_arrow_down.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.DOWN);
                }
                return false;
            }

        });

        card_minus = (Button) findViewById(R.id.card_minus);
        card_minus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.VOLUMNDOWN);
                }
                return false;
            }

        });

        card_speaker = (Button) findViewById(R.id.card_speaker);
        card_speaker.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    dvd_speaker_sta = (dvd_speaker_sta + 1) % 2;
                    if (dvd_speaker_sta == 1) {
                        Command.DVD.CardRemoteButton(Command.RemoteButton.MUTE);
                    } else {
                        Command.DVD.CardRemoteButton(Command.RemoteButton.UNMUTE);
                    }
                }
                return false;
            }

        });

        card_plus = (Button) findViewById(R.id.card_plus);
        card_plus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.VOLUMNUP);
                }
                return false;
            }

        });

        card_backward = (Button) findViewById(R.id.card_backward);
        card_backward.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.REWIND10);
                }
                return false;
            }

        });

        card_forward = (Button) findViewById(R.id.card_forward);
        card_forward.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.FASTFORWARD10);
                }
                return false;
            }

        });

        card_previous = (Button) findViewById(R.id.card_previous);
        card_previous.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.LAST);
                }
                return false;
            }

        });

        card_next = (Button) findViewById(R.id.card_next);
        card_next.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.NEXT);
                }
                return false;
            }

        });

        card_menu = (Button) findViewById(R.id.card_menu);
        card_menu.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.MENU);
                }
                return false;
            }

        });

        card_back = (Button) findViewById(R.id.card_back);
        card_back.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.CardRemoteButton(Command.RemoteButton.BACK);
                }
                return false;
            }

        });


        // /////////////////////////////////////////////////////////////////////


        dvd_include_external = (LinearLayout) findViewById(R.id.dvd_include_externalinput);

        ext_minus = (Button) findViewById(R.id.ext_minus);
        ext_minus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.ExteralRemoteButton(Command.RemoteButton.VOLUMNDOWN);
                }
                return false;
            }

        });

        ext_speaker = (Button) findViewById(R.id.ext_speaker);
        ext_speaker.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    dvd_speaker_sta = (dvd_speaker_sta + 1) % 2;
                    if (dvd_speaker_sta == 1) {
                        Command.DVD.ExteralRemoteButton(Command.RemoteButton.MUTE);
                    } else {
                        Command.DVD.ExteralRemoteButton(Command.RemoteButton.UNMUTE);
                    }
                }
                return false;
            }

        });

        ext_plus = (Button) findViewById(R.id.ext_plus);
        ext_plus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.ExteralRemoteButton(Command.RemoteButton.VOLUMNUP);
                }
                return false;
            }

        });

        dvd_include_smartdock = (LinearLayout) findViewById(R.id.dvd_include_smartdock);

        smart_minus = (Button) findViewById(R.id.smart_minus);
        smart_minus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.SmartRemoteButton(Command.RemoteButton.VOLUMNDOWN);
                }
                return false;
            }

        });

        smart_speaker = (Button) findViewById(R.id.smart_speaker);
        smart_speaker.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    dvd_speaker_sta = (dvd_speaker_sta + 1) % 2;
                    if (dvd_speaker_sta == 1) {
                        Command.DVD.SmartRemoteButton(Command.RemoteButton.MUTE);
                    } else {
                        Command.DVD.SmartRemoteButton(Command.RemoteButton.UNMUTE);
                    }
                }
                return false;
            }

        });

        smart_plus = (Button) findViewById(R.id.smart_plus);
        smart_plus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {
                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(dvd_bluray);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    Command.DVD.SmartRemoteButton(Command.RemoteButton.VOLUMNUP);
                }
                return false;
            }

        });

        dvd_topmenu.setText(MainApplication.getLabel("TOP MENU"));
        dvd_popupmenu.setText(MainApplication.getLabel("POP UP/MENU"));
        dvd_bluray.setText(MainApplication.getLabel("mm.bluray"));
        dvd_smartdock.setText(MainApplication.getLabel("mm.smartPhoneDock"));
        dvd_externalinput.setText(MainApplication.getLabel("mm.externalInput"));
        dvd_cardreader.setText(MainApplication.getLabel("mm.cardReader"));
        dvd_return.setText(MainApplication.getLabel("RETURN"));
        dvd_option.setText(MainApplication.getLabel("OPTIONS"));
        dvd_off.setText(MainApplication.getLabel("OFF"));
        dvd_subtitle.setText(MainApplication.getLabel("SUBTITLE"));
        dvd_audio.setText(MainApplication.getLabel("AUDIO"));
        dvd_display.setText(MainApplication.getLabel("DISPLAY"));
        dvd_home.setText(MainApplication.getLabel("HOME"));
        card_back.setText(MainApplication.getLabel("mm.back"));
        card_menu.setText(MainApplication.getLabel("mm.menu"));

        Helper.setAppFonts(dvd_topmenu);
        Helper.setAppFonts(dvd_popupmenu);
        Helper.setAppFonts(dvd_bluray);
        Helper.setAppFonts(dvd_smartdock);
        Helper.setAppFonts(dvd_externalinput);
        Helper.setAppFonts(dvd_cardreader);
        Helper.setAppFonts(dvd_return);
        Helper.setAppFonts(dvd_option);
        Helper.setAppFonts(dvd_off);
        Helper.setAppFonts(dvd_subtitle);
        Helper.setAppFonts(dvd_audio);
        Helper.setAppFonts(dvd_display);
        Helper.setAppFonts(dvd_home);
        Helper.setAppFonts(card_menu);
        Helper.setAppFonts(card_back);
    }

}