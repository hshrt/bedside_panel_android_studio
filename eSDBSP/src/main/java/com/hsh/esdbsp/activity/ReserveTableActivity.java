package com.hsh.esdbsp.activity;

import java.net.URI;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.global.Helper;

public class ReserveTableActivity extends BaseActivity {

	private WebView webView;
	private ProgressBar webViewProgressIndicator;
	private View viewRootHTML;
	private String[] whitelistDomain = {"opentable.com"};
	private Button backBtn;
	private Button forwardBtn2;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news);
		
		viewRootHTML = findViewById( R.id.rootContainer);
		
		backBtn = (Button) findViewById(R.id.backBtn);
		backBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(webView.canGoBack()) {
					webView.goBack();
				}
			}
		});

		forwardBtn2 = (Button) findViewById(R.id.forwardBtn2);
		forwardBtn2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(webView.canGoForward()) {
					webView.goForward();
				}
			}
		});

		Helper.showInternetWarningToast();
		
		String titleId = getIntent().getStringExtra("titleId");
		
		webViewProgressIndicator = (ProgressBar) findViewById(R.id.webViewProgressIndicator);

		Bundle bundle = new Bundle();
        bundle.putString("titleId", titleId);
        initTopBar(bundle);
        initBottomBar();

		webViewProgressIndicator.setVisibility(View.GONE);

		webView = (WebView) findViewById(R.id.newsWebView);
		webView.getSettings().setJavaScriptEnabled(true);
		
		webView.getSettings().setRenderPriority(RenderPriority.HIGH);
		webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
		
		webView.setWebViewClient(new WebViewClient() {
			
	        @Override
	        public void onReceivedError(WebView view, int errorCode,
	                String description, String failingUrl) {
	            Log.d(TAG, "webView Error: " + errorCode + ": " + description + " @ " + failingUrl);
	        }
	        
	        @Override
	        public void onPageFinished(WebView view, String url) {
	            super.onPageFinished(view, url);
	            webViewProgressIndicator.setVisibility(View.GONE);
	        }

	        @Override
	        public void onPageStarted(WebView view, String url, Bitmap favicon) {
	            super.onPageStarted(view, url, favicon);
	            webViewProgressIndicator.setVisibility(View.VISIBLE);

	        }
	        	
	        @Override
	        public boolean shouldOverrideUrlLoading(WebView view, String url) {
	        	boolean shouldLoad = false;
	        	// check white list
	        	URI uri = URI.create(url);
	        	if (uri != null) {
	        		for (String domain : whitelistDomain) {
	        			if (uri.getHost().endsWith(domain)) {
	        				shouldLoad = true;
	        				break;
	        			}
	        		}
	        	}
	        	if (shouldLoad) {
	        		view.loadUrl(url);
	        	}
	        	Log.d(TAG, "webView soul " + (shouldLoad ? "ok! " : "ng. ") + url);
	            return true;
	        }
	    });
		
		webView.setWebChromeClient(new WebChromeClient() {
			  public void onConsoleMessage(String message, int lineNumber, String sourceID) {
			    Log.d("MyApplication", message + " -- From line "
			                         + lineNumber + " of "
			                         + sourceID);
			  }
		});

		webView.loadUrl(MainApplication.getApiBase()+"cms/test/opentable.html");
		
		webView.setOnTouchListener(new View.OnTouchListener() {
	        public boolean onTouch(View v, MotionEvent event) {
	            // The code of the hiding goest here, just call hideSoftKeyboard(View v);
	        	//hideSoftKeyboard(v);
	            return false;  
	            }     
	        }); 
	}
	
	public void hideSoftKeyboard(View v) {
        /*Activity activity = (Activity) v.getContext();
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);*/
    }
	
	@Override
	public void onDestroy() {
		Log.i(TAG, "news onDestroy");
		super.onDestroy();
		webView.clearHistory();
		webView.clearCache(true);
		webView.destroy();
		super.onDestroy();
	}
	
}
