package com.hsh.esdbsp.activity;

import java.net.URI;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.global.Helper;

public class NewsActivity extends BaseActivity {

	private WebView webView;
	private ProgressBar webViewProgressIndicator;
	private View viewRootHTML;
	private static int iViewRootHTMLHeightDifferent;
	private String[] whitelistDomain = {
			"pressreader.com",
			"pressdisplay.com",
			"opentable.com",
			"nytimes.com",
			"peninsula.com",
			"nba.com" };

	private LinearLayout mainContainer;
	private RelativeLayout subContainer;
	
	private String url = "http://www.pressreader.com/";

	private Button backBtn;
	private Button forwardBtn2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.news);

		url = getIntent().getStringExtra("url");
		Log.i(TAG, "Url = " + url);

		if (url == null) {
			url = "http://www.pressreader.com/";
		}
		 //url = "http://www.nba.com/";
		backBtn = (Button) findViewById(R.id.backBtn);
		backBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (webView.canGoBack()) {
					webView.goBack();
				}
			}
		});

		forwardBtn2 = (Button) findViewById(R.id.forwardBtn2);
		forwardBtn2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (webView.canGoForward()) {
					webView.goForward();
				}
			}
		});

		LinearLayout webControlContainer = (LinearLayout) findViewById(R.id.webControlContainer);
		webControlContainer.setVisibility(View.GONE);

		if (getIntent().getBooleanExtra("hasWebControl", false)) {
			webControlContainer.setVisibility(View.VISIBLE);
		}
		webControlContainer.bringToFront();
		
		mainContainer = (LinearLayout) findViewById(R.id.mainContainer);
		subContainer = (RelativeLayout) findViewById(R.id.subContainer);
		
		if(url.equalsIgnoreCase("http://www.nytimes.com/")){
			subContainer.removeView(webControlContainer);
			mainContainer.addView(webControlContainer,1);
		}
		

		viewRootHTML = findViewById(R.id.rootContainer);

		Helper.showInternetWarningToast();

		String titleId = getIntent().getStringExtra("titleId");

		webViewProgressIndicator = (ProgressBar) findViewById(R.id.webViewProgressIndicator);

		Bundle bundle = new Bundle();
		bundle.putString("titleId", titleId);
		initTopBar(bundle);
		initBottomBar();

		webViewProgressIndicator.setVisibility(View.GONE);

		webView = (WebView) findViewById(R.id.newsWebView);
		webView.getSettings().setJavaScriptEnabled(true);

		webView.getSettings().setRenderPriority(RenderPriority.NORMAL);
		webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);

		webView.setWebViewClient(new WebViewClient() {

			@Override
			public void onReceivedError(WebView view, int errorCode,
					String description, String failingUrl) {
				Log.d(TAG, "webView Error: " + errorCode + ": " + description
						+ " @ " + failingUrl);
			}

			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				webViewProgressIndicator.setVisibility(View.GONE);
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {

				super.onPageStarted(view, url, favicon);
				webViewProgressIndicator.setVisibility(View.VISIBLE);
				
				if(NewsActivity.this.url.equalsIgnoreCase("http://www.nytimes.com/")){
					webViewProgressIndicator.setVisibility(View.GONE);
				}
			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				boolean shouldLoad = true;
				// check white list
				URI uri = URI.create(url);
				if (uri != null) {
					for (String domain : whitelistDomain) {
						if (uri.getHost().endsWith(domain)) {
							shouldLoad = true;
							break;
						}
					}
				}
				if (shouldLoad) {
					view.loadUrl(url);
				}
				Log.d(TAG, "webView soul " + (shouldLoad ? "ok! " : "ng. ")
						+ url);
				return true;
			}
		});

		webView.setWebChromeClient(new WebChromeClient() {
			public void onConsoleMessage(String message, int lineNumber, String sourceID) {
				Log.d("MyApplication", message + " -- From line " + lineNumber + " of " + sourceID);
			}
		});

		webView.loadUrl(url);

		webView.setOnTouchListener(new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				// The code of the hiding goest here, just call
				// hideSoftKeyboard(View v);
				hideSoftKeyboard(v);
				return false;
			}
		});
	}

	public void hideSoftKeyboard(View v) {
		Activity activity = (Activity) v.getContext();
		InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
	}

	@Override
	public void onDestroy() {
		webView.clearHistory();
		webView.clearCache(true);
		webView.destroy();
		super.onDestroy();

	}

	private void fKeyboardClose() {
		InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(getCurrentFocus()
				.getWindowToken(), 0);
	}

	public OnGlobalLayoutListener onGlobalLayoutListener = new OnGlobalLayoutListener() {
		@Override
		public void onGlobalLayout() {
			Rect rect = new Rect();
			viewRootHTML.getWindowVisibleDisplayFrame(rect);
			iViewRootHTMLHeightDifferent = viewRootHTML.getRootView().getHeight() - (rect.bottom - rect.top);
			if (iViewRootHTMLHeightDifferent > 50) {
				fKeyboardClose();
			}
		}
	};
}
