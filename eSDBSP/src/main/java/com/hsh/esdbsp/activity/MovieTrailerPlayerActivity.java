/*
 * Copyright (C) 2013 Zhang Rui <bbcallen@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hsh.esdbsp.activity;

import com.hsh.esdbsp.R;
import com.hsh.esdbsp.view.MyTextView;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import tv.danmaku.ijk.media.player.IMediaPlayer;
import tv.danmaku.ijk.media.player.IMediaPlayer.OnCompletionListener;
import tv.danmaku.ijk.media.player.IMediaPlayer.OnErrorListener;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;
import tv.danmaku.ijk.media.widget.VideoView;

public class MovieTrailerPlayerActivity extends Activity {
    private static final String TAG = MovieTrailerPlayerActivity.class.getName();
    private VideoView mVideoView;
    private View mBufferingIndicator;
    private Button mBackBtn;
    private MyTextView mTitleTextView;
    private String mVideoPath, mVideoTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trailer_player);

        IjkMediaPlayer.loadLibrariesOnce(null);

        mVideoPath = getIntent().getStringExtra("videoPath");
        mVideoTitle = getIntent().getStringExtra("videoTitle");

        mBackBtn = (Button) findViewById(R.id.backBtn);
        mBackBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mTitleTextView = (MyTextView) findViewById(R.id.videoTitle);
        mTitleTextView.setText(mVideoTitle);

        mBufferingIndicator = findViewById(R.id.buffering_indicator);

        mVideoView = (VideoView) findViewById(R.id.video_view);
        mVideoView.setOnErrorListener(new OnErrorListener() {

            @Override
            public boolean onError(IMediaPlayer mp, int what, int extra) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MovieTrailerPlayerActivity.this);
                builder.setTitle("Sorry")
                        .setMessage("Sorry, this video cannot be played.")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onBackPressed();
                            }

                        })
                        .create()
                        .show();
                return true;
            }

        });

        mVideoView.setOnCompletionListener(new OnCompletionListener() {

            @Override
            public void onCompletion(IMediaPlayer mp) {
                onBackPressed();
            }
        });

        mVideoView.setMediaBufferingIndicator(mBufferingIndicator);
        Log.d(TAG, "Going to play video: " + mVideoPath);
        mVideoView.setVideoPath(mVideoPath);
        mVideoView.requestFocus();
        mVideoView.start();
    }
}
