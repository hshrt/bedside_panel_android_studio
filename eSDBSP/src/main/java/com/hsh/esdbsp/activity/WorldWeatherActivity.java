package com.hsh.esdbsp.activity;

import java.util.ArrayList;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.adapter.WorldWeatherItemAdapter;
import com.hsh.esdbsp.model.WorldWeather;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class WorldWeatherActivity extends BaseActivity {

	private GridView ww_gridView;

	private WorldWeatherItemAdapter mAdapter;

	private ArrayList<WorldWeather> wwArray;

	private RelativeLayout loadingProgressBar;
	private AsyncHttpClient client;

	private Handler refreshHandler = new Handler();
	private int refreshInterval = 10000;
	
	private Runnable refreshRunnable = new Runnable() {

		public void run() {
				callWeatherData();
				refreshHandler.removeCallbacks(refreshRunnable);
				refreshHandler.postDelayed(refreshRunnable, refreshInterval);
		}
	};

	@Override
	protected void onDestroy() {
		Log.i(TAG, "onDestroy fire");
		refreshHandler.removeCallbacks(refreshRunnable);
		super.onDestroy();
		// getSupportFragmentManager().
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.world_weather);

		initUI();

		Bundle bundle = new Bundle();
		bundle.putBoolean("hideBackBtn", false);
		bundle.putString("hightLightChoice", "world weather");
		initTopBar(bundle);
		initBottomBar();

		if (wwArray == null) {
			wwArray = new ArrayList<WorldWeather>();
		}

		if (mAdapter == null) {
			mAdapter = new WorldWeatherItemAdapter(this, wwArray);
		}

		ww_gridView.setAdapter(mAdapter);

		callWeatherData();
		addRunnableCall();

	}

	private void callWeatherData() {
		Log.i(TAG, "CallWeatherData fire");
		
		client = new AsyncHttpClient();
		client.setMaxRetriesAndTimeout(0, 10000);

		String url = MainApplication.getCmsApiBase() + getString(R.string.get_world_weather);

		Log.i(TAG, "URL = " + url);

		client.get(url, new AsyncHttpResponseHandler() {

			@Override
			public void onStart() {
				// called before request is started
			}

			@Override
			public void onSuccess(int arg0, Header[] headers, byte[] response) {
				String s = new String(response);
				Log.i(TAG, "success response = " + s);

				JSONObject restObject;
				try {
					restObject = new JSONObject(s);

					JSONArray jArray = restObject.getJSONArray("data");

					ArrayList<WorldWeather> wwTempArray = new ArrayList<WorldWeather>();
					int length = jArray.length();

					for (int x = 0; x < length; x++) {
						JSONObject wwObj = jArray.getJSONObject(x);
						WorldWeather ww = new WorldWeather("");
						if (wwObj.has("city"))
							ww.setCity(wwObj.getString("city"));
						if (wwObj.has("temp"))
							ww.setTemperature(wwObj.getInt("temp"));
						if (wwObj.has("icon"))
							ww.setIcon(wwObj.getInt("icon"));
						if (wwObj.has("time"))
							ww.setTime(wwObj.getString("time"));
						wwTempArray.add(ww);
					}

					wwArray.clear();
					wwArray.addAll(wwTempArray);

					wwTempArray.clear();

					mAdapter.notifyDataSetChanged();

				} catch (JSONException e) {
					e.printStackTrace();
				}

				hideProgress();

			}

			@Override
			public void onFailure(int arg0, Header[] headers, byte[] response,
					Throwable e) {

				if (response != null) {
					String s = new String(response);
					Log.i(TAG, s);
				}

				Log.i(TAG, "Stop Retry");
				hideProgress();

			}

			@Override
			public void onRetry(int retryNo) {
				// called when request is retried
				Log.i(TAG, "retrying number = " + retryNo);
			}

		});
	}

	private void addRunnableCall(){
		refreshHandler.removeCallbacks(refreshRunnable);
		refreshHandler.postDelayed(refreshRunnable, refreshInterval);
	}

	private void initUI() {
		progressBar = (ProgressBar) findViewById(R.id.progressIndicator);
		ww_gridView = (GridView) findViewById(R.id.ww_gridView);
		loadingProgressBar = (RelativeLayout) findViewById(R.id.loadingProgressBar);
		loadingProgressBar.setVisibility(View.GONE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cms_service_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}