package com.hsh.esdbsp.activity;

import java.net.URL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.comm.MASComm;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.network.XMLCaller;


public abstract class BaseActivity extends FragmentActivity implements XMLCaller.InternetCallbacks, SensorEventListener {

    public String TAG;
    protected String masBase;
    protected String maslrBase = "";
    protected String massrBase = "";
    protected String massbBase = "";
    protected String masavBase = "";
    protected String masdrBase = "";
    protected String masorBase = "";
    protected String previouslyStartedmasString;
    protected ProgressBar progressBar;
    protected ProgressDialog mDialog;
    protected TextView errorText;
    public RelativeLayout errorPopup;

    protected TopBarFragment topBarFragment = null;
    protected BottomBarFragment bottomBarFragment = null;
    public boolean isTopBarInit = false;
    public boolean isBottomBarInit = false;

    private SensorManager sensorMan;
    private Sensor accelerometer;
    private float[] mGravity;
    private float mAccel;
    private float mAccelCurrent;
    private float mAccelLast;

    private AsyncTask<URL, Void, String> currentTask;
    private Context context;
    public SharedPreferences settings;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        TAG = this.getClass().getSimpleName();
        context = this;

        masBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.mas_base_dev) : this.getString(R.string.mas_base);
        previouslyStartedmasString = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.pref_previously_startedmas_dev) : this.getString(R.string.pref_previously_startedmas);

        maslrBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.maslr_base_dev) : this.getString(R.string.maslr_base);
        massrBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.massr_base_dev) : this.getString(R.string.massr_base);
        massbBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.massb_base_dev) : this.getString(R.string.massb_base);
        masavBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.masav_base_dev) : this.getString(R.string.masav_base);
        masdrBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.masdr_base_dev) : this.getString(R.string.masdr_base);
        masorBase = (BuildConfig.BUILD_TYPE.equals("debug")) ? this.getString(R.string.masor_base_dev) : this.getString(R.string.masor_base);

        setGSensor();
        MainApplication.setCurrentActivity(this);

        if (!InitActivity.class.isInstance(this)) {
            if (MainApplication.getContext() == null) {
                settings = this.getAppContext().getSharedPreferences("MyPrefsFile", 0);
            } else {
                settings = MainApplication.getContext().getSharedPreferences("MyPrefsFile", 0);
            }
        }
    }

    public MainApplication getAppContext() {
        return ((MainApplication) getApplicationContext());
    }

    @Override
    public void onUserInteraction() {
        Log.i("BaseActivity", "onUserInteraction fire");
        if (topBarFragment != null)
            topBarFragment.resetBackMainTimer();
    }

    public void hideError() {
        errorPopup.setVisibility(View.GONE);
    }

    public void showError() {
        errorPopup.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void showDialog() {
        Log.i("damn", "mDialog = " + mDialog);
        mDialog.show();
    }

    public void hideDialog() {
        mDialog.hide();
    }

    public ProgressDialog getDialog() {
        return mDialog;
    }

    @Override
    public boolean hasInternet() {
        return Helper.hasInternet(false);
    }

    @Override
    public void onError(int failMode, boolean isPostExecute) {
        if (progressBar != null) {
            hideProgress();
        }
        if (mDialog != null) {
            hideDialog();
        }
        //this is general connection error
        errorText.setText(this.getString(R.string.error_network_api));

        //this is timeout connection error
        if (failMode == XMLCaller.FAIL_MODE_DISPLAY_RETRY_TIMEOUT) {
            errorText.setText(this.getString(R.string.error_network_timeout));
        }

        showError();

        Log.i("BaseActivity", "onerror");
    }


    @Override
    public void postExecute(String xml) {
        if (progressBar != null) {
            hideProgress();
        }
        if (mDialog != null) {
            hideDialog();
        }
    }

    @Override
    public void postExecuteWithCellId(String xml, String cellId) {
        // TODO Auto-generated method stub

    }

    public AsyncTask<URL, Void, String> getCurrentTask() {
        return currentTask;
    }

    public void setCurrentTask(AsyncTask<URL, Void, String> currentTask) {
        this.currentTask = currentTask;
    }

    public void disableBackMainTimer() {
        if (topBarFragment != null) {
            topBarFragment.disableBackMainTimer();
        }
    }

    public void enableBackMainTimer() {
        if (topBarFragment != null) {
            topBarFragment.enableBackMainTimer();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!InitActivity.class.isInstance(this)) {
            addMASListener();
        }
        setGSensor();
        MainApplication.setCurrentActivity(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!InitActivity.class.isInstance(this)) {
            removeMASListener();
        }
        setGSensor();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        topBarFragment = null;
        if (RoomControlActivity.class.isInstance(this)) {
            if(sensorMan != null) {
                sensorMan.unregisterListener(this, accelerometer);
            }
        }
        clearReferences();
    }

    public void goMain() {
        Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        MainApplication.getCurrentActivity().startActivity(intent);
        finish();
    }

    private void clearReferences() {
        Activity currActivity = MainApplication.getCurrentActivity();
        if (currActivity != null && currActivity.equals(this)) {
            MainApplication.setCurrentActivity(null);
        }
    }

    public void initTopBar(){
        initTopBar(new Bundle());
    }

    public void initTopBar(Bundle bundle){

        topBarFragment = new TopBarFragment();
        topBarFragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.topBarFragmentContainer, topBarFragment);
        transaction.commit();

        if(topBarFragment != null && isTopBarInit){
            topBarFragment.loadPageLabel();
        }
    }

    public void initBottomBar(){
        initBottomBar(new Bundle());
    }

    public void initBottomBar(Bundle bundle){

        bottomBarFragment = new BottomBarFragment();
        bottomBarFragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.bottomBarFragmentContainer, bottomBarFragment);
        transaction.commit();

        if(bottomBarFragment != null && isBottomBarInit){
            bottomBarFragment.loadRCLabel();
        }

    }

    public void clearFragment() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();

        if(topBarFragment != null && isTopBarInit) {
            transaction.remove(topBarFragment);
        }

        if(bottomBarFragment != null && isBottomBarInit){
            transaction.remove(bottomBarFragment);
        }
        transaction.commit();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    // General Loading
    /////////////////////////////////////////////////////////////////////////////////////////////////////////


    public void showLoading() {
        android.util.Log.i(this.getClass().getSimpleName(), "showLoading fire");
        RelativeLayout loadingProgressBar = (RelativeLayout) findViewById(R.id.loadingProgressBar);
        loadingProgressBar.setVisibility(View.VISIBLE);

        if (RoomControlActivity.class.isInstance(this)) {
            Button btnTouchWake = (Button) findViewById(R.id.btnTouchWake);
            btnTouchWake.bringToFront();
        }
    }

    public void hideLoading() {
        android.util.Log.i(this.getClass().getSimpleName(), "hideLoading fire");
        RelativeLayout loadingProgressBar = (RelativeLayout) findViewById(R.id.loadingProgressBar);
        loadingProgressBar.setVisibility(View.GONE);

        // for the case that no internet, we have to show "no interest" after we
        // got the dictionary from local file
        Helper.showInternetWarningToast();

        if (RoomControlActivity.class.isInstance(this)) {
            // force reload the MainApplication
            if(MainApplication.getLabel("Services").equalsIgnoreCase("")) {
                android.util.Log.i("RoomControl", "ForceReload start");
                DataCacheManager.getInstance().forceReload();
            }
        }

    }


    //////////////////////////////////////////////////////////////////////////////////////////////////////////
    // MAS
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

    public void addMASListener() {

        MainApplication.getMAS().addMASListener(new MASComm.MASListener() {

            @Override
            public void onAlarmChanged(boolean isAlarmOn, String hour, String minute, String snooze, String time, String settime, String curtime) {
                Log.d("MASListener","onAlarmChanged");
                if(topBarFragment != null && isTopBarInit){
                    topBarFragment.setAlarmSetting(isAlarmOn, hour, minute, snooze, time, settime, curtime);
                }
                ((BaseActivity)context).onAlarmChanged(isAlarmOn, hour, minute, snooze, time, settime);
            }

            @Override
            public void onRoomChanged(String roomNumber, String roomType) {
                Log.d("MASListener","onRoomChanged");
                if(bottomBarFragment != null && isBottomBarInit){
                    bottomBarFragment.setRoomNumber(roomNumber);
                }
                ((BaseActivity)context).onRoomChanged(roomNumber, roomType);
            }

            @Override
            public void onDateTimeChanged(String currentTime, String currentDate) {
                Log.d("MASListener","onDateTimeChanged");
                if(bottomBarFragment != null && isBottomBarInit){
                    bottomBarFragment.setTime(currentTime);
                }

                if(topBarFragment != null && isTopBarInit){
                    topBarFragment.checkAlarm(currentTime);
                }

                ((BaseActivity)context).onDateTimeChanged(currentTime, currentDate);
            }

            @Override
            public void onLanguageChanged(String language) {
                Log.d("MASListener","onLanguageChanged");
                if(bottomBarFragment != null && isBottomBarInit){
                    bottomBarFragment.loadRCLabel();
                }

                if(topBarFragment != null && isTopBarInit){
                    topBarFragment.loadPageLabel();
                }

                ((BaseActivity)context).onLanguageChanged(language);
            }

            @Override
            public void onMURChanged(boolean isMur) {
                Log.d("MASListener","onMURChanged");
                if(bottomBarFragment != null && isBottomBarInit){
                    bottomBarFragment.setMur(isMur);
                }
                ((BaseActivity)context).onMURChanged(isMur);
            }

            @Override
            public void onDNDChanged(boolean isDnd) {
                Log.d("MASListener","onDNDChanged");
                if(bottomBarFragment != null && isBottomBarInit){
                    bottomBarFragment.setDnd(isDnd);
                }
                ((BaseActivity)context).onDNDChanged(isDnd);
            }

            @Override
            public void onValetChanged(boolean isValet) {
                Log.d("MASListener","onValetChanged");
                if(bottomBarFragment != null && isBottomBarInit){
                    bottomBarFragment.setValet(isValet);
                }
                ((BaseActivity)context).onValetChanged(isValet);
            }

            @Override
            public void onRoomControlChanged(String temperature, String temperatureCF, String fan, boolean isNightLightOn, String roomLight, boolean isMasterLightOn) {
                Log.d("MASListener","onRoomControlChanged");
                ((BaseActivity)context).onRoomControlChanged(temperature, temperatureCF, fan, isNightLightOn, roomLight, isMasterLightOn);
            }

            @Override
            public void onFaxChanged(boolean haveFax) {
                Log.d("MASListener","onFaxChanged");
                if(topBarFragment != null && isTopBarInit){
                    topBarFragment.setFax(haveFax);
                }

                ((BaseActivity)context).onFaxChanged(haveFax);
            }

            @Override
            public void onMessageChanged(boolean haveMessage) {
                Log.d("MASListener","onMessageChanged");
                if(topBarFragment != null && isTopBarInit){
                    topBarFragment.setMessage(haveMessage);
                }

                ((BaseActivity)context).onMessageChanged(haveMessage);
            }

            @Override
            public void onMASFailed() {
                Log.d("MASListener","onMASFailed");
                if(bottomBarFragment != null && isBottomBarInit){
                    bottomBarFragment.setTimeBySystem();
                }
            }
        });

    }

    //for child activity to override
    protected void onAlarmChanged(boolean isAlarmOn, String hour, String minute, String snooze, String time, String settime) {

    }

    protected void onRoomChanged(String roomNumber, String roomType) {

    }

    protected void onDateTimeChanged(String currentTime, String currentDate) {

    }

    protected void onLanguageChanged(String language) {

    }

    protected void onMURChanged(boolean isMur) {

    }

    protected void onDNDChanged(boolean isDnd) {

    }

    protected void onValetChanged(boolean isValet) {

    }

    protected void onRoomControlChanged(String temperature, String temperatureCF, String fan, boolean isNightLightOn, String roomLight, boolean isMasterLightOn) {

    }

    protected void onFaxChanged(boolean haveFax) {

    }

    protected void onMessageChanged(boolean haveMessage) {

    }

    private void removeMASListener() {
        MainApplication.getMAS().removeMASListener();
    }

    public void restartTimeoutCount(){
        if(topBarFragment != null && isTopBarInit){
            topBarFragment.addRunnableCall();
        }
    }


    private void setGSensor() {
        // g-sensor
        if (RoomControlActivity.class.isInstance(this)) {
            sensorMan = (SensorManager) this.getSystemService(this.SENSOR_SERVICE);
            accelerometer = sensorMan.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mAccel = 0.00f;
            mAccelCurrent = SensorManager.GRAVITY_EARTH;
            mAccelLast = SensorManager.GRAVITY_EARTH;
            sensorMan.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_UI);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        try {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                mGravity = event.values.clone();
                // Shake detection
                float x = mGravity[0];
                float y = mGravity[1];
                float z = mGravity[2];
                mAccelLast = mAccelCurrent;
                mAccelCurrent = (float) Math.sqrt(x * x + y * y + z * z);
                float delta = mAccelCurrent - mAccelLast;

                mAccel = mAccel * 0.9f + delta;
                // Make this higher or lower according to how much
                // motion you want to detect
                if (mAccel > 1.75) { // default is 3
                    // seems move
                    if (RoomControlActivity.class.isInstance(this)) {
                        ((RoomControlActivity)this).wakeBSP();
                    }
                }
            }

        } catch (Exception e) {
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
