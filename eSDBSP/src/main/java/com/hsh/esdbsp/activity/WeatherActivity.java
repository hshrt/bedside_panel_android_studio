package com.hsh.esdbsp.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.global.Helper;

public class WeatherActivity extends BaseActivity {

    static String day_0_wdate = "";
    static String day_0_low = "";
    static String day_0_high = "";
    static String day_0_cond = "";
    static String day_0_temp = "";
    static String day_0_hum = "";
    static String day_0_winsp = "";
    static String day_0_windir = "";
    static String day_0_uv = "";
    static String day_0_windchill = "";

    static String day_1_wdate = "";
    static String day_1_low = "";
    static String day_1_high = "";
    static String day_1_cond = "";

    static String day_2_wdate = "";
    static String day_2_low = "";
    static String day_2_high = "";
    static String day_2_cond = "";

    static String day_3_wdate = "";
    static String day_3_low = "";
    static String day_3_high = "";
    static String day_3_cond = "";

    // -----------

    TextView wBigTemp;
    ImageView wBigIcon;

    TextView wTempLabel;
    TextView wTemp;

    TextView wHumLabel;
    TextView wHum;

    TextView wWindDirLabel;
    TextView wWindDir;

    TextView wUVLabel;
    TextView wUV;

    TextView wF1Temp;
    ImageView wF1Icon;
    TextView wF1Day;

    TextView wF2Temp;
    ImageView wF2Icon;
    TextView wF2Day;

    TextView wF3Temp;
    ImageView wF3Icon;
    TextView wF3Day;

    LinearLayout layoutPage;

    static Handler guiHandler = new Handler();
    static int guiDelay = 500;

    @Override
    protected void onResume() {
        addRunnableCall();
        super.onResume();
    }

    @Override
    protected void onPause() {
        removeRunnableCall();
        super.onPause();
    }

    // ////////////////////////////////////////////////////////

    private void addRunnableCall() {
        guiHandler.removeCallbacks(guiRunnable);
        guiHandler.postDelayed(guiRunnable, guiDelay);
    }

    private void removeRunnableCall() {
        guiHandler.removeCallbacks(guiRunnable);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.weather_all);

        // for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        FlurryAgent.logEvent("Weather", map);

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("hightLightChoice", "weather");
        initTopBar(bundle);
        initBottomBar();

        Helper.showInternetWarningToast();

        loadPageItem();

        addRunnableCall();

        // loadPageLabel();

    }


    private void loadPageItem() {


        MainApplication.brightUp();

        layoutPage = (LinearLayout) findViewById(R.id.layoutPage);
        layoutPage.setVisibility(View.INVISIBLE); // hide first

        wBigTemp = (MyTextView) findViewById(R.id.wBigTemp);
        wBigIcon = (ImageView) findViewById(R.id.wBigIcon);

        wTempLabel = (MyTextView) findViewById(R.id.wTempLabel);
        wTempLabel.setText(MainApplication.getLabel("wwTemperature"));

        wTemp = (MyTextView) findViewById(R.id.wTemp);

        wHumLabel = (MyTextView) findViewById(R.id.wHumLabel);
        wHumLabel.setText(MainApplication.getLabel("wwHumidity"));

        wHum = (MyTextView) findViewById(R.id.wHum);

        wWindDirLabel = (MyTextView) findViewById(R.id.wWindDirLabel);
        wWindDirLabel.setText(MainApplication.getLabel("wwWind"));

        wWindDir = (MyTextView) findViewById(R.id.wWindDir);

        wUVLabel = (MyTextView) findViewById(R.id.wUVLabel);
        wUVLabel.setText(MainApplication.getLabel("wwUV"));

        wUV = (MyTextView) findViewById(R.id.wUV);

        wF1Temp = (MyTextView) findViewById(R.id.wF1Temp);
        wF1Icon = (ImageView) findViewById(R.id.wF1Icon);
        wF1Day = (MyTextView) findViewById(R.id.wF1Day);

        wF2Temp = (MyTextView) findViewById(R.id.wF2Temp);
        wF2Icon = (ImageView) findViewById(R.id.wF2Icon);
        wF2Day = (MyTextView) findViewById(R.id.wF2Day);

        wF3Temp = (MyTextView) findViewById(R.id.wF3Temp);
        wF3Icon = (ImageView) findViewById(R.id.wF3Icon);
        wF3Day = (MyTextView) findViewById(R.id.wF3Day);
    }

    private Runnable guiRunnable = new Runnable() {

        public void run() {


            guiHandler.removeCallbacks(guiRunnable);


            String fullJSON = MainApplication.getWeather().getJSON();

            JSONObject jsonResponse = null;

            try {
                jsonResponse = new JSONObject(new String(fullJSON));

                day_0_wdate = (jsonResponse.get("day_0_wdate").toString());
                day_0_low = "" + f2c(Integer.parseInt((jsonResponse.get("day_0_low").toString()).trim()));
                day_0_high = "" + f2c(Integer.parseInt((jsonResponse.get("day_0_high").toString()).trim()));
                // day_0_cond = feedCond(
                // (jsonResponse.get("day_0_cond").toString()).trim().toLowerCase());

                day_0_cond = "w" + (jsonResponse.get("day_0_pic").toString()).trim().toLowerCase();
                day_0_temp = (jsonResponse.get("day_0_temp").toString());
                day_0_hum = (jsonResponse.get("day_0_hum").toString());
                day_0_winsp = (jsonResponse.get("day_0_winsp").toString());

                int myWindDir = Integer.parseInt(jsonResponse.get(
                        "day_0_windir").toString());
                if (myWindDir < 45) {
                    day_0_windir = "N";
                } else if (myWindDir < 90) {
                    day_0_windir = "NE";
                } else if (myWindDir < 135) {
                    day_0_windir = "E";
                } else if (myWindDir < 180) {
                    day_0_windir = "SE";
                } else if (myWindDir < 225) {
                    day_0_windir = "S";
                } else if (myWindDir < 270) {
                    day_0_windir = "SW";
                } else if (myWindDir < 315) {
                    day_0_windir = "W";
                } else if (myWindDir < 360) {
                    day_0_windir = "NW";
                }
                day_0_uv = (jsonResponse.get("day_0_uv").toString());
                day_0_windchill = (jsonResponse.get("day_0_windchill").toString());

                day_1_wdate = (jsonResponse.get("day_1_wdate").toString());
                day_1_low = "" + f2c(Integer.parseInt((jsonResponse.get("day_1_low").toString()).trim()));
                day_1_high = "" + f2c(Integer.parseInt((jsonResponse.get("day_1_high").toString()).trim()));
                // day_1_cond = feedCond(
                // (jsonResponse.get("day_1_cond").toString()).trim().toLowerCase());
                day_1_cond = "w" + (jsonResponse.get("day_1_pic").toString()).trim().toLowerCase();
                day_2_wdate = (jsonResponse.get("day_2_wdate").toString());
                day_2_low = "" + f2c(Integer.parseInt((jsonResponse.get("day_2_low").toString()).trim()));
                day_2_high = "" + f2c(Integer.parseInt((jsonResponse.get("day_2_high").toString()).trim()));
                // day_2_cond = feedCond(
                // (jsonResponse.get("day_2_cond").toString()).trim().toLowerCase());
                day_2_cond = "w" + (jsonResponse.get("day_2_pic").toString()).trim().toLowerCase();
                day_3_wdate = (jsonResponse.get("day_3_wdate").toString());
                day_3_low = "" + f2c(Integer.parseInt((jsonResponse.get("day_3_low").toString()).trim()));
                day_3_high = "" + f2c(Integer.parseInt((jsonResponse.get("day_3_high").toString()).trim()));
                // day_3_cond = feedCond(
                // (jsonResponse.get("day_3_cond").toString()).trim().toLowerCase());
                day_3_cond = "w" + (jsonResponse.get("day_3_pic").toString()).trim().toLowerCase();

            } catch (JSONException e) {
                e.printStackTrace();
            }

            // / set it
            wBigIcon.setImageResource(getResources().getIdentifier(
                    day_0_cond, "drawable", getPackageName()));
            Log.i("WeatherActivity", "the weather drawable = " + day_0_cond);

            if (MainApplication.getMAS().getData("data_temperature_cf").equalsIgnoreCase("C")) {
                wTemp.setText(Html.fromHtml(day_0_low + "&deg;C - " + day_0_high + "&deg;C"));
                wBigTemp.setText(Html.fromHtml(day_0_temp + "&deg;C"));
                wF1Temp.setText(Html.fromHtml(day_1_low + "&deg;C - " + day_1_high + "&deg;C"));
                wF1Icon.setImageResource(getResources().getIdentifier(day_1_cond, "drawable", getPackageName()));
                wF1Day.setText(Html.fromHtml(MainApplication.getLabel((day_1_wdate.trim()))));
                wF2Temp.setText(Html.fromHtml(day_2_low + "&deg;C - " + day_2_high + "&deg;C"));
                wF2Icon.setImageResource(getResources().getIdentifier(day_2_cond, "drawable", getPackageName()));
                wF2Day.setText(Html.fromHtml(MainApplication.getLabel((day_2_wdate.trim()))));
                wF3Temp.setText(Html.fromHtml(day_3_low + "&deg;C - " + day_3_high + "&deg;C"));
                wF3Icon.setImageResource(getResources().getIdentifier(day_3_cond, "drawable", getPackageName()));
                wF3Day.setText(Html.fromHtml(MainApplication.getLabel((day_3_wdate.trim()))));
                wWindDir.setText(Html.fromHtml(day_0_windir + "@" + day_0_winsp + "Km/h"));

            } else {

                try {

                    wTemp.setText(Html.fromHtml(Math.round(Float.parseFloat(day_0_low) * 9 / 5 + 32f)
                            + "&deg;F - "
                            + Math.round(Float.parseFloat(day_0_high) * 9 / 5 + 32f)
                            + "&deg;F"));
                    wBigTemp.setText(Html.fromHtml(Math.round(Float.parseFloat(day_0_temp) * 9 / 5 + 32f) + "&deg;F"));

                    wF1Temp.setText(Html.fromHtml(Math.round(Float.parseFloat(day_1_low) * 9 / 5 + 32f)
                            + "&deg;F - "
                            + Math.round(Float.parseFloat(day_1_high) * 9 / 5 + 32f)
                            + "&deg;F"));
                    wF1Icon.setImageResource(getResources().getIdentifier(day_1_cond, "drawable", getPackageName()));
                    wF1Day.setText(Html.fromHtml(MainApplication.getLabel((day_1_wdate.trim()))));

                    wF2Temp.setText(Html.fromHtml(Math.round(Float.parseFloat(day_2_low) * 9 / 5 + 32f)
                            + "&deg;F - "
                            + Math.round(Float.parseFloat(day_2_high) * 9 / 5 + 32f)
                            + "&deg;F"));
                    wF2Icon.setImageResource(getResources().getIdentifier(day_2_cond, "drawable", getPackageName()));
                    wF2Day.setText(Html.fromHtml(MainApplication.getLabel((day_2_wdate.trim()))));

                    wF3Temp.setText(Html.fromHtml(Math.round(Float.parseFloat(day_3_low) * 9 / 5 + 32f)
                            + "&deg;F - "
                            + Math.round(Float.parseFloat(day_3_high) * 9 / 5 + 32f)
                            + "&deg;F"));
                    wF3Icon.setImageResource(getResources().getIdentifier(day_3_cond, "drawable", getPackageName()));
                    wF3Day.setText(Html.fromHtml(MainApplication.getLabel((day_3_wdate.trim()))));

                    wWindDir.setText(Html.fromHtml(day_0_windir
                            + "@"
                            + Math.round((Float.parseFloat(day_0_winsp) / 1.609344))
                            + "mph"));
                }catch (Exception ex){

                }
            }
            wHum.setText(Html.fromHtml(day_0_hum + "%"));
            wUV.setText(Html.fromHtml(day_0_uv));

            // ////////////////////////////////////////////////////////
            // loadPageLabel();
            layoutPage.setVisibility(View.VISIBLE); //

            guiHandler.removeCallbacks(guiRunnable);
            guiHandler.postDelayed(guiRunnable, guiDelay);
        }
    };

    // /////////////////////////////////////

    private int f2c(int myF) {
        int myC = 0;
        myC = Math.round((myF - 32) * 5 / 9);
        return myC;
    }

    private String feedCond(String myCond) {

        String myCondition = (myCond.toString()).trim().toLowerCase();

        if (myCondition.contains("sunny")) {
            myCondition = "dayclear";

        } else if (myCondition.contains("night")) {
            myCondition = "nightclear";

        } else if (myCondition.contains("clear")) {
            myCondition = "dayclear";

        } else if (myCondition.contains("rain")) {
            myCondition = "rainy";

        } else if (myCondition.contains("shower")) {
            myCondition = "rainy";

        } else if (myCondition.contains("storm")) {
            myCondition = "storm";

        } else if (myCondition.contains("cloud")) {
            myCondition = "cloudy";

        } else if (myCondition.contains("mist")) {
            myCondition = "cloudy";

        } else if (myCondition.contains("snow")) {
            myCondition = "snow";

        } else {
            myCondition = "cloudy";
        }

        return myCondition;

    }

    // /////////////////////////////////////////////////

    private String checkMyLabel(String myWord) {
        String myChk = MainApplication.getLabel(myWord.trim());
        return myChk;
    }

    @Override
    protected void onLanguageChanged(String language) {
        loadPageItem();
    }

}
