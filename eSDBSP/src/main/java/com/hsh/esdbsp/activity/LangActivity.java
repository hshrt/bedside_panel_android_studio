package com.hsh.esdbsp.activity;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.widget.Command;


public class LangActivity extends BaseActivity {

    String myLOG = "LangActivity";

    LinearLayout langE;
    LinearLayout langCT;
    LinearLayout langCS;
    LinearLayout langJ;
    LinearLayout langF;
    LinearLayout langK;
    LinearLayout langP;
    LinearLayout langS;
    LinearLayout langG;
    LinearLayout langR;
    LinearLayout langA;
    LinearLayout langI;

    private LinearLayout pinDialogLayout;
    private RelativeLayout pinDialogInnerLayout;
    private Button okButton;
    private ImageView pinCloseImage;
    private EditText pinInput;
    private Button goPinButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.language_all);

        //for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        FlurryAgent.logEvent("Languages", map);

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", true);
        bundle.putString("hightLightChoice", "lang");
        initTopBar(bundle);
        initBottomBar();

        Helper.showInternetWarningToast();

        loadPageItem();
    }

    private void loadPageItem() {

        MainApplication.brightUp();

        ImageView langEicon = (ImageView) findViewById(R.id.langEngFlag);

        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)
                || GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)
                || GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            langEicon.setImageResource(R.drawable.usa);
        } else {
            langEicon.setImageResource(R.drawable.english);
        }

        //for PLN model room
        if (GlobalValue.getInstance().getHotel().equals(Hotel.LONDON)) {
            langEicon.setImageResource(R.drawable.english);
        }

        langE = (LinearLayout) findViewById(R.id.langE);
        langE.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("English", map);

                Command.Language.Language("E");

                DataCacheManager.getInstance().setLang("E");
                MainApplication.getMAS().setData("language", "E");
                goMain();

            }

        });


        langCT = (LinearLayout) findViewById(R.id.langCT);
        langCT.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("TraditionalChinese", map);

                Command.Language.Language("CT");

                DataCacheManager.getInstance().setLang("CT");
                MainApplication.getMAS().setData("language", "CT");
                goMain();
            }

        });


        langCS = (LinearLayout) findViewById(R.id.langCS);
        langCS.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("SimpleChinese", map);

                Command.Language.Language("CS");

                DataCacheManager.getInstance().setLang("CS");
                MainApplication.getMAS().setData("language", "CS");
                goMain();

            }

        });


        langJ = (LinearLayout) findViewById(R.id.langJ);
        langJ.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("Japanese", map);

                Command.Language.Language("J");

                DataCacheManager.getInstance().setLang("J");
                MainApplication.getMAS().setData("language", "J");

                goMain();
            }

        });


        langF = (LinearLayout) findViewById(R.id.langF);
        langF.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("French", map);

                Command.Language.Language("F");

                DataCacheManager.getInstance().setLang("F");
                MainApplication.getMAS().setData("language", "F");
                goMain();
            }

        });


        langK = (LinearLayout) findViewById(R.id.langK);
        langK.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("Korean", map);

                Command.Language.Language("K");

                DataCacheManager.getInstance().setLang("K");
                MainApplication.getMAS().setData("language", "K");
                goMain();
            }

        });


        langP = (LinearLayout) findViewById(R.id.langP);
        langP.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("Portuguese", map);

                Command.Language.Language("P");

                DataCacheManager.getInstance().setLang("P");
                MainApplication.getMAS().setData("language", "P");
                goMain();

            }

        });


        langS = (LinearLayout) findViewById(R.id.langS);
        langS.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("Spanish", map);

                Command.Language.Language("S");

                DataCacheManager.getInstance().setLang("S");
                MainApplication.getMAS().setData("language", "S");
                goMain();
            }

        });


        langG = (LinearLayout) findViewById(R.id.langG);
        langG.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("German", map);

                Command.Language.Language("G");

                DataCacheManager.getInstance().setLang("G");
                MainApplication.getMAS().setData("language", "G");
                goMain();
            }

        });


        langR = (LinearLayout) findViewById(R.id.langR);
        langR.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("Russian", map);

                Command.Language.Language("R");

                DataCacheManager.getInstance().setLang("R");
                MainApplication.getMAS().setData("language", "R");
                goMain();
            }

        });


        langA = (LinearLayout) findViewById(R.id.langA);
        langA.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("Arabic", map);

                Command.Language.Language("A");

                DataCacheManager.getInstance().setLang("A");
                MainApplication.getMAS().setData("language", "A");
                goMain();
            }

        });

        langI = (LinearLayout) findViewById(R.id.langI);
        langI.setVisibility(View.GONE);

        if (GlobalValue.getInstance().getHotel().equals(Hotel.ISTANBUL)) {
            langI.setVisibility(View.VISIBLE);
        }

        langI.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("Turkish", map);

                Command.Language.Language("I");

                DataCacheManager.getInstance().setLang("I");
                MainApplication.getMAS().setData("language", "I");
                goMain();
            }

        });

        pinDialogLayout = (LinearLayout) findViewById(R.id.pinDialogLayout);
        pinDialogInnerLayout = (RelativeLayout) findViewById(R.id.pinDialogInnerLayout);
        pinInput = (EditText) findViewById(R.id.pinInput);

        goPinButton = (Button) findViewById(R.id.goPinButton);

        goPinButton.setOnLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                pinDialogLayout.setVisibility(View.VISIBLE);
                pinDialogInnerLayout.setVisibility(View.VISIBLE);
                topBarFragment.resetBackMainTimer();
                return false;
            }
        });

        okButton = (Button) findViewById(R.id.okButton);
        okButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (pinInput.getText().toString().equalsIgnoreCase(getString(R.string.pin))) {
                    //start intent to SettingActivity
                    Intent intent = new Intent(MainApplication.getContext(), SettingActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    MainApplication.getCurrentActivity().startActivity(intent);
                    LangActivity.this.finish();
                }
            }
        });

        pinCloseImage = (ImageView) findViewById(R.id.pinCloseImage);
        pinCloseImage.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                pinInput.setText("");
                pinDialogLayout.setVisibility(View.GONE);

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(pinInput.getWindowToken(), 0);

            }
        });
    }

}
