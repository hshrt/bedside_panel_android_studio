package com.hsh.esdbsp.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.astuetz.PagerSlidingTabStrip;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.adapter.dvd.DVDGenreListAdapter;
import com.hsh.esdbsp.model.DVDGenre;
import com.hsh.esdbsp.model.Hotel;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class DVDBorrowingActivity extends BaseActivity {

	private AsyncHttpClient client;
	
	private PagerSlidingTabStrip vpiMovieList;
	private android.support.v4.view.ViewPager vpMovieList;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Log.i(TAG, "onCreate fire");

		setContentView(R.layout.activity_dvd_borrowing);

		initUI();
		
		FragmentManager fragmentManager = getSupportFragmentManager();
		
		Bundle bundle = new Bundle();
		bundle.putBoolean("hideBackBtn", false);
		bundle.putString("titleId", "DVD");
		bundle.putString("hightLightChoice", "DVD");
		bundle.putInt("choice", 0);
        initTopBar(bundle);
        initBottomBar();

	}
	

	
	private void initUI(){
		
		vpMovieList = (android.support.v4.view.ViewPager)findViewById(R.id.vpMovieList);
		vpiMovieList = (PagerSlidingTabStrip)findViewById(R.id.vpiMovieList);
		
		client = new AsyncHttpClient();
		client.setMaxRetriesAndTimeout(0, 10000);

		String url = MainApplication.getApiBase() + getString(R.string.dvd_borrowing_api) + getString(R.string.get_genrelist);
	
		Log.i(TAG, "URL = " + url);
	
		client.get(url, new AsyncHttpResponseHandler() {
	
			@Override
			public void onStart() {
				// called before request is started
			}
	
			@Override
			public void onSuccess(int arg0, Header[] headers, byte[] response) {
				String s = new String(response);
				Log.i(TAG, "success response = " + s);
	
				JSONObject restObject;
				
				try {
					restObject = new JSONObject(s);
	
					JSONArray jArray = restObject.getJSONArray("data");
			
					int length = jArray.length();
		
					
					List<DVDGenre> genreList = new ArrayList<DVDGenre>();
			
					DVDGenre dvdGenre = new DVDGenre();
					
					//for Latest
					Map<String, String> languageMap = new HashMap<String, String>();
					
					String latestString = MainApplication.getLabel("dvd.latest");
					
					languageMap.put("E", latestString);
					languageMap.put("F", latestString);
					languageMap.put("CT", latestString);
					languageMap.put("CS", latestString);
					languageMap.put("J", latestString);
					languageMap.put("G", latestString);
					languageMap.put("K", latestString);
					languageMap.put("R", latestString);
					languageMap.put("P", latestString);
					languageMap.put("A", latestString);
					languageMap.put("S", latestString);
					
					dvdGenre.setLanguageMap(languageMap);
					dvdGenre.setGenreId(-1);
					genreList.add(dvdGenre);
					
				
					//for other genre List
					for(int i=0; i<length;i++) {
				
						dvdGenre = new DVDGenre();
						languageMap = new HashMap<String, String>();
						languageMap.put("E", jArray.getJSONObject(i).getString("en"));
						languageMap.put("F", jArray.getJSONObject(i).getString("fr"));
						languageMap.put("CT", jArray.getJSONObject(i).getString("zh_hk"));
						languageMap.put("CS", jArray.getJSONObject(i).getString("zh_cn"));
						languageMap.put("J", jArray.getJSONObject(i).getString("jp"));
						languageMap.put("G", jArray.getJSONObject(i).getString("de"));
						languageMap.put("K", jArray.getJSONObject(i).getString("ko"));
						languageMap.put("R", jArray.getJSONObject(i).getString("ru"));
						languageMap.put("P", jArray.getJSONObject(i).getString("pt"));
						languageMap.put("A", jArray.getJSONObject(i).getString("ar"));
						languageMap.put("S", jArray.getJSONObject(i).getString("es"));
						
						dvdGenre.setLanguageMap(languageMap);
						dvdGenre.setGenreId(jArray.getJSONObject(i).getInt("id"));
						genreList.add(dvdGenre);
					}
			
		
					FragmentManager fm = getSupportFragmentManager();
					DVDGenreListAdapter mAdapter =  new DVDGenreListAdapter(fm);
					mAdapter.initData(genreList);
					
			        vpMovieList.setAdapter(mAdapter);
			        vpiMovieList.setTypeface(Typeface.createFromAsset(getApplicationContext().getAssets(), "minion.otf"), Typeface.BOLD);

					vpiMovieList.setTextSize(60);

			        vpiMovieList.setIndicatorColor(getResources().getColor(R.color.bsp_gold));
			        vpiMovieList.setUnderlineColor(getResources().getColor(R.color.bsp_gold));
			        vpiMovieList.setVisibility(View.VISIBLE);
			        vpiMovieList.setViewPager(vpMovieList);
					
			
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	
	
			}
	
			@Override
			public void onFailure(int arg0, Header[] headers, byte[] response,
					Throwable e) {
	
				if (response != null) {
					String s = new String(response);
					Log.i(TAG, s);
				}
	
				Log.i(TAG, "Stop Retry");
				//hideProgress();
	
			}
	
			@Override
			public void onRetry(int retryNo) {
				// called when request is retried
				Log.i(TAG, "retrying number = " + retryNo);
			}
	
		});
		
		
	}

	@Override
	protected void onLanguageChanged(String language) {
		initUI();
	}


}