
package com.hsh.esdbsp.activity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tv.danmaku.ijk.media.player.IMediaPlayer;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.comm.RadioAsyncFeed;
import com.hsh.esdbsp.comm.RadioAsyncGenLink;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.widget.Command;
import com.hsh.esdbsp.widget.StringWidget;

public class MusicActivity extends BaseActivity {

    static int toRoom = 0;
    static int myVol = 20; // tablet sound level

    static int restartCount = 0;
    static int restartMax = 10;

    static String radioUrl = "";
    static String radioName = "";

    static IjkMediaPlayer aPlayer; // default player

    static int initLinkWait = 500;
    static Handler initLinkLooper = new Handler();

    private RadioAsyncFeed aRadioFeed;
    private RadioAsyncGenLink aRadioGenLink;

    static Handler realPlayHandler = new Handler();
    static int realPlayDelay = 1000;

    static String myCurType = "Genre";

    ListView lvItem1;
    ListView lvSubItem;

    ArrayList<String> subItemArray;
    ArrayAdapter<String> subItemAdapter;

    ImageView radioLoad;
    ImageView radioStop;
    ImageView radioMinus;
    ImageView radioPlus;

    MyTextView footRoom;
    MyTextView footOtherWord;

    LinearLayout streamToRoom;
    MyTextView streamToRoomLabel;

    LinearLayout layoutPage;

    static String myP1List = "";

    //////////////////////////////////////////////////////

    static Handler guiHandler = new Handler();
    static int guiDelay = 500;
    static int guiDelayLong = 5 * 1000;

    static int feedDelay = 100;
    static Handler feedPresetHandler = new Handler();
    static Handler feedP1Handler = new Handler();
    static Handler feedP2Handler = new Handler();
    static Handler feedP3Handler = new Handler();

    //////////////////////////////////////////////////////

    @Override
    protected void onResume() {

        radioUrl = "";
        radioName = "";

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            toRoom = 1;
        } else {
            toRoom = 0;
        }
        addRunnableCall();
        super.onResume();
    }

    @Override
    protected void onPause() {

        radioUrl = "";
        radioName = "";
        stop();

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            toRoom = 1;
        } else {
            toRoom = 0;
        }
        removeRunnableCall();
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        radioUrl = "";
        radioName = "";
        stop();

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            toRoom = 1;
        } else {
            toRoom = 0;
        }
        removeRunnableCall();
        super.onDestroy();
    }

//////////////////////////////////////////////////////////

    private void addRunnableCall() {
        guiHandler.removeCallbacks(guiRunnable);
        guiHandler.postDelayed(guiRunnable, guiDelay);
        hideLoad();
    }

    private void removeRunnableCall() {
        feedP1Handler.removeCallbacks(feedP1Runnable);
        guiHandler.removeCallbacks(guiRunnable);
    }


//////////////////////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.radiomusic_all);

        //for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        FlurryAgent.logEvent("Music", map);

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            toRoom = 1;
        }

        loadPageItem();
        addRunnableCall();
        loadPageLabel();

        Bundle bundle = new Bundle();
        // bundle.putString("titleId", "");
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("titleId", "TV/AUDIO");
        bundle.putString("hightLightChoice", "music");
        bundle.putInt("choice", 3);

        if(GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)){
            bundle.putInt("choice", 2);
        } else if(GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)){
            bundle.putInt("choice", 2);
        } else if (MainApplication.getLabel("service.dvdborrowingsystem.enabled").equals("1")) {
            bundle.putInt("choice", 4);
        }

        initTopBar(bundle);
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0);

        Helper.showInternetWarningToast();
    }


    private void loadPageLabel() {

        footRoom.setText(Html.fromHtml(MainApplication.getLabel("Room") + "<br>" + MainApplication.getMAS().getData("data_myroom")));

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
            footRoom.setVisibility(View.INVISIBLE);
        }
        streamToRoomLabel.setText(MainApplication.getLabel("RadioStreamToRoom"));
    }


    private void loadPageItem() {

        MainApplication.brightUp();

        layoutPage = (LinearLayout) findViewById(R.id.layoutPage);

        footRoom = (MyTextView) findViewById(R.id.footRoomNum);
        footRoom.setText("");

        footOtherWord = (MyTextView) findViewById(R.id.footOtherWord);
        footOtherWord.setText("");

        streamToRoomLabel = (MyTextView) findViewById(R.id.streamToRoomLabel);
        streamToRoom = (LinearLayout) findViewById(R.id.streamToRoom);
        streamToRoom.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                MainApplication.playClickSound(streamToRoom);
                stop();
                // send off to room when switching
                if (toRoom == 1) {
                    Command.Music.RemoteButton(Command.RemoteButton.OFF);
                }
                toRoom = (toRoom + 1) % 2;
                streamToRoom_logic();
                return false;
            }

        });

        //for PBH, there is no stream to Room function
        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {

            streamToRoomLabel.setVisibility(View.GONE);
            streamToRoom.setVisibility(View.GONE);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.addRule(RelativeLayout.RIGHT_OF, R.id.footRoomNum);
            footOtherWord.setLayoutParams(params);

        } else {
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.addRule(RelativeLayout.RIGHT_OF, R.id.streamToRoom);
            footOtherWord.setLayoutParams(params);
        }

        loadRadioItem();
        streamToRoom_logic();
    }

//////////////////////////////////////////////////////////////////

    public void streamToRoom_logic() {

        if (toRoom == 0) {
            streamToRoom.setActivated(false);
            if (radioUrl != "") {
                start(radioUrl, radioName, 0);
            }
        } else {
            streamToRoom.setActivated(true);
            if (radioUrl != "") {
                start(radioUrl, radioName, 0);
            }
        }
    }

//////////////////////////////////////////////////////////////////

    private void showLoad() {
        if (radioLoad != null) {
            radioLoad.setVisibility(View.VISIBLE);
            radioLoad.startAnimation(MainApplication.getAnim());
        }
    }

    private void hideLoad() {
        if (radioLoad != null) {
            radioLoad.setVisibility(View.INVISIBLE);
            radioLoad.clearAnimation();
        }
    }


    private void loadRadioItem() {

        radioLoad = (ImageView) findViewById(R.id.radioLoad);
        radioLoad.setAnimation(MainApplication.getAnim());
        radioLoad.setVisibility(View.INVISIBLE);

        footOtherWord = (MyTextView) findViewById(R.id.footOtherWord);
        radioStop = (ImageView) findViewById(R.id.radioStop);
        radioStop.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {

                radioUrl = "";
                radioName = "";

                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(radioStop);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    stop();

                    if (toRoom == 1) {
                        Command.Music.RemoteButton(Command.RemoteButton.OFF);
                    }
                }
                return false;
            }

        });

        radioMinus = (ImageView) findViewById(R.id.radioMinus);
        radioMinus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {

                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(radioMinus);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

                    if (toRoom == 0) {
                        myVol = myVol - 1;
                        if (myVol < 0) {
                            myVol = 0;
                        }
                        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0);

                    } else {
                        Command.Music.RemoteButton(Command.RemoteButton.VOLUMNDOWN);
                    }
                }
                return false;
            }

        });

        radioPlus = (ImageView) findViewById(R.id.radioPlus);
        radioPlus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {

                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(radioPlus);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {

                    if (toRoom == 0) {
                        myVol = myVol + 1;
                        if (myVol >= 20) {
                            myVol = 20;
                        }
                        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0);

                    } else {
                        Command.Music.RemoteButton(Command.RemoteButton.VOLUMNUP);
                    }
                }
                return false;
            }

        });
        loadRadioThreeList();
        loadDefaultList();
    }

    private void loadDefaultList() {

        myCurType = "Genre";
        subItemAdapter.clear();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "BASE", myCurType);
        } else {
            aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().execute("BASE", myCurType);
        }

        MainApplication.getRadio().setCountryOK(0);

        feedP1Handler.removeCallbacks(feedP1Runnable);
        feedP1Handler.postDelayed(feedP1Runnable, feedDelay);
    }

    private void loadRadioThreeList() {

        lvItem1 = (ListView) this.findViewById(R.id.typeList1);
        lvItem1.setOverScrollMode(ListView.OVER_SCROLL_NEVER);

        final String[] myMusicCatFeed = new String[]{
                "Alternative",
                "Ambient",
                "Blues",
                "Celtic",
                "Classical",
                "College",
                "Country",
                "Dance",
                "Electronica",
                "Folk",
                "Holiday",
                "Jazz",
                "Pop",
                "R&B",
                "Reggae",
                "Rock",
                "Soundtracks"
        };


        String[] lvItem1values = new String[]{
                MainApplication.getRadio().checkMyCountryLabel("Alternative"),
                MainApplication.getRadio().checkMyCountryLabel("Ambient"),
                MainApplication.getRadio().checkMyCountryLabel("Blues"),
                MainApplication.getRadio().checkMyCountryLabel("Celtic"),
                MainApplication.getRadio().checkMyCountryLabel("Classical"),
                MainApplication.getRadio().checkMyCountryLabel("College"),
                MainApplication.getRadio().checkMyCountryLabel("Country"),
                MainApplication.getRadio().checkMyCountryLabel("Dance"),
                MainApplication.getRadio().checkMyCountryLabel("Electronica"),
                MainApplication.getRadio().checkMyCountryLabel("Folk"),
                MainApplication.getRadio().checkMyCountryLabel("Holiday"),
                MainApplication.getRadio().checkMyCountryLabel("Jazz"),
                MainApplication.getRadio().checkMyCountryLabel("Pop"),
                MainApplication.getRadio().checkMyCountryLabel("R&B"),
                MainApplication.getRadio().checkMyCountryLabel("Reggae"),
                MainApplication.getRadio().checkMyCountryLabel("Rock"),
                MainApplication.getRadio().checkMyCountryLabel("Soundtracks")
        };


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.radio_scrollview_row, R.id.label, lvItem1values);
        lvItem1.setAdapter(adapter);
        lvItem1.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {

                String strListType = myMusicCatFeed[position];
                String myFilter = strListType;

                myFilter = myFilter.replaceAll("&amp;", "%26");
                myFilter = myFilter.replaceAll("&", "%26");


                if (subItemAdapter != null) {
                    subItemAdapter.clear();
                }

                if (aRadioFeed != null) {
                    aRadioFeed.cancel(true);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, myFilter, myCurType);
                } else {
                    aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().execute(myFilter, myCurType);
                }

                MainApplication.getRadio().setRadioOK(0);

                feedP1Handler.removeCallbacks(feedP1Runnable);
                feedP1Handler.postDelayed(feedP1Runnable, feedDelay);

            }
        });

        //////////////////////

        lvSubItem = (ListView) this.findViewById(R.id.typeList2);
        lvSubItem.setOverScrollMode(ListView.OVER_SCROLL_NEVER);

        subItemArray = new ArrayList<String>();
        subItemArray.clear();

        subItemAdapter = new ArrayAdapter<String>(this, R.layout.radio_scrollview_row, R.id.label, subItemArray);
        lvSubItem.setAdapter(subItemAdapter);

        lvSubItem.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {

                String[] myFeed = myP1List.split("@");
                String strStation = myFeed[(myFeed.length - 1) - position];
                String[] strStationDetail = strStation.split("#");
                start(strStationDetail[1], strStationDetail[0], 0);

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                map.put("Name", strStationDetail[0]);
                FlurryAgent.logEvent("RadioStation", map);
            }
        });

    }


    private Runnable feedP1Runnable = new Runnable() {

        @Override
        public void run() {

            String[] strFEED;
            String[] strFEEDDetail;

            feedP1Handler.removeCallbacks(feedP1Runnable);

            if (MainApplication.getRadio().getRadioOK() == 0) {
                feedP1Handler.removeCallbacks(feedP1Runnable);
                feedP1Handler.postDelayed(feedP1Runnable, feedDelay);
            } else {
                myP1List = MainApplication.getRadio().getRadio();
                strFEED = myP1List.split("@");

                for (int i = 0; i < strFEED.length; i++) {
                    strFEEDDetail = strFEED[i].split("#");
                    String tmp;

                    if (strFEEDDetail.length == 2) {
                        try {
                            tmp = URLDecoder.decode(StringWidget.UrlDecoderReplaceSpecialChar(strFEEDDetail[1]), "UTF-8");
                            addSubList("" + tmp);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    } else {
                        try {
                            tmp = URLDecoder.decode(StringWidget.UrlDecoderReplaceSpecialChar(strFEEDDetail[0]), "UTF-8");
                            addSubList("" + tmp);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

    };


///////////////////////////////////////////////////////////////////


    protected void addSubList(String name) {
        subItemArray.add(0, Html.fromHtml(name) + "");
        subItemAdapter.notifyDataSetChanged();
    }

///////////////////////////////////////////////////////////////////

    private void restart() {
        start(radioUrl, radioName, restartCount);
    }

    private void start(final String myurl, String name, int isRestart) {

        String myRadioName = name;

        radioUrl = myurl;
        radioName = name;
        restartCount++;

        if (toRoom == 0) {

            footOtherWord.setText(Html.fromHtml("STREAMING"));
            showLoad();

            myRadioName = name;
            myRadioName = myRadioName.replace(" ", "%20");
            myRadioName = myRadioName.replace("&amp;", "%26");

            initLinkLooper.removeCallbacks(initLinkLoop);
            initLinkLooper.postDelayed(initLinkLoop, initLinkWait);

        } else {

            hideLoad();
            myRadioName = name;
            myRadioName = myRadioName.replace(" ", "%20");
            myRadioName = myRadioName.replace("&amp;", "%26");

           Command.Music.MusicMode();
           Command.Music.MusicChannel(myRadioName);
           footOtherWord.setText(Html.fromHtml("") + radioName);
        }
    }


    private void stop() {

        initLinkLooper.removeCallbacks(initLinkLoop);
        realPlayHandler.removeCallbacks(realPlayRunnable);

        if (aPlayer != null) {
            aPlayer.stop();
            aPlayer = null;
        }
        footOtherWord.setText("");

        hideLoad();

        if (toRoom == 1) {

        }

        if (aRadioGenLink != null) {
            aRadioGenLink.cancel(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            aRadioGenLink = (RadioAsyncGenLink) new RadioAsyncGenLink().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "STOP");
        } else {
            aRadioGenLink = (RadioAsyncGenLink) new RadioAsyncGenLink().execute("STOP");
        }
    }


    private void realStart(String radioLink) throws IOException {

        final String url;

        realPlayHandler.removeCallbacks(realPlayRunnable);

        String radioPath = (BuildConfig.BUILD_TYPE.equals("debug")) ? MainApplication.getContext().getString(R.string.radio_path_dev) : MainApplication.getContext().getString(R.string.radio_path);

        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            url = "http://" + radioPath + ":" + MainApplication.getRadio().getPort();
        } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
            url = "http://" + MainApplication.getMAS().getMASPath() + ":" + MainApplication.getRadio().getPort();
        } else {
            url = "http://" + MainApplication.getMAS().getMASPath() + ":" + MainApplication.getRadio().getPort();
        }

        aPlayer = null;
        aPlayer = new IjkMediaPlayer();
        aPlayer.setDisplay(null);
        aPlayer.reset();

        aPlayer.setOnCompletionListener(
                new IjkMediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(IMediaPlayer mp) {
                        try {
                            //Toast.makeText(getApplicationContext(), "Link completed ...", Toast.LENGTH_LONG).show();

                            // should not complete
                            restartCount = 0;
                            restart();

                        } catch (Exception e) {

                            restartCount = 0;
                            restart();

                        }

                    }
                });

        aPlayer.setOnPreparedListener(
                new IjkMediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(IMediaPlayer mp) {
                        // TODO Auto-generated method stub

                        try {

                            try {
                                hideLoad();

                            } catch (Exception e) {
                            }

                            aPlayer.start();
                            footOtherWord.setText(Html.fromHtml("") + radioName);
                        } catch (Exception e) {

                            restartCount = 0;
                            restart();
                        }
                    }
                });

        aPlayer.setOnErrorListener(

                new IjkMediaPlayer.OnErrorListener() {

                    @Override
                    public boolean onError(IMediaPlayer mp, int what, int extra) {
                        // TODO Auto-generated method stub

                        try {

                            if (restartCount < restartMax) {
                                stop();
                                restart();

                            } else {


                                footOtherWord.setText(Html.fromHtml("Sorry, this radio cannot be streamed"));
                                hideLoad();


                            }

                        } catch (Exception e) {
                            footOtherWord.setText(Html.fromHtml("Sorry, this radio cannot be streamed"));
                            hideLoad();
                        }

                        return true;
                    }

                });


        // first start

        aPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        aPlayer.setDataSource(radioLink);
        aPlayer.prepareAsync();
    }


//////////////////////////////////////////////////////////////////    

    private Runnable realPlayRunnable = new Runnable() {

        public void run() {

            realPlayHandler.removeCallbacks(realPlayRunnable);
            try {
                realStart(radioUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };


    private Runnable initLinkLoop = new Runnable() {

        public void run() {

            String[] strFEED;

            initLinkLooper.removeCallbacks(initLinkLoop);

            stop();

            if (toRoom == 1) {
                //Command.Music.AVOff();
            }

            footOtherWord.setText(Html.fromHtml("STREAMING"));

            showLoad();

            if (aRadioGenLink != null) {
                aRadioGenLink.cancel(true);
            }


				/*
                 * after ijkplayer , no need gen
				 * 
				if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
					aRadioGenLink = (RadioAsyncGenLink) new RadioAsyncGenLink().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,"START",radioUrl);
    			else
    				aRadioGenLink = (RadioAsyncGenLink) new RadioAsyncGenLink().execute("START",radioUrl); 
				*/
            realPlayHandler.removeCallbacks(realPlayRunnable);
            realPlayHandler.postDelayed(realPlayRunnable, realPlayDelay);
        }
    };

    ////////////////////////////////////////////////////

    private Runnable guiRunnable = new Runnable() {

        public void run() {
            guiHandler.removeCallbacks(guiRunnable);
            loadPageLabel();
            guiHandler.postDelayed(guiRunnable, guiDelay);
        }
    };


/////////////////////////////////////////////////////

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Log.v("feed",""+event.getKeyCode());
        //AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 30, 0);

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            myVol = myVol - 1;
            if (myVol < 0) {
                myVol = 0;
            }

            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0);
            return true;
        }

        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
            // no system bar
            //Process proc = Runtime.getRuntime().exec(new String[]{"su","-c","service call activity 79 s16 com.android.systemui"});
            //proc.waitFor();

            myVol = myVol + 1;
            if (myVol >= 20) {
                myVol = 20;
            }
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0);
            return true;
        }

        if (keyCode == KeyEvent.KEYCODE_POWER) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onLanguageChanged(String language) {
        loadPageItem();
        loadPageLabel();
    }

}