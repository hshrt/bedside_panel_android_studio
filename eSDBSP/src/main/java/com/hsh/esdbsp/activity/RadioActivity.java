
package com.hsh.esdbsp.activity;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import tv.danmaku.ijk.media.player.IMediaPlayer;
import tv.danmaku.ijk.media.player.IjkMediaPlayer;

import android.app.ActionBar.LayoutParams;
import android.content.Context;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.comm.RadioAsyncFeed;
import com.hsh.esdbsp.comm.RadioAsyncGenLink;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.widget.Command;
import com.hsh.esdbsp.widget.StringWidget;

public class RadioActivity extends BaseActivity {

    boolean isStreamToRoom = false;

    static int myVol = 20; // tablet sound level

    static int restartCount = 0;
    static int restartMax = 10;

    static String radioUrl = "";
    static String radioName = "";

    static IjkMediaPlayer aPlayer; // default player

    static int initLinkWait = 500;
    static Handler initLinkLooper = new Handler();

    private RadioAsyncFeed aRadioFeed;
    private RadioAsyncGenLink aRadioGenLink;

    static Handler realPlayHandler = new Handler();
    static int realPlayDelay = 1000;

    static String myCurType = "Country";

    //-----------

    ListView lvItem1;
    ListView lvItem2;
    ListView lvItem3;

    ArrayList<String> itemArray2;
    ArrayAdapter<String> itemAdapter2;

    ArrayList<String> itemArray3;
    ArrayAdapter<String> itemAdapter3;

    ImageView radioBBC;
    ImageView radioRFI;
    ImageView radioVOA;
    ImageView radioRTHK;
    ImageView radioDLF;

    ImageView radioLoad;
    ImageView radioStop;
    ImageView radioMinus;
    ImageView radioPlus;

    //-----------

    MyTextView footRoom;

    MyTextView footOtherWord;


    LinearLayout streamToRoom;
    MyTextView streamToRoomLabel;

    //-----------

    //////////////////////////////////////////////////////

    static String myPresetList = "";
    static String myP1List = "";
    static String myP2List = "";


    //////////////////////////////////////////////////////

    static Handler guiHandler = new Handler();
    static int guiDelay = 500;

    static int feedDelay = 100;
    static Handler feedPresetHandler = new Handler();
    static Handler feedP1Handler = new Handler();
    static Handler feedP2Handler = new Handler();

    @Override
    protected void onResume() {

        radioUrl = "";
        radioName = "";

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            isStreamToRoom = true;
        } else {
            isStreamToRoom = false;
        }
        addRunnableCall();
        super.onResume();
    }

    @Override
    protected void onPause() {

        radioUrl = "";
        radioName = "";
        stop();

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            isStreamToRoom = true;
        } else {
            isStreamToRoom = false;
        }

        removeRunnableCall();
        super.onPause();
    }


    @Override
    protected void onDestroy() {

        radioUrl = "";
        radioName = "";
        stop();

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            isStreamToRoom = true;
        } else {
            isStreamToRoom = false;
        }

        removeRunnableCall();
        super.onDestroy();
    }

//////////////////////////////////////////////////////////

    private void addRunnableCall() {
        guiHandler.removeCallbacks(guiRunnable);
        guiHandler.postDelayed(guiRunnable, guiDelay);
        hideLoad();
    }

    private void removeRunnableCall() {
        feedP1Handler.removeCallbacks(feedP1Runnable);
        feedP2Handler.removeCallbacks(feedP2Runnable);
        guiHandler.removeCallbacks(guiRunnable);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.radio_all);

        //for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        FlurryAgent.logEvent("Radio", map);

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            isStreamToRoom = true;
        }

        loadPageItem();
        addRunnableCall();
        loadPageLabel();

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("titleId", "TV/AUDIO");
        bundle.putString("hightLightChoice", "radio");
        bundle.putInt("choice", 2);

        if (GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            bundle.putInt("choice", 1);
        } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
            bundle.putInt("choice", 1);
        } else if (MainApplication.getLabel("service.dvdborrowingsystem.enabled").equals("1")) {
            bundle.putInt("choice", 3);
        }

        initTopBar(bundle);

        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0);

        Helper.showInternetWarningToast();
    }


    private void loadPageLabel() {

        footRoom.setText(Html.fromHtml(MainApplication.getLabel("Room") + "<br>" + MainApplication.getMAS().getData("data_myroom")));

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
            footRoom.setVisibility(View.INVISIBLE);
        }

        streamToRoomLabel.setText(MainApplication.getLabel("RadioStreamToRoom"));
    }


    private void loadPageItem() {

        MainApplication.brightUp();

        footRoom = (MyTextView) findViewById(R.id.footRoomNum);
        footRoom.setText("");

        footOtherWord = (MyTextView) findViewById(R.id.footOtherWord);
        footOtherWord.setText("");

        streamToRoomLabel = (MyTextView) findViewById(R.id.streamToRoomLabel);
        streamToRoom = (LinearLayout) findViewById(R.id.streamToRoom);
        streamToRoom_logic();


        //for PBH, there is no stream to Room function
        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {

            streamToRoomLabel.setVisibility(View.GONE);
            streamToRoom.setVisibility(View.GONE);

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.addRule(RelativeLayout.RIGHT_OF, R.id.footRoomNum);
            footOtherWord.setLayoutParams(params);

        } else {

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
            params.addRule(RelativeLayout.RIGHT_OF, R.id.streamToRoom);
            footOtherWord.setLayoutParams(params);
        }


        streamToRoom.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                // TODO Auto-generated method stub

                MainApplication.playClickSound(streamToRoom);
                stop();

                // send off to room when switching
                if (isStreamToRoom) {
                    Command.Radio.RemoteButton(Command.RemoteButton.OFF);
                    isStreamToRoom = false;
                } else {
                    isStreamToRoom = true;
                }
                streamToRoom_logic();
                return false;
            }
        });

        loadRadioItem();
    }

//////////////////////////////////////////////////////////////////

    public void streamToRoom_logic() {

        if (!isStreamToRoom) {
            streamToRoom.setActivated(false);
            if (radioUrl != "") {
                start(radioUrl, radioName, 0);
            }
        } else {
            streamToRoom.setActivated(true);
            if (radioUrl != "") {
                start(radioUrl, radioName, 0);
            }
        }
    }

    private void showLoad() {
        if (radioLoad != null) {
            radioLoad.setVisibility(View.VISIBLE);
            radioLoad.startAnimation(MainApplication.getAnim());
        }
    }

    private void hideLoad() {
        if (radioLoad != null) {
            radioLoad.setVisibility(View.INVISIBLE);
            radioLoad.clearAnimation();
        }
    }

    private void loadRadioItem() {

        radioLoad = (ImageView) findViewById(R.id.radioLoad);
        radioLoad.setAnimation(MainApplication.getAnim());
        radioLoad.setVisibility(View.INVISIBLE);

        footOtherWord = (MyTextView) findViewById(R.id.footOtherWord);

        radioStop = (ImageView) findViewById(R.id.radioStop);
        radioStop.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {

                radioUrl = "";
                radioName = "";

                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(radioStop);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    stop();
                    if (isStreamToRoom) {
                        Command.Radio.RemoteButton(Command.RemoteButton.OFF);
                    }
                }
                return false;
            }

        });

        radioMinus = (ImageView) findViewById(R.id.radioMinus);
        radioMinus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {

                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(radioMinus);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    if (!isStreamToRoom) {
                        myVol = myVol - 1;
                        if (myVol < 0) {
                            myVol = 0;
                        }
                        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0);
                    } else {
                        Command.Radio.RemoteButton(Command.RemoteButton.VOLUMNDOWN);
                    }
                }
                return false;
            }

        });

        radioPlus = (ImageView) findViewById(R.id.radioPlus);
        radioPlus.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent event) {

                if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    MainApplication.playClickSound(radioPlus);
                } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                    if (!isStreamToRoom) {
                        myVol = myVol + 1;
                        if (myVol >= 20) {
                            myVol = 20;
                        }
                        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0);
                    } else {
                        Command.Radio.RemoteButton(Command.RemoteButton.VOLUMNUP);
                    }
                }

                return false;
            }

        });


        radioBBC = (ImageView) findViewById(R.id.radioBBC);
        radioBBC.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View arg0) {
                String[] myPlayURL = myPresetList.split("@");
                if (myPlayURL.length < 1 || myPlayURL[0] == null) {
                    feedPresetHandler.removeCallbacks(feedPresetRunnable);
                    feedPresetHandler.postDelayed(feedPresetRunnable, feedDelay);
                } else {
                    start(myPlayURL[0], "PRESET BBC", 0);
                }
            }

        });

        radioRFI = (ImageView) findViewById(R.id.radioRFI);
        radioRFI.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View arg0) {
                String[] myPlayURL = myPresetList.split("@");
                if (myPlayURL.length < 2 || myPlayURL[1] == null) {
                    feedPresetHandler.removeCallbacks(feedPresetRunnable);
                    feedPresetHandler.postDelayed(feedPresetRunnable, feedDelay);
                } else {
                    start(myPlayURL[1], "PRESET RFI", 0);
                }
            }

        });

        radioVOA = (ImageView) findViewById(R.id.radioVOA);
        radioVOA.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View arg0) {
                String[] myPlayURL = myPresetList.split("@");
                if (myPlayURL.length < 3 || myPlayURL[2] == null) {
                    feedPresetHandler.removeCallbacks(feedPresetRunnable);
                    feedPresetHandler.postDelayed(feedPresetRunnable, feedDelay);
                } else {
                    start(myPlayURL[2], "PRESET VOA", 0);
                }
            }

        });

        radioRTHK = (ImageView) findViewById(R.id.radioRTHK);
        radioRTHK.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View arg0) {
                String[] myPlayURL = myPresetList.split("@");
                if (myPlayURL.length < 4 || myPlayURL[3] == null) {
                    feedPresetHandler.removeCallbacks(feedPresetRunnable);
                    feedPresetHandler.postDelayed(feedPresetRunnable, feedDelay);
                } else {
                    start(myPlayURL[3], "PRESET RTHK", 0);
                }
            }

        });

        radioDLF = (ImageView) findViewById(R.id.radioDLF);
        radioDLF.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View arg0) {
                String[] myPlayURL = myPresetList.split("@");
                if (myPlayURL.length < 5 || myPlayURL[4] == null) {
                    feedPresetHandler.removeCallbacks(feedPresetRunnable);
                    feedPresetHandler.postDelayed(feedPresetRunnable, feedDelay);
                } else {
                    start(myPlayURL[4], "PRESET Deutschlandfunk", 0);
                }
            }

        });

        MainApplication.getRadio().setPresetOK(0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "PRESET", myCurType);
        } else {
            aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().execute("PRESET", myCurType);
        }

        feedPresetHandler.removeCallbacks(feedPresetRunnable);
        feedPresetHandler.postDelayed(feedPresetRunnable, feedDelay);

        loadRadioThreeList();
        loadDefaultList();
    }

    private void loadDefaultList() {

			/* load the first item */
        myCurType = "Country";

        itemAdapter2.clear();
        itemAdapter3.clear();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "BASE", myCurType);
        } else {
            aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().execute("BASE", myCurType);
        }


        MainApplication.getRadio().setCountryOK(0);
        feedP1Handler.removeCallbacks(feedP1Runnable);
        feedP1Handler.postDelayed(feedP1Runnable, feedDelay);
    }

    private void loadRadioThreeList() {

        lvItem1 = (ListView) this.findViewById(R.id.typeList1);
        lvItem1.setOverScrollMode(ListView.OVER_SCROLL_NEVER);

        String[] lvItem1values = new String[]{MainApplication.getLabel("RadioCountry"), MainApplication.getLabel("RadioGenre")};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.radio_scrollview_row, R.id.label, lvItem1values);
        lvItem1.setAdapter(adapter);

        lvItem1.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {

                if (position == 0) {
                    myCurType = "Country";
                    //for Flurry log
                    final Map<String, String> map = new HashMap<String, String>();
                    map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                    FlurryAgent.logEvent("RadioCountry", map);
                } else {
                    myCurType = "Genre";
                    //for Flurry log
                    final Map<String, String> map = new HashMap<String, String>();
                    map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                    FlurryAgent.logEvent("RadioGenres", map);
                }

                itemAdapter2.clear();
                itemAdapter3.clear();

                if (aRadioFeed != null) {
                    aRadioFeed.cancel(true);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "BASE", myCurType);
                } else {
                    aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().execute("BASE", myCurType);
                }

                if (position == 0) {
                    MainApplication.getRadio().setCountryOK(0);
                } else {
                    MainApplication.getRadio().setGenreOK(0);
                }
                feedP1Handler.removeCallbacks(feedP1Runnable);
                feedP1Handler.postDelayed(feedP1Runnable, feedDelay);
            }
        });

        //////////////////////

        lvItem2 = (ListView) this.findViewById(R.id.typeList2);
        lvItem2.setOverScrollMode(ListView.OVER_SCROLL_NEVER);

        itemArray2 = new ArrayList<String>();
        itemArray2.clear();

        itemAdapter2 = new ArrayAdapter<String>(this, R.layout.radio_scrollview_row, R.id.label, itemArray2);
        lvItem2.setAdapter(itemAdapter2);


        lvItem2.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {

                String[] myFeed = myP1List.split("@");

                String strList2Type = myFeed[(myFeed.length - 1) - position];
                String[] strList2TypeDetail = strList2Type.split("#");

                String myFilter = strList2TypeDetail[0].toString();

                myFilter = myFilter.replaceAll("&amp;", "%26");
                myFilter = myFilter.replaceAll("&", "%26");

                Log.i("Radio", "myFilter = " + myFilter);

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                map.put("Type", myFilter);
                FlurryAgent.logEvent("RadioType", map);

                itemAdapter3.clear();

                if (aRadioFeed != null) {
                    aRadioFeed.cancel(true);
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, myFilter, myCurType);
                } else {
                    aRadioFeed = (RadioAsyncFeed) new RadioAsyncFeed().execute(myFilter, myCurType);
                }

                MainApplication.getRadio().setRadioOK(0);
                feedP2Handler.removeCallbacks(feedP2Runnable);
                feedP2Handler.postDelayed(feedP2Runnable, feedDelay);
            }
        });


        //////////////////////

        lvItem3 = (ListView) this.findViewById(R.id.typeList3);
        lvItem3.setOverScrollMode(ListView.OVER_SCROLL_NEVER);

        itemArray3 = new ArrayList<String>();
        itemArray3.clear();

        itemAdapter3 = new ArrayAdapter<String>(this, R.layout.radio_scrollview_row, R.id.label, itemArray3);
        lvItem3.setAdapter(itemAdapter3);

        lvItem3.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {

                String[] myFeed = myP2List.split("@");
                String strStation = myFeed[(myFeed.length - 1) - position];
                String[] strStationDetail = strStation.split("#");
                start(strStationDetail[1], strStationDetail[0], 0);

                //for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                map.put("Name", strStationDetail[0]);
                FlurryAgent.logEvent("RadioStation", map);
            }
        });

    }

    Runnable feedPresetRunnable = new Runnable() {

        @Override
        public void run() {

            feedPresetHandler.removeCallbacks(feedPresetRunnable);

            if (MainApplication.getRadio().getPresetOK() == 0) {
                feedPresetHandler.removeCallbacks(feedPresetRunnable);
                feedPresetHandler.postDelayed(feedPresetRunnable, feedDelay);
            } else {
                myPresetList = MainApplication.getRadio().getPreset();
            }
        }


    };


    Runnable feedP1Runnable = new Runnable() {

        @Override
        public void run() {

            String[] strFEED;
            String[] strFEEDDetail;
            int myOK = 0;

            feedP1Handler.removeCallbacks(feedP1Runnable);

            if (myCurType.equalsIgnoreCase("Country")) {
                myOK = MainApplication.getRadio().getCountryOK();
            } else {
                myOK = MainApplication.getRadio().getGenreOK();
            }

            //myOK = 1; // changed no async
            if (myOK == 0) {
                feedP1Handler.removeCallbacks(feedP1Runnable);
                feedP1Handler.postDelayed(feedP1Runnable, feedDelay);

            } else {

                if (myCurType.equalsIgnoreCase("Country")) {
                    myP1List = MainApplication.getRadio().getCountry();
                } else {
                    myP1List = MainApplication.getRadio().getGenre();
                }

                strFEED = myP1List.split("@");

                for (int i = 0; i < strFEED.length; i++) {
                    strFEEDDetail = strFEED[i].split("#");
                    if (strFEEDDetail.length == 2) {
                        addList1("" + strFEEDDetail[1]);
                    } else {
                        addList1("" + MainApplication.getRadio().checkMyCountryLabel(strFEEDDetail[0].trim() + ""));
                    }
                }
            }
        }


    };


    Runnable feedP2Runnable = new Runnable() {

        @Override
        public void run() {

            String[] strFEED;
            String[] strFEEDDetail;

            feedP2Handler.removeCallbacks(feedP2Runnable);

            if (MainApplication.getRadio().getRadioOK() == 0) {
                feedP2Handler.removeCallbacks(feedP2Runnable);
                feedP2Handler.postDelayed(feedP2Runnable, feedDelay);

            } else {
                myP2List = MainApplication.getRadio().getRadio();
                strFEED = myP2List.split("@");

                for (int i = 0; i < strFEED.length; i++) {
                    strFEEDDetail = strFEED[i].split("#");
                    String tmp;

                    if (strFEEDDetail.length == 2) {
                        try {
                            tmp = URLDecoder.decode(StringWidget.UrlDecoderReplaceSpecialChar(strFEEDDetail[1]), "UTF-8");
                            addList2("" + tmp);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            tmp = URLDecoder.decode(StringWidget.UrlDecoderReplaceSpecialChar(strFEEDDetail[0]), "UTF-8");
                            addList2("" + tmp);
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }


    };


///////////////////////////////////////////////////////////////////


    protected void addList1(String name) {
        itemArray2.add(0, Html.fromHtml(name) + "");
        itemAdapter2.notifyDataSetChanged();
    }

    protected void addList2(String name) {
        itemArray3.add(0, Html.fromHtml(name) + "");
        itemAdapter3.notifyDataSetChanged();
    }


///////////////////////////////////////////////////////////////////

    private void restart() {
        start(radioUrl, radioName, restartCount);
    }

    private void start(final String myurl, String name, int isRestart) {

        String myRadioName = name;
        radioUrl = myurl;
        radioName = name;

        if (isRestart == 0) {
            restartCount = 1;
        } else {
            restartCount = restartCount + 1;
        }

        if (!isStreamToRoom) {

            footOtherWord.setText(Html.fromHtml("STREAMING"));
            showLoad();

            myRadioName = name;
            myRadioName = myRadioName.replace(" ", "%20");
            myRadioName = myRadioName.replace("&amp;", "%26");
            initLinkLooper.removeCallbacks(initLinkLoop);
            initLinkLooper.postDelayed(initLinkLoop, initLinkWait);

        } else {

            hideLoad();

            myRadioName = name;
            myRadioName = myRadioName.replace(" ", "%20");
            myRadioName = myRadioName.replace("&amp;", "%26");
            Command.Radio.RadioMode();
            Command.Radio.SwitchChannel(myRadioName);
            footOtherWord.setText(Html.fromHtml("") + radioName);
        }
    }


    private void stop() {

        initLinkLooper.removeCallbacks(initLinkLoop);
        realPlayHandler.removeCallbacks(realPlayRunnable);

        if (aPlayer != null) {
            aPlayer.stop();
            aPlayer = null;
        }
        footOtherWord.setText("");

        hideLoad();

        if (aRadioGenLink != null) {
            aRadioGenLink.cancel(true);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            aRadioGenLink = (RadioAsyncGenLink) new RadioAsyncGenLink().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "STOP");
        } else {
            aRadioGenLink = (RadioAsyncGenLink) new RadioAsyncGenLink().execute("STOP");
        }

    }

    private void realStart(String radioLink) throws IOException {

        final String url;

        realPlayHandler.removeCallbacks(realPlayRunnable);

        String radioPath = (BuildConfig.BUILD_TYPE.equals("debug")) ? MainApplication.getContext().getString(R.string.radio_path_dev) : MainApplication.getContext().getString(R.string.radio_path);

        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEVERLYHILLS)) {
            url = "http://" + radioPath + ":" + MainApplication.getRadio().getPort();

        } else if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
            url = "http://" + MainApplication.getMAS().getMASPath() + ":" + MainApplication.getRadio().getPort();
        } else {
            url = "http://" + MainApplication.getMAS().getMASPath() + ":" + MainApplication.getRadio().getPort();
        }

        aPlayer = null;
        aPlayer = new IjkMediaPlayer();
        aPlayer.setDisplay(null);
        aPlayer.reset();

        aPlayer.setOnCompletionListener(
                new IjkMediaPlayer.OnCompletionListener() {

                    @Override
                    public void onCompletion(IMediaPlayer arg0) {
                        // TODO Auto-generated method stub
                        try {
                            // should not complete
                            restartCount = 0;
                            restart();
                        } catch (Exception e) {
                            restartCount = 0;
                            restart();

                        }
                    }
                });

        aPlayer.setOnPreparedListener(
                new IjkMediaPlayer.OnPreparedListener() {

                    @Override
                    public void onPrepared(IMediaPlayer mp) {
                        // TODO Auto-generated method stub

                        try {
                            hideLoad();
                            aPlayer.start();
                            footOtherWord.setText(Html.fromHtml("") + radioName);
                        } catch (Exception e) {
                            restartCount = 0;
                            restart();
                        }
                    }
                });

        aPlayer.setOnErrorListener(

                new IjkMediaPlayer.OnErrorListener() {

                    @Override
                    public boolean onError(IMediaPlayer mp, int what, int extra) {
                        // TODO Auto-generated method stub

                        try {
                            if (restartCount < restartMax) {
                                stop();
                                restart();
                            } else {
                                footOtherWord.setText(Html.fromHtml("Sorry, this radio cannot be streamed"));
                                hideLoad();
                            }

                        } catch (Exception e) {
                            footOtherWord.setText(Html.fromHtml("Sorry, this radio cannot be streamed"));
                            hideLoad();
                        }

                        return true;
                    }
                });

        // first start
        aPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        aPlayer.setDataSource(radioLink);
        aPlayer.prepareAsync();

    }

//////////////////////////////////////////////////////////////////    

    private Runnable realPlayRunnable = new Runnable() {

        public void run() {

            realPlayHandler.removeCallbacks(realPlayRunnable);
            try {
                realStart(radioUrl);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };


    private Runnable initLinkLoop = new Runnable() {

        public void run() {

            initLinkLooper.removeCallbacks(initLinkLoop);
            stop();

            if (isStreamToRoom) {
                //Command.Radio.RemoteButton(Command.RemoteButton.OFF);
            }

            footOtherWord.setText(Html.fromHtml("STREAMING"));

            showLoad();

            if (aRadioGenLink != null) {
                aRadioGenLink.cancel(true);
            }
            realPlayHandler.removeCallbacks(realPlayRunnable);
            realPlayHandler.postDelayed(realPlayRunnable, realPlayDelay);
        }
    };


    ////////////////////////////////////////////////////


    private Runnable guiRunnable = new Runnable() {

        public void run() {
            guiHandler.removeCallbacks(guiRunnable);
            loadPageLabel();
            guiHandler.postDelayed(guiRunnable, guiDelay);
        }
    };


/////////////////////////////////////////////////////

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if (keyCode == KeyEvent.KEYCODE_BACK) {
            return true;
        }

        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {

            myVol = myVol - 1;
            if (myVol < 0) {
                myVol = 0;
            }
            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0);
            return true;
        }

        if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {

            // no system bar
            //Process proc = Runtime.getRuntime().exec(new String[]{"su","-c","service call activity 79 s16 com.android.systemui"});
            //proc.waitFor();

            myVol = myVol + 1;
            if (myVol >= 20) {
                myVol = 20;
            }

            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, myVol, 0);
            return true;
        }

        if (keyCode == KeyEvent.KEYCODE_POWER) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onLanguageChanged(String language) {
        loadPageItem();
        loadPageLabel();
    }

}