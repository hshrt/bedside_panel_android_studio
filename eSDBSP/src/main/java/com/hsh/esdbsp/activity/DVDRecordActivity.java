package com.hsh.esdbsp.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.adapter.dvd.DVDRecordListAdapter;
import com.hsh.esdbsp.adapter.dvd.DVDRecordListAdapter.ItemClickListener;
import com.hsh.esdbsp.model.DVDRecordListItem;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class DVDRecordActivity extends BaseActivity {

    private AsyncHttpClient client;

    private RecyclerView recyclerView;
    private TextView noRecordText;
    private Context context;
    private DVDRecordListAdapter dVDRecordListAdapter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        Log.i(TAG, "onCreate fire");

        setContentView(R.layout.activity_dvd_record);

        initUI();

        FragmentManager fragmentManager = getSupportFragmentManager();

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("titleId", "DVD");
        bundle.putString("hightLightChoice", "DVD");
        bundle.putInt("choice", 1);
        initTopBar(bundle);
        initBottomBar();
    }


    private void initUI() {

        recyclerView = (RecyclerView) findViewById(R.id.rvRecordlist);
        noRecordText = (TextView) findViewById(R.id.tvNoRecord);
        noRecordText.setText(MainApplication.getLabel("dvd.norecord"));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(0, 10000);

        RequestParams params;
        params = new RequestParams();

        String currAPILang = "en";
        String currLang = MainApplication.getMAS().getData("data_language") == "" ? "E" : MainApplication.getMAS().getData("data_language");

        if (currLang.equals("E")) {
            currAPILang = "en";
        } else if (currLang.equals("F")) {
            currAPILang = "fr";
        } else if (currLang.equals("CT")) {
            currAPILang = "zh_hk";
        } else if (currLang.equals("CS")) {
            currAPILang = "zh_cn";
        } else if (currLang.equals("J")) {
            currAPILang = "jp";
        } else if (currLang.equals("G")) {
            currAPILang = "de";
        } else if (currLang.equals("K")) {
            currAPILang = "ko";
        } else if (currLang.equals("R")) {
            currAPILang = "ru";
        } else if (currLang.equals("P")) {
            currAPILang = "pt";
        } else if (currLang.equals("A")) {
            currAPILang = "ar";
        } else if (currLang.equals("S")) {
            currAPILang = "es";
        }
        params.add("lang", currAPILang);
        params.add("room", MainApplication.getMAS().getData("data_myroom"));

        String url = MainApplication.getApiBase() + getString(R.string.dvd_borrowing_api) + getString(R.string.get_borrow_record);

        Log.i(TAG, "URL = " + url);

        client.get(url, params, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int arg0, Header[] headers, byte[] response) {
                String s = new String(response);
                Log.i(TAG, "success response = " + s);

                JSONObject restObject;

                try {
                    restObject = new JSONObject(s);

                    JSONArray jArray = restObject.getJSONArray("data");

                    int length = jArray.length();


                    if (length < 1) {
                        recyclerView.setVisibility(View.GONE);
                        noRecordText.setVisibility(View.VISIBLE);

                    } else {
                        recyclerView.setVisibility(View.VISIBLE);
                        noRecordText.setVisibility(View.GONE);

                        final List<DVDRecordListItem> recordList = new ArrayList<DVDRecordListItem>();

                        for (int i = 0; i < length; i++) {
                            DVDRecordListItem dVDRecordListItem = new DVDRecordListItem();


                            dVDRecordListItem.setBorrowTime(jArray.getJSONObject(i).getString("requesttime"));
                            dVDRecordListItem.setGenre(jArray.getJSONObject(i).getString("genre"));
                            dVDRecordListItem.setMovieId(jArray.getJSONObject(i).getString("movieId"));
                            dVDRecordListItem.setPosterUrl(jArray.getJSONObject(i).getString("poster"));
                            dVDRecordListItem.setRecordId(jArray.getJSONObject(i).getInt("id"));
                            dVDRecordListItem.setStatusId(jArray.getJSONObject(i).getInt("statusId"));
                            dVDRecordListItem.setTitle(jArray.getJSONObject(i).getString("title"));
                            dVDRecordListItem.setYear(jArray.getJSONObject(i).getString("year"));

                            recordList.add(dVDRecordListItem);
                        }

                        dVDRecordListAdapter = new DVDRecordListAdapter(context, recordList);
                        dVDRecordListAdapter.setClickListener(new ItemClickListener() {

                            @Override
                            public void onItemClick(View view, final int position) {

                                if (recordList.get(position).getStatusId() == 2) {

                                    String recordIdString = recordList.get(position).getRecordId() + "";
                                    String statusIdString = "3";

                                    RequestParams params;
                                    params = new RequestParams();
                                    params.add("requestId", recordIdString);
                                    params.add("statusId", statusIdString);

                                    String url = MainApplication.getApiBase() + getString(R.string.dvd_borrowing_api)
                                            + getString(R.string.update_borrow_record);

                                    Log.i(TAG, "URL = " + url);

                                    client.post(url, params, new AsyncHttpResponseHandler() {

                                        @Override
                                        public void onSuccess(int arg0, Header[] headers, byte[] response) {
                                            String s = new String(response);
                                            Log.i(TAG, "success response = " + s);
                                            recordList.get(position).setStatusId(3);
                                            dVDRecordListAdapter.notifyDataSetChanged();
                                        }

                                        @Override
                                        public void onFailure(int arg0, Header[] headers, byte[] response, Throwable e) {

                                        }
                                    });
                                }
                            }

                        });
                        recyclerView.setAdapter(dVDRecordListAdapter);
                    }


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(int arg0, Header[] headers, byte[] response,
                                  Throwable e) {

                if (response != null) {
                    String s = new String(response);
                    Log.i(TAG, s);
                }

                Log.i(TAG, "Stop Retry");
                recyclerView.setVisibility(View.GONE);
                noRecordText.setVisibility(View.VISIBLE);

            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                Log.i(TAG, "retrying number = " + retryNo);
            }
        });

    }

    @Override
    protected void onLanguageChanged(String language) {
        initUI();
    }

}