package com.hsh.esdbsp.activity;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.comm.MASComm;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.InRoomDinOrderFragment;
import com.hsh.esdbsp.fragment.InRoomDinTimeFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.adapter.dining.FoodOrderConfirmAdapter;
import com.hsh.esdbsp.adapter.dining.GeneralItemWrapper;
import com.hsh.esdbsp.model.FoodOrderData;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.FoodOrder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ServiceInRoomDinningConfirmActivity extends BaseActivity implements FoodOrderConfirmAdapter.Callback, InRoomDinOrderFragment.Callback, InRoomDinTimeFragment.Callback {

	private ListView listView;
	private FrameLayout optionFragmentContainer;
	private RelativeLayout orderBar;
	private Button willServePlusBtn;
	private Button willServeMinusBtn;
	private Button submitBtn;
	private MyTextView reviewOrderHeaderText;
	private MyTextView emptyListText;
	private MyTextView orderWillServeText;
	private MyTextView orderWillServeQtyText;
	private LinearLayout errorMessageDialogLayout;
	private ImageView errorMessageDialogCloseImage;
	private MyTextView errorMessageDialogLabel;
	
	private RelativeLayout deliveryTimeBox;
	private Button timeBoxCloseBtn, timeBoxConfirmBtn;
	private TextView selectDeliveryTime, notAvailableText;
	private TextView dayTxt, hourTxt, minTxt, dayHeaderTxt, hourHeaderTxt, minHeaderTxt;
	private ImageButton dayUpBtn, dayDownBtn, hourUpBtn, hourDownBtn, minUpBtn, minDownBtn;
	private int deliveryDay, deliveryHour, deliveryMin;
	
	private ArrayList<FoodOrder> orderDetail;
	private FoodOrderConfirmAdapter mAdapter;
	private boolean isDeliveryBoxShown = false;

	private int orderWillServe = 1;

	private LinearLayout messageDialogLayout;
	private MyTextView messageDialogLabel;
	private MyTextView messageDialogTitle;
	private ImageView messageDialogCloseImage;
	private Button messageDialogConfirmBtn;
	private Button messageDialogCancelBtn;
	private int messageDialogType;
	private static final int DIALOG_TYPE_CONFIRM = 1;
	private static final int DIALOG_TYPE_CONFLICT = 2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.cms_food_order_confirm);

		mDialog = new ProgressDialog(this);

		Bundle bundle = new Bundle();
		bundle.putString("titleId", "Review Order");
		initTopBar(bundle);
		initBottomBar();

		orderDetail = FoodOrderData.getInstance().getFoodOrder();

		initUI();
		onPlaceOrder();
	}

	private void initUI() {
		reviewOrderHeaderText = (MyTextView) findViewById(R.id.title);
		reviewOrderHeaderText.setText(MainApplication.getLabel("INROOM_REVIEW_ORDER"));
		orderBar = (RelativeLayout) findViewById(R.id.orderBar);
		orderWillServeText = (MyTextView) findViewById(R.id.order_will_serve_txt);
		orderWillServeText.setText(MainApplication.getLabel("INROOM_THIS_ORDER_WILL_SERVE"));
		willServeMinusBtn = (Button) findViewById(R.id.orderMinusBtn);
		willServeMinusBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(isDeliveryBoxShown) {
					return;
				}
				updateWillServeCount(-1);
			}
		});
		willServePlusBtn = (Button) findViewById(R.id.orderPlusBtn);
		willServePlusBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if(isDeliveryBoxShown) {
					return;
				}
				updateWillServeCount(1);
			}
		});
		orderWillServeQtyText = (MyTextView) findViewById(R.id.orderQtyText);
		optionFragmentContainer = (FrameLayout) findViewById(R.id.optionFragmentContainer);
		emptyListText = (MyTextView) findViewById(R.id.emptyList);
		emptyListText.setText(MainApplication.getLabel("INROOM_ORDER_EMPTY"));
		listView = (ListView) findViewById(R.id.list);
		listView.setEmptyView(emptyListText);

		mAdapter = new FoodOrderConfirmAdapter(this, orderDetail);
		mAdapter.setCallback(this);
		listView.setAdapter(mAdapter);

		updateWillServeCount(0);

		submitBtn = (Button) findViewById(R.id.submitBtn);
		Helper.setAppFonts(submitBtn);
		submitBtn.setText(MainApplication.getLabel("Submit"));
		submitBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				deliveryTimeBox.setVisibility(View.VISIBLE);
				isDeliveryBoxShown = true;
			}
			
//			@Override
//			public void onClick(View v) {
//				messageDialogType = DIALOG_TYPE_CONFIRM;
//				messageDialogTitle.setText(MainApplication.getLabel("INROOM_ORDER_CONFIRM_TITLE"));
//				messageDialogLabel.setText(Html.fromHtml(buildConfirmLabel()));
//				messageDialogCancelBtn.setVisibility(View.VISIBLE);
//				messageDialogConfirmBtn.setText(MainApplication.getLabel("Confirm"));
//				messageDialogLayout.setVisibility(View.VISIBLE);
//
//			}
		});
		errorMessageDialogLayout = (LinearLayout) findViewById(R.id.errorMessageDialogLayout);
		errorMessageDialogCloseImage = (ImageView) findViewById(R.id.errorMessageDialogCloseImage);
		errorMessageDialogCloseImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				errorMessageDialogLayout.setVisibility(View.GONE);
				onBackPressed();
			}
		});
		errorMessageDialogLabel = (MyTextView) findViewById(R.id.errorMessageDialogLabel);
		errorMessageDialogLabel.setText(MainApplication.getLabel("INROOM_ORDER_SUBMIT_OK"));
		messageDialogLayout = (LinearLayout) findViewById(R.id.messageDialogLayout);
		messageDialogCloseImage = (ImageView) findViewById(R.id.messageDialogCloseImage);
		messageDialogCloseImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				messageDialogLayout.setVisibility(View.GONE);
			}
		});
		messageDialogTitle = (MyTextView) findViewById(R.id.messageDialogTitle);
		messageDialogTitle.setText(MainApplication.getLabel("INROOM_ORDER_CONFIRM_TITLE"));
		messageDialogLabel = (MyTextView) findViewById(R.id.messageDialogLabel);
		messageDialogConfirmBtn = (Button) findViewById(R.id.messageDialogConfirmBtn);
		messageDialogConfirmBtn.setText(MainApplication.getLabel("Confirm"));
		messageDialogConfirmBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				messageDialogLayout.setVisibility(View.GONE);
				if (messageDialogType == DIALOG_TYPE_CONFIRM) {
					new OrderProcessor().execute();
				}
			}
		});
		Helper.setAppFonts(messageDialogConfirmBtn);
		messageDialogCancelBtn = (Button) findViewById(R.id.messageDialogCancelBtn);
		messageDialogCancelBtn.setText(MainApplication.getLabel("Cancel"));
		messageDialogCancelBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				messageDialogLayout.setVisibility(View.GONE);
			}
		});
		Helper.setAppFonts(messageDialogCancelBtn);
		 
		deliveryTimeBox = (RelativeLayout) findViewById(R.id.deliveryTimeBox);
		
		deliveryTimeBox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				//to override other background onclick
			}
		});
		
		selectDeliveryTime = (TextView) findViewById(R.id.selectDeliveryTime);
		selectDeliveryTime.setText(MainApplication.getLabel("INROOM_ORDER_TIME_SELECT"));
		notAvailableText = (TextView) findViewById(R.id.notAvailableText);
		notAvailableText.setText(MainApplication.getLabel("INROOM_ORDER_TIME_NA"));
		notAvailableText.setVisibility(View.INVISIBLE);
		dayHeaderTxt = (TextView) findViewById(R.id.dayHeaderText);
		dayHeaderTxt.setText(MainApplication.getLabel("INROOM_ORDER_DAY"));
		hourHeaderTxt = (TextView) findViewById(R.id.hourHeaderTxt);
		hourHeaderTxt.setText(MainApplication.getLabel("INROOM_ORDER_HOUR"));
		minHeaderTxt = (TextView) findViewById(R.id.minHeaderTxt);
		minHeaderTxt.setText(MainApplication.getLabel("INROOM_ORDER_MINUTE"));
		dayTxt = (TextView) findViewById(R.id.dayText);
		hourTxt = (TextView) findViewById(R.id.hourTxt);
		minTxt = (TextView) findViewById(R.id.minTxt);
		
		timeBoxCloseBtn = (Button) findViewById(R.id.timeBoxCloseBtn);
		timeBoxCloseBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				deliveryTimeBox.setVisibility(View.INVISIBLE);
				isDeliveryBoxShown = false;
			}
		});
		
		
		timeBoxConfirmBtn = (Button) findViewById(R.id.timeBoxConfirmBtn);
		Helper.setAppFonts(timeBoxConfirmBtn);
		timeBoxConfirmBtn.setText(MainApplication.getLabel("Order Item"));
		timeBoxConfirmBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				deliveryTimeBox.setVisibility(View.INVISIBLE);
				isDeliveryBoxShown = false;
				
				for(FoodOrder foodorder : FoodOrderData.getInstance().getFoodOrder()) {
					foodorder.setDelivery(deliveryDay, deliveryHour, deliveryMin);
				}
			
				messageDialogType = DIALOG_TYPE_CONFIRM;
				messageDialogTitle.setText(MainApplication.getLabel("INROOM_ORDER_CONFIRM_TITLE"));
				messageDialogLabel.setText(Html.fromHtml(buildConfirmLabel()));
				messageDialogCancelBtn.setVisibility(View.VISIBLE);
				messageDialogConfirmBtn.setText(MainApplication.getLabel("Confirm"));
				messageDialogLayout.setVisibility(View.VISIBLE);
			}
		});
		
		dayUpBtn = (ImageButton) findViewById(R.id.dayUpBtn);
		dayUpBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeDay(1 - deliveryDay);
			}
		});
		dayDownBtn = (ImageButton) findViewById(R.id.dayDownBtn);
		dayDownBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeDay(1 - deliveryDay);
			}
		});
		hourUpBtn = (ImageButton) findViewById(R.id.hourUpBtn);
		hourUpBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeTime(-1, 0);
			}
		});
		hourDownBtn = (ImageButton) findViewById(R.id.hourDownBtn);
		hourDownBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeTime(1, 0);
			}
		});
		minUpBtn = (ImageButton) findViewById(R.id.minUpBtn);
		minUpBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeTime(0, -10);
			}
		});
		minDownBtn = (ImageButton) findViewById(R.id.minDownBtn);
		minDownBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeTime(0, 10);
			}
		});
        setDefaltOrderTime();
	}

	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mDialog.dismiss();
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}

	@Override
	public void postExecute(String json) {
		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);
		hideDialog();
	}

	@Override
	public void onEdit(int position, int day, int hour, int min) {
		
		if(isDeliveryBoxShown) {
			return;
		}
		optionFragmentContainer.setVisibility(View.VISIBLE);
		Fragment fragment = InRoomDinOrderFragment.newInstance(position, day, hour, min, this);
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(R.id.optionFragmentContainer, fragment);
		ft.commit();
	}

	@Override
	public void onDelete(int position) {
		
		if(isDeliveryBoxShown) {
			return;
		}
		FoodOrderData.getInstance().getFoodOrder().remove(position);
		onPlaceOrder();
	}

	@Override
	public void onClose(Fragment fragment) {
		optionFragmentContainer.setVisibility(View.GONE);
		getSupportFragmentManager().beginTransaction().remove(fragment).commit();
	}

	@Override
	public void onPlaceOrder() {
		mAdapter.notifyDataSetChanged();
		if (mAdapter.getCount() == 0) {
			orderBar.setVisibility(View.INVISIBLE);
		} else {
			orderBar.setVisibility(View.VISIBLE);
		}
	}

	public void updateWillServeCount(int delta) {
		orderWillServe += delta;
		if (orderWillServe < 1) {
			orderWillServe = 1;
		}
		if (orderWillServe > 9) {
			orderWillServe = 9;
		}
		orderWillServeQtyText.setText(Integer.toString(orderWillServe));
	}

	@Override
	public void onTimeConfirm(Fragment fragment, int day, int hour, int min) {
		optionFragmentContainer.setVisibility(View.GONE);
		if (fragment != null) {
			getSupportFragmentManager().beginTransaction().remove(fragment).commit();
		}
		mAdapter.notifyDataSetChanged();
	}


	private int[] getDefaultOrderTime() {
		int currentHour = 0, currentMin = 0;
		String tmpHeadClock[] = MainApplication.getMAS().getData("data_curtime").split(":");
		if (tmpHeadClock.length == 2) {
			currentHour = Integer.parseInt(tmpHeadClock[0]);
			currentMin = Integer.parseInt(tmpHeadClock[1]);
		} else {
			currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			currentMin = Calendar.getInstance().get(Calendar.MINUTE);
		}
		currentMin += 30;
		if (currentMin % 10 != 0) {
			currentMin = (currentMin / 10 + 1) * 10;
		}
		if (currentMin >= 60) {
			currentMin -= 60;
			currentHour++;
		}
		if (currentHour > 23) {
			currentHour = 0;
		}
		return new int[] { currentHour, currentMin };
	}

	private String buildConfirmLabel() {
		StringBuffer sb = new StringBuffer();
		sb.append(MainApplication.getLabel("INROOM_ORDER_CONFIRM_TEXT"));
		sb.append("<br/><br/>");
		for (FoodOrder item : orderDetail) {
			sb.append("<font face=\"monospace\">");
			sb.append(item.getOrderQty()).append("</font> ");
			sb.append(MainApplication.getLabel(item.getFood().getItem().getTitleId()));
			ArrayList<String> optionItemName = item.getFood().getCheckedItems("");
			if (optionItemName.size() > 0) {
				sb.append(" (");
				for (int i = 0; i < optionItemName.size(); i++) {
					if (i > 0) {
						sb.append("<br/>");
					}
					sb.append(optionItemName.get(i));
				}
				sb.append(")");
			}
			sb.append("<br/>");
			sb.append(MainApplication.getLabel("Delivery Time") + " - ");
			sb.append(item.getDeliveryTimeStr());
			sb.append("<br/>");
		}
		sb.append("<br/>");
		return sb.toString();
	}

	class OrderProcessor extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			submitBtn.setEnabled(false);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			String roomId = MainApplication.getMAS().getData("data_myroom");
			if (roomId == null || "".equals(roomId)) {
				roomId = "9999"; // Test only
			}

			String orderTime = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime());
			String curdate = MainApplication.getMAS().getData("data_curdate");
			String curtime = MainApplication.getMAS().getData("data_curtime");
			if (!"".equals(curdate) && !"".equals(curtime)) {
				long diffInMs = Calendar.getInstance().getTime().getTime() - MASComm.data_last_update.getTime();
				long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMs);
				if (diffInSec <= 600) {
					orderTime = curdate + " " + curtime;
				} else {

					String dtStart = curdate + " " + curtime;
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					try {
						Date date = format.parse(dtStart);

						Calendar calendar = Calendar.getInstance();
						calendar.setTime(date);
						calendar.add(Calendar.SECOND, Long.valueOf(diffInSec).intValue());

						orderTime = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(calendar.getTime());
					} catch (ParseException e) {
						e.printStackTrace();
					}
				}
			}

			for (FoodOrder item : orderDetail) {
				String foodIdListStr = item.getFood().getItem().getItemId();
				ArrayList<String> foodIdList = new ArrayList<>();
				for (GeneralItemWrapper i : item.getFood().getOptionSet()) {
					foodIdList.addAll(i.buildFoodIdString(""));
				}
				for (String s : foodIdList) {
					foodIdListStr += "," + s;
				}
				
				/*
				for (GeneralItemWrapper option : item.getOptions()) {
					int qty = option.getQuantity();
					if (qty == 0) {
						qty = 1;
					}
					foodIdList = foodIdList + "," + option.getItem().getItemId() + ":" + qty;
				}
				*/
				String urlString = MainApplication.getCmsApiBase();
				try {
					urlString += "ird/submitOrder.php?";
					urlString += "room=" + URLEncoder.encode(roomId, "UTF-8");
					urlString += "&foodIdList=" + URLEncoder.encode(foodIdListStr, "UTF-8");
					urlString += "&orderTime=" + URLEncoder.encode(orderTime, "UTF-8");
					urlString += "&deliveryTime=" + URLEncoder.encode(item.getDeliveryTimeStr(), "UTF-8");
					urlString += "&numOfGuest=" + URLEncoder.encode(Integer.toString(orderWillServe), "UTF-8");
					urlString += "&quantity=" + URLEncoder.encode(Integer.toString(item.getOrderQty()), "UTF-8");
				} catch (UnsupportedEncodingException e1) {
					e1.printStackTrace();
				}
				URL url = null;
				Log.d("SubmitOrder", urlString);
				HttpURLConnection urlConnection = null;

				try {
					url = new URL(urlString);
					urlConnection = (HttpURLConnection) url.openConnection();
					InputStream in = new BufferedInputStream(urlConnection.getInputStream());
					String response = "";
					int c = in.read();
					while (c != -1) {
						response += (char) c;
						c = in.read();
					}
					Log.d("SubmitOrder", response);
					in.close();
					JSONObject responseJson = new JSONObject(response);
					if (responseJson.getInt("status") != 1) {
						return false;
					}
					final Map<String, String> map = new HashMap<String, String>();
					map.put("Room", MainApplication.getMAS().getData("data_myroom"));
					map.put("DeviceTime", new SimpleDateFormat("yyyy-MM-dd HH:mm").format(Calendar.getInstance().getTime()));
					map.put("ServerTime", curdate + " " + curtime);
					map.put("OrderItems", URLEncoder.encode(foodIdListStr, "UTF-8"));
					FlurryAgent.logEvent("IRD-Order", map);
				} catch (MalformedURLException e) {
					e.printStackTrace();
					return false;
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				} catch (JSONException e) {
					e.printStackTrace();
					return false;
				} finally {
					if (urlConnection != null) {
						urlConnection.disconnect();
					}
				}
			}
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			submitBtn.setEnabled(true);
			if (result) {
				sendHotsosCommand();
				orderDetail.clear();
				mAdapter.notifyDataSetChanged();
				errorMessageDialogLabel.setText(MainApplication.getLabel("INROOM_ORDER_SUBMIT_OK"));
				
				// for Flurry log
				final Map<String, String> map = new HashMap<String, String>();
				map.put("Room", MainApplication.getMAS().getData("data_myroom"));
				FlurryAgent.logEvent("IRD-OrderMade", map);
				
			} else {
				errorMessageDialogLabel.setText(MainApplication.getLabel("INROOM_ORDER_SUBMIT_ERROR"));
			}
			errorMessageDialogLayout.setVisibility(View.VISIBLE);
		}
	}

	private void sendHotsosCommand() {
		AsyncHttpClient client = new AsyncHttpClient();
		client.setMaxRetriesAndTimeout(0, 10000);
    
		String url = "http://"+MainApplication.getMAS().getMASPath()+"/mas.php?cmd=SET%20"+"ROOMSERVICE" +"%20ON";
    
		Log.i(TAG, "URL = " + url);
    
		client.get(url, new AsyncHttpResponseHandler() {

	        @Override
	        public void onStart() {
	            // called before request is started
	        }
	        
	        @Override
	        public void onSuccess(int arg0, Header[] headers, byte[] response) {
	        	String s = new String(response);                                               
	            Log.i(TAG,"success response = " +  s);
	        }
        
	        @Override
            public void onFailure(int arg0, Header[] headers, byte[] response, Throwable e) {
                                    
                if(response != null){
                	String s = new String(response);               
                    Log.i(TAG, s);
                }
                                
                Log.i(TAG, "Error = " + e.toString());
	        }

	        @Override
	        public void onRetry(int retryNo) {
	        	// called when request is retried
	        	Log.i(TAG,"retrying number = " + retryNo);
            }
		});
	}
	
	
	private void setDefaltOrderTime() {
		deliveryDay = 0;
		String tmpHeadClock[] = MainApplication.getMAS().getData("data_curtime").split(":");
		if (tmpHeadClock.length == 2) {
			deliveryHour = Integer.parseInt(tmpHeadClock[0]);
			deliveryMin = Integer.parseInt(tmpHeadClock[1]);
		} else {
			deliveryHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
			deliveryMin = Calendar.getInstance().get(Calendar.MINUTE);
		}
		
		// First default: 30 minutes after current time
		deliveryMin += 30;
		if (deliveryMin % 10 != 0) {
			deliveryMin = (deliveryMin / 10 + 1) * 10;
		}
		if (deliveryMin >= 60) {
			deliveryMin -= 60;
			deliveryHour++;
		}
		if (deliveryHour > 23) {
			deliveryHour = 0;
		}
		
		if (!isTimeValidForFood(deliveryHour, deliveryMin)) {
			// Second default: Earliest start time of the food item
			String[] timeParts = FoodOrderData.getInstance().getStartTime().split(":");
			if (timeParts.length >= 2) {
				deliveryHour = Integer.parseInt(timeParts[0]);
				deliveryMin = Integer.parseInt(timeParts[1]);
				if (!isTimeAfterCurrentPlus30Mins(deliveryDay, deliveryHour, deliveryMin)) {
					deliveryDay = 1;
				}
			}
		}
		changeDay(deliveryDay);
		changeTime(0, 0);
	}
	
	private void changeDay(int newDay) {
		deliveryDay = newDay;
		if (deliveryDay == 0) {
			dayTxt.setText(MainApplication.getLabel("Today"));
		} else if (deliveryDay == 1) {
			dayTxt.setText(MainApplication.getLabel("Tomorrow"));
		}
		enableDeliveryTimeConfirmButton(
				isTimeAfterCurrentPlus30Mins(deliveryDay, deliveryHour, deliveryMin) && 
				isTimeValidForFood(deliveryHour, deliveryMin));
	}
	
	private void changeTime(int hourDelta, int minDelta) {
		deliveryHour += hourDelta;
		deliveryMin += minDelta;
		if (deliveryHour < 0) {
			deliveryHour = 23;
		}
		if (deliveryHour > 23) {
			deliveryHour = 0;
		}
		if (deliveryMin < 0) {
			deliveryMin = 50;
		}
		if (deliveryMin >= 60) {
			deliveryMin = 0;
		}
		hourTxt.setText(String.format("%02d", deliveryHour));
		minTxt.setText(String.format("%02d", deliveryMin));
		enableDeliveryTimeConfirmButton(
				isTimeAfterCurrentPlus30Mins(deliveryDay, deliveryHour, deliveryMin) && 
				isTimeValidForFood(deliveryHour, deliveryMin));
	}
	
	private boolean isTimeAfterCurrentPlus30Mins(int day, int hour, int min) {
		int checkTime = 0;
		String tmpHeadClock[] = MainApplication.getMAS()
				.getData("data_curtime").split(":");
		if (tmpHeadClock.length == 2) {
			checkTime = Integer.parseInt(tmpHeadClock[0]) * 60 + Integer.parseInt(tmpHeadClock[1]) + 30; 
		} else {
			Calendar c = Calendar.getInstance();
			checkTime = c.get(Calendar.HOUR_OF_DAY) * 60 + c.get(Calendar.MINUTE) + 30;
		}
		return checkTime < day * 1440 + hour * 60 + min;
	}
	
	private boolean isTimeValidForFood(int hour, int min) {
		
		int startEndTimes[][];

		if(FoodOrderData.getInstance().getStartTime().equals("0:00") && FoodOrderData.getInstance().getEndTime().equals("0:00")) {
			return true;
		}

		if ((FoodOrderData.getInstance().getStartTime2() != null && !"0:00".equals(FoodOrderData.getInstance().getStartTime2())) || 
				(FoodOrderData.getInstance().getEndTime2() != null && !"0:00".equals(FoodOrderData.getInstance().getEndTime2()))) {
			// Two available time slots
			startEndTimes = new int[2][2];
			startEndTimes[0][0] = stringTimeToInt(FoodOrderData.getInstance().getStartTime());
			startEndTimes[0][1] = stringTimeToInt(FoodOrderData.getInstance().getEndTime());
			startEndTimes[1][0] = stringTimeToInt(FoodOrderData.getInstance().getStartTime2());
			startEndTimes[1][1] = stringTimeToInt(FoodOrderData.getInstance().getEndTime2());
		} else {
			startEndTimes = new int[1][2];
			startEndTimes[0][0] = stringTimeToInt(FoodOrderData.getInstance().getStartTime());
			startEndTimes[0][1] = stringTimeToInt(FoodOrderData.getInstance().getEndTime());
		}
		for (int i = 0; i < startEndTimes.length; i++) {
			int foodTime = hour * 60 + min;
			int startTimeInt = startEndTimes[i][0];
			int endTimeInt = startEndTimes[i][1];
			
			if (endTimeInt > startTimeInt) {
				if (foodTime >= startTimeInt && foodTime <= endTimeInt) {
					return true;
				}
			} else {
				if  ((foodTime >= startTimeInt && foodTime <= 1440) || 
						(foodTime <= endTimeInt)) {
					return true;
				}
			}
		}
		return false;
	}
	
	private void enableDeliveryTimeConfirmButton(boolean enable) {
		notAvailableText.setVisibility(enable ? View.INVISIBLE : View.VISIBLE);
		timeBoxConfirmBtn.setEnabled(enable);
	}
	
	private int stringTimeToInt(String time) {
		String[] timeParts = time.split(":");
		if (timeParts.length >= 2) {
			return Integer.parseInt(timeParts[0]) * 60 + Integer.parseInt(timeParts[1]); 
		} else {
			return -1;
		}
	}

	@Override
	protected void onLanguageChanged(String language) {
		initUI();
	}
	
}
