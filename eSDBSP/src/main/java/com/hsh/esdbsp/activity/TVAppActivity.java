package com.hsh.esdbsp.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.http.Header;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.widget.Command;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class TVAppActivity extends BaseActivity {

    private ImageView airplay, ipTV, netflix, spotify, youtube, googleplay, ted, weather;

    boolean isChormeCast = false;

    Context context;

    Button powerBtn, offBtn, arrowUpBtn, backBtn, arrowDownBtn, arrowLeftBtn, arrowRightBtn, okBtn, movieBtn, ccBtn, menuBtn;
    Button chMinusBtn, chPlusBtn, soundMinusBtn, soundMuteBtn, soundPlusBtn;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.tv_app);
        initUI();
        loadTVFixItem();

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("titleId", "TV/AUDIO");
        bundle.putString("hightLightChoice", "TV Apps");
        bundle.putInt("choice", 1);
        initTopBar(bundle);
        initBottomBar();
    }

    private void initUI() {
        airplay = (ImageView) findViewById(R.id.airplay);
        airplay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChormeCast) {
                    isChormeCast = false;
                    Command.TVApp.Item(Command.STBItem.AIRPLAY);
                } else {
                    isChormeCast = true;
                    Command.TVApp.Item(Command.STBItem.CHORMECAST);
                }
            }
        });

        ipTV = (ImageView) findViewById(R.id.ipTV);
        ipTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Command.TVApp.Item(Command.STBItem.IPTV);
            }
        });

        netflix = (ImageView) findViewById(R.id.netflix);
        netflix.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Command.TVApp.Item(Command.STBItem.NETFLIX);
            }
        });

        spotify = (ImageView) findViewById(R.id.spotify);
        spotify.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Command.TVApp.Item(Command.STBItem.SPOTIFY);
            }
        });

        youtube = (ImageView) findViewById(R.id.youtube);
        youtube.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Command.TVApp.Item(Command.STBItem.YOUTUBE);
            }
        });

        googleplay = (ImageView) findViewById(R.id.googleplay);
        googleplay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Command.TVApp.Item(Command.STBItem.GOOGLEPLAY);
            }
        });

        ted = (ImageView) findViewById(R.id.ted);
        ted.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Command.TVApp.Item(Command.STBItem.TED);
            }
        });

        weather = (ImageView) findViewById(R.id.weather);
        weather.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Command.TVApp.Item(Command.STBItem.WEATHER);
            }
        });
    }

    private void loadTVFixItem() {

        powerBtn = (Button) findViewById(R.id.on_btn);
        powerBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.ON);
            }
        });
        powerBtn.setVisibility(View.VISIBLE);

        offBtn = (Button) findViewById(R.id.offBtn);
        offBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.OFF);
            }
        });

        arrowUpBtn = (Button) findViewById(R.id.arrow_up);
        arrowUpBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.UP);
            }
        });

        arrowDownBtn = (Button) findViewById(R.id.arrow_down);
        arrowDownBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.DOWN);
            }
        });

        arrowLeftBtn = (Button) findViewById(R.id.arrow_left);
        arrowLeftBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.LEFT);
            }
        });

        arrowRightBtn = (Button) findViewById(R.id.arrow_right);
        arrowRightBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.RIGHT);
            }
        });

        ccBtn = (Button) findViewById(R.id.cc_button);
        ccBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.CC);
            }
        });

        if (GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            ccBtn.setBackgroundResource(R.drawable.yellow_button_selector);
        }

        menuBtn = (Button) findViewById(R.id.menu_button);
        menuBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.MENU);
            }
        });

        okBtn = (Button) findViewById(R.id.ok_button);
        okBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.OK);
            }
        });

        chMinusBtn = (Button) findViewById(R.id.ch_minus);
        chMinusBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.CHANNELDOWN);
            }
        });

        chPlusBtn = (Button) findViewById(R.id.ch_plus);
        chPlusBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.CHANNELUP);
            }
        });

        soundMinusBtn = (Button) findViewById(R.id.sound_minus);
        soundMinusBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.VOLUMNDOWN);
            }
        });

        soundMuteBtn = (Button) findViewById(R.id.sound_mute);
        soundMuteBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.MUTE);
            }
        });

        soundPlusBtn = (Button) findViewById(R.id.sound_plus);
        soundPlusBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TVApp.RemoteButton(Command.RemoteButton.VOLUMNUP);
            }
        });
    }
}