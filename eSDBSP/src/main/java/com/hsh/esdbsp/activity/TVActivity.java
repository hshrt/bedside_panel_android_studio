package com.hsh.esdbsp.activity;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.view.*;
import com.hsh.esdbsp.adapter.GridViewAdapter;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.widget.Command;

import java.util.ArrayList;

import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;


public class TVActivity extends BaseActivity {

    //-----------

    MyGridView gridView;
    GridViewAdapter customGridAdapter;

    int isTVon = 0;

    Button powerBtn, offBtn, arrowUpBtn, backBtn, arrowDownBtn, arrowLeftBtn, arrowRightBtn, okBtn, movieBtn, ccBtn, menuBtn;
    Button chMinusBtn, chPlusBtn, soundMinusBtn, soundMuteBtn, soundPlusBtn;

    Button playPauseBtn, stopBtn, rewindBtn, forwardBtn, lastBtn, nextBtn;

    LinearLayout control_layer_0_5, control_layer1, control_layer2, control_layer3;

    //////////////////////////////////////////////////////

    static Handler tvChannelHandler = new Handler();
    static int tvChannelDelay = 500;


//////////////////////////////////////////////////////////

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if(BuildConfig.FLAVOR.equalsIgnoreCase(Hotel.CHICAGO)){
            setContentView(R.layout.tv_all_pch);
        } else {
            setContentView(R.layout.tv_all);
        }

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("titleId", "TV/AUDIO");
        bundle.putString("hightLightChoice", "tv");
        bundle.putInt("choice", 0);
        initTopBar(bundle);
        initBottomBar();

        if (MainApplication.getTVGrid() == null) {
            new Thread(new Runnable() {
                public void run() {
                    loadPageItem();
                    setupTVControl();
                }
            }).start();
        } else {
            loadPageItem();
            setupTVControl();
        }
    }

    public void onDestroy() {
        super.onDestroy();

        if (gridView != null) {
            gridView.setAdapter(null);
        }
    }

    private void setupTVControl() {
        Log.i(TAG, "SetupTVCONTROL_" + GlobalValue.getInstance().getHotel());
        if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
            backBtn.setVisibility(View.VISIBLE);
            //movieBtn.setVisibility(View.GONE);
            offBtn.setVisibility(View.GONE);
            control_layer_0_5.setVisibility(View.GONE);
            control_layer1.setVisibility(View.GONE);
            control_layer2.setVisibility(View.GONE);
            control_layer3.setVisibility(View.GONE);
            ccBtn.setVisibility(View.GONE);
            menuBtn.setVisibility(View.GONE);
        }
    }


    private void loadPageItem() {
        MainApplication.brightUp();
        ///////////////////////////////////////////////////////////
        loadTVFixItem();
        tvChannelHandler.removeCallbacks(tvChannelRunnable);
        tvChannelHandler.postDelayed(tvChannelRunnable, tvChannelDelay);
    }


    private ArrayList getChannelData() {
        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();
        for (int i = 2; i <= 84; i++) {
            imageItems.add(getChannelDataSub(i));
        }
        return imageItems;
    }


    private ImageItem getChannelDataSub(int i) {
        ImageItem myIMG = null;
        String imageID;
        String j = "00";

        if (i < 10) {
            j = "0" + i;
        } else {
            j = "" + i;
        }

        imageID = "tv" + j + "_selector";
        int resID = getResources().getIdentifier(imageID, "drawable", getPackageName());
        myIMG = new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i);

        return myIMG;
    }


/////////////////////////////////////////////////////////////////////////////////////	


    private ArrayList getChannelDataPBJ() {

        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();

        for (int i = 1; i <= 39; i++) {
            String imageID;
            String j = "00";

            if (i < 10) {
                j = "0" + i;
            } else {
                j = "" + i;
            }

            imageID = "pbjtv" + j + "_selector";
            int resID = getResources().getIdentifier(imageID, "drawable", getPackageName());
            imageItems.add(new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i));
        }
        return imageItems;
    }


/////////////////////////////////////////////////////////////////////////////////////	


    private ArrayList getChannelDataPNY() {

        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();
        Log.i("TV", "get NY ICON fire");

        for (int i = 1; i <= 105; i++) {

            String imageID;
            String j = "00";

            if (i < 10) {
                j = "0" + i;
            } else {
                j = "" + i;
            }

            imageID = "pnytv" + j + "_selector";

            int resID = getResources().getIdentifier(imageID, "drawable", getPackageName());
            imageItems.add(new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i));

        }

        return imageItems;
    }


//////////////////////////////////////////////////////////////////////////////////////////


    private ArrayList getChannelDataPLNModel() {

        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();

        for (int i = 1; i <= 8; i++) {

            String imageID;
            String j = "00";

            if (i < 10) {
                j = "0" + i;
            } else {
                j = "" + i;
            }

            imageID = "plnmtv" + j + "_selector";

            int resID = getResources().getIdentifier(imageID, "drawable", getPackageName());
            imageItems.add(new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i));

        }
        return imageItems;
    }


//////////////////////////////////////////////////////////////////////////////////////////	


    private ArrayList getChannelDataPHK() {

        final ArrayList<ImageItem> imageItems = new ArrayList<ImageItem>();

        for (int i = 2; i <= 87; i++) {

            String imageID;
            String j = "00";

            if (i < 10) {
                j = "0" + i;
            } else {
                j = "" + i;
            }

            imageID = "phktv" + j + "_selector";

            int resID = getResources().getIdentifier(imageID, "drawable", getPackageName());
            imageItems.add(new ImageItem(resID, "Channel " + j, "SET%20TV%20" + i));
        }

        return imageItems;
    }


//////////////////////////////////////////////////////////////////////////////////////////	

    private void loadTVFixItem() {

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            powerBtn = (Button) findViewById(R.id.on_btn);
            powerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainApplication.playClickSound(v);
                    Command.TV.RemoteButton(Command.RemoteButton.ON);
                }
            });
        } else {
            powerBtn = (Button) findViewById(R.id.power_btn);
            powerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isTVon = (isTVon + 1) % 2;
                    MainApplication.playClickSound(v);
                    if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
                        Command.TV.RemoteButton(Command.RemoteButton.ON);
                    } else {
                        Command.TV.RemoteButton(Command.RemoteButton.OFF);
                    }
                }
            });
        }
        powerBtn.setVisibility(View.VISIBLE);

        offBtn = (Button) findViewById(R.id.offBtn);
        offBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.OFF);
            }
        });

        arrowUpBtn = (Button) findViewById(R.id.arrow_up);
        arrowUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.UP);
            }
        });

        arrowDownBtn = (Button) findViewById(R.id.arrow_down);
        arrowDownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.DOWN);
            }
        });

        arrowLeftBtn = (Button) findViewById(R.id.arrow_left);
        arrowLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.LEFT);
            }
        });

        arrowRightBtn = (Button) findViewById(R.id.arrow_right);
        arrowRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.RIGHT);
            }
        });

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            backBtn = (Button) findViewById(R.id.bottomBackBtn);
            backBtn.setVisibility(View.VISIBLE);
            backBtn.setText("Back");
            Helper.setAppFonts(backBtn);
        } else {
            backBtn = (Button) findViewById(R.id.back_btn);
        }
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.BACK);
            }
        });

        movieBtn = (Button) findViewById(R.id.movieBtn);
        movieBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.MOVIE);
            }
        });

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
            movieBtn.setBackgroundResource(R.drawable.base_button_selector);
            movieBtn.setText(MainApplication.getLabel("Movie"));
        }

        ccBtn = (Button) findViewById(R.id.cc_button);
        if (GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            ccBtn.setBackgroundResource(R.drawable.yellow_button_selector);
        }
        ccBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.CC);
            }
        });

        menuBtn = (Button) findViewById(R.id.menu_button);
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            menuBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainApplication.playClickSound(v);
                    Command.TV.RemoteButton(Command.RemoteButton.MENU);
                }
            });
        }

        okBtn = (Button) findViewById(R.id.ok_button);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.OK);
            }
        });

        chMinusBtn = (Button) findViewById(R.id.ch_minus);
        chMinusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.CHANNELDOWN);
            }
        });

        chPlusBtn = (Button) findViewById(R.id.ch_plus);
        chPlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.CHANNELUP);
            }
        });

        soundMinusBtn = (Button) findViewById(R.id.sound_minus);
        soundMinusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.VOLUMNDOWN);
            }
        });

        soundMuteBtn = (Button) findViewById(R.id.sound_mute);
        soundMuteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.MUTE);
            }
        });

        soundPlusBtn = (Button) findViewById(R.id.sound_plus);
        soundPlusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.TV.RemoteButton(Command.RemoteButton.VOLUMNUP);
            }
        });

        playPauseBtn = (Button) findViewById(R.id.playPauseBtn);
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            playPauseBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainApplication.playClickSound(v);
                    Command.TV.RemoteButton(Command.RemoteButton.PLAYPAUSE);
                }
            });
        }

        stopBtn = (Button) findViewById(R.id.stopBtn);
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            stopBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainApplication.playClickSound(v);
                    Command.TV.RemoteButton(Command.RemoteButton.STOP);
                }
            });
        }

        rewindBtn = (Button) findViewById(R.id.rewindBtn);
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            rewindBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainApplication.playClickSound(v);
                    Command.TV.RemoteButton(Command.RemoteButton.REWIND10);
                }
            });
        }

        forwardBtn = (Button) findViewById(R.id.forwardBtn);
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            forwardBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainApplication.playClickSound(v);
                    Command.TV.RemoteButton(Command.RemoteButton.FASTFORWARD10);
                }
            });
        }

        lastBtn = (Button) findViewById(R.id.lastBtn);
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            lastBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainApplication.playClickSound(v);
                    Command.TV.RemoteButton(Command.RemoteButton.LAST);
                }
            });
        }

        nextBtn = (Button) findViewById(R.id.nextBtn);
        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO) ||
                GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
            nextBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MainApplication.playClickSound(v);
                    Command.TV.RemoteButton(Command.RemoteButton.NEXT);
                }
            });
        }

        control_layer_0_5 = (LinearLayout) findViewById(R.id.control_layer_0_5);
        control_layer1 = (LinearLayout) findViewById(R.id.control_layer1);
        control_layer2 = (LinearLayout) findViewById(R.id.control_layer2);
        control_layer3 = (LinearLayout) findViewById(R.id.control_layer3);
    }


    private void loadTVItem() {

        gridView = (MyGridView) findViewById(R.id.gridView);

        if (MainApplication.getTVGrid() == null) {
            if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {
                customGridAdapter = new GridViewAdapter(this, R.layout.tv_channel, getChannelData());
//                MainApplication.setTVGrid(customGridAdapter);
            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.BEIJING)) {
                customGridAdapter = new GridViewAdapter(this, R.layout.tv_channel, getChannelDataPBJ());
//                MainApplication.setTVGrid(customGridAdapter);
            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                customGridAdapter = new GridViewAdapter(this, R.layout.tv_channel, getChannelDataPHK());
//                MainApplication.setTVGrid(customGridAdapter);
            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.NEWYORK)) {
                customGridAdapter = new GridViewAdapter(this, R.layout.tv_channel, getChannelDataPNY());
//                MainApplication.setTVGrid(customGridAdapter);
            } else if (GlobalValue.getInstance().getHotel().equals(Hotel.LONDON)) {
                customGridAdapter = new GridViewAdapter(this, R.layout.tv_channel, getChannelDataPLNModel());
//                MainApplication.setTVGrid(customGridAdapter);
            }
        } else {
            customGridAdapter = MainApplication.getTVGrid();
        }

        gridView.setAdapter(customGridAdapter);

        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                ImageItem item = (ImageItem) MainApplication.getRadio().getTVData().get(position);
                Command.Other.DirectCommand(item.getCmd(), false);
            }

        });
    }

    private Runnable tvChannelRunnable = new Runnable() {
        public void run() {
            tvChannelHandler.removeCallbacks(tvChannelRunnable);
            loadTVItem();
        }
    };

}
