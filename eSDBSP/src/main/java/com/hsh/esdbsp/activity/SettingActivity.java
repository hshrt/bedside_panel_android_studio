
package com.hsh.esdbsp.activity;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings.Secure;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.Hotel;
import com.nostra13.universalimageloader.core.ImageLoader;

import pub.devrel.easypermissions.EasyPermissions;


public class SettingActivity extends BaseActivity {


    public static final int RC_PHONE_STATUS_PERM = 123;

    private TextView deviceIdText;
    private TextView versionText;
    private TextView macAddressText;
    private TextView buildText;
    private TextView mainTenText;
    private Button clearAllButton;
    private Button clearTextButton;
    private Button clearImageMemoryButton;
    private Button clearImageDiskButton;
    private Button housekeepingButton;
    private Button backBtn;
    private CheckBox checkbox_enable_log;
    private CheckBox checkbox_enable_curtain;
    private CheckBox checkbox_enable_noac;

    //-----------

    LinearLayout layoutPage;

    private TopBarFragment topBarFragment;

    //////////////////////////////////////////////////////

    // tester part

    TextView deviceWiFiSSID;
    TextView deviceWiFiSignalStrenght;
    TextView deviceIP;
    TextView mas100;
    TextView mas101;
    TextView mas102;
    TextView mas103;
    TextView mas104;
    TextView cms;
    TextView debugDateTime;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);

        // by pass the async send cmd
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Helper.showInternetWarningToast();
        initUI();
    }

    private void initUI() {

        checkbox_enable_log = (CheckBox) findViewById(R.id.checkbox_enable_log);
        boolean enableLog = settings.getBoolean("enableLog", false);
        checkbox_enable_log.setChecked(enableLog);

        checkbox_enable_curtain = (CheckBox) findViewById(R.id.checkbox_enable_curtain);
        boolean hasCurtain = settings.getBoolean("hasCurtain", false);
        checkbox_enable_curtain.setChecked(hasCurtain);

        checkbox_enable_noac = (CheckBox) findViewById(R.id.checkbox_enable_noac);
        boolean noAC = settings.getBoolean("noAC", false);
        checkbox_enable_noac.setChecked(noAC);

        deviceIdText = (TextView) findViewById(R.id.deviceIdText);

        TelephonyManager mngr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

        String device_unique_id = Secure.getString(this.getContentResolver(), Secure.ANDROID_ID);

        deviceIdText.setText("DeviceId: " + device_unique_id.toUpperCase());

        versionText = (TextView) findViewById(R.id.versionText);

        String versionName = MainApplication.getAppVersion();
        versionText.setText("App Version: " + versionName);

        macAddressText = (TextView) findViewById(R.id.macAddressText);
        macAddressText.setText("MAC Address: " + Helper.getMacAddress(this));

        buildText = (TextView) findViewById(R.id.buildText);
        Date buildDate = new Date(BuildConfig.BUILDTIME);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String buildDateString = df.format(buildDate);
        buildText.setText("Build Date: " + buildDateString);

        mainTenText = (TextView) findViewById(R.id.maintenText);
        clearAllButton = (Button) findViewById(R.id.clearAllButton);
        clearAllButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(TAG, "clearAllButton clicked");
                ImageLoader.getInstance().clearMemoryCache();
                ImageLoader.getInstance().clearDiskCache();
                settings.edit().clear().commit();
            }
        });

        clearTextButton = (Button) findViewById(R.id.clearTextButton);
        clearTextButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(TAG, "clearTextButton clicked");
                settings.edit().putLong("lastDictUpdateTime", 0).commit();
            }
        });

        clearImageMemoryButton = (Button) findViewById(R.id.clearImageMemoryButton);
        clearImageMemoryButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(TAG, "clearAllButton clicked");
                ImageLoader.getInstance().clearMemoryCache();
            }
        });

        clearImageDiskButton = (Button) findViewById(R.id.clearImageDiskButton);
        clearImageDiskButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(TAG, "clearAllButton clicked");
                ImageLoader.getInstance().clearDiskCache();
                settings.edit().remove("lastMediaUpdateTime").commit();
            }
        });

        housekeepingButton = (Button) findViewById(R.id.housekeepingButton);
        housekeepingButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(TAG, "housekeepingButton clicked");
                Intent intent = new Intent(MainApplication.getContext(),
                        HousekeepingModeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                MainApplication.getCurrentActivity().startActivity(intent);
                SettingActivity.this.finish();
            }
        });

        backBtn = (Button) findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Log.i(TAG, "backBtn clicked");
                settings.edit().putBoolean("enableLog", checkbox_enable_log.isChecked()).commit();
                settings.edit().putBoolean("hasCurtain", checkbox_enable_curtain.isChecked()).commit();
                settings.edit().putBoolean("noAC", checkbox_enable_noac.isChecked()).commit();

                DataCacheManager.getInstance().setIsCacheInited(false);
//                DataCacheManager.getInstance().startLoadData();

                Intent intent = new Intent(MainApplication.getContext(), RoomControlActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                MainApplication.getCurrentActivity().startActivity(intent);
                SettingActivity.this.finish();
            }
        });

        Helper.setAppFonts(versionText);
        Helper.setAppFonts(macAddressText);
        Helper.setAppFonts(mainTenText);
        Helper.setAppFonts(buildText);
        Helper.setAppFonts(clearAllButton);
        Helper.setAppFonts(clearTextButton);
        Helper.setAppFonts(clearImageMemoryButton);
        Helper.setAppFonts(clearImageDiskButton);
        Helper.setAppFonts(housekeepingButton);
        Helper.setAppFonts(deviceIdText);

        Log.i(TAG, "initUI end");

        ///////////////////////

        if (GlobalValue.getInstance().getHotel().equals(Hotel.CHICAGO)) {

            deviceWiFiSSID = (TextView) findViewById(R.id.deviceWiFiSSID);
            deviceWiFiSSID.setText("SSID: " + getCurrentSsid() + "[" + getCurrentSsidID() + "]");

            deviceWiFiSignalStrenght = (TextView) findViewById(R.id.deviceWiFiSignalStrenght);
            deviceWiFiSignalStrenght.setText("Signal Strenght: " + getSignalStrenght());

            deviceIP = (TextView) findViewById(R.id.deviceIP);
            deviceIP.setText("IP: " + getIpAddrFull());

            ///////////////

            mas100 = (TextView) findViewById(R.id.mas100);
            mas100.setText("mas 100:" + testMAS(100));

            mas101 = (TextView) findViewById(R.id.mas101);
            mas101.setText("mas 101:" + testMAS(101));

            mas102 = (TextView) findViewById(R.id.mas102);
            mas102.setText("mas 102:" + testMAS(102));

            mas103 = (TextView) findViewById(R.id.mas103);
            mas103.setText("mas 103:" + testMAS(103));

            mas104 = (TextView) findViewById(R.id.mas104);
            mas104.setText("mas 104:" + testMAS(104));


        }

        cms = (TextView) findViewById(R.id.cmslink);
        cms.setText("cms:" + testCMS());

    }

    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();
        settings.edit().putBoolean("enableLog", checked).commit();
    }


//////////////////////////////////////////////////////////

    public String getMacAddress(Context context) {
        WifiManager wimanager = (WifiManager) MainApplication.getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        String macAddress = wimanager.getConnectionInfo().getMacAddress();
        if (macAddress == null) {
            macAddress = "Device don't have mac address or wi-fi is disabled";
        }
        return macAddress;
    }

//////////////////////////////////////////////////////////

    // added for wifi and connectivity
    private int getCurrentSsidID() {
        String ssid = null;
        WifiManager wifiManager = (WifiManager) MainApplication.getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        return wifiManager.getConnectionInfo().getNetworkId();
    }

    private String getCurrentSsid() {
        String ssid = null;

        ConnectivityManager connManager = (ConnectivityManager) MainApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            final WifiManager wifiManager = (WifiManager) MainApplication.getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
                ssid = connectionInfo.getSSID();

            }
        }
        return ssid;
    }


    private int getSignalStrenght() {

        int signalStrenght = 0;
        ConnectivityManager connManager = (ConnectivityManager) MainApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (networkInfo.isConnected()) {
            final WifiManager wifiManager = (WifiManager) MainApplication.getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
            if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {

                signalStrenght = connectionInfo.getRssi();
            }
        }
        return signalStrenght;
    }


    private String getIpAddrFull() {
        WifiManager wifiManager = (WifiManager) MainApplication.getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        String ipString = String.format("%d.%d.%d.%d", (ip & 0xff), (ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));
        return ipString;
    }


    private String getIpAddr() {
        WifiManager wifiManager = (WifiManager) MainApplication.getContext().getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ip = wifiInfo.getIpAddress();
        String ipString = String.format("%d.%d.%d.", (ip & 0xff), (ip >> 8 & 0xff), (ip >> 16 & 0xff), (ip >> 24 & 0xff));
        return ipString;
    }


    //////////////////////////

    private String specialSend(String url) {
        InputStream inputStream = null;
        String result = "";

        try {

            // create HttpClient
            HttpClient httpclient = new DefaultHttpClient();

            // make GET request to the given URL
            HttpResponse httpResponse = httpclient.execute(new HttpGet(url));

            // receive response as inputStream
            inputStream = httpResponse.getEntity().getContent();


            BufferedReader in = new BufferedReader(new InputStreamReader(
                    inputStream));


            String line = "";
            String fullSTA = "";
            while ((line = in.readLine()) != null) {
                fullSTA = fullSTA + line;
            }

            result = fullSTA;

        } catch (Exception e) {
            return result;
        }

        return result;

    }


    private String testMAS(int ip) {

        String url1 = "http://" + getIpAddr() + "" + ip + "/mas.php?cmd=GET%20STATUS";
        String url2 = "http://" + getIpAddr() + "" + ip + ":9111/rcpu.cgi?cmd=H0";

        String chkMAS = (specialSend(url1).length() > 3 ? "OK" : "FAIL");
        String chkCPU = (specialSend(url2).length() > 3 ? "OK" : "FAIL");

        //String chkCPU = specialSend(url2);

        return "[uplink:" + chkMAS + "]," + "[inroom-cpu:" + chkCPU + "]";

    }


    private String testCMS() {
        String url = MainApplication.getApiBase();
        String chkCMS = (specialSend(url).length() > 3 ? "OK" : "FAIL");
        return url + " - Status: " + chkCMS;
    }

}
