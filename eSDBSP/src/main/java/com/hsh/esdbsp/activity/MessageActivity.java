package com.hsh.esdbsp.activity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Html;
import android.text.format.Time;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.adapter.MessageItemAdapter;
import com.hsh.esdbsp.global.CheckMessageUpdateManager;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.MessageItem;
import com.hsh.esdbsp.parser.GetMessageParser;
import com.hsh.esdbsp.parser.GetMessageParser.GetMessageParserInterface;

public class MessageActivity extends BaseActivity implements GetMessageParserInterface {

	private ArrayList<MessageItem> items = new ArrayList<MessageItem>();
	private MessageItemAdapter adapter;
	private MyTextView messageSubjectText;
	private MyTextView messageTimeText;
	private TextView messageTextView;
	private TextView noMessageTextView;
	private ListView listView;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.cms_message);

		settings.edit().putBoolean("pending_fax", false).commit();

		String dateString = MainApplication.getMAS().getData("year") + "-"
				+ MainApplication.getMAS().getData("month") + "-"
				+ MainApplication.getMAS().getData("day") + " "
				+ MainApplication.getMAS().getData("hour") + ":"
				+ MainApplication.getMAS().getData("minute");
		
		Log.i(TAG, "dateString = " + dateString);

		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = null;
		try {
			date = format.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		if (date != null) {
			settings.edit().putLong("lastMessageVisitTime", date.getTime()).commit();
			Log.i(TAG, "stored UpdateTime =" + date.getTime());
		}
		
		CheckMessageUpdateManager.getInstance().returnMsgSignalToMAS(0);
		CheckMessageUpdateManager.getInstance().setIsMsgOn(false);

		if (CheckMessageUpdateManager.getInstance().getLastMsgSource() != null) {
			CheckMessageUpdateManager.getInstance().returnMsgSignalToMAS(0);
		}

		// for Flurry log
		final Map<String, String> map = new HashMap<String, String>();
		map.put("Room", MainApplication.getMAS().getData("data_myroom"));
		FlurryAgent.logEvent("Message", map);

		//topBarFragment.setMessage(false);

		Helper.showInternetWarningToast();

		progressBar = (ProgressBar) findViewById(R.id.progressIndicator);

		String titleId = getIntent().getStringExtra("titleId");
		Bundle bundle = new Bundle();
		bundle.putString("titleId", titleId);
		bundle.putBoolean("hideBackBtn", true);
		initTopBar(bundle);
		initBottomBar();

		mDialog = new ProgressDialog(this);

		adapter = new MessageItemAdapter(this, 0, items);

		listView = (ListView) findViewById(R.id.messageListView);
		listView.setAdapter(adapter);

		messageSubjectText = (MyTextView) findViewById(R.id.messageSubjectText);
		messageTimeText = (MyTextView) findViewById(R.id.messageTimeText);
		messageTextView = (TextView) findViewById(R.id.messageTextView);
		noMessageTextView = (TextView) findViewById(R.id.noMessageTextView);
		messageTextView.setHorizontalScrollBarEnabled(false);
		messageTextView.setVerticalScrollBarEnabled(false);

		getMessage();
		
	}

	public void onPressMessage(int position) {
		messageSubjectText.setText(items.get(position).getSubject());
		messageTimeText.setText(items.get(position).getLastUpdate());

		String message = items.get(position).getDescription();

		StringBuffer localStringBuffer = new StringBuffer(message);

		// only do this for raw Text from TC3
		if (items.get(position).getLastUpdateBy().equalsIgnoreCase("TC3")) {
			localStringBuffer = new StringBuffer(message.replace("%", "&#37;")
					.replace("'", "&#39;").replace("\n", "<br/>")
					.replace("\r", "<br/>"));
		}

		String pish = "<html><head></head><body>";
		String pas = "</body></html>";

		localStringBuffer.insert(0, pish);
		localStringBuffer.append(pas);

		messageTextView.setText(Html.fromHtml(localStringBuffer.toString()).toString());

	}

	public void getMessage() {
		mDialog.setCancelable(false);
		mDialog.setMessage(MainApplication.getLabel("loading"));
		showProgress();
		Log.i(TAG, "getMessage start");

		if (MainApplication.useLocalFile) {
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						"message.json")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			String myjsonstring = sb.toString();

			postExecute(myjsonstring);
		} else {

			String currentTime = "";

			String year = MainApplication.getMAS().getData("year");
			String month = MainApplication.getMAS().getData("month");
			String day = MainApplication.getMAS().getData("day");
			String hour = MainApplication.getMAS().getData("hour");
			String minute = MainApplication.getMAS().getData("minute");

			if (year.equalsIgnoreCase("")) {
				Time today = new Time(Time.getCurrentTimezone());
				today.setToNow();

				year = today.year + "";
				month = (today.month + 1) + "";
				day = today.monthDay + "";
				hour = today.hour + "";
				if (hour.length() == 1) {
					hour = "0" + hour;
				}
				minute = today.minute + "";
				if (minute.length() == 1) {
					minute = "0" + minute;
				}

			}

			currentTime = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":00";

			// TODO: replace hardcoded roomID;
			// MainApplication.getMAS().getData("data_myroom")
			// new MessageAsyncFeed().execute(new String[] { "402" });
			/*
			 * new MessageAsyncFeed().execute(new String[]
			 * {MainApplication.getMAS().getData("data_myroom"),
			 * MainApplication.getMAS().getData("data_language") == "" ? "E" :
			 * MainApplication.getMAS().getData("data_language").toString()
			 * ,currentTime});
			 */

			/* MainApplication.getMAS().getData("data_myroom") */
			String roomNum = MainApplication.getMAS().getData("data_myroom");
			
			
			// to fit PHK case, PHK room number is like 0503
			if (roomNum.length() > 0 && roomNum.charAt(0) == '0') {
				roomNum = roomNum.substring(1, roomNum.length());
			}
			
			Log.i(TAG, "Room number = " + roomNum + "");		

			new MessageAsyncFeed()
					.execute(new String[] {
							roomNum,
							MainApplication.getMAS().getData("data_language") == "" ? "E"
									: MainApplication.getMAS()
											.getData("data_language")
											.toString(), currentTime });
		}
	}

	@Override
	public void postExecute(String json) {
		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);

		if (json != null) {
			GetMessageParser parser = new GetMessageParser(json, this);
			parser.startParsing();
		}

	}

	@Override
	public void onGetDictParsingError(int failMode, boolean isPostExecute) {
		// TODO Auto-generated method stub
		hideProgress();
	}

	@Override
	public void onGetDictFinishParsing(ArrayList<MessageItem> generalItemArray) {
		// TODO Auto-generated method stub
		hideProgress();

		items.clear();
		items.addAll(generalItemArray);

		adapter.notifyDataSetChanged();

		// TODO: Replace hardcoded "No Messages" message with multilingual
		// support
		if (items.size() == 0) {
			Log.i(TAG, "size = 0 ");

			// messageWebView.setVisibility(View.INVISIBLE);
			// infoContentLayout.setVisibility(View.INVISIBLE);
			String noMessage =  MainApplication.getLabel("msg.noMessage");
			noMessageTextView.setText(noMessage);
			noMessageTextView.setVisibility(View.VISIBLE);

		} else {
			noMessageTextView.setVisibility(View.GONE);
			listView.performItemClick(listView.getAdapter().getView(0, null, null), 0, 0);
		}

		if (generalItemArray.size() > 0)
			onPressMessage(0);
	}

	@Override
	public void onGetDictError() {
		// TODO Auto-generated method stub
		hideProgress();

		// try to get from cache data
	}

	public class MessageAsyncFeed extends AsyncTask<String, String, String> {

		// static String myLOG = "MessageAsyncFeed";

		// static Handler mWeatherHandler = new Handler();
		// static int mWeatherDelay = 15*60*1000;

		// static MessageAsyncFeed mMessgaeFeed;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			try {
				Context context = MainApplication.getContext();
				URL ww = new URL(MainApplication.getCmsApiBase()
						+ String.format(
								context.getString(R.string.get_message),
								URLEncoder.encode(params[0], "utf-8"),
								URLEncoder.encode(params[1], "utf-8"),
								URLEncoder.encode(params[2], "utf-8")));
				URLConnection tc = ww.openConnection();

				Log.i("Message", ww.toString());

				BufferedReader in = new BufferedReader(new InputStreamReader(
						tc.getInputStream()));

				String line = "";
				String fullJSON = "";
				while ((line = in.readLine()) != null) {
					fullJSON = fullJSON + line;
				}

				return fullJSON;

			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}

			// return null;
		}

		@Override
		protected void onPostExecute(String result) {
			postExecute(result);
		}
	}

}
