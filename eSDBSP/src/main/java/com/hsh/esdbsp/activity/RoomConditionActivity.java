package com.hsh.esdbsp.activity;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.global.Helper;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class RoomConditionActivity extends BaseActivity {

    private AsyncHttpClient client;
    private TextView tempLabel, tempValue, humidityLabel, humidityValue, luxLabel, luxValue, dustLabel, dustValue, pressureLabel, pressureValue, noiseLabel, noiseValue,
            co2Label, co2Value;
    private WebView historyWeb;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_condition);

        initUI();

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("hightLightChoice", "room condition");
        initTopBar(bundle);
        initBottomBar();

        callRoomConditionData();
    }

    private void callRoomConditionData() {

        client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(0, 10000);

        String url = getString(R.string.room_condition_server_ip) + getString(R.string.room_condition_api);
        Log.i(TAG, "URL = " + url);

        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int arg0, Header[] headers, byte[] response) {
                String s = new String(response);
                Log.i(TAG, "success response = " + s);

                JSONObject restObject;
                try {
                    restObject = new JSONObject(s);

                    String co2 = restObject.getString("co2") + " ppm";
                    String lux = restObject.getString("lux") + " lx";
                    String humidity = restObject.getString("humidity") + " %";
                    String temperature = restObject.getString("temperature") + " &deg;C";
                    String dust = restObject.getString("dust") + " AQI";
                    String pressure = restObject.getString("pressure") + " ppm";
                    String noise = restObject.getString("noise") + " dB";

                    co2Value.setText(co2);
                    luxValue.setText(lux);
                    humidityValue.setText(humidity);
                    tempValue.setText(Html.fromHtml(temperature));
                    dustValue.setText(dust);
                    pressureValue.setText(pressure);
                    noiseValue.setText(noise);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                hideProgress();

            }

            @Override
            public void onFailure(int arg0, Header[] headers, byte[] response, Throwable e) {

                if (response != null) {
                    String s = new String(response);
                    Log.i(TAG, s);
                }
                Log.i(TAG, "Stop Retry");
                hideProgress();
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                Log.i(TAG, "retrying number = " + retryNo);
            }

        });
    }

    private void initUI() {

        tempLabel = (TextView) findViewById(R.id.tempLabel);
        tempValue = (TextView) findViewById(R.id.tempValue);
        humidityLabel = (TextView) findViewById(R.id.humidityLabel);
        humidityValue = (TextView) findViewById(R.id.humidityValue);
        luxLabel = (TextView) findViewById(R.id.luxLabel);
        luxValue = (TextView) findViewById(R.id.luxValue);
        dustLabel = (TextView) findViewById(R.id.dustLabel);
        dustValue = (TextView) findViewById(R.id.dustValue);
        pressureLabel = (TextView) findViewById(R.id.pressureLabel);
        pressureValue = (TextView) findViewById(R.id.pressureValue);
        noiseLabel = (TextView) findViewById(R.id.noiseLabel);
        noiseValue = (TextView) findViewById(R.id.noiseValue);
        co2Label = (TextView) findViewById(R.id.co2Label);
        co2Value = (TextView) findViewById(R.id.co2Value);

        historyWeb = (WebView) findViewById(R.id.historyWeb);
        historyWeb.setBackgroundColor(Color.TRANSPARENT);
        historyWeb.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);

        WebSettings webSettings = historyWeb.getSettings();
        webSettings.setJavaScriptEnabled(true);
        historyWeb.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        historyWeb.setScrollbarFadingEnabled(false);

        historyWeb.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {
                Log.d(TAG, "descriptionText webView Error: " + errorCode + ": " + description + " @ " + failingUrl);
            }
        });

        historyWeb.setWebChromeClient(new WebChromeClient() {
            public void onConsoleMessage(String message, int lineNumber,
                                         String sourceID) {
                Log.d("MyApplication", message + " -- From line " + lineNumber + " of " + sourceID);
            }
        });

        String url = getString(R.string.room_condition_server_ip) + getString(R.string.room_condition_chart);
        historyWeb.loadUrl(url);

        Helper.setAppFonts(tempLabel);
        Helper.setAppFonts(tempValue);
        Helper.setAppFonts(humidityLabel);
        Helper.setAppFonts(humidityValue);
        Helper.setAppFonts(luxLabel);
        Helper.setAppFonts(luxValue);
        Helper.setAppFonts(dustLabel);
        Helper.setAppFonts(dustValue);
        Helper.setAppFonts(pressureLabel);
        Helper.setAppFonts(pressureValue);
        Helper.setAppFonts(noiseLabel);
        Helper.setAppFonts(noiseValue);
        Helper.setAppFonts(co2Label);
        Helper.setAppFonts(co2Value);

        tempLabel.setText(MainApplication.getLabel("Temperature"));
        humidityLabel.setText(MainApplication.getLabel("Humidity"));
        luxLabel.setText(MainApplication.getLabel("Lux"));
        dustLabel.setText(MainApplication.getLabel("Dust (PM 2.5)"));
        pressureLabel.setText(MainApplication.getLabel("Pressure"));
        noiseLabel.setText(MainApplication.getLabel("Noise"));
        co2Label.setText(MainApplication.getLabel("CO2"));

        progressBar = (ProgressBar) findViewById(R.id.progressIndicator);
    }

    @Override
    protected void onLanguageChanged(String language) {
        initUI();
    }

}