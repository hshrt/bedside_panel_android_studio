package com.hsh.esdbsp.activity;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.TypedValue;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.MessagePopupFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.fragment.VideoPopupFragment;
import com.hsh.esdbsp.adapter.GeneralItemAdapter;
import com.hsh.esdbsp.model.Hotel;
import com.hsh.esdbsp.view.EnlargeImageView;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.ImageWidget;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.network.ApiRequest;
import com.hsh.esdbsp.network.XMLCaller;
import com.hsh.esdbsp.parser.GetGeneralItemParser;
import com.hsh.esdbsp.parser.GetGeneralItemParser.GetGeneralItemParserInterface;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

public class ServiceGeneralItemActivity extends BaseActivity implements GetGeneralItemParserInterface {

    private ListView leftMenuListView;
    private TextView descriptionText;

    private GeneralItemAdapter mAdapter;

    private ImageButton richMediaPlayBtn;
    private Button scaleUpBtn;
    private Button scaleDownBtn;
    private Button arrowBtn;
    private Button printBtn;

    private FrameLayout scroller;

    private Boolean scrollerClose = false;

    private int textSize = 25;

    private int animation_duration = 500;

    private int currentPosition;

    private EnlargeImageView bgPhoto;

    private String parentId;
    private String titleId;

    private ArrayList<GeneralItem> leftItemArray;

    private AsyncHttpClient client;

    private BottomBarFragment bottomBarFragment;

    // a key for what is the current topic of this page and what to get from DB
    private String objectString;

    public enum ApiCallType {
        GET_GENERAL_ITEMS
    }

    ApiCallType apiCallType;

    private int retryNum = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, TAG + "onCreate");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.cms_two_column_general);

        mDialog = new ProgressDialog(this);

        textSize = (int) getResources().getDimension(R.dimen.default_text_size);

        initUI();

        leftMenuListView.setVerticalScrollBarEnabled(false);

        Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                leftMenuListView.setVerticalScrollBarEnabled(true);
            }
        };
        handler.postDelayed(r, 1000);

        parentId = getIntent().getStringExtra("parentId");
        titleId = getIntent().getStringExtra("titleId");

        FragmentManager fragmentManager = getSupportFragmentManager();

        Bundle bundle = new Bundle();
        bundle.putString("titleId", titleId);
        initTopBar(bundle);
        initBottomBar();

        descriptionText.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        if (leftItemArray == null) {
            leftItemArray = new ArrayList<GeneralItem>();
        }

        // get itemFromCacheArray
        ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();

        for (GeneralItem item : tempArray) {
            if (item.getParentId().equalsIgnoreCase(parentId)) {
                leftItemArray.add(item);
            }
        }

        Log.i(TAG, "length = " + leftItemArray.size());

        if (mAdapter == null) {
            mAdapter = new GeneralItemAdapter(this, leftItemArray);
            leftMenuListView.setAdapter(mAdapter);
        }

        mAdapter.notifyDataSetChanged();

        changeDescriptionText(0);
    }

    private void initUI() {
        leftMenuListView = (ListView) findViewById(R.id.list);
        descriptionText = (TextView) findViewById(R.id.descriptionText);
        Helper.setAppFonts(descriptionText);
        richMediaPlayBtn = (ImageButton) findViewById(R.id.playBtn);
        richMediaPlayBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videoURLGetter != null) {
                    videoURLGetter.cancel(true);
                }
                videoURLGetter = new VideoURLGetter();
                videoURLGetter.executeOnExecutor(
                        AsyncTask.THREAD_POOL_EXECUTOR,
                        leftItemArray.get(currentPosition).getVideoId());
            }
        });

        scaleUpBtn = (Button) findViewById(R.id.scaleUpBtn);
        scaleUpBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textSize < 40) {
                    textSize += 5;
                }
                descriptionText.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                // for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room",
                        MainApplication.getMAS().getData("data_myroom"));

                FlurryAgent.logEvent("ServiceZoomIn", map);
            }
        });


        scaleDownBtn = (Button) findViewById(R.id.scaleDownBtn);
        scaleDownBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textSize > (int) getResources().getDimension(
                        R.dimen.default_text_size)) {
                    textSize -= 5;
                }
                descriptionText.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

                // for Flurry log
                final Map<String, String> map2 = new HashMap<String, String>();
                map2.put("Room", MainApplication.getMAS().getData("data_myroom"));
                FlurryAgent.logEvent("ServiceZoomOut", map2);
            }
        });


        printBtn = (Button) findViewById(R.id.printBtn);
        printBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                print();
            }
        });


        bgPhoto = (EnlargeImageView) findViewById(R.id.bgPhoto);

        scroller = (FrameLayout) findViewById(R.id.scroller);
        arrowBtn = (Button) findViewById(R.id.arrowBtn);
        arrowBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                Display display = getWindowManager().getDefaultDisplay();
                int width = display.getWidth();

                // to differentiate the high density display galaxy tab S2 and
                // other tablet, 1 = normal, 2 = high density
                int divider = 1;

                if (width == 2048 || GlobalValue.getInstance().getHotel().equals(Hotel.HONGKONG)) {
                    divider = 2;
                }

                Log.i(TAG, "scroll distance =" + Helper.getPixelFromDpNormalScale((int) getResources()
                        .getDimension(R.dimen.scroller_width)));
                Log.i(TAG, "scroll distance2 =" + Helper.getPixelFromDpNormalScale((int) getResources()
                        .getDimension(R.dimen.scroller_width)) / divider);
                // for Flurry log
                final Map<String, String> map = new HashMap<String, String>();
                map.put("Room", MainApplication.getMAS().getData("data_myroom"));
                map.put("Close", scrollerClose == true ? 1 + "" : 0 + "");
                FlurryAgent.logEvent("ServiceItemDescription", map);

                if (!scrollerClose) {
                    ObjectAnimator scrollerAnim = ObjectAnimator.ofFloat(
                            scroller,
                            "translationX",
                            0,
                            Helper.getPixelFromDpNormalScale((int) getResources()
                                    .getDimension(R.dimen.scroller_width))
                                    / divider);
                    ObjectAnimator arrowAnim = ObjectAnimator.ofFloat(
                            arrowBtn,
                            "translationX",
                            0,
                            Helper.getPixelFromDpNormalScale((int) getResources()
                                    .getDimension(R.dimen.scroller_width))
                                    / divider);
                    scrollerAnim.setDuration(animation_duration);
                    scrollerAnim.start();
                    arrowAnim.setDuration(animation_duration);
                    arrowAnim.addListener(new AnimatorListener() {

                        @Override
                        public void onAnimationStart(Animator animation) {
                            //richMediaPlayBtn.setVisibility(View.GONE);
                            scaleDownBtn.setVisibility(View.GONE);
                            scaleUpBtn.setVisibility(View.GONE);
                            printBtn.setVisibility(View.GONE);
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            arrowBtn.setBackgroundResource(R.drawable.info_open);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }
                    });
                    arrowAnim.start();
                    scrollerClose = true;
                } else {
                    ObjectAnimator scollerAnim = ObjectAnimator.ofFloat(
                            scroller,
                            "translationX",
                            Helper.getPixelFromDpNormalScale((int) getResources()
                                    .getDimension(R.dimen.scroller_width)
                                    / divider), 0);
                    ObjectAnimator arrowAnim = ObjectAnimator.ofFloat(
                            arrowBtn,
                            "translationX",
                            Helper.getPixelFromDpNormalScale((int) getResources()
                                    .getDimension(R.dimen.scroller_width)
                                    / divider), 0);
                    scollerAnim.setDuration(animation_duration);
                    scollerAnim.start();
                    arrowAnim.setDuration(animation_duration);
                    arrowAnim.addListener(new AnimatorListener() {

                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            arrowBtn.setBackgroundResource(R.drawable.info_close);
                            scaleDownBtn.setVisibility(View.VISIBLE);
                            scaleUpBtn.setVisibility(View.VISIBLE);

                            if (leftItemArray.get(currentPosition).getPrint()
                                    .equalsIgnoreCase("1")) {
                                printBtn.setVisibility(View.VISIBLE);
                            } else {
                                printBtn.setVisibility(View.GONE);
                            }
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }
                    });
                    arrowAnim.start();
                    scrollerClose = false;
                    scroller.scrollTo(0, 0);
                }
            }
        });
    }

    private void print() {
        Log.i(TAG, "print fire");

        MessagePopupFragment fragment = new MessagePopupFragment();
        Bundle args = new Bundle();
        args.putString("message", "print.popup.message");
        //args.putInt("timeout", 10000);
        fragment.setArguments(args);
        getSupportFragmentManager().beginTransaction()
                .add(android.R.id.content, fragment).commit();

        client = new AsyncHttpClient();
        client.setMaxRetriesAndTimeout(0, 6000000);

        String printIP = this.getString(R.string.printServer);
        // http://172.30.6.220
        // String printIP = "http://192.168.11.61";
        String url = printIP
                + "/printme.php?url="
                + "%22"
                + MainApplication.getApiBase()
                + "cms"
                + GlobalValue.getInstance().getHotel()
                + "/api/print/print.php?itemId="
                + leftItemArray.get(currentPosition).getItemId()
                + "%26lang="
                + Helper.changeLangCodePrint(MainApplication.getMAS().getData("data_language").toString()) + "%22&room="
                + MainApplication.getMAS().getData("data_myroom");

        Log.i(TAG, "URL = " + url);

        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onStart() {
                // called before request is started
            }

            @Override
            public void onSuccess(int arg0, Header[] headers, byte[] response) {
                String s = new String(response);
                Log.i(TAG, "success response = " + s);
                Toast t = Toast.makeText(ServiceGeneralItemActivity.this, "print command sent", Toast.LENGTH_SHORT);
                t.show();
                // every request should have 3 time retry
                retryNum = 0;
            }

            @Override
            public void onFailure(int arg0, Header[] headers, byte[] response,
                                  Throwable e) {

                if (response != null) {
                    String s = new String(response);
                    Log.i(TAG, s);
                }

                Log.i(TAG, "Error = " + e.toString());

                if (retryNum < 2) {
                    retryNum++;
                    // print();

                } else {
                    retryNum = 0;
                    Log.i(TAG, "Stop Retry");
                    // notice the user check the network
                    // showMessage(MainApplication.getLabel("housekeeping.error.msg"));
                }
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
                Log.i(TAG, "retrying number = " + retryNo);
            }

        });
    }

    public void changeDescriptionText(int position) {

        // for Flurry log
        final Map<String, String> map = new HashMap<String, String>();
        map.put("Room", MainApplication.getMAS().getData("data_myroom"));
        map.put("Choice", MainApplication.getEngLabel(leftItemArray.get(
                position).getTitleId()));

        FlurryAgent.logEvent("ServiceItemChoice", map);

        currentPosition = position;

        // avoid crash
        if (leftItemArray.size() == 0) {
            return;
        }

        if (leftItemArray.get(position).getPrint().equalsIgnoreCase("1")) {
            printBtn.setVisibility(View.VISIBLE);
        } else {
            printBtn.setVisibility(View.GONE);
        }

        if (null != leftItemArray.get(currentPosition).getVideoId() &&
                !"".equals(leftItemArray.get(currentPosition).getVideoId()) && !(leftItemArray.get(position).getVideoThumbnail().equalsIgnoreCase("null")) &&
                !(leftItemArray.get(position).getVideoThumbnail().equalsIgnoreCase(""))) {
            richMediaPlayBtn.setVisibility(View.VISIBLE);
            ImageWidget.loadMediaPlayButtonImage(richMediaPlayBtn, leftItemArray.get(position).getVideoThumbnail(), leftItemArray.get(position).getExt(), ImageWidget.SIZE_S);
        } else {
            richMediaPlayBtn.setVisibility(View.GONE);
        }

        //<style type="text/css">@font-face {font-family: MyFont;src: url("file:///android_asset/gill_san_light.otf")}body {font-family: MyFont;  color:#fff}</style>
        String pish = "<html><head></head><body>";
        String pas = "</body></html>";

			/*
             * This section make sure the html align the right hand side for the
			 * language Arabic
			 */
        String currLang = "";
        currLang = MainApplication.getMAS().getData("data_language") == "" ? "E"
                : MainApplication.getMAS().getData("data_language");

        if (currLang.equalsIgnoreCase("")) {
            currLang = "E";
        }

        if (currLang.equalsIgnoreCase("A")
                || DataCacheManager.getInstance().getLang()
                .equalsIgnoreCase("A")) {
            pish += "<span dir=\"rtl\">";
            pas = "</span>" + pas;
        }
            /* *********************** */

        String myHtmlString = pish + MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()) + pas;
        descriptionText.setText(Html.fromHtml(myHtmlString));

        Log.i(TAG, "html text = " + myHtmlString);

        bgPhoto.setImageResource(R.drawable.the_pen_4_3);

        if (leftItemArray.get(position).getImageName() != null
                && !leftItemArray.get(position).getImageName()
                .equalsIgnoreCase("")
                && !leftItemArray.get(position).getImageName()
                .equalsIgnoreCase("null")) {

            DisplayImageOptions options = new DisplayImageOptions.Builder()
                    .showImageOnLoading(R.drawable.the_pen_4_3)
                    .showImageForEmptyUri(R.drawable.the_pen_4_3)
                    .showImageOnFail(R.drawable.the_pen_4_3)
                    .cacheInMemory(true).cacheOnDisk(true)
                    .considerExifParams(true)
                    .bitmapConfig(Bitmap.Config.RGB_565).build();

            ImageWidget.loadImage(bgPhoto, options, leftItemArray.get(position).getImageName(), leftItemArray.get(position).getExt(), ImageWidget.SIZE_M);
        }

        // descriptionText.loadDataWithBaseURL("file:///android_asset/style.css",
        // MainApplication.getLabel(leftItemArray.get(position).getDescriptionId()),
        // "text/html","utf-8", null);
        ;
    }

    @Override
    protected void onStop() {
        // clearFragment();
        super.onStop();
    }

    @Override
    protected void onDestroy() {

        Log.i(TAG, "OnDestroy");

        FragmentManager fm = getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            Log.i(TAG, "clear Fragment");
            fm.popBackStack();
        }

        topBarFragment = null;
        bottomBarFragment = null;
        mAdapter.clear();
        mAdapter = null;

        System.gc();
        // this.setContentView(null);
        super.onDestroy();
    }

    private void getGeneralItems() {
        mDialog.setCancelable(false);
        mDialog.setMessage(MainApplication.getLabel("loading"));
        showDialog();
        Log.i(TAG, "getGeneralItems start");
        apiCallType = ApiCallType.GET_GENERAL_ITEMS;

        if (MainApplication.useLocalFile) {
            // Reading text file from assets folder
            StringBuffer sb = new StringBuffer();
            BufferedReader br = null;
            try {
                br = new BufferedReader(new InputStreamReader(getAssets().open(
                        objectString + ".txt")));
                String temp;
                while ((temp = br.readLine()) != null)
                    sb.append(temp);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    br.close(); // stop reading
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            String myjsonstring = sb.toString();

            postExecute(myjsonstring);
        }

        try {
            URL url = null;

            url = new URL(MainApplication.getApiBase()
                    + this.getString(R.string.get_table) + "?table="
                    + objectString);
            Log.e(TAG, "url = " + url);

            Bundle bundle = new Bundle();
            ApiRequest.request(this, url, "get", bundle);

        } catch (MalformedURLException e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        } catch (Exception e) {
            e.printStackTrace();
            onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
        }
    }

    @Override
    public boolean hasInternet() {
        return Helper.hasInternet(false);
    }

    @Override
    public void onError(int failMode, boolean isPostExecute) {
        mDialog.dismiss();
        Log.i(TAG, "failMode" + failMode);
        Log.i(TAG, "onerror");
    }

    @Override
    public void postExecute(String json) {

        Log.i(TAG, "postExecute");
        Log.i("XMLContent", "api xml =" + json);

        hideDialog();
        if (apiCallType == ApiCallType.GET_GENERAL_ITEMS) {
            GetGeneralItemParser parser = new GetGeneralItemParser(json, this);
            parser.startParsing();
        }
    }

    @Override
    public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute) {
        // TODO Auto-generated method stub
        mDialog.dismiss();
    }

    @Override
    public void onGetGeneralItemFinishParsing(
            ArrayList<GeneralItem> generalItemArray) {
        // TODO Auto-generated method stub
        mDialog.dismiss();

        leftItemArray.clear();
        leftItemArray.addAll(generalItemArray);
        Log.i(TAG, leftItemArray.toString());
        mAdapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        clearFragment();
    }

    @Override
    public void onGetGeneralItemError() {
        // TODO Auto-generated method stub
        mDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.cms_service_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public String getTitleId() {
        return titleId;
    }

    public void setTitleId(String titleId) {
        this.titleId = titleId;
    }

    private VideoURLGetter videoURLGetter;

    private class VideoURLGetter extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            if (params == null || params.length < 1 || params[0] == null || "".equals(params[0])) {
                return null;
            }

            URL url = null;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(MainApplication.getCmsApiBase()
                        + "video/getOnlineVideo.php");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(
                        urlConnection.getInputStream());
                String response = "";
                int c = in.read();
                while (c != -1) {
                    response += (char) c;
                    c = in.read();
                }
                in.close();

                // Process response
                JSONObject responseJson = new JSONObject(response);
                JSONArray contentJson = responseJson.getJSONArray("content");
                for (int i = 0; i < contentJson.length(); i++) {
                    String id = contentJson.getJSONObject(i).getString("id");
                    if (params[0].equals(id)) {
                        return contentJson.getJSONObject(i).getString(
                                "playlist_name");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } catch (JSONException e) {
                e.printStackTrace();
                return null;
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d("RichMedia", "postExecute result: " + result);
            VideoPopupFragment fragment = new VideoPopupFragment();
            Bundle args = new Bundle();
            args.putString("url", getString(R.string.video_base) + result);
            fragment.setArguments(args);
            if (!isFinishing() && !isDestroyed()) {
                getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment).commitAllowingStateLoss();
            }
        }
    }

    @Override
    protected void onLanguageChanged(String language) {
        initUI();
        leftItemArray.clear();
        // get itemFromCacheArray
        ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();
        for (GeneralItem item : tempArray) {
            if (item.getParentId().equalsIgnoreCase(parentId)) {
                leftItemArray.add(item);
            }
        }
        mAdapter.notifyDataSetChanged();

        changeDescriptionText(0);
    }

}