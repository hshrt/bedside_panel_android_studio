package com.hsh.esdbsp.activity;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.earthtv.publicapihelper.EarthTvPublicApiClient_;
import com.earthtv.publicapihelper.PlayListItem;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.adapter.GeneralItemAdapter;
import com.hsh.esdbsp.global.DataCacheManager;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.GeneralItem;

public class EarthTVActivity extends BaseActivity {

    EarthTvPublicApiClient_ apiClient;
    Button reloadButton;
    List<PlayListItem> playList;
    PlayListItem currentClip;
    VideoView videoPlayer;
    MediaController mediaController;
    Iterator<PlayListItem> playListIterator;
    TextView textView;
    
    private ListView leftMenuListView;
    
    private ArrayList<GeneralItem> leftItemArray;
    
    private ProgressBar webViewProgressIndicator;
    
    private String titleId;
    
    private String parentId;
    
    private GeneralItemAdapter mAdapter;
    
    private int currentIndex = 0;

    protected void startPlayList(final int index){

        Log.i("Main", "startPlayList fire");
        Log.i("Main", "language =" + Helper.changeLangCodeEarthTV(MainApplication.getMAS().getData(
				"data_language") == "" ? "E"
				: MainApplication.getMAS().getData(
						"data_language")));
        
        String langCode = MainApplication.getMAS().getData(
				"data_language") == "" ? "E"
				: MainApplication.getMAS().getData(
						"data_language");
						
						
        if(getString(R.string.local_change_lang).equalsIgnoreCase("1")){
        	langCode = DataCacheManager.getInstance().getLang().toString();
        }
        
        final String finalLangCode = langCode;
        
        
        // 1080 HD example, EarthTV selected clips only
        // playList = apiClient.buildPlaylist("limit=20&channel=BestOf&location_id=BKK&location_id=HKG&location_id=HND", 1080, 5000000L, 360);
        //
        // 720 HD example, last 20 clips recorded
        // playList = apiClient.buildPlaylist("limit=20&channel=Latest", 720, 2500000L, 360);
        //
        // How to use tags, sunset hour clips only
        // playList = apiClient.buildPlaylist("limit=20&tags=sunset", 360, 1800000L, 360);
        //
        
       	
		
		new AsyncTask<Void, Void, Void>() {
	        @Override
	        protected Void doInBackground(Void... params) {
	            // your async action
	        	playList = apiClient.buildPlaylist("limit=20&channel=Latest&location_id="+leftItemArray.get(index).getCommand()+"&language="+Helper.changeLangCodeEarthTV(finalLangCode), 360, 2500000L, 360);  

	        	return null;
	        }

	        @Override
	        protected void onPostExecute(Void aVoid) {
	            // update the UI (this is executed on UI thread)
	        	playListIterator = playList.iterator();
		        playNextVideo();
	            super.onPostExecute(aVoid);
	        }
	    }.execute();
		
        // Mobile Optimized Bitrate Example
        
    }

    protected void playNextVideo(){
        if (playListIterator.hasNext()) {
            // Get next clip from playlist
            currentClip = playListIterator.next();
            // Send Statistic Event to EarthTV
            apiClient.sendStatisticVideoPlayEvent(currentClip);
            
            Log.i(TAG, "currentCLip = "+ currentClip.videoURI);
            
            String videoURL = currentClip.videoURI;//.replace("http://cdn.earthtv.com","http://cliptest.earthtv.com.global.prod.fastly.net/mserv");
            // Load video
            Log.i(TAG, "changed currentCLip = "+ videoURL); 
            videoPlayer.setVideoURI(Uri.parse(videoURL));
            videoPlayer.requestFocus();

        } else {
            Log.d("Player", "Nothing to play");
            textView.setText("");
            
            if (videoPlayer.isPlaying()) videoPlayer.stopPlayback();
            startPlayList(currentIndex);
        }
    }


    @Override
	public void onCreate(Bundle savedInstanceState) {
    	
    	//new branch comment
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earthtv);

        Log.i(TAG, "onCreate fire");
        
        webViewProgressIndicator = (ProgressBar) findViewById(R.id.webViewProgressIndicator);
        progressBar = webViewProgressIndicator;
        
        this.reloadButton = (Button)this.findViewById(R.id.button);
        this.videoPlayer = (VideoView)this.findViewById(R.id.videoView);
        this.textView = (TextView)this.findViewById(R.id.textView);
        this.textView.setVisibility(View.INVISIBLE);

        Helper.setAppFonts(this.textView);
        
        titleId = getIntent().getStringExtra("titleId");
        parentId = getIntent().getStringExtra("parentId");

		Bundle bundle = new Bundle();
		bundle.putString("titleId", titleId);
        initTopBar(bundle);
        initBottomBar();

		leftMenuListView = (ListView) findViewById(R.id.list);
        
        if (leftItemArray == null) {
			leftItemArray = new ArrayList<GeneralItem>();
		}
        
        ArrayList<GeneralItem> tempArray = GlobalValue.getInstance()
				.getAllItemArray();

		// Log.i(TAG, "temp Array length = " + tempArray.size());

		for (GeneralItem item : tempArray) {
			// Log.i(TAG, "temp Arary item = " + item.getItemId());
			if (item.getParentId().equalsIgnoreCase(parentId)) {
				leftItemArray.add(item);
			}
		}
		
		if (mAdapter == null) {
			mAdapter = new GeneralItemAdapter(this, leftItemArray);
			leftMenuListView.setAdapter(mAdapter);
		}
		
		mAdapter.notifyDataSetChanged();

        mediaController = new MediaController(this);
        mediaController.setAnchorView(videoPlayer);
        //videoPlayer.setMediaController(mediaController);

        //float cutOffRatio = 0.1f;
        float cutOffRatio = 0.35f;

		// Resize Player
        Point screenSize = new Point();
        //getWindowManager().getDefaultDisplay().getSize(screenSize);
        float screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        Log.i(TAG, "width = " + screenWidth);
        
        //screenWidth = 1800;
        
        if(screenWidth>2000){
        	cutOffRatio = 0.4f;
        }

        ViewGroup.LayoutParams playerSize = videoPlayer.getLayoutParams();
        playerSize.width = Math.round(screenWidth - (screenWidth *cutOffRatio));
        playerSize.height = Math.round((screenWidth- (screenWidth *cutOffRatio)) * 9f / 16f);
        videoPlayer.setLayoutParams(playerSize);


        // Init Library
        apiClient = EarthTvPublicApiClient_.getInstance_(EarthTVActivity.this);
        // Set token, origin identifier and language.
        // Set token, origin identifier and language.
        apiClient.initEarthTVPublicAPIClient("55b1e8d7bbf04785a35f6af7", "http://www.hshgroup.com/", "zh_CN");

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        EarthTVActivity.this.reloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videoPlayer.isPlaying()) videoPlayer.stopPlayback();
                startPlayList(currentIndex);
            }
        });


        videoPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // show some brief clip info
                textView.setText(currentClip.country + ", " + currentClip.city);
                // start playback
                hideProgress();
                videoPlayer.start();
            }
        });


        videoPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // prepare next clip from playlist
                playNextVideo();
            }
        });
        

        //startPlayList();
	
        
        Handler handler = new Handler();
	    //final String ext = extList.get(loadIndex).equalsIgnoreCase("j")?"jpg":"png";
		handler.postDelayed(new Runnable() {
			@Override
			public void run() {
				//simulate press first button
				try{
					onClickList(0);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
		}, 100);		    
    }
    
    public void onClickList(int index){
    	
    	if(index >=0 && index<leftItemArray.size()){
    		showProgress();
    		currentIndex = index;
    		startPlayList(index);
    	}
    }

}
