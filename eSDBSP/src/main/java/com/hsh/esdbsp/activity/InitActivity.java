package com.hsh.esdbsp.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;

import com.hsh.esdbsp.BuildConfig;
import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.widget.Log;
import com.splunk.mint.Mint;

import java.util.Arrays;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by lawrencetsang on 2017-11-29.
 */

public class InitActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks{

    public boolean isPermissionGranted = false;

    private static final int REQUEST_CODE_WRITE_SETTINGS = 3001;
    private final int REQUEST_CODE_PHONE_STATUS = 3002;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (grantWriteSettings()) {
            grantPhoneStatusPermissions();
        }
    }

    private boolean grantWriteSettings() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Settings.System.canWrite(this)) {
                return true;
            } else {
                Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, REQUEST_CODE_WRITE_SETTINGS);
            }
        } else {
            return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_WRITE_SETTINGS) {
            if (grantWriteSettings()) {
                grantPhoneStatusPermissions();
            } else {
                finish();
            }
        }
    }

    private void initBSP() {

        ((MainApplication) this.getApplication()).initSetting();
        ((MainApplication) this.getApplication()).initAssetManager();
        ((MainApplication) this.getApplication()).initImageLoader();
        ((MainApplication) this.getApplication()).initComm();
        ((MainApplication) this.getApplication()).loadSetting();
        ((MainApplication) this.getApplication()).startMoveTop();
        ((MainApplication) this.getApplication()).setInitStatus(true);
        ((MainApplication) this.getApplication()).initFlurry();
        ((MainApplication) this.getApplication()).initMint();
        ((MainApplication) this.getApplication()).initMessageListener();

//        //build target version 7.0
//        if(Build.VERSION > Build.VERSION_CODES.M){
//            ((MainApplication) this.getApplication()).setupWifiReceiver();
//        }
        goToRoomControl();
    }

    private void goToRoomControl() {
        Intent intent = new Intent(this, RoomControlActivity.class);
        intent.putExtra("launch", true);
        startActivity(intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        initBSP();
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        finish();
    }

    // Permissions
    public void grantPhoneStatusPermissions() {

        String[] permissions = {Manifest.permission.READ_PHONE_STATE};
        if (EasyPermissions.hasPermissions(this, permissions)) {
            isPermissionGranted = true;
            // if permissions are already granted, call onPermissionsGranted manually
            onPermissionsGranted(REQUEST_CODE_PHONE_STATUS, Arrays.asList(permissions));
        } else {
            isPermissionGranted = false;
            EasyPermissions.requestPermissions(this, "Press \"OK\" then \"Allow\" to proceed", REQUEST_CODE_PHONE_STATUS, permissions);
        }
    }

}
