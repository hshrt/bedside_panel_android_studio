package com.hsh.esdbsp.activity;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ListView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.adapter.InfoAdapter;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.widget.Log;
import com.hsh.esdbsp.model.GeneralItem;
import com.hsh.esdbsp.network.ApiRequest;
import com.hsh.esdbsp.network.XMLCaller;
import com.hsh.esdbsp.parser.GetGeneralItemParser;
import com.hsh.esdbsp.parser.GetGeneralItemParser.GetGeneralItemParserInterface;


public class ServiceInfoActivity extends BaseActivity implements GetGeneralItemParserInterface{

	private ListView infoListView;
	private InfoAdapter mAdapter;

	private String parentId;
	private String titleId;
	
	private ArrayList<GeneralItem> leftItemArray;
	
	//a key for what is the current topic of this page and what to get from DB
	private String objectString;
	
	public enum ApiCallType {
		GET_GENERAL_ITEMS
	}
	
	ApiCallType apiCallType;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(TAG, TAG+"onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cms_info);

		mDialog = new ProgressDialog(this);
		
		initUI();

		infoListView.setVerticalScrollBarEnabled(false);	
		
		Handler handler = new Handler();
		final Runnable r = new Runnable(){
		    public void run() {
		    	infoListView.setVerticalScrollBarEnabled(true);
		    }
		};
		handler.postDelayed(r, 1000);
		
		parentId = getIntent().getStringExtra("parentId");
		titleId = getIntent().getStringExtra("titleId");

		Bundle bundle = new Bundle();
        bundle.putString("titleId", titleId);
        initTopBar(bundle);
        initBottomBar();

		if(leftItemArray == null){
			leftItemArray = new ArrayList<GeneralItem>();
		}

		//get itemFromCacheArray
		ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();
		

		for(GeneralItem item: tempArray){
			if(item.getParentId().equalsIgnoreCase(parentId)){
				leftItemArray.add(item);
			}		
		}
		
		Log.i(TAG, "length = " + leftItemArray.size());
		
		if(mAdapter == null){
			mAdapter= new InfoAdapter(this, leftItemArray);
			infoListView.setAdapter(mAdapter);
		}
		
		mAdapter.notifyDataSetChanged();
	}
	
	private void initUI(){
		infoListView = (ListView) findViewById(R.id.infoList);
	}
	
	
	private void getGeneralItems() {
		mDialog.setCancelable(false);
		mDialog.setMessage(MainApplication.getLabel("loading"));
		showDialog();
		Log.i(TAG, "getGeneralItems start");
		apiCallType = ApiCallType.GET_GENERAL_ITEMS;
		
		
		if(MainApplication.useLocalFile){
			// Reading text file from assets folder
			StringBuffer sb = new StringBuffer();
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(getAssets().open(
						objectString+".txt")));
				String temp;
				while ((temp = br.readLine()) != null)
					sb.append(temp);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					br.close(); // stop reading
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	
			String myjsonstring = sb.toString();
			
			postExecute(myjsonstring);
		}
				
		try {
			URL url = null;

			url = new URL(MainApplication.getApiBase()
					+ this.getString(R.string.get_table) + "?table="+objectString);
			Log.e(TAG, "url = " + url);

			Bundle bundle = new Bundle();
			ApiRequest.request(this, url, "get", bundle);  

		} catch (MalformedURLException e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		} catch (Exception e) {
			e.printStackTrace();
			onError(XMLCaller.FAIL_MODE_DISPLAY_ERROR, false);
		}
	}
	
	@Override
	public boolean hasInternet() {
		return Helper.hasInternet(false);
	}

	@Override
	public void onError(int failMode, boolean isPostExecute) {
		mDialog.dismiss();
		Log.i(TAG, "failMode" + failMode);
		Log.i(TAG, "onerror");
	}

	@Override
	public void postExecute(String json) {

		Log.i(TAG, "postExecute");
		Log.i("XMLContent", "api xml =" + json);

		hideDialog();
		if (apiCallType == ApiCallType.GET_GENERAL_ITEMS) {
			GetGeneralItemParser parser = new GetGeneralItemParser(json, this);
			parser.startParsing();
		} 
	}
	
	@Override
	public void onGetGeneralItemParsingError(int failMode, boolean isPostExecute) {
		mDialog.dismiss();
	}

	@Override
	public void onGetGeneralItemFinishParsing(ArrayList<GeneralItem> generalItemArray) {

		mDialog.dismiss();
		
		leftItemArray.clear();
		leftItemArray.addAll(generalItemArray);
		Log.i(TAG, leftItemArray.toString());
		mAdapter.notifyDataSetChanged();
	}

	@Override
	public void onGetGeneralItemError() {
		mDialog.dismiss();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.cms_service_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public String getTitleId() {
		return titleId;
	}

	public void setTitleId(String titleId) {
		this.titleId = titleId;
	}


	@Override
	protected void onLanguageChanged(String language) {
		initUI();
		//get itemFromCacheArray
		leftItemArray.clear();
		ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();


		for(GeneralItem item: tempArray){
			if(item.getParentId().equalsIgnoreCase(parentId)){
				leftItemArray.add(item);
			}
		}

		mAdapter.notifyDataSetChanged();
	}


}