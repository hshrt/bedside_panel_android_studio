package com.hsh.esdbsp.activity;

import java.util.ArrayList;


import org.apache.http.Header;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.adapter.dvd.DVDMovieListAdapter;
import com.hsh.esdbsp.fragment.BottomBarFragment;
import com.hsh.esdbsp.fragment.TopBarFragment;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.adapter.HouseKeepingItemAdapter;
import com.hsh.esdbsp.global.GlobalValue;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.GeneralItem;
import com.loopj.android.http.*;

public class ServiceHouseKeepingActivity extends BaseActivity {
	
	private GridView menu_gridView;
	
	private HouseKeepingItemAdapter mAdapter;
	
	
	private ArrayList<GeneralItem> itemArray;

	private TextView staticText;
	private Button submitBtn;
	
	private String parentId;
	private String titleId;
	private String type;
	
	private int choice;
	
	private View bgView;
	
	String[] keyArr = {"hotel_restaurants","hotel_restaurants","hotel_restaurants","wellness",
            "guest_services","transportation"};
	
	Boolean[] requestChecker; //a boolean array to check if the request of the service sent out successfully
	String[] commandList;
	
	private int currentIndex;
	private int retryNum = 0;
	
	private LinearLayout errorMessageDialogLayout;
	private ImageView errorMessageDialogCloseImage;
	private MyTextView errorMessageDialogLabel;
	
	private AsyncHttpClient client;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.cms_housekeeping);

		initUI();
		
		parentId = getIntent().getStringExtra("parentId");
		titleId = getIntent().getStringExtra("titleId");
		choice = getIntent().getIntExtra("choice", 0);
		type = getIntent().getStringExtra("type");

		if(itemArray == null){
			itemArray = new ArrayList<GeneralItem>();
		}
		
		//get itemFromCacheArray
		ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();
		ArrayList<GeneralItem> tempItemArray = new ArrayList<GeneralItem>();
		
		for(GeneralItem item: tempArray){
			if(item.getParentId().equalsIgnoreCase(parentId)){
				tempItemArray.add(item);
			}		
		}
		
		String choiceId = tempItemArray.get(choice).getItemId();
		
		for(GeneralItem item: tempArray){
			if(item.getParentId().equalsIgnoreCase(choiceId)){
				itemArray.add(item);
			}		
		}
		
		Log.i(TAG, itemArray.toString());
		
		if(mAdapter == null){
			mAdapter= new HouseKeepingItemAdapter(this, itemArray);
		}
		
		menu_gridView.setAdapter(mAdapter);
		
		Bundle bundle = new Bundle();
		bundle.putString("titleId", titleId);
		bundle.putString("type", type);
		bundle.putInt("choice", choice);
		bundle.putParcelableArrayList("choiceList", tempItemArray);
		initTopBar(bundle);
		initBottomBar();

	}
	public void refreshWithChoice(int choice){

		itemArray.clear();
		
		//get itemFromCacheArray
		ArrayList<GeneralItem> tempArray = GlobalValue.getInstance().getAllItemArray();
		ArrayList<GeneralItem> tempItemArray = new ArrayList<GeneralItem>();
		
		for(GeneralItem item: tempArray){
			//Log.i(TAG, "temp Arary item = " + item.getItemId());
			if(item.getParentId().equalsIgnoreCase(parentId)){
				tempItemArray.add(item);
			}		
		}
		
		String choiceId = tempItemArray.get(choice).getItemId();
		
		for(GeneralItem item: tempArray){
			if(item.getParentId().equalsIgnoreCase(choiceId)){
				itemArray.add(item);
			}		
		}

		mAdapter= new HouseKeepingItemAdapter(this, itemArray);
		menu_gridView.setAdapter(mAdapter);
	}

	public void onItemclick(int position){
		if(position == 0){
			Intent intent = new Intent(ServiceHouseKeepingActivity.this, ServiceInRoomDinningActivity.class);
			intent.putExtra("objectString", keyArr[position]);
			startActivityForResult(intent, 0);
		}
		else if(position==2){
			Intent intent = new Intent(ServiceHouseKeepingActivity.this, ServiceGeneralMenuActivity.class);
			intent.putExtra("objectString", keyArr[position]);
			startActivityForResult(intent, 0);
		}
		else if(position<=5){
			Intent intent = new Intent(ServiceHouseKeepingActivity.this,ServiceGeneralItemActivity.class);
			intent.putExtra("objectString", keyArr[position]);
			startActivityForResult(intent, 0);
		}
	}
	
	private void initUI(){
		menu_gridView = (GridView) findViewById(R.id.menu_gridView);
		submitBtn = (Button) findViewById(R.id.submitBtn);
		submitBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.i(TAG,mAdapter.getClickArr().toString());

				int clickCount = 0;

				for(int x = 0; x < mAdapter.getClickArr().length ; x++){

					if(mAdapter.getClickArr()[x] == true){
						clickCount++;
					}
				}

				commandList = new String[clickCount];

				int temp_clickCount = 0;

				for(int x = 0; x < mAdapter.getClickArr().length ; x++){

					if(mAdapter.getClickArr()[x] == true){
						commandList[temp_clickCount] = itemArray.get(x).getCommand();
						temp_clickCount++;
					}
				}

				Log.i(TAG,"commandList = " + commandList.toString());

				requestChecker = new Boolean[clickCount];
				for(int x = 0; x < requestChecker.length ; x++){
					requestChecker[x] = false;
				}

				if(commandList.length > 0){
					//send out Order Serice Request to MAS
					showProgress();
					bgView.setVisibility(View.VISIBLE);
					requestService(currentIndex);
				}
			}
		});

		staticText = (TextView) findViewById(R.id.static_text);
		
		bgView = (View) findViewById(R.id.bgView);
		progressBar = (ProgressBar) findViewById(R.id.progressIndicator);
		
		errorMessageDialogLayout = (LinearLayout) findViewById(R.id.errorMessageDialogLayout);
		errorMessageDialogCloseImage = (ImageView) findViewById(R.id.errorMessageDialogCloseImage);
		errorMessageDialogCloseImage.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				errorMessageDialogLayout.setVisibility(View.GONE);
			}
		});
		errorMessageDialogLabel = (MyTextView) findViewById(R.id.errorMessageDialogLabel);
		
		staticText.setText(MainApplication.getLabel("housekeeping.selectservice.msg"));
		submitBtn.setText(MainApplication.getLabel("housekeeping.submit.label"));

		Helper.setAppFonts(submitBtn);
		Helper.setAppFonts(staticText);
	}
	
	private void showMessage(String msg) {
		hideProgress();
		bgView.setVisibility(View.GONE);
		errorMessageDialogLayout.setVisibility(View.VISIBLE);
		errorMessageDialogLabel.setText(msg);
	}
	
	private void requestService(final int index){

		client = new AsyncHttpClient();
		client.setMaxRetriesAndTimeout(0, 10000);
		
		String url = "http://"+MainApplication.getMAS().getMASPath()+"/mas.php?cmd=SET%20"+commandList[index]+"%20ON";
		
		Log.i(TAG, "URL = " + url);
		
		client.get(url, new AsyncHttpResponseHandler() {

		    @Override
		    public void onStart() {
		        // called before request is started
		    }
		    
		    @Override
			public void onSuccess(int arg0, Header[] headers, byte[] response) {
				String s = new String(response);			
				Log.i(TAG,"success response = " +  s);
				
				//every request should have 3 time retry
				retryNum = 0;
				
				if(currentIndex != commandList.length-1){
					currentIndex++;
					requestService(currentIndex);
				}
				else{
					currentIndex = 0;
					Log.i(TAG,"finish the loop");
					//finish, hide the loading
					hideProgress();
					showMessage(MainApplication.getLabel("housekeeping.thankyou.msg"));
					mAdapter.resetClickArr();
				}
			}
		    
			@Override
			public void onFailure(int arg0, Header[] headers, byte[] response,
					Throwable e) {
				
				if(response != null){
					String s = new String(response);	
					Log.i(TAG, s);
				}
				
				Log.i(TAG, "Error = " + e.toString());
				
				if(retryNum<2){
					retryNum++;
					requestService(currentIndex);
					
				}
				else{
					retryNum = 0;
					Log.i(TAG, "Stop Retry");
					//notice the user check the network
					showMessage(MainApplication.getLabel("housekeeping.error.msg"));
				}
			}

		    @Override
		    public void onRetry(int retryNo) {
		        // called when request is retried
		    	Log.i(TAG,"retrying number = " + retryNo);
			}
		
		});
	}
	
	
	@Override
	public void onDestroy() {
		Log.i(TAG, "HouseKeepingActivituy onDestroy");
		
		if(client != null){
			client.cancelAllRequests(true);	
		}
		super.onDestroy();
	}
	
	@Override
	public void onPause() {
		
		Log.i(TAG, "HouseKeepingActivituy onPause");
		if(client != null){
			client.cancelAllRequests(true);	
		}
		super.onPause();
	}
	
	@Override
	public void onStop() {
		Log.i(TAG, "HouseKeepingActivituy onStop");
		
		if(client != null){
			client.cancelAllRequests(true);	
		}
		
		super.onStop();
	}
	
	//MAS IP http://<masip>/mas.php?cmd=SET%20EXTRAWATER%20ON

	@Override
	protected void onLanguageChanged(String language) {
		initUI();
		refreshWithChoice(choice);
	}

}