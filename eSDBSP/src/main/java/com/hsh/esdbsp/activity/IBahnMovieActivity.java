package com.hsh.esdbsp.activity;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.comm.IBahnComm;
import com.hsh.esdbsp.view.MyTextView;
import com.hsh.esdbsp.adapter.ibahn.IBahnMovieGenreAdapter;
import com.hsh.esdbsp.adapter.ibahn.IBahnMovieItemAdapter;
import com.hsh.esdbsp.global.Helper;
import com.hsh.esdbsp.model.IBahnMovie;
import com.hsh.esdbsp.model.IBahnTvChannel;
import com.hsh.esdbsp.model.Movie;
import com.hsh.esdbsp.widget.Command;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

public class IBahnMovieActivity extends BaseActivity {

    private GridView movieGridView;
    private ListView genreListView;

    private ArrayList<Movie> movieItems;

    private IBahnMovieItemAdapter mAdapter;
    private IBahnMovieGenreAdapter mGenreAdapter;

    private ImageView detailPhoto;
    private Button closeBtn;
    private RelativeLayout popContainer;
    private Button trailerBtn, watchBtn;

    IBahnMovie currentChoice;

    List<IBahnMovie> allMovies;
    List<String> movieGenres;

    Button onBtn, powerBtn, arrowUpBtn, arrowDownBtn, arrowLeftBtn, arrowRightBtn, okBtn;
    Button soundMinusBtn, soundMuteBtn, soundPlusBtn;
    Button playBtn, pauseBtn, stopBtn, rwBtn, ffBtn, skipbBtn, skipfBtn;

    MyTextView movieName, movieTagline, movieDescription;

    String myCMD;

    String iBahnParamRoom = "";
    String iBahnParamBox = "";
    String iBahnParamToken = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.ibahn_movies);

        Bundle bundle = new Bundle();
        bundle.putBoolean("hideBackBtn", false);
        bundle.putString("titleId", "TV/AUDIO");
        bundle.putString("hightLightChoice", "movie");
        bundle.putInt("choice", 1);

        //for Demo STB day 2017/4/7
        if (this.getString(R.string.forDemoRoom).equalsIgnoreCase("1")) {
            bundle.putInt("choice", 2);
        }

        initTopBar(bundle);
        initBottomBar();

        genreListView = (ListView) this.findViewById(R.id.movie_genre_list);
        movieGridView = (GridView) this.findViewById(R.id.movie_grid_view);
        detailPhoto = (ImageView) this.findViewById(R.id.detailPhoto);
        movieName = (MyTextView) this.findViewById(R.id.movieName);
        movieTagline = (MyTextView) this.findViewById(R.id.movieTagline);
        movieDescription = (MyTextView) this.findViewById(R.id.movieDescription);

        popContainer = (RelativeLayout) this.findViewById(R.id.popContainer);
        closeBtn = (Button) this.findViewById(R.id.closeBtn);
        trailerBtn = (Button) this.findViewById(R.id.trailerBtn);
        watchBtn = (Button) this.findViewById(R.id.watchBtn);

        movieGridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                popContainer.setVisibility(View.VISIBLE);
                currentChoice = (IBahnMovie) parent.getAdapter().getItem(position);
                setDetailPhoto(currentChoice);
            }
        });

        closeBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                popContainer.setVisibility(View.GONE);

            }

        });

        trailerBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(IBahnMovieActivity.this, MovieTrailerPlayerActivity.class);
                Log.d("IBahnTrailer", currentChoice.getWebTrailerUrl());
                intent.putExtra("videoPath", currentChoice.getWebTrailerUrl());
                intent.putExtra("videoTitle", currentChoice.getTitle());
                startActivity(intent);
            }
        });
        Helper.setAppFonts(trailerBtn);

        watchBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                IBahnComm comm = new IBahnComm();
                comm.selectMovie(iBahnParamRoom, iBahnParamToken, currentChoice.getLanguages().get(0).getGvid(), null);
            }

        });

        watchBtn.setText(MainApplication.getLabel("watch"));
        Helper.setAppFonts(watchBtn);

        setupRemoteControl();

        new IBahnSetup().execute();

        popContainer.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        ;

    }

    private class IBahnSetup extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            iBahnParamRoom = MainApplication.getMAS().getData("data_myroom").replaceAll("^0+", "");
            Log.d("setupIBahnParam", "Zone = " + MainApplication.getMAS().getSelectedArea());
            iBahnParamBox = Integer.toString(MainApplication.getMAS().getSelectedArea());
            String urlString = MainApplication.getCmsApiBase();
            urlString += "ibahn/getToken.php?room=" + iBahnParamRoom;
            URL url = null;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(urlString);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String response = "";
                int c = in.read();
                while (c != -1) {
                    response += (char) c;
                    c = in.read();
                }
                Log.d("getToken", response);
                in.close();
                JSONObject responseJson = new JSONObject(response);
                if (responseJson.getInt("status") == 1) {
                    JSONArray dataArray = responseJson.getJSONArray("data");
                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject dataObj = dataArray.getJSONObject(i);
                        if (iBahnParamBox.equals(dataObj.getString("box"))) {
                            iBahnParamToken = dataObj.getString("token");
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            Log.d("setupIBahnParam", iBahnParamRoom + ", " + iBahnParamBox + ", " + iBahnParamToken);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            new IBahnComm().getMovies(iBahnParamRoom, new IBahnComm.Callback() {

                @Override
                public void onGetMovieResult(List<IBahnMovie> movieResult) {
                    allMovies = movieResult;
                    // Build GenreList
                    movieGenres = Arrays.asList(MainApplication.getLabel("movie.ibahn.showgenre").split(","));
                    mGenreAdapter = new IBahnMovieGenreAdapter(IBahnMovieActivity.this, movieGenres);
                    mGenreAdapter.setSelectedItem(0);
                    genreListView.setAdapter(mGenreAdapter);
                    genreListView.setOnItemClickListener(new OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            mGenreAdapter.setSelectedItem(position);
                            mGenreAdapter.notifyDataSetChanged();
                            updateMovieListByGenre(position);
                        }
                    });
                    updateMovieListByGenre(0);
                }

                @Override
                public void onGetTvChannelResult(List<IBahnTvChannel> tvChannelResult) {
                    // TODO Auto-generated method stub

                }

            });
        }
    }

    private void updateMovieListByGenre(int position) {
        String genre = movieGenres.get(position);
        ArrayList<IBahnMovie> movies = new ArrayList<>();
        for (IBahnMovie m : allMovies) {
            if (m.getTags().contains(genre)) {
                movies.add(m);
            }
        }
        mAdapter = new IBahnMovieItemAdapter(IBahnMovieActivity.this, movies);
        movieGridView.setAdapter(mAdapter);

    }

    private void setupRemoteControl() {
        onBtn = (Button) findViewById(R.id.on_btn);
        onBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.ON, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        powerBtn = (Button) findViewById(R.id.power_btn);
        powerBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.OFF, iBahnParamRoom, iBahnParamBox, iBahnParamToken);

            }
        });

        arrowUpBtn = (Button) findViewById(R.id.arrow_up);
        arrowUpBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.UP, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        arrowDownBtn = (Button) findViewById(R.id.arrow_down);
        arrowDownBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.DOWN, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        arrowLeftBtn = (Button) findViewById(R.id.arrow_left);
        arrowLeftBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.LEFT, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        arrowRightBtn = (Button) findViewById(R.id.arrow_right);
        arrowRightBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.RIGHT, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        okBtn = (Button) findViewById(R.id.ok_button);
        okBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.OK, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        soundMinusBtn = (Button) findViewById(R.id.sound_minus);
        soundMinusBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.VOLUMNDOWN, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        soundMuteBtn = (Button) findViewById(R.id.sound_mute);
        soundMuteBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.MUTE, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        soundPlusBtn = (Button) findViewById(R.id.sound_plus);
        soundPlusBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.VOLUMNUP, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });


        playBtn = (Button) findViewById(R.id.playBtn);
        playBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.PLAY, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        pauseBtn = (Button) findViewById(R.id.pauseBtn);
        pauseBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.PAUSE, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        stopBtn = (Button) findViewById(R.id.stopBtn);
        stopBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.STOP, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
                }
        });

        rwBtn = (Button) findViewById(R.id.rwBtn);
        rwBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.REWIND10, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        ffBtn = (Button) findViewById(R.id.ffBtn);
        ffBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.FASTFORWARD10, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        skipbBtn = (Button) findViewById(R.id.skipbBtn);
        skipbBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.SKIP_BACK, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });

        skipfBtn = (Button) findViewById(R.id.skipfBtn);
        skipfBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                MainApplication.playClickSound(v);
                Command.IBahnMovie.RemoteButton(Command.RemoteButton.SKIP_FORWARD, iBahnParamRoom, iBahnParamBox, iBahnParamToken);
            }
        });
    }

    private void setDetailPhoto(IBahnMovie item) {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.movie_poster_ibahn_empty)
                .showImageForEmptyUri(R.drawable.movie_poster_ibahn_empty)
                .showImageOnFail(R.drawable.movie_poster_ibahn_empty)
                .cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();

        String lang = "e";
        String imageLink = item.getWebPosterUrl();
        Log.i(TAG, "imageLink = " + imageLink);

        ImageLoader.getInstance().displayImage(imageLink, detailPhoto,
                options);
        movieName.setText(item.getTitle());
        movieTagline.setText(item.getTagline());
        movieDescription.setText(item.getDescription());
    }

}
