/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hsh.esdbsp.dialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hsh.esdbsp.MainApplication;
import com.hsh.esdbsp.R;
import com.hsh.esdbsp.model.DVDMovieListItem;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

public class DVDMovieDetailDialog extends AlertDialog {

	private String TAG = this.getClass().getSimpleName();
	
	private Context context;
	private String movieId;
	private AsyncHttpClient client;
	private BorrowClickListener borrowClickListener;
	
	
	ImageView poster;
	ImageView btnClose;
	Button btnBorrow;
	TextView outOfStock;
	TextView tvTitle;
	TextView tvGenre;
	TextView tvYear;
	TextView tvlanguageTitle;
	TextView tvlanguage;
	TextView tvSubtitleTitle;
	TextView tvSubtitle;
	TextView tvDescription;
	RatingBar rbRating;
	TextView tvRating;
	LinearLayout llRating;
	
	public DVDMovieDetailDialog(Context context, String movieId) {
		super(context);
		this.context = context;
		this.movieId = movieId;
	}

	public Dialog d;
	public Button yes, no;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dvd_movie_detail);
		initUI();
	}
	

	private void initUI() {
		poster = (ImageView) findViewById(R.id.ivPoster);
		btnClose = (ImageView) findViewById(R.id.btnClose);
		btnBorrow = (Button) findViewById(R.id.btnBorrow);
		outOfStock = (TextView) findViewById(R.id.tvOutOfStock);
		tvTitle = (TextView) findViewById(R.id.tvTitle);
		tvGenre = (TextView) findViewById(R.id.tvGenre);
		tvYear = (TextView) findViewById(R.id.tvYear);
		tvlanguageTitle = (TextView) findViewById(R.id.tvlanguageTitle);
		tvlanguage = (TextView) findViewById(R.id.tvlanguage);
		tvSubtitleTitle = (TextView) findViewById(R.id.tvSubtitleTitle);
		tvSubtitle = (TextView) findViewById(R.id.tvSubtitle);
		tvDescription = (TextView) findViewById(R.id.tvDescription);
		rbRating = (RatingBar) findViewById(R.id.rbRating);
		tvRating = (TextView) findViewById(R.id.tvRating);
		llRating = (LinearLayout) findViewById(R.id.llRating);
		
		outOfStock.setText(MainApplication.getLabel("dvd.outofstock"));
		btnBorrow.setText(MainApplication.getLabel("dvd.borrow"));
		tvlanguageTitle.setText(MainApplication.getLabel("dvd.language")+":");
		tvSubtitleTitle.setText(MainApplication.getLabel("dvd.subtitle")+":");
		
		client = new AsyncHttpClient();
    	client.setMaxRetriesAndTimeout(0, 10000);

    	RequestParams params;
        params = new RequestParams();
        params.add("movieId", movieId);
        
        String currAPILang = "en";
        String currLang = MainApplication.getMAS().getData("data_language") == "" ? "E" : MainApplication.getMAS().getData("data_language");
        
        if(currLang.equals("E")) {
        	currAPILang = "en";
        } else if(currLang.equals("F")) {
        	currAPILang = "fr";
        } else if(currLang.equals("CT")) {
        	currAPILang = "zh_hk";
        } else if(currLang.equals("CS")) {
        	currAPILang = "zh_cn";
        } else if(currLang.equals("J")) {
        	currAPILang = "jp";
        } else if(currLang.equals("G")) {
        	currAPILang = "de";
        } else if(currLang.equals("K")) {
        	currAPILang = "ko";
        } else if(currLang.equals("R")) {
        	currAPILang = "ru";
        } else if(currLang.equals("P")) {
        	currAPILang = "pt";
        } else if(currLang.equals("A")) {
        	currAPILang = "ar";
        } else if(currLang.equals("S")) {
        	currAPILang = "es";
        }
		params.add("lang", currAPILang);
		

    	String url = MainApplication.getApiBase() + context.getString(R.string.dvd_borrowing_api)
    			+ context.getString(R.string.get_movieDetail);

    	Log.i(TAG, "URL = " + url);
    	
    	client.get(url, params, new AsyncHttpResponseHandler() {

    		@Override
    		public void onStart() {
    			// called before request is started
    		}

    		@Override
    		public void onSuccess(int arg0, Header[] headers, byte[] response) {
    			String s = new String(response);
    			Log.i(TAG, "success response = " + s);

    			JSONObject restObject;
    			
    			try {
    				restObject = new JSONObject(s);
    				JSONArray jArray = restObject.getJSONArray("data");
    		
					final DVDMovieListItem dVDMovieListItem = new DVDMovieListItem();
					dVDMovieListItem.setMovieId(jArray.getJSONObject(0).getString("movieId"));
					dVDMovieListItem.setTitle(jArray.getJSONObject(0).getString("movieTitle"));
    				dVDMovieListItem.setDescription(jArray.getJSONObject(0).getString("movieDescription"));
    				dVDMovieListItem.setYear(jArray.getJSONObject(0).getString("year"));
    				dVDMovieListItem.setPosterUrl(jArray.getJSONObject(0).getString("poster"));
    				dVDMovieListItem.setStock(jArray.getJSONObject(0).getInt("stock"));
    				dVDMovieListItem.setAvailable(jArray.getJSONObject(0).getInt("available"));
    				dVDMovieListItem.setGenre(jArray.getJSONObject(0).getString("genre"));
    				dVDMovieListItem.setLanguage(jArray.getJSONObject(0).getString("language"));
    				dVDMovieListItem.setSubtitle(jArray.getJSONObject(0).getString("subtitle"));
    				dVDMovieListItem.setRating(jArray.getJSONObject(0).getDouble("rating"));
	    		  		 	
    				if(dVDMovieListItem.getStock() > 0 && dVDMovieListItem.getAvailable() > 0) {
    					outOfStock.setVisibility(View.INVISIBLE);
    					btnBorrow.setTextColor(context.getResources().getColor(R.color.white));
    					btnBorrow.setEnabled(true);
    				} else {
    					outOfStock.setVisibility(View.VISIBLE);
    					btnBorrow.setTextColor(context.getResources().getColor(R.color.black_90_percent));
    					btnBorrow.setEnabled(false);
    				
    				}
    				
    				tvTitle.setText(dVDMovieListItem.getTitle());  	
    				tvGenre.setText(dVDMovieListItem.getGenre());
    				tvYear.setText(dVDMovieListItem.getYear());
    				//TextView tvlanguageTitle = (TextView) findViewById(R.id.tvlanguageTitle);
    				tvlanguage.setText(dVDMovieListItem.getLanguage());
    				//TextView tvSubtitleTitle = (TextView) findViewById(R.id.tvSubtitleTitle);
    				tvSubtitle.setText(dVDMovieListItem.getSubtitle());
    				tvDescription.setText(Html.fromHtml(dVDMovieListItem.getDescription()));
    				
    				if(dVDMovieListItem.getRating() > 0 ) {
    					tvRating.setText(dVDMovieListItem.getRating()+"");
        				rbRating.setRating((float)dVDMovieListItem.getRating());
        				llRating.setVisibility(View.VISIBLE);
    				} else {
    					llRating.setVisibility(View.INVISIBLE);
    				}
    				
    				tvRating.setText(dVDMovieListItem.getRating()+"");
    				
    				
    				rbRating.setRating((float)dVDMovieListItem.getRating());

    				String posterUrl = MainApplication.getApiBase() + context.getString(R.string.dvd_borrowing) + dVDMovieListItem.getPosterUrl();

    		        DisplayImageOptions options = new DisplayImageOptions.Builder()
    						.showImageOnLoading(R.drawable.thumb_transparent)
    						.showImageForEmptyUri(R.drawable.thumb_transparent)
    						.showImageOnFail(R.drawable.thumb_transparent)
    						.cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
    						.bitmapConfig(Bitmap.Config.RGB_565).build();
    		        
    		        ImageLoader.getInstance().displayImage(posterUrl, poster,options);
    				
    		        btnClose.setOnClickListener(new android.view.View.OnClickListener() {
						@Override
						public void onClick(View v) {
							dismiss();
						}
    		        
    		        });
    		        
    		        btnBorrow.setOnClickListener(new Button.OnClickListener() {

						@Override
						public void onClick(View v) {
							borrowClickListener.onBorrowClicked(dVDMovieListItem.getMovieId());
						}
    		        });
    		        

    			} catch (JSONException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}


    		}

    		@Override
    		public void onFailure(int arg0, Header[] headers, byte[] response, Throwable e) {
    			if (response != null) {
    				String s = new String(response);
    				Log.i(TAG, s);
    			}
    			Log.i(TAG, "Stop Retry");
    		}

    		@Override
    		public void onRetry(int retryNo) {
    			// called when request is retried
    			Log.i(TAG, "retrying number = " + retryNo);
    		}

    	});
		
	}
	
	
    public void setOnClickListener(BorrowClickListener borrowClickListener) {
        this.borrowClickListener = borrowClickListener;
    }

    public interface BorrowClickListener {
        void onBorrowClicked(String movieId);
    }
    
    public void popupDialog(String message) {
    	DVDMovieDetailPopupDialog dVDMovieDetailPopupDialog = new DVDMovieDetailPopupDialog(context, message);
    	dVDMovieDetailPopupDialog.show();
    }
    
    
}
