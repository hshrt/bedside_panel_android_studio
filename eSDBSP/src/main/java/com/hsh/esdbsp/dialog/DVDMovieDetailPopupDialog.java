/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hsh.esdbsp.dialog;



import com.hsh.esdbsp.R;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class DVDMovieDetailPopupDialog extends AlertDialog {

	private String TAG = this.getClass().getSimpleName();
	private Context context;
	private String message;

	Button btnOkay;
	TextView tvMessage;

	public DVDMovieDetailPopupDialog(Context context, String message) {
		super(context);
		this.context = context;
		this.message = message;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dvd_movie_detail_popup);
		initUI();
	}
	

	private void initUI() {
	
		btnOkay = (Button) findViewById(R.id.btnOkay);
		tvMessage = (TextView) findViewById(R.id.tvMessage);
		
		tvMessage.setText(message);
		
		btnOkay.setOnClickListener(new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
    	
	}

}
